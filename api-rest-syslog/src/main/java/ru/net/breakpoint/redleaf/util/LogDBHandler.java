package ru.net.breakpoint.redleaf.util;

import java.util.logging.Handler;
import java.util.logging.LogRecord;
import org.springframework.beans.factory.annotation.Autowired;
import ru.net.breakpoint.redleaf.domain.entity.Log;
import ru.net.breakpoint.redleaf.domain.entity.User;
import ru.net.breakpoint.redleaf.interfaces.ILogService;
import ru.net.breakpoint.redleaf.interfaces.IUserService;

public class LogDBHandler extends Handler { 
    
    private final IUserService userService;
    private final ILogService logService;
    
    private final String className;
    
    public LogDBHandler(String className, ILogService logService, IUserService userService) {
        this.className = className;
        this.userService = userService;
        this.logService = logService;
//        this.logAppender = new LogDBAppender();
    }
    
    static public String truncate(String str, int length) {
        if (str.length() < length)
            return str;
        return (str.substring(0, length));
    }

    @Override
    public void publish(LogRecord record) {
        if (getFilter() != null) {
            if (!getFilter().isLoggable(record))
                return;
        }

        try {
            Log log = new Log();
            
            log.setMessage(truncate(record.getMessage(), 1024));
            log.setLogMessageType(record.getLevel().intValue());
            log.setCallingClass(this.className);
            
            User user = userService.getCurrentUser();
            log.setUser(user);
            
            logService.saveOrUpdate(log);
//            log.setUser(user);
//            logAppender.executeQuery(level, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
//            logAppender.close();
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    @Override
    public void flush() {
    }
}
