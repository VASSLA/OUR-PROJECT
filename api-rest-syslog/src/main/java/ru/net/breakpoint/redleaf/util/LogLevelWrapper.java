package ru.net.breakpoint.redleaf.util;

public class LogLevelWrapper {
    
    private Integer level;
    private String name;
    private String resourceBundleName;
    private String localizedName;
    
    public LogLevelWrapper(Integer level, String name, String resourceBundleName, String localizedName) {
        this.level = level;
        this.name = name;
        this.resourceBundleName = resourceBundleName;
        this.localizedName = localizedName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceBundleName() {
        return resourceBundleName;
    }

    public void setResourceBundleName(String resourceBundleName) {
        this.resourceBundleName = resourceBundleName;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }
}
