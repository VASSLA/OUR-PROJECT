package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.LogMessageType;

public interface ILogMessageTypeService extends IService<LogMessageType> {
    
}
