package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.LogMessageTypeDao;
import ru.net.breakpoint.redleaf.domain.entity.LogMessageType;
import ru.net.breakpoint.redleaf.interfaces.ILogMessageTypeService;

@Service
@Transactional
public class LogMessageTypeService implements ILogMessageTypeService {

    @Autowired
    private LogMessageTypeDao logMessageTypeDao;

    @Override
    public LogMessageType get(Long id) {
        return logMessageTypeDao.get(id);
    }

    @Override
    public void saveOrUpdate(LogMessageType persistent) {
        logMessageTypeDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(LogMessageType persistent) {
        logMessageTypeDao.delete(persistent);
    }

    @Override
    public ArrayList<LogMessageType> getList() {
        return logMessageTypeDao.getList();
    }

    @Override
    public ArrayList<LogMessageType> getList(Map<String, String> filters) {
        return logMessageTypeDao.getList(filters);
    }
}
