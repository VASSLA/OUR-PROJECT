package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.Log;

public interface ILogService extends IService<Log> {
    
}
