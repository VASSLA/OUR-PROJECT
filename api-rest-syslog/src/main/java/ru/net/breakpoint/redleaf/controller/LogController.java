package ru.net.breakpoint.redleaf.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Log;
import ru.net.breakpoint.redleaf.interfaces.ILogService;
import ru.net.breakpoint.redleaf.interfaces.IUserService;
import ru.net.breakpoint.redleaf.util.LogDBHandler;
import ru.net.breakpoint.redleaf.util.LogLevelWrapper;

@RestController
public class LogController {
    private static final Long LOGS_ON_PAGE = 100l;
    private static final Logger logger = Logger.getLogger(LogController.class.getName());
//    private static final Logger logger = Logger.getLogger(LogController.class);
    
    @PostConstruct
    public void init() {
        LogDBHandler handler = new LogDBHandler(LogController.class.getName(), logService, userService);
        logger.addHandler(handler);
    }
    
    @Autowired
    private ILogService logService;
    
    @Autowired
    private IUserService userService;
    
    @RequestMapping(value = "/api/v{version}/log", method = RequestMethod.GET)
    public ResponseEntity<List<Log>> getLogList(@PathVariable Integer version, @RequestParam Map<String, String> filters) {
//        logger.info("Requested /api/v" + version.toString() + "/log");
//        if (!filters.containsKey("limit")) {
//            filters.put("limit", Long.toString(LOGS_ON_PAGE));
//        }

        List<Log> logs = logService.getList(filters);
        return new ResponseEntity<List<Log>> (logs, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/log/levels", method = RequestMethod.GET)
    public ResponseEntity<List<LogLevelWrapper>> getLogLevelList(@PathVariable Integer version) {
//        logger.info("Requested /api/v" + version.toString() + "/log/level");
        
        ArrayList<LogLevelWrapper> lvlList = new ArrayList<>();
        lvlList.add(new LogLevelWrapper(Level.ALL.intValue(), Level.ALL.getName(), Level.ALL.getResourceBundleName(), Level.ALL.getLocalizedName()));
        lvlList.add(new LogLevelWrapper(Level.CONFIG.intValue(), Level.CONFIG.getName(), Level.CONFIG.getResourceBundleName(), "Конфигурация"));
        lvlList.add(new LogLevelWrapper(Level.FINE.intValue(), Level.FINE.getName(), Level.FINE.getResourceBundleName(), Level.FINE.getLocalizedName()));
        lvlList.add(new LogLevelWrapper(Level.FINER.intValue(), Level.FINER.getName(), Level.FINER.getResourceBundleName(), Level.FINER.getLocalizedName()));
        lvlList.add(new LogLevelWrapper(Level.FINEST.intValue(), Level.FINEST.getName(), Level.FINEST.getResourceBundleName(), Level.FINEST.getLocalizedName()));
        lvlList.add(new LogLevelWrapper(Level.INFO.intValue(), Level.INFO.getName(), Level.INFO.getResourceBundleName(), "Уведомление"));
        lvlList.add(new LogLevelWrapper(Level.OFF.intValue(), Level.OFF.getName(), Level.OFF.getResourceBundleName(), Level.OFF.getLocalizedName()));
        lvlList.add(new LogLevelWrapper(Level.SEVERE.intValue(), Level.SEVERE.getName(), Level.SEVERE.getResourceBundleName(), "Ошибка"));
        lvlList.add(new LogLevelWrapper(Level.WARNING.intValue(), Level.WARNING.getName(), Level.WARNING.getResourceBundleName(), "Предупреждение"));
        
        return new ResponseEntity<List<LogLevelWrapper>> (lvlList, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/log/{id}", method = RequestMethod.GET)
    public ResponseEntity<Log> getLog(@PathVariable Integer version, @PathVariable Long id) {
//        logger.info("Requested /api/v" + version.toString() + "/log/" + id.toString());
        return new ResponseEntity<> (logService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/log", method = RequestMethod.POST)
    public ResponseEntity<Log> postLog(@PathVariable Integer version, @RequestBody Log log) {
//        logger.info("Requested /api/v" + version.toString() + "/log");
        logService.saveOrUpdate(log);
        return new ResponseEntity<> (log, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/log", method = RequestMethod.PUT)
    public ResponseEntity<Log> putLog(@PathVariable Integer version, @RequestBody Log log) {
//        logger.info("Requested /api/v" + version.toString() + "/log");
        logService.saveOrUpdate(log);
        return new ResponseEntity<> (log, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/log/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Log> deleteLog(@PathVariable Integer version, @PathVariable Long id) {
//        logger.info("Requested /api/v" + version.toString() + "/log");
        Log log = logService.get(id);
        if (log != null) {
            logService.delete(log);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }
}
