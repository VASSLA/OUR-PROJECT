package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "LogMessageType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogMessageType.findAll", query = "SELECT l FROM LogMessageType l"),
    @NamedQuery(name = "LogMessageType.findById", query = "SELECT l FROM LogMessageType l WHERE l.id = :id"),
    @NamedQuery(name = "LogMessageType.findByKey", query = "SELECT l FROM LogMessageType l WHERE l.key = :key"),
    @NamedQuery(name = "LogMessageType.findByDescription", query = "SELECT l FROM LogMessageType l WHERE l.description = :description")})
public class LogMessageType implements IPersistent, Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "key")
    private String key;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "logMessageType")
    private Collection<Log> logCollection;

    public LogMessageType() {
    }

    public LogMessageType(Long id) {
        this.id = id;
    }

    public LogMessageType(Long id, String key, String description) {
        this.id = id;
        this.key = key;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Log> getLogCollection() {
        return logCollection;
    }

    public void setLogCollection(Collection<Log> logCollection) {
        this.logCollection = logCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogMessageType)) {
            return false;
        }
        LogMessageType other = (LogMessageType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.LogMessageType[ id=" + id + " ]";
    }

}
