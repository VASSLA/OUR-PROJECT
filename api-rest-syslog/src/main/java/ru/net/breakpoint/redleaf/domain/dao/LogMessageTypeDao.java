package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.LogMessageType;

@Repository
public class LogMessageTypeDao extends AbstractDao<LogMessageType> {
    
}
