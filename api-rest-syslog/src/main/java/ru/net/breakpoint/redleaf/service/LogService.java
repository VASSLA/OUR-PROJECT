package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.LogDao;
import ru.net.breakpoint.redleaf.domain.entity.Log;
import ru.net.breakpoint.redleaf.interfaces.ILogService;

@Service
@Transactional
public class LogService implements ILogService {

    @Autowired
    private LogDao logDao;

    @Override
    public Log get(Long id) {
        return logDao.get(id);
    }

    @Override
    public void saveOrUpdate(Log persistent) {
        logDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Log persistent) {
        logDao.delete(persistent);
    }

    @Override
    public ArrayList<Log> getList() {
        return logDao.getList();
    }

    @Override
    public ArrayList<Log> getList(Map<String, String> filters) {
        return logDao.getList(filters);
    }
}
