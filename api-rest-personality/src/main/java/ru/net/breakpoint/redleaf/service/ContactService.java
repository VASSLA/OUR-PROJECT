/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: ContactService.java
 * Date: Nov 17, 2016 8:52:16 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.ContactDao;
import ru.net.breakpoint.redleaf.domain.entity.Contact;
import ru.net.breakpoint.redleaf.interfaces.IContactService;

@Service
@Transactional
public class ContactService implements IContactService{
    @Autowired
    private ContactDao contactDao;

    @Override
    public Contact get(Long id) {
        return contactDao.get(id);
    }

    @Override
    public void saveOrUpdate(Contact persistent) {
        contactDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Contact persistent) {
        contactDao.delete(persistent);
    }

    @Override
    public ArrayList<Contact> getList() {
        return contactDao.getList();
    }

    @Override
    public ArrayList<Contact> getList(Map<String, String> filters) {
        return contactDao.getList(filters);
    }
}
