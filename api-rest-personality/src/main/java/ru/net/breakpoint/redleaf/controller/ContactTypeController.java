/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: ContactTypeController.java
 * Date: Oct 1, 2016 6:13:56 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.ContactType;
import ru.net.breakpoint.redleaf.interfaces.IContactTypeService;

@RestController
public class ContactTypeController {
    private static final Logger logger = Logger.getLogger(ContactTypeController.class);

    @Autowired
    private IContactTypeService contactTypeService;

    @RequestMapping(value = "/api/v{version}/contacttype", method = RequestMethod.GET)
    public ResponseEntity<List<ContactType>> getContactTypeList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/contacttype");
        return new ResponseEntity<List<ContactType>> (contactTypeService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/contacttype/{id}", method = RequestMethod.GET)
    public ResponseEntity<ContactType> getContactType(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/contacttype/" + id.toString());
        return new ResponseEntity<> (contactTypeService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/contacttype", method = RequestMethod.POST)
    public ResponseEntity<ContactType> postContactType(@PathVariable Integer version, @RequestBody ContactType contactType) {
        logger.trace("Requested /api/v" + version.toString() + "/contacttype");
        contactTypeService.saveOrUpdate(contactType);
        return new ResponseEntity<> (contactType, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/contacttype", method = RequestMethod.PUT)
    public ResponseEntity<ContactType> putContactType(@PathVariable Integer version, @RequestBody ContactType contactType) {
        logger.trace("Requested /api/v" + version.toString() + "/contacttype");
        contactTypeService.saveOrUpdate(contactType);
        return new ResponseEntity<> (contactType, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/contacttype/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ContactType> deleteContactType(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/contacttype");
        ContactType contactType = contactTypeService.get(id);
        if (contactType != null) {
            contactTypeService.delete(contactType);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }

}
