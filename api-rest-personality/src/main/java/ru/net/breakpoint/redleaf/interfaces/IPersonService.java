/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: IPersonService.java
 * Date: Jun 27, 2016 2:45:43 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.Person;
import ru.net.breakpoint.redleaf.interfaces.IService;

public interface IPersonService extends IService<Person>{

}
