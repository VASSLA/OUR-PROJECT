/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: PersonService.java
 * Date: Jun 27, 2016 2:46:11 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.PersonDao;
import ru.net.breakpoint.redleaf.domain.entity.Person;
import ru.net.breakpoint.redleaf.interfaces.IPersonService;

@Service
@Transactional
public class PersonService implements IPersonService{
    @Autowired
    private PersonDao personDao;

    @Override
    public Person get(Long id) {
        return personDao.get(id);
    }

    @Override
    public void saveOrUpdate(Person persistent) {
        personDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Person persistent) {
        personDao.delete(persistent);
    }

    @Override
    public ArrayList<Person> getList() {
        return personDao.getList();
    }

    @Override
    public ArrayList<Person> getList(Map<String, String> filters) {
        return personDao.getList(filters);
    }
}
