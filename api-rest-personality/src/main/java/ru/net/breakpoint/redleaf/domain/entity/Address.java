/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: Address.java
 * Date: Nov 23, 2016 11:04:21 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "Address")
@NamedQueries({
    @NamedQuery(name = "Address.findAll", query = "SELECT a FROM Address a")
    , @NamedQuery(name = "Address.findById", query = "SELECT a FROM Address a WHERE a.id = :id")
    , @NamedQuery(name = "Address.findByKladrId", query = "SELECT a FROM Address a WHERE a.kladrId = :kladrId")
    , @NamedQuery(name = "Address.findByAddress", query = "SELECT a FROM Address a WHERE a.address = :address")})
public class Address implements Serializable, IPersistent {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "kladrId")
    private String kladrId;
    @Column(name = "zip")
    private String zip;
    @Column(name = "region")
    private String region;
    @Column(name = "regionTypeShort")
    private String regionTypeShort;
    @Column(name = "regionType")
    private String regionType;
    @Column(name = "district")
    private String district;
    @Column(name = "districtTypeShort")
    private String districtTypeShort;
    @Column(name = "districtType")
    private String districtType;
    @Column(name = "city")
    private String city;
    @Column(name = "cityTypeShort")
    private String cityTypeShort;
    @Column(name = "cityType")
    private String cityType;
    @Column(name = "street")
    private String street;
    @Column(name = "streetTypeShort")
    private String streetTypeShort;
    @Column(name = "streetType")
    private String streetType;
    @Column(name = "building")
    private String building;
    @Column(name = "buildingTypeShort")
    private String buildingTypeShort;
    @Column(name = "buildingType")
    private String buildingType;
    @Column(name = "apartment")
    private String apartment;
    @Column(name = "extras")
    private String extras;
    @Column(name = "longitude")
    private Float longitude;
    @Column(name = "altitude")
    private Float altitude;
    @Column(name = "address")
    private String address;
    @Basic(optional = true)
    @Column(name = "defaultAddress")
    private Integer defaultAddress;
    @Column(name = "uniqueId")
    private String uniqueId;


    public Address() {
    }

    public Address(Long id) {
        this.id = id;
    }

    public Address(Long id, Long personId) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKladrId() {
        return kladrId;
    }

    public void setKladrId(String kladrId) {
        this.kladrId = kladrId;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegionTypeShort() {
        return regionTypeShort;
    }

    public void setRegionTypeShort(String regionTypeShort) {
        this.regionTypeShort = regionTypeShort;
    }

    public String getRegionType() {
        return regionType;
    }

    public void setRegionType(String regionType) {
        this.regionType = regionType;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDistrictTypeShort() {
        return districtTypeShort;
    }

    public void setDistrictTypeShort(String districtTypeShort) {
        this.districtTypeShort = districtTypeShort;
    }

    public String getDistrictType() {
        return districtType;
    }

    public void setDistrictType(String districtType) {
        this.districtType = districtType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityTypeShort() {
        return cityTypeShort;
    }

    public void setCityTypeShort(String cityTypeShort) {
        this.cityTypeShort = cityTypeShort;
    }

    public String getCityType() {
        return cityType;
    }

    public void setCityType(String cityType) {
        this.cityType = cityType;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetTypeShort() {
        return streetTypeShort;
    }

    public void setStreetTypeShort(String streetTypeShort) {
        this.streetTypeShort = streetTypeShort;
    }

    public String getStreetType() {
        return streetType;
    }

    public void setStreetType(String streetType) {
        this.streetType = streetType;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getBuildingTypeShort() {
        return buildingTypeShort;
    }

    public void setBuildingTypeShort(String buildingTypeShort) {
        this.buildingTypeShort = buildingTypeShort;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getAltitude() {
        return altitude;
    }

    public void setAltitude(Float altitude) {
        this.altitude = altitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(Integer defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Address)) {
            return false;
        }
        Address other = (Address) object;
        return !((this.id == null && other.id != null) ||
                 (this.id != null && !this.id.equals(other.id)) ||
                 (!this.getUniqueId().equals(other.getUniqueId())));
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.Address[ id=" + id + " ]";
    }

}
