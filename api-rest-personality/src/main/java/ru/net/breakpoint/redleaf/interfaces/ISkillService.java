/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: ISkillService.java
 * Date: Dec 4, 2016 3:21:59 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.Skill;

public interface ISkillService extends IService<Skill>{

}
