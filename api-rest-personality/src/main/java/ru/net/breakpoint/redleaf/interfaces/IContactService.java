/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: IContactService.java
 * Date: Nov 17, 2016 8:49:52 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.Contact;

public interface IContactService extends IService<Contact>{

}
