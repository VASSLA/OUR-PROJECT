/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: IAddressService.java
 * Date: Nov 23, 2016 11:07:12 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.Address;

public interface IAddressService extends IService<Address>{

}
