/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-addressality
 * File Name: AddressController.java
 * Date: Nov 23, 2016 11:09:57 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Address;
import ru.net.breakpoint.redleaf.interfaces.IAddressService;

@RestController
public class AddressController {
    private static final Logger logger = Logger.getLogger(AddressController.class);

    @Autowired
    private IAddressService addressService;

    @RequestMapping(value = "/api/v{version}/address", method = RequestMethod.GET)
    public ResponseEntity<List<Address>> getAddressList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/address");
        return new ResponseEntity<List<Address>> (addressService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/address/{id}", method = RequestMethod.GET)
    public ResponseEntity<Address> getAddress(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/address/" + id.toString());
        return new ResponseEntity<> (addressService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/address", method = RequestMethod.POST)
    public ResponseEntity<Address> postAddress(@PathVariable Integer version, @RequestBody Address address) {
        logger.trace("Requested /api/v" + version.toString() + "/address");
        addressService.saveOrUpdate(address);
        return new ResponseEntity<> (address, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/address", method = RequestMethod.PUT)
    public ResponseEntity<Address> putAddress(@PathVariable Integer version, @RequestBody Address address) {
        logger.trace("Requested /api/v" + version.toString() + "/address");
        addressService.saveOrUpdate(address);
        return new ResponseEntity<> (address, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/address/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Address> deleteAddress(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/address");
        Address address = addressService.get(id);
        if (address != null) {
            addressService.delete(address);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }

}
