/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: ContactController.java
 * Date: Nov 17, 2016 8:56:37 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Contact;
import ru.net.breakpoint.redleaf.interfaces.IContactService;

@RestController
public class ContactController {
    private static final Logger logger = Logger.getLogger(ContactController.class);

    @Autowired
    private IContactService contactService;

    @RequestMapping(value = "/api/v{version}/contact", method = RequestMethod.GET)
    public ResponseEntity<List<Contact>> getContactList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/contact");
        return new ResponseEntity<List<Contact>> (contactService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/contact/{id}", method = RequestMethod.GET)
    public ResponseEntity<Contact> getContact(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/contact/" + id.toString());
        return new ResponseEntity<> (contactService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/contact", method = RequestMethod.POST)
    public ResponseEntity<Contact> postContact(@PathVariable Integer version, @RequestBody Contact contact) {
        logger.trace("Requested /api/v" + version.toString() + "/contact");
        contactService.saveOrUpdate(contact);
        return new ResponseEntity<> (contact, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/contact", method = RequestMethod.PUT)
    public ResponseEntity<Contact> putContact(@PathVariable Integer version, @RequestBody Contact contact) {
        logger.trace("Requested /api/v" + version.toString() + "/contact");
        contactService.saveOrUpdate(contact);
        return new ResponseEntity<> (contact, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/contact/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Contact> deleteContact(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/contact");
        Contact contact = contactService.get(id);
        if (contact != null) {
            contactService.delete(contact);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }

}
