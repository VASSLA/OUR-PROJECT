/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: ContactDao.java
 * Date: Nov 17, 2016 8:48:23 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Contact;

@Repository
public class ContactDao extends AbstractDao<Contact>{

}
