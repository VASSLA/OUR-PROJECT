/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.dao.AbstractDao;
import ru.net.breakpoint.redleaf.domain.entity.ContactType;

@Repository
public class ContactTypeDao extends AbstractDao<ContactType> {

}
