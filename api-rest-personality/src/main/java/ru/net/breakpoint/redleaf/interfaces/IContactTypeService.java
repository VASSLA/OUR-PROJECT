/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: IContactTypeService.java
 * Date: Oct 1, 2016 5:11:07 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.ContactType;
import ru.net.breakpoint.redleaf.interfaces.IService;

public interface IContactTypeService extends IService<ContactType> {

}
