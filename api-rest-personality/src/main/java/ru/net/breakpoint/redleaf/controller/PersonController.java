/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: PersonController.java
 * Date: Jun 27, 2016 3:45:00 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Person;
import ru.net.breakpoint.redleaf.interfaces.IPersonService;

@RestController
public class PersonController {
    private static final Logger logger = Logger.getLogger(PersonController.class);

    @Autowired
    private IPersonService personService;

    @RequestMapping(value = "/api/v{version}/person", method = RequestMethod.GET)
    public ResponseEntity<List<Person>> getPersonList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/person");
        return new ResponseEntity<List<Person>> (personService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/person/{id}", method = RequestMethod.GET)
    public ResponseEntity<Person> getPerson(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/person/" + id.toString());
        return new ResponseEntity<> (personService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/person", method = RequestMethod.POST)
    public ResponseEntity<Person> postPerson(@PathVariable Integer version, @RequestBody Person person) {
        logger.trace("Requested /api/v" + version.toString() + "/person");
        personService.saveOrUpdate(person);
        return new ResponseEntity<> (person, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/person", method = RequestMethod.PUT)
    public ResponseEntity<Person> putPerson(@PathVariable Integer version, @RequestBody Person person) {
        logger.trace("Requested /api/v" + version.toString() + "/person");
        personService.saveOrUpdate(person);
        return new ResponseEntity<> (person, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/person/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Person> deletePerson(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/person");
        Person person = personService.get(id);
        if (person != null) {
            personService.delete(person);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }
}

