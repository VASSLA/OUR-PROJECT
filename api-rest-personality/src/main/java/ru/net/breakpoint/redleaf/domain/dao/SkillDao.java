/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: SkillDao.java
 * Date: Dec 4, 2016 3:21:02 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Skill;

@Repository
public class SkillDao extends AbstractDao<Skill>{

}
