/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: PersonDao.java
 * Date: Jun 27, 2016 2:43:43 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.dao.AbstractDao;
import ru.net.breakpoint.redleaf.domain.entity.Person;

@Repository
public class PersonDao extends AbstractDao<Person>{

}
