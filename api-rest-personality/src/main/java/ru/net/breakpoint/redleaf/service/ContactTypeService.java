/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: ContactTypeService.java
 * Date: Oct 1, 2016 5:57:25 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.net.breakpoint.redleaf.domain.dao.ContactTypeDao;
import ru.net.breakpoint.redleaf.domain.entity.ContactType;
import ru.net.breakpoint.redleaf.interfaces.IContactTypeService;

@Service
@Transactional
public class ContactTypeService implements IContactTypeService {
    @Autowired
    private ContactTypeDao contactTypeDao;

    @Override
    public ContactType get(Long id) {
        return contactTypeDao.get(id);
    }

    @Override
    public void saveOrUpdate(ContactType persistent) {
        contactTypeDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(ContactType persistent) {
        contactTypeDao.delete(persistent);
    }

    @Override
    public ArrayList<ContactType> getList() {
        return contactTypeDao.getList();
    }

    @Override
    public ArrayList<ContactType> getList(Map<String, String> filters) {
        return contactTypeDao.getList(filters);
    }
}
