/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: AddressDao.java
 * Date: Nov 23, 2016 11:06:06 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Address;

@Repository
public class AddressDao extends AbstractDao<Address>{

}
