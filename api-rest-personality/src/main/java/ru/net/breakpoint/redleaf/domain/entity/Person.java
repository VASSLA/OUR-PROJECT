/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: Person.java
 * Date: Jun 27, 2016 2:08:23 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "Person")
@NamedQueries({
    @NamedQuery(name = "Person.findAll", query = "SELECT p FROM Person p")})
public class Person implements IPersistent, Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "middleName")
    private String middleName;

    @Column(name = "lastName")
    private String lastName;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy", timezone = "GMT+3")
    private Date birthDate;

    @Column(name = "sex")
    private int sex;

    @Column(name = "pictureId")
    private Long pictureId;

    //@OneToMany(targetEntity = Contact.class, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, mappedBy = "personId")
    //private Set<Contact> contacts;
    @ManyToMany(targetEntity = Contact.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "PersonContact",
            joinColumns = {
                @JoinColumn(name = "personId")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "contactId")
            })
    private Set<Contact> contacts;

    @ManyToMany(targetEntity = Address.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "PersonAddress",
            joinColumns = {
                @JoinColumn(name = "personId")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "addressId")
            })
    private Set<Address> addresses;

    @ManyToMany(targetEntity = Skill.class, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "PersonSkill",
            joinColumns = {
                @JoinColumn(name = "personId"/*, nullable = false, insertable = false, updatable = false*/)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "skillId"/*, nullable = false, insertable = false, updatable = false*/)
            })
    private Set<Skill> skills;

    public Person() {
    }

    public Person(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }


    public Long getPictureId() {
        return pictureId;
    }

    public void setPictureId(Long pictureId) {
        this.pictureId = pictureId;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Person)) {
            return false;
        }
        Person other = (Person) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.Person[ id=" + id + " ]";
    }

}
