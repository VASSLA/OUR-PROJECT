/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: AddressService.java
 * Date: Nov 23, 2016 11:07:39 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.AddressDao;
import ru.net.breakpoint.redleaf.domain.entity.Address;
import ru.net.breakpoint.redleaf.interfaces.IAddressService;

@Service
@Transactional
public class AddressService implements IAddressService{
    @Autowired
    private AddressDao addressDao;

    @Override
    public Address get(Long id) {
        return addressDao.get(id);
    }

    @Override
    public void saveOrUpdate(Address persistent) {
        addressDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Address persistent) {
        addressDao.delete(persistent);
    }

    @Override
    public ArrayList<Address> getList() {
        return addressDao.getList();
    }

    @Override
    public ArrayList<Address> getList(Map<String, String> filters) {
        return addressDao.getList(filters);
    }
}
