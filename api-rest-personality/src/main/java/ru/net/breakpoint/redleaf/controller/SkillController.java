/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: SkillController.java
 * Date: Dec 4, 2016 3:26:16 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Skill;
import ru.net.breakpoint.redleaf.interfaces.ISkillService;

@RestController
public class SkillController {
    private static final Logger logger = Logger.getLogger(SkillController.class);

    @Autowired
    private ISkillService skillService;

    @RequestMapping(value = "/api/v{version}/skill", method = RequestMethod.GET)
    public ResponseEntity<List<Skill>> getSkillList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/skill");
        return new ResponseEntity<List<Skill>> (skillService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/skill/{id}", method = RequestMethod.GET)
    public ResponseEntity<Skill> getSkill(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/skill/" + id.toString());
        return new ResponseEntity<> (skillService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/skill", method = RequestMethod.POST)
    public ResponseEntity<Skill> postSkill(@PathVariable Integer version, @RequestBody Skill skill) {
        logger.trace("Requested /api/v" + version.toString() + "/skill");
        skillService.saveOrUpdate(skill);
        return new ResponseEntity<> (skill, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/skill", method = RequestMethod.PUT)
    public ResponseEntity<Skill> putSkill(@PathVariable Integer version, @RequestBody Skill skill) {
        logger.trace("Requested /api/v" + version.toString() + "/skill");
        skillService.saveOrUpdate(skill);
        return new ResponseEntity<> (skill, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/skill/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Skill> deleteSkill(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/skill");
        Skill skill = skillService.get(id);
        if (skill != null) {
            skillService.delete(skill);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }
}
