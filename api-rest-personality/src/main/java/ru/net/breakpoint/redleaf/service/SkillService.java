/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-personality
 * File Name: SkillService.java
 * Date: Dec 4, 2016 3:22:42 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.SkillDao;
import ru.net.breakpoint.redleaf.domain.entity.Skill;
import ru.net.breakpoint.redleaf.interfaces.ISkillService;

@Service
@Transactional
public class SkillService implements ISkillService{
    @Autowired
    private SkillDao skillDao;

    @Override
    public Skill get(Long id) {
        return skillDao.get(id);
    }

    @Override
    public void saveOrUpdate(Skill persistent) {
        skillDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Skill persistent) {
        skillDao.delete(persistent);
    }

    @Override
    public ArrayList<Skill> getList() {
        return skillDao.getList();
    }

    @Override
    public ArrayList<Skill> getList(Map<String, String> filters) {
        return skillDao.getList(filters);
    }
}
