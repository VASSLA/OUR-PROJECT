/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: FormController.java
 * Date: 25.10.2016 21:10:36
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FormController {
    private static final Logger logger = Logger.getLogger(ApplicationController.class);

    @RequestMapping ("/form/request")
    public String getFormRequest() {
        logger.trace("Requsted URL: /form/request");
        return "form-request";
    }

    @RequestMapping ("/form/media")
    public String getFormMedia() {
        logger.trace("Requsted URL: /form/media");
        return "form-media";
    }

    @RequestMapping ("/form/feedbackmessage")
    public String getFormFeedbackmessage() {
        logger.trace("Requsted URL: /form/feedbackmessage");
        return "form-feedbackmessage";
    }

    @RequestMapping ("/form/addresslist")
    public String getFormAddressList() {
        logger.trace("Requsted URL: /form/addresslist");
        return "form-addresslist";
    }

    @RequestMapping ("/form/contactlist")
    public String getFormContactList() {
        logger.trace("Requsted URL: /form/contactlist");
        return "form-contactlist";
    }

    @RequestMapping ("/form/eventeditor")
    public String getFormEventEditor() {
        logger.trace("Requsted URL: /form/eventeditor");
        return "form-eventeditor";
    }

    @RequestMapping ("/form/placeselector")
    public String getPlaceSelector() {
        logger.trace("Requsted URL: /form/placeselector");
        return "form-placeselector";
    }
}