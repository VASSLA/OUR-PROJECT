/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ReportController.java
 * Date: Feb 12, 2017 12:55:20 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ReportController {
    private static final Logger logger = Logger.getLogger(BlockController.class.getName());

    @RequestMapping ("/report/empty")
    public String getReportEmpty() {
        logger.info("Requsted URL: /report/empty");
        return "report-empty";
    }

    @RequestMapping ("/report/request")
    public String getReportRequest() {
        logger.info("Requsted URL: /report/request");
        return "report-request";
    }    
}
