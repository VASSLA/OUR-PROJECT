/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ConnectivityController.java
 * Date: Nov 28, 2016 9:55:52 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConnectivityController {
    private Logger logger = Logger.getLogger(ConnectivityController.class);

    @RequestMapping(value = "/api/v{version}/connectivity", method = RequestMethod.POST)
    public ResponseEntity getCycles(@PathVariable Integer version, @RequestBody Integer cycleCounter) {
        logger.trace("Requested /api/v" + version.toString() + "/connectivity");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal().equals("anonymousUser")) {
            return new ResponseEntity (-1, HttpStatus.FORBIDDEN);
        } else {
            return new ResponseEntity (++cycleCounter, HttpStatus.OK);
        }
    }
}
