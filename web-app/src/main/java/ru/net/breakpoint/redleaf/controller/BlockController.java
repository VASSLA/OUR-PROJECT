/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: BlockController.java
 * Date: Nov 3, 2016 6:53:21 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BlockController {
    private static final Logger logger = Logger.getLogger(BlockController.class);

    @RequestMapping ("/block/coordinator-requestcard")
    public String getBlockCoordinatorRequest() {
        logger.trace("Requsted URL: /block/coordinator-requestcard");
        return "block-coordinator-requestcard";
    }

    @RequestMapping ("/block/requestcard")
    public String getBlockRequest() {
        logger.trace("Requsted URL: /block/requestcard");
        return "block-requestcard";
    }

    @RequestMapping ("/block/skill")
    public String getBlockSkill() {
        logger.trace("Requsted URL: /block/skill");
        return "block-skill";
    }

    @RequestMapping ("/block/buddy")
    public String getBlockBuddy() {
        logger.trace("Requsted URL: /block/buddy");
        return "block-buddy";
    }

    @RequestMapping ("/block/message")
    public String getBlockMessage() {
        logger.trace("Requsted URL: /block/message");
        return "block-message";
    }

    @RequestMapping ("/block/message-tab")
    public String getBlockMessageTab() {
        logger.trace("Requsted URL: /block/message-tab");
        return "block-message-tab";
    }

    @RequestMapping ("/block/message-container")
    public String getBlockMessageContainer() {
        logger.trace("Requsted URL: /block/message-container");
        return "block-message-container";
    }

    @RequestMapping ("/block/image-container")
    public String getImageContainer() {
        logger.trace("Requsted URL: /block/image-container");
        return "block-image-container";
    }

    @RequestMapping ("/block/wait")
    public String getBlockWait() {
        logger.trace("Requsted URL: /block/wait");
        return "block-wait";
    }

    @RequestMapping ("/block/address")
    public String getBlockAddress() {
        logger.trace("Requsted URL: /block/address");
        return "block-address";
    }

    @RequestMapping ("/block/contact")
    public String getBlockContact() {
        logger.trace("Requsted URL: /block/contact");
        return "block-contact";
    }
    
    @RequestMapping ("/block/event-participant")
    public String getBlockEventParticipant() {
        logger.trace("Requsted URL: /block/event-participant");
        return "event-participant";
    }
    
    @RequestMapping ("/block/event-type")
    public String getBlockEventType() {
        logger.trace("Requsted URL: /block/event-type");
        return "event-type";
    }
}
