/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RegistrationController.java
 * Date: Sep 21, 2016 11:09:32 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.net.breakpoint.redleaf.domain.entity.Role;
import ru.net.breakpoint.redleaf.domain.entity.User;
import ru.net.breakpoint.redleaf.interfaces.IRoleService;
import ru.net.breakpoint.redleaf.interfaces.IUserService;
import ru.net.breakpoint.redleaf.utils.MD5;
import ru.net.breakpoint.redleaf.service.Mailer;
import ru.net.breakpoint.redleaf.utils.TokenGenerator;

@Controller
public class RegistrationController {

    private static final Logger logger = Logger.getLogger(ApplicationController.class);
    private static final String PAR_NAME_USERNAME = "username";
    private static final String PAR_NAME_EMAIL = "email";
    private static final String PAR_NAME_PWD1 = "pwd1";
    private static final String PAR_NAME_PWD2 = "pwd2";
    private static final String PAR_NAME_AGREED = "agreed";
    private static final String PAR_NAME_TOKEN = "token";

    @Autowired
    private IUserService userService;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private Mailer mailer;

    @RequestMapping(value = {"/register/form", "/register"}, method = RequestMethod.GET)
    public String registerUser() {
        logger.trace("Requsted URL: /register");
        return "register-form";
    }

    @RequestMapping(value = "/register/validate", method = RequestMethod.POST)
    public String registerUser(@RequestParam Map<String, String> parameters, Map<String, Object> model) {
        String username = parameters.get(PAR_NAME_USERNAME);
        String email = parameters.get(PAR_NAME_EMAIL);
        String pwd1 = parameters.get(PAR_NAME_PWD1);
        String pwd2 = parameters.get(PAR_NAME_PWD2);
        String agreed = parameters.get(PAR_NAME_AGREED);
        if (!pwd1.equals(pwd2)) {
            return "register";
        } else {
            String passwordHash = MD5.getMD5Digest(pwd1);
            User user = new User();
            user.setLogin(username);
            user.setEmail(email);
            user.setPasswordHash(passwordHash);
            user.setDisabled(true);
            user.setConfirmed(false);
            user.setConfirmationToken(TokenGenerator.generate());
            Role role = roleService.get(new Long(3));
            Set<Role> roles = new HashSet<Role>();
            roles.add(role);
            user.setRoles(roles);
            userService.saveOrUpdate(user);
            mailer.sendMail(email, "Лиза Алерт, подтверждение регистрации", "Ваш код подтверждения: " + user.getConfirmationToken());
            model.put("email", user.getEmail());
            return "register-confirm";
        }
    }

    @RequestMapping(value = {"/register/confirm"}, method = RequestMethod.POST)
    public String confirmEmail(@RequestParam Map<String, String> parameters, Map<String, Object> model) {
        //logger.trace("Requsted URL: /register/confirm");
        Map<String, String> filters = new HashMap<>();
        String email = parameters.get(PAR_NAME_EMAIL);
        String token = parameters.get(PAR_NAME_TOKEN);
        filters.put("email", email);
        ArrayList<User> users = userService.getList(filters);
        if (users.size() < 1) {
            return "register";
        }
        User user = users.get(0);
        if (user.getConfirmationToken().equals(token)) {
            user.setConfirmed(true);
            userService.saveOrUpdate(user);
            return "redirect:/";
        } else {
            model.put("email", email);
            model.put("error", "Ошибка: неверный код подтверждения");
            return "register-confirm";
        }
    }
}
