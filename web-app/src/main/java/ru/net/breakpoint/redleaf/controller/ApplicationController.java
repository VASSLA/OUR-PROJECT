/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ApplicationController.java
 * Date: Jul 15, 2016 11:36:45 AM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.net.breakpoint.redleaf.domain.entity.User;
import ru.net.breakpoint.redleaf.interfaces.IUserService;

@Controller
public class ApplicationController {

    private static final Logger logger = Logger.getLogger(ApplicationController.class);

    @Autowired
    private IUserService userService;

    @RequestMapping(value = {"/index", "/"}, method = RequestMethod.GET)
    public String home(Map<String, Object> model) {
        logger.trace("Requested /");
        User user = userService.getCurrentUser();
        if (user != null && user.getConfirmed()) {
            model.put("title", "Welcome");
            return "index";
        } else if (user != null && !user.getConfirmed()) {
            model.put("email", user.getEmail());
            return "register-confirm";
        } else {
            model.put("title", "Login");
            return "login";
        }
    }

    @RequestMapping("/login")
    public String getLogin() {
        logger.trace("Requsted URL: /login");
        return "login";
    }

    @RequestMapping("/dashboard")
    public String getDashboard() {
        logger.trace("Requsted URL: /dashboard");
        return "dashboard";
    }

    @RequestMapping("/profile")
    public String getProfile() {
        logger.trace("Requsted URL: /profile");
        return "profile";
    }

    @RequestMapping("/requests")
    public String getRequests() {
        logger.trace("Requsted URL: /requests");
        return "requests";
    }

    @RequestMapping("/events")
    public String getEvents() {
        logger.trace("Requsted URL: /events");
        return "events";
    }

    @RequestMapping("/messages")
    public String getMessages() {
        logger.trace("Requsted URL: /messages");
        return "messages";
    }

    @RequestMapping("/coordinator")
    public String getCoordinator() {
        logger.trace("Requsted URL: /coordinator");
        return "coordinator";
    }

    @RequestMapping("/systemsettings")
    public String getSettings() {
        logger.trace("Requsted URL: /systemsettings");
        return "systemsettings";
    }

    @RequestMapping("/administrator")
    public String getAdministrator() {
        logger.trace("Requsted URL: /administrator");
        return "administrator";
    }

    @RequestMapping("/logs")
    public String getLogs() {
        logger.trace("Requsted URL: /logs");
        return "logs";
    }

    @RequestMapping("/feedback")
    public String getFeedback() {
        logger.trace("Requsted URL: /feedback");
        return "feedback";
    }

    @RequestMapping("/requestcard")
    public String getRequestCard() {
        logger.trace("Requsted URL: /requestcard");
        return "requestcard";
    }

    @RequestMapping("/debug")
    public String getDebug() {
        logger.trace("Requsted URL: /debug");
        return "debug";
    }

    @RequestMapping("/medialibrary")
    public String getMediaLibrary() {
        logger.trace("Requsted URL: /medialibrary");
        return "medialibrary";
    }
}
