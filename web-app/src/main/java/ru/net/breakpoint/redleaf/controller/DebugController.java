/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: DebugController.java
 * Date: Nov 7, 2016 11:07:44 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.service.Mailer;

@RestController
public class DebugController {
    @Autowired
    private Mailer mailer;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @RequestMapping(value = "/debug/sendmail", method = RequestMethod.GET)
    public void sendMail() {
        mailer.sendMail("alex.blade.box@gmail.com", "Liza Alert mail notification", "Code is: 8768768768");
    }

    @RequestMapping(value = "/debug/jsontree", method = RequestMethod.GET)
    public ResponseEntity<Boolean> jsonTree() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
