<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>LizaAlert| Страница регистрации</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Favicon -->
        <link rel="icon" href="<spring:url value="/resources/app/img/logo.png"/>" type="image/x-png">
        <link rel="shortcut icon" href="<spring:url value="/resources/app/img/logo.png"/>" type="image/x-png" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap/css/bootstrap.min.css"/>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/adminlte/css/AdminLTE.min.css"/>">
        <!-- iCheck -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/icheck/square/blue.css"/>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition register-page">
        <div class="register-box">
            <div class="register-logo">
                <b>Lisa</b>Alert
            </div>

            <div class="register-box-body">
                <p class="login-box-msg">Введите код подтверждения</p>

                <form action="<spring:url value="/register/confirm"/>" method="post">
                    <div class="form-group">
                        <c:if test="${error == null}">
                            <label class="text-sm">Вам выслан проверочный код на адрес почты, указанный при регистрации</label>
                        </c:if>
                        <c:if test="${error != ''}">
                            <label class="text-sm text-red"><c:out value="${error}"/></label>
                        </c:if>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="hidden" name="email" id="email" value="<c:out value="${email}"/>"/>
                        <input type="text" class="form-control" placeholder="Код подтверждения" name="token" id="token">
                        <span class="glyphicon glyphicon-adjust form-control-feedback"></span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Далее</button>
                    </div>
            </div>
        </form>
    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="<spring:url value="/resources/lib/jquery/jquery-2.2.3.min.js"/>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<spring:url value="/resources/lib/bootstrap/js/bootstrap.min.js"/>"></script>
</body>
</html>
