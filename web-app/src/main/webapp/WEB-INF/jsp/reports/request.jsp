<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Лиза Алерт</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Favicon -->
        <link rel="icon" href="<spring:url value="/resources/app/img/logo.png"/>" type="image/x-png">
        <link rel="shortcut icon" href="<spring:url value="/resources/app/img/logo.png"/>" type="image/x-png" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap/css/bootstrap.min.css"/>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    </head>
    <body style="margin: 10px; background-color: #eee">
        <div class="container" style="padding: 25px; border: 2px solid gray; background-color: white">
            <div class="row with-border">
                <div class="col-xs-4">
                    <img src="/api/v1/getthumbnail/{missingPerson.pictureId}" class="img-bordered img-responsive"/>
                </div>
                <div class="col-xs-8">
                    <h1 class="text-warning text-bold">ВНИМАНИЕ: РАЗЫСКИВАЕТСЯ</h1>
                    <hr/>
                    <h1 class="text-bold">{missingPerson.lastName} {missingPerson.firstName} {missingPerson.middleName}</h1>
                    <h2>Год рождения: {missingPerson.birthDate}</h2>
                    <h2>Дата пропажи: {missingFrom}</h2>
                </div>
            </div>
        </div>
    </body>
</html>
