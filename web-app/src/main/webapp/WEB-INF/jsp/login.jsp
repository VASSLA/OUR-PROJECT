<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>LizaAlert | Страница входа</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Favicon -->
        <link rel="icon" href="<spring:url value="/resources/app/img/logo.png"/>" type="image/x-png">
        <link rel="shortcut icon" href="<spring:url value="/resources/app/img/logo.png"/>" type="image/x-png" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap/css/bootstrap.min.css"/>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/adminlte/css/AdminLTE.min.css"/>">
        <!-- iCheck -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/icheck/square/blue.css"/>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery 2.2.3 -->
        <script src="<spring:url value="/resources/lib/jquery/jquery-2.2.3.min.js"/>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<spring:url value="/resources/lib/bootstrap/js/bootstrap.min.js"/>"></script>
        <!-- iCheck -->
        <script src="<spring:url value="/resources/lib/icheck/icheck.min.js"/>"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <b>Liza</b>Alert
            </div>
            <!--<div class="alert text-info text-center">
                Система находится на обслуживании<br/>Вход невозможен
            </div>-->
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Войдите, чтобы начать работу</p>

                <form action="/login" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Имя Пользователя" id="username" name="username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Пароль" id="password" name="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox"> Запомнить меня
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Вход</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <!--<div class="social-auth-links text-center">
                    <p>- OR -</p>
                    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                        Facebook</a>
                    <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                        Google+</a>
                </div>
                <!-- /.social-auth-links -->

                <a href="#">Я забыл свой пароль</a><br>
                <a href="/register" class="text-center">Зарегистрировать нового пользователя</a>

            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
    </body>
</html>
