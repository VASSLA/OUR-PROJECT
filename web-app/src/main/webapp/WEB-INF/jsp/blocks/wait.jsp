<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="box box-widget" style="background-color: #eee; z-index: 99999; width: 200px; position: absolute; top: 0px; border: 1px solid gray;">
    <div class="box-header">
        <h3 class="box-title" style="font-size: 0.95em">Подождите</h3>
    </div>
    <div class="progress progress-xs active" style="width: 90%; margin: 0 auto 0;">
        <div class="progress-bar progress-bar-gray progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        </div>
    </div>
    <div class="box-body" name="waitIndicatorMessage">
        Обмен с сервером
    </div>
</div>