<%-- 
    Document   : coordinator-requestcard
    Created on : 16.01.2017, 19:20:27
    Author     : Никита
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
    <div class="box box-widget widget-user-2" data-request-card-id="{requestId}">
        <div class="widget-user-header bg-gray-active">
            <div class="widget-user-image">
                <img src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" style="width: 170px; height: 170px; padding-right: 10px;" alt="Фото"/>
            </div>
            <h4 class="widget-user-username" style="font-size: 1.3em">{lastName} {firstName} {middleName}</h4>
            <h6 class="widget-user-desc">Разыскивается с {missingFrom}</h6>
        </div>
        <div class="box-footer no-padding">
            <ul class="nav nav-stacked">
                <li style="margin-left: 180px"><a href="#">Статус <span class="pull-right badge bg-blue">Набор</span></a></li>
                <li style="margin-left: 180px"><a href="#">Группа <span class="pull-right badge bg-aqua">0/15</span></a></li>
            </ul>
        </div>
    </div>
</div>