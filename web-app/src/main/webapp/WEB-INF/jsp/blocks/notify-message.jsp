<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<li>
    <a href="#">
        <div class="pull-left">
            <img src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" class="img-circle" alt="Кто прислал">
        </div>
        <h4>
            Кто прислал (имя)
            <small><i class="fa fa-clock-o"></i> Сколько назад</small>
        </h4>
        <p>Часть сообщения для вывода (128 знаков)</p>
    </a>
</li>
