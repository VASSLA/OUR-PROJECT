<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<li data-contact-unique-id="{id}">
    <span class="text">{contact-type} : {contact}</span>
    <small class="label label-primary pull-right {hidden}"><i class="fa fa-home"></i> Oсновной</small>
    <div class="tools">
        <i class="fa fa-home"></i>
        <i class="fa fa-edit"></i>
        <i class="fa fa-trash-o"></i>
    </div>
</li>