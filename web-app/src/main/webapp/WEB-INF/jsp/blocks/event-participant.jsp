<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<li participant-id="{userId}">
    <img class="contacts-list-img" src="{picture}" style="width: 40px; height: 40px;"/>
    <div class="contacts-list-info">
        <i class="tool-btn-close fa fa-remove pull-right {btnHidden}" participant-id="{userId}"></i>
        <span class="contacts-list-name text-black">
            {fullName}
        </span>
        <span class="contacts-list-msg">{nick}</span>
        <span class="contacts-list-msg pull-right {hidden}"><i class="fa fa-star"></i> Инициатор</span>
    </div>
</li>