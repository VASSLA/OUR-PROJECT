<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<a href="#skill{id}" class="{skillColor}" data-skill="{id}" id="{id}"><div class="col-md-4 col-sm-6 col-xs-12"><i class="fa {skillIcon}"></i> {skill}</div></a>