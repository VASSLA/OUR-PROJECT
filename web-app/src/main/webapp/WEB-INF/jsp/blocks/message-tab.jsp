<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<li data-user-id="{userId}">
    <a href="#user{userId}" data-toggle="tab">{fullName} <!--<span data-toggle="tooltip" class="badge bg-yellow" data-original-title="2 New Messages">2</span>--></a>
    <i class="tool-btn-close fa fa-remove" data-user-id="{userId}"></i>
</li>
