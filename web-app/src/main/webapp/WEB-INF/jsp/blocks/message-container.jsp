<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div id="user{userId}" class="tab-pane" data-user-id="{userId}">
    <div class="box direct-chat direct-chat-warning">
        <div class="box-body">
            <div class="direct-chat-messages">
            </div>
        </div>
        <div class="box-footer">
            <div class="input-group">
                <input type="text" name="message" placeholder="Type Message ..." class="form-control" data-user-id="{userId}">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-warning btn-flat" data-user-id="{userId}">Send</button>
                </span>
            </div>
        </div>
    </div>
</div>
