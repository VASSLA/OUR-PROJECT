<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
    <div class="box box-widget widget-user-2 request-card request-card-mm-transition" data-request-id="{requestId}">
        <div class="widget-user-header">
            <div class="widget-user-image">
                <img src="{picture}" alt="Фото" style="width: 65px; height: 65px;"/>
            </div>
            <h4 class="widget-user-username" style="font-size: 1.3em">{lastName} {firstName} {middleName}</h4>
            <h4 class="widget-user-username" style="font-size: 1.1em">Возраст: {age}</h4>
            <h6 class="widget-user-desc">Разыскивается с {missingFrom}</h6>
        </div>
            <div class="box-footer no-padding" style="padding-top: 20px !important; padding-bottom: 6px !important; padding-right: 6px !important;">
            <div class="btn-group pull-right">
                <button class="btn btn-sm btn-default" data-request-id="{requestId}" data-prep-type="pdf"><i class="fa fa-file-pdf-o"></i></button>
                <button class="btn btn-sm btn-default" data-request-id="{requestId}" data-prep-type="html"><i class="fa fa-print"></i></button>
                <!--<button class="btn btn-sm btn-social-icon btn-vk"><i class="fa fa-vk"></i></button>-->
            </div>
        </div>
    </div>
</div>

