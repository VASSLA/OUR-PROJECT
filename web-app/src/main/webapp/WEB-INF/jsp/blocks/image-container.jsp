<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!-- Media Library styles -->
<link rel="stylesheet" href="<spring:url value="/resources/app/css/medialibrary.css"/>">

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bigdiv" mediaInfoId="{media-id}">
    <div class="hovereffect">
        <img class="img-responsive" src="{img-source}" alt="">
            <div class="overlay">
                <h2></h2>
                <p class="set1">
                    <a href="#">
                        <i class="fa fa-check btnMediaSelect" data-toggle="tooltip" title="Выбрать"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-save btnMediaDownload" data-toggle="tooltip" title="Скачать"></i>
                    </a>
                </p>
                <hr>
                <hr>
                <p class="set2">
                    <a href="#">
                        <i class="fa fa-link btnMediaLink" data-toggle="tooltip" title="Получить ссылку"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-remove btnMediaDelete" data-toggle="tooltip" title="Удалить"></i>
                    </a>
                </p>
            </div>
    </div>
</div>