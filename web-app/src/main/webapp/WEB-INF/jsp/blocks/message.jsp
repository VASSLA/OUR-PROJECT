<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="direct-chat-msg">
    <div class="direct-chat-info clearfix">
        <span class="direct-chat-name pull-left">{fullname}</span>
        <span class="direct-chat-timestamp pull-right">{datetime}</span>
    </div>
    <img class="direct-chat-img" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="Message User Image"><!-- /.direct-chat-img -->
    <div class="direct-chat-text">
        {message}
    </div>
</div>