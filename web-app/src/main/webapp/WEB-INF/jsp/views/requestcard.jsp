<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<section class="content">
    <div class="nav-tabs-custom">
        <!-- TTab headers -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#requestCardInfoView" id="requestCardInfoTab" data-toggle="tab">Информация</a></li>
            <li><a href="#requestCardGroupView" id="requestCardGroupTab" data-toggle="tab">Участники и группы</a></li>
            <li><a href="#requestCardTasksView" id="requestCardTasksTab" data-toggle="tab">Задания</a></li>
            <li><a href="#requestCardMapsView" id="requestCardMapsTab" data-toggle="tab">Карты</a></li>
            <li><a href="#requestCardPlanView" id="requestCardPlanTab" data-toggle="tab">Планирование</a></li>
        </ul>
        <!-- Tab contents -->
        <div class="tab-content">
            <div class="active tab-pane" id="requestCardInfoView">
                <div class="row">
                    <div class="col-md-5 col-sm-4">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Фотографии</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="2" class="active"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="item">
                                            <img src="http://placehold.it/500x500/999999/ffffff&amp;text=Фотография 1" alt="Фотография 1">
                                            <div class="carousel-caption">
                                                Комментарий к фото 1
                                            </div>
                                        </div>
                                        <div class="item">
                                            <img src="http://placehold.it/500x500/AAAAAA/ffffff&amp;text=Фотография 2" alt="Фотография 2">
                                            <div class="carousel-caption">
                                                Комментарий к фото 2
                                            </div>
                                        </div>
                                        <div class="item active">
                                            <img src="http://placehold.it/500x500/888888/ffffff&amp;text=Фотография 3" alt="Фотография 3">
                                            <div class="carousel-caption">
                                                Комментарий к фото 3
                                            </div>
                                        </div>
                                    </div>
                                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                        <span class="fa fa-angle-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                        <span class="fa fa-angle-right"></span>
                                    </a>
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-8">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title text-white">Общая информация</h3>
                            </div>
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>Ф.И.О</dt>
                                    <dd>{lastName} {firstName} {middleName}</dd>
                                    <dt>Возраст</dt>
                                    <dd>{age}</dd>
                                    <dt>Одет</dt>
                                    <dd>{dressedIn}</dd>
                                    <dt>Особые приметы</dt>
                                    <dd>{appearance}</dd>
                                    <dt>Разыскивается с:</dt>
                                    <dd>{missingFrom}</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Контактная информация</h3>
                            </div>
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>Телефон разыскиваемого</dt>
                                    <dd>{missingPersonContact}</dd>
                                    <dt>Телефон заявителя</dt>
                                    <dd>{declarantPersonContact}</dd>
                                    <dt>E-Mail заявителя</dt>
                                    <dd><a href="mailto:email@email.com">{declarantPersonMail}</a></dd>
                                    <dt>Адрес проживания</dt>
                                    <dd>{missingPersonAddress}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="requestCardGroupView">
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Участники поисков</h3>
                            </div>
                            <div class="body">
                                <div class="box box-widget widget-user-2">
                                    <div class="widget-user-header bg-gray" style="padding: 15px 15px 20px 15px">
                                        <div class="widget-user-image">
                                            <img class="img-md" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                        </div>
                                        <h3 class="widget-user-username" style="font-size: 1.3em">Сергей Петров</h3>
                                        <span style="padding: 0px 15px 0px 15px">
                                            <i class="fa fa-car" title="Есть автомобиль"></i>
                                            <i class="fa fa-motorcycle" title="Есть мотоцикл"></i>
                                            <i class="fa fa-tree" title="Поиск в лесу"></i>
                                            <i class="fa fa-camera" title="Есть фотокамера"></i>
                                            <i class="fa fa-video-camera" title="Есть видеокамера"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="box box-widget widget-user-2">
                                    <div class="widget-user-header bg-gray" style="padding: 15px 15px 20px 15px">
                                        <div class="widget-user-image">
                                            <img class="img-md" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                        </div>
                                        <h3 class="widget-user-username" style="font-size: 1.3em">Василий Зайцев</h3>
                                        <span style="padding: 0px 15px 0px 15px">
                                            <i class="fa fa-male" title="Пешеход"></i>
                                            <i class="fa fa-tree" title="Поиск в лесу"></i>
                                            <i class="fa fa-medkit"title="Медик"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="box box-widget widget-user-2">
                                    <div class="widget-user-header bg-gray" style="padding: 15px 15px 20px 15px">
                                        <div class="widget-user-image">
                                            <img class="img-md" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                        </div>
                                        <h3 class="widget-user-username" style="font-size: 1.3em">Роман Семенов</h3>
                                        <span style="padding: 0px 15px 0px 15px">
                                            <i class="fa fa-male" title="Пешеход"></i>
                                            <i class="fa fa-car" title="Есть автомобиль"></i>
                                            <i class="fa fa-tree" title="Поиск в лесу"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Группы</h3>
                            </div>
                            <div class="body">
                                <div class="box box-solid box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Лиса 1</h3>
                                        <span style="padding: 0px 15px 0px 15px">
                                            <i class="fa fa-car" title="Есть автомобиль"></i>
                                            <i class="fa fa-tree" title="Поиск в лесу"></i>
                                        </span>
                                        <div class="box-tools pull-right">
                                            <span data-toggle="tooltip" title="" class="badge bg-light-blue" data-original-title="3 участника">3</span>
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                            <button class="btn btn-box-tool" data-toggle="tooltip" title="Сообщение группе" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="padding:10px">
                                        <img class="img-bordered-sm" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                        <img class="img-bordered-sm" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                        <img class="img-bordered-sm" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                    </div>
                                    <div class="box-footer">
                                        <form action="#" method="post">
                                            <div class="input-group">
                                                <input type="text" name="message" placeholder="Сообщение группе" class="form-control">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-primary btn-flat">Отправить</button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="box box-solid box-danger">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Лиса 2</h3>
                                        <span style="padding: 0px 15px 0px 15px">
                                            <i class="fa fa-car" title="Есть автомобиль"></i>
                                            <i class="fa fa-motorcycle" title="Есть мотоцикл"></i>
                                            <i class="fa fa-tree" title="Поиск в лесу"></i>
                                            <i class="fa fa-camera" title="Есть фотокамера"></i>
                                        </span>
                                        <div class="box-tools pull-right">
                                            <span data-toggle="tooltip" title="" class="badge bg-red" data-original-title="2 участника">2</span>
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                            <button class="btn btn-box-tool" data-toggle="tooltip" title="Сообщение группе" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="padding:10px">
                                        <img class="img-bordered-sm" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                        <img class="img-bordered-sm" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                    </div>
                                    <div class="box-footer">
                                        <form action="#" method="post">
                                            <div class="input-group">
                                                <input type="text" name="message" placeholder="Сообщение группе" class="form-control">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-danger btn-flat">Отправить</button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="box box-solid box-success">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Лиса 3</h3>
                                        <span style="padding: 0px 15px 0px 15px">
                                            <i class="fa fa-tree" title="Поиск в лесу"></i>
                                            <i class="fa fa-camera" title="Есть фотокамера"></i>
                                            <i class="fa fa-video-camera" title="Есть видеокамера"></i>
                                        </span>
                                        <div class="box-tools pull-right">
                                            <span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="4 участника">4</span>
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                            <button class="btn btn-box-tool" data-toggle="tooltip" title="Сообщение группе" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="padding:10px">
                                        <img class="img-bordered-sm" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                        <img class="img-bordered-sm" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                        <img class="img-bordered-sm" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                        <img class="img-bordered-sm" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" alt="User Avatar" style="width: 60px; height: 60px">
                                    </div>
                                    <div class="box-footer">
                                        <form action="#" method="post">
                                            <div class="input-group">
                                                <input type="text" name="message" placeholder="Сообщение группе" class="form-control">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-flat">Отправить</button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="requestCardTasksView">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Задания</h3>
                                <div class="box-tools pull-right">
                                    <div class="has-feedback">
                                        <input type="text" class="form-control input-sm" placeholder="Поиск задачи">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </div><!-- /.box-tools -->
                            </div><!-- /.box-header -->
                            <div class="box-body no-padding">
                                <div class="mailbox-controls">
                                    <!-- Check all button -->
                                    <button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-sm"><i class="fa fa-tree" title="Задание в лесу"></i></button>
                                        <button class="btn btn-default btn-sm"><i class="fa fa-home" title="Задание в городе"></i></button>
                                        <button class="btn btn-default btn-sm"><i class="fa fa-newspaper-o" title="Расклейка объявлений"></i></button>
                                        <button class="btn btn-default btn-sm"><i class="fa fa-phone" title="Обзвон"></i></button>
                                        <button class="btn btn-default btn-sm"><i class="fa fa-ellipsis-h" title="Больше заданий"></i></button>
                                    </div><!-- /.btn-group -->
                                    <button class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                                    <button class="btn btn-default btn-sm"><i class="fa fa-trash"></i></button>
                                    <div class="pull-right">
                                        1-20/4
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                            <button class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                                        </div><!-- /.btn-group -->
                                    </div><!-- /.pull-right -->
                                </div>
                                <div class="table-responsive mailbox-messages">
                                    <table class="table table-hover table-striped">
                                        <tbody>
                                            <tr>
                                                <td><div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>
                                                <td class="mailbox-star"><a href="#"><i class="fa fa-tree text-yellow"></i></a></td>
                                                <td class="mailbox-name"><a href="#">Лиса 1</a></td>
                                                <td class="mailbox-subject">Поиск/Лес, Зона 1</td>
                                                <td class="mailbox-attachment"></td>
                                                <td class="mailbox-date">Активна 30 минут</td>
                                            </tr>
                                            <tr>
                                                <td><div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>
                                                <td class="mailbox-star"><a href="#"><i class="fa fa-tree text-yellow"></i></a></td>
                                                <td class="mailbox-name"><a href="#">Лиса 4</a></td>
                                                <td class="mailbox-subject">Поиск/Лес, Зона 5</td>
                                                <td class="mailbox-attachment"></td>
                                                <td class="mailbox-date">Активна 15 минут</td>
                                            </tr>
                                            <tr>
                                                <td><div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>
                                                <td class="mailbox-star"><a href="#"><i class="fa fa-home text-yellow"></i></a></td>
                                                <td class="mailbox-name"><a href="#">Лиса 2</a></td>
                                                <td class="mailbox-subject">Поиск/Город, Зона 3</td>
                                                <td class="mailbox-attachment"></i></td>
                                                <td class="mailbox-date">Активна 12 минут</td>
                                            </tr>
                                            <tr>
                                                <td><div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>
                                                <td class="mailbox-star"><a href="#"><i class="fa fa-phone text-gray"></i></a></td>
                                                <td class="mailbox-name"><a href="#">Лиса 3</a></td>
                                                <td class="mailbox-subject">Поиск/Город, Зона 3</td>
                                                <td class="mailbox-attachment"></i></td>
                                                <td class="mailbox-date">Завершено</td>
                                            </tr>
                                        </tbody>
                                    </table><!-- /.table -->
                                </div><!-- /.mail-box-messages -->
                            </div><!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="requestCardMapsView">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div id="mapviewport" name="mapviewport">
                            <div id="requestCardMap" style="width: 100%; min-height: 600px; margin-top:5px">
                                <div class="modal-dialog map-crop-dialog" id="map-crop-modal">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Разбиение на квадраты</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal">
                                                <fieldset>
                                                    <div class="form-group">
                                                      <div class="col-md-12">
                                                          <select id="grid-box-width" class="form-control" name="grid-box-width">
                                                              <option value="500" selected>500m</option>
                                                              <option value="1000">1000m</option>
                                                              <option value="2000">2000m</option>
                                                          </select>
                                                        <span class="help-block">Ширина квадрата в метрах</span>  
                                                      </div>
                                                    </div>
                                                    <div class="form-group">
                                                      <div class="col-md-12" id="grid-box-points">
                                                          <p id="grid-box-points-no-info" class="text-danger">Добавьте границы зоны разбиения!</p>  
                                                      </div>
                                                    </div>
                                                    <div class="form-group">
                                                      <div class="col-md-12">
                                                        <button id="crop-box-ok" name="crop-box-ok" class="btn btn-success">Разбить</button>
                                                        <button id="crop-box-cancel" name="crop-box-cancel" class="btn btn-danger">Отмена</button>
                                                      </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" id="map-pulse-save-button" onclick="return false;" style="display:none;">
                                    <div class="map-pulse-save-button-circle-fill" style="transform-origin: center;"></div>
                                    <div class="map-pulse-save-button-img-circle" style="transform-origin: center;">
                                        <div class="map-pulse-save-button-img-circleblock" style="transform-origin: center;"></div>  
                                    </div>
                                </a>
                                <a href="#" id="map-pulse-reload-button" onclick="return false;" style="display:none;">
                                    <div class="map-pulse-reload-button-circle-fill" style="transform-origin: center;"></div>
                                    <div class="map-pulse-reload-button-img-circle" style="transform-origin: center;">
                                        <div class="map-pulse-reload-button-img-circleblock" style="transform-origin: center;"></div>  
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="requestCardPlanView">
                <div class="row">
                    <div class="col-md-9">
                        <div class="box box-default">
                            <div class="box-body no-padding">
                                <!-- THE CALENDAR -->
                                <div id="calendar" class="fc fc-ltr fc-unthemed">
                                    <div class="fc-toolbar">
                                        <div class="fc-left">
                                            <div class="fc-button-group">
                                                <button type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left"><span class="fc-icon fc-icon-left-single-arrow"></span></button>
                                                <button type="button" class="fc-next-button fc-button fc-state-default fc-corner-right"><span class="fc-icon fc-icon-right-single-arrow"></span></button>
                                            </div>
                                            <button type="button" class="fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right fc-state-disabled" disabled="disabled">Сегодня</button>
                                        </div>
                                        <div class="fc-right">
                                            <div class="fc-button-group">
                                                <button type="button" class="fc-month-button fc-button fc-state-default fc-corner-left fc-state-active">Месяц</button>
                                                <button type="button" class="fc-agendaWeek-button fc-button fc-state-default">Неделя</button>
                                                <button type="button" class="fc-agendaDay-button fc-button fc-state-default fc-corner-right">День</button>
                                            </div>
                                        </div>
                                        <div class="fc-center">
                                            <h2>Сентябрь 2016</h2>
                                        </div>
                                        <div class="fc-clear"></div>
                                    </div>
                                    <div class="fc-view-container" style="">
                                        <div class="fc-view fc-month-view fc-basic-view" style="font-size: 0.9em">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <td class="fc-widget-header">
                                                            <div class="fc-row fc-widget-header">
                                                                <table>
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="fc-day-header fc-widget-header fc-mon">Пн</th>
                                                                            <th class="fc-day-header fc-widget-header fc-tue">Вт</th>
                                                                            <th class="fc-day-header fc-widget-header fc-wed">Ср</th>
                                                                            <th class="fc-day-header fc-widget-header fc-thu">Чт</th>
                                                                            <th class="fc-day-header fc-widget-header fc-fri">Пт</th>
                                                                            <th class="fc-day-header fc-widget-header fc-sat">Сб</th>
                                                                            <th class="fc-day-header fc-widget-header fc-sun">Вс</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="fc-widget-content">
                                                            <div class="fc-day-grid-container">
                                                                <div class="fc-day-grid"  style="font-size: 0.8em">
                                                                    <div class="fc-row fc-week fc-widget-content" style="height: 80px;">
                                                                        <div class="fc-bg">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="fc-day fc-widget-content fc-sun fc-other-month fc-past" data-date="2016-08-28"></td>
                                                                                        <td class="fc-day fc-widget-content fc-mon fc-other-month fc-past" data-date="2016-08-29"></td>
                                                                                        <td class="fc-day fc-widget-content fc-tue fc-other-month fc-past" data-date="2016-08-30"></td>
                                                                                        <td class="fc-day fc-widget-content fc-wed fc-other-month fc-past" data-date="2016-08-31"></td>
                                                                                        <td class="fc-day fc-widget-content fc-thu fc-past" data-date="2016-09-01"></td>
                                                                                        <td class="fc-day fc-widget-content fc-fri fc-past" data-date="2016-09-02"></td>
                                                                                        <td class="fc-day fc-widget-content fc-sat fc-past" data-date="2016-09-03"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="fc-content-skeleton">
                                                                            <table>
                                                                                <thead>
                                                                                    <tr>
                                                                                        <td class="fc-day-number fc-sun fc-other-month fc-past" data-date="2016-08-28">28</td>
                                                                                        <td class="fc-day-number fc-mon fc-other-month fc-past" data-date="2016-08-29">29</td>
                                                                                        <td class="fc-day-number fc-tue fc-other-month fc-past" data-date="2016-08-30">30</td>
                                                                                        <td class="fc-day-number fc-wed fc-other-month fc-past" data-date="2016-08-31">31</td>
                                                                                        <td class="fc-day-number fc-thu fc-past" data-date="2016-09-01">1</td>
                                                                                        <td class="fc-day-number fc-fri fc-past" data-date="2016-09-02">2</td>
                                                                                        <td class="fc-day-number fc-sat fc-past" data-date="2016-09-03">3</td>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td class="fc-event-container">
                                                                                            <a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" style="background-color:#f56954;border-color:#f56954">
                                                                                                <div class="fc-content">
                                                                                                    <span class="fc-time">12:00</span>
                                                                                                    <span class="fc-title">Тренировка</span>
                                                                                                </div>
                                                                                            </a>
                                                                                        </td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="fc-row fc-week fc-widget-content" style="height: 80px;">
                                                                        <div class="fc-bg">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="fc-day fc-widget-content fc-sun fc-past" data-date="2016-09-04"></td>
                                                                                        <td class="fc-day fc-widget-content fc-mon fc-past" data-date="2016-09-05"></td>
                                                                                        <td class="fc-day fc-widget-content fc-tue fc-past" data-date="2016-09-06"></td>
                                                                                        <td class="fc-day fc-widget-content fc-wed fc-past" data-date="2016-09-07"></td>
                                                                                        <td class="fc-day fc-widget-content fc-thu fc-past" data-date="2016-09-08"></td>
                                                                                        <td class="fc-day fc-widget-content fc-fri fc-past" data-date="2016-09-09"></td>
                                                                                        <td class="fc-day fc-widget-content fc-sat fc-past" data-date="2016-09-10"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="fc-content-skeleton">
                                                                            <table>
                                                                                <thead>
                                                                                    <tr>
                                                                                        <td class="fc-day-number fc-sun fc-past" data-date="2016-09-04">4</td>
                                                                                        <td class="fc-day-number fc-mon fc-past" data-date="2016-09-05">5</td>
                                                                                        <td class="fc-day-number fc-tue fc-past" data-date="2016-09-06">6</td>
                                                                                        <td class="fc-day-number fc-wed fc-past" data-date="2016-09-07">7</td>
                                                                                        <td class="fc-day-number fc-thu fc-past" data-date="2016-09-08">8</td>
                                                                                        <td class="fc-day-number fc-fri fc-past" data-date="2016-09-09">9</td>
                                                                                        <td class="fc-day-number fc-sat fc-past" data-date="2016-09-10">10</td>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="fc-row fc-week fc-widget-content" style="height: 80px;">
                                                                        <div class="fc-bg">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="fc-day fc-widget-content fc-sun fc-past" data-date="2016-09-11"></td>
                                                                                        <td class="fc-day fc-widget-content fc-mon fc-past" data-date="2016-09-12"></td>
                                                                                        <td class="fc-day fc-widget-content fc-tue fc-past" data-date="2016-09-13"></td>
                                                                                        <td class="fc-day fc-widget-content fc-wed fc-past" data-date="2016-09-14"></td>
                                                                                        <td class="fc-day fc-widget-content fc-thu fc-past" data-date="2016-09-15"></td>
                                                                                        <td class="fc-day fc-widget-content fc-fri fc-past" data-date="2016-09-16"></td>
                                                                                        <td class="fc-day fc-widget-content fc-sat fc-past" data-date="2016-09-17"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="fc-content-skeleton">
                                                                            <table>
                                                                                <thead>
                                                                                    <tr>
                                                                                        <td class="fc-day-number fc-sun fc-past" data-date="2016-09-11">11</td>
                                                                                        <td class="fc-day-number fc-mon fc-past" data-date="2016-09-12">12</td>
                                                                                        <td class="fc-day-number fc-tue fc-past" data-date="2016-09-13">13</td>
                                                                                        <td class="fc-day-number fc-wed fc-past" data-date="2016-09-14">14</td>
                                                                                        <td class="fc-day-number fc-thu fc-past" data-date="2016-09-15">15</td>
                                                                                        <td class="fc-day-number fc-fri fc-past" data-date="2016-09-16">16</td>
                                                                                        <td class="fc-day-number fc-sat fc-past" data-date="2016-09-17">17</td>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="fc-row fc-week fc-widget-content" style="height: 80px;">
                                                                        <div class="fc-bg">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="fc-day fc-widget-content fc-sun fc-past" data-date="2016-09-18"></td>
                                                                                        <td class="fc-day fc-widget-content fc-mon fc-past" data-date="2016-09-19"></td>
                                                                                        <td class="fc-day fc-widget-content fc-tue fc-past" data-date="2016-09-20"></td>
                                                                                        <td class="fc-day fc-widget-content fc-wed fc-past" data-date="2016-09-21"></td>
                                                                                        <td class="fc-day fc-widget-content fc-thu fc-past" data-date="2016-09-22"></td>
                                                                                        <td class="fc-day fc-widget-content fc-fri fc-past" data-date="2016-09-23"></td>
                                                                                        <td class="fc-day fc-widget-content fc-sat fc-past" data-date="2016-09-24"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="fc-content-skeleton">
                                                                            <table>
                                                                                <thead>
                                                                                    <tr>
                                                                                        <td class="fc-day-number fc-sun fc-past" data-date="2016-09-18">18</td>
                                                                                        <td class="fc-day-number fc-mon fc-past" data-date="2016-09-19">19</td>
                                                                                        <td class="fc-day-number fc-tue fc-past" data-date="2016-09-20">20</td>
                                                                                        <td class="fc-day-number fc-wed fc-past" data-date="2016-09-21">21</td>
                                                                                        <td class="fc-day-number fc-thu fc-past" data-date="2016-09-22">22</td>
                                                                                        <td class="fc-day-number fc-fri fc-past" data-date="2016-09-23">23</td>
                                                                                        <td class="fc-day-number fc-sat fc-past" data-date="2016-09-24">24</td>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td class="fc-event-container" colspan="3">
                                                                                            <a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" style="background-color:#f39c12;border-color:#f39c12">
                                                                                                <div class="fc-content">
                                                                                                    <span class="fc-time">12:00</span>
                                                                                                    <span class="fc-title">Очень длинное трехдневное событие</span>
                                                                                                </div>
                                                                                            </a>
                                                                                        </td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="fc-row fc-week fc-widget-content" style="height: 80px;">
                                                                        <div class="fc-bg">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="fc-day fc-widget-content fc-sun fc-past" data-date="2016-09-25"></td>
                                                                                        <td class="fc-day fc-widget-content fc-mon fc-today fc-state-highlight" data-date="2016-09-26"></td>
                                                                                        <td class="fc-day fc-widget-content fc-tue fc-future" data-date="2016-09-27"></td>
                                                                                        <td class="fc-day fc-widget-content fc-wed fc-future" data-date="2016-09-28"></td>
                                                                                        <td class="fc-day fc-widget-content fc-thu fc-future" data-date="2016-09-29"></td>
                                                                                        <td class="fc-day fc-widget-content fc-fri fc-future" data-date="2016-09-30"></td>
                                                                                        <td class="fc-day fc-widget-content fc-sat fc-other-month fc-future" data-date="2016-10-01"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="fc-content-skeleton">
                                                                            <table>
                                                                                <thead>
                                                                                    <tr>
                                                                                        <td class="fc-day-number fc-sun fc-past" data-date="2016-09-25">25</td>
                                                                                        <td class="fc-day-number fc-mon fc-today fc-state-highlight" data-date="2016-09-26">26</td>
                                                                                        <td class="fc-day-number fc-tue fc-future" data-date="2016-09-27">27</td>
                                                                                        <td class="fc-day-number fc-wed fc-future" data-date="2016-09-28">28</td>
                                                                                        <td class="fc-day-number fc-thu fc-future" data-date="2016-09-29">29</td>
                                                                                        <td class="fc-day-number fc-fri fc-future" data-date="2016-09-30">30</td>
                                                                                        <td class="fc-day-number fc-sat fc-other-month fc-future" data-date="2016-10-01">1</td>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td rowspan="2"></td>
                                                                                        <td class="fc-event-container">
                                                                                            <a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" style="background-color:#0073b7;border-color:#0073b7">
                                                                                                <div class="fc-content">
                                                                                                    <span class="fc-time">10:30</span>
                                                                                                    <span class="fc-title">Встреча группы</span>
                                                                                                </div>
                                                                                            </a>
                                                                                        </td>
                                                                                        <td class="fc-event-container" rowspan="2">
                                                                                            <a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" style="background-color:#00a65a;border-color:#00a65a">
                                                                                                <div class="fc-content">
                                                                                                    <span class="fc-time">19:00</span>
                                                                                                    <span class="fc-title">Обучение</span>
                                                                                                </div>
                                                                                            </a>
                                                                                        </td>
                                                                                        <td class="fc-event-container" rowspan="2">
                                                                                            <a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" href="http://google.com/" style="background-color:#3c8dbc;border-color:#3c8dbc">
                                                                                                <div class="fc-content">
                                                                                                    <span class="fc-time">12:00</span>
                                                                                                    <span class="fc-title">Заметка/Напоминание</span>
                                                                                                </div>
                                                                                            </a>
                                                                                        </td>
                                                                                        <td rowspan="2"></td>
                                                                                        <td rowspan="2"></td>
                                                                                        <td rowspan="2"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="fc-event-container">
                                                                                            <a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" style="background-color:#00c0ef;border-color:#00c0ef">
                                                                                                <div class="fc-content">
                                                                                                    <span class="fc-time">19:00</span>
                                                                                                    <span class="fc-title">Встреча</span>
                                                                                                </div>
                                                                                            </a>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="fc-row fc-week fc-widget-content" style="height: 149px;">
                                                                        <div class="fc-bg">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="fc-day fc-widget-content fc-sun fc-other-month fc-future" data-date="2016-10-02"></td>
                                                                                        <td class="fc-day fc-widget-content fc-mon fc-other-month fc-future" data-date="2016-10-03"></td>
                                                                                        <td class="fc-day fc-widget-content fc-tue fc-other-month fc-future" data-date="2016-10-04"></td>
                                                                                        <td class="fc-day fc-widget-content fc-wed fc-other-month fc-future" data-date="2016-10-05"></td>
                                                                                        <td class="fc-day fc-widget-content fc-thu fc-other-month fc-future" data-date="2016-10-06"></td>
                                                                                        <td class="fc-day fc-widget-content fc-fri fc-other-month fc-future" data-date="2016-10-07"></td>
                                                                                        <td class="fc-day fc-widget-content fc-sat fc-other-month fc-future" data-date="2016-10-08"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="fc-content-skeleton">
                                                                            <table>
                                                                                <thead>
                                                                                    <tr>
                                                                                        <td class="fc-day-number fc-sun fc-other-month fc-future" data-date="2016-10-02">2</td>
                                                                                        <td class="fc-day-number fc-mon fc-other-month fc-future" data-date="2016-10-03">3</td>
                                                                                        <td class="fc-day-number fc-tue fc-other-month fc-future" data-date="2016-10-04">4</td>
                                                                                        <td class="fc-day-number fc-wed fc-other-month fc-future" data-date="2016-10-05">5</td>
                                                                                        <td class="fc-day-number fc-thu fc-other-month fc-future" data-date="2016-10-06">6</td>
                                                                                        <td class="fc-day-number fc-fri fc-other-month fc-future" data-date="2016-10-07">7</td>
                                                                                        <td class="fc-day-number fc-sat fc-other-month fc-future" data-date="2016-10-08">8</td>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                        </div><!-- /. box -->
                    </div><!-- /.col -->

                    <div class="col-md-3">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h4 class="box-title">Типы событий</h4>
                            </div>
                            <div class="box-body">
                                <!-- the events -->
                                <div id="external-events">
                                    <div class="external-event bg-green ui-draggable ui-draggable-handle" style="position: relative;">Тренировка</div>
                                    <div class="external-event bg-yellow ui-draggable ui-draggable-handle" style="position: relative;">Поиски</div>
                                    <div class="external-event bg-aqua ui-draggable ui-draggable-handle" style="position: relative;">Встреча</div>
                                    <div class="checkbox">
                                        <label for="drop-remove">
                                            <input type="checkbox" id="drop-remove">
                                            Удалять после перетаскивания
                                        </label>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                        </div><!-- /. box -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Создать тип события</h3>
                            </div>
                            <div class="box-body">
                                <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                                    <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                                    <ul class="fc-color-picker" id="color-chooser">
                                        <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                                        <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                                        <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                                        <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                                        <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                                        <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                                    </ul>
                                </div><!-- /btn-group -->
                                <div class="input-group">
                                    <input id="new-event" type="text" class="form-control" placeholder="Event Title">
                                    <div class="input-group-btn">
                                        <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
                                    </div><!-- /btn-group -->
                                </div><!-- /input-group -->
                            </div>
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                <script src="<spring:url value="/resources/lib/fullcalendar/lib/moment.min.js"/>"></script>
                <script src="<spring:url value="/resources/lib/fullcalendar/fullcalendar.min.js"/>"></script>
            </div>
        </div>
    </div>
</section>
