<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Параметры функционирования системы</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>Дата изменения</th>
                                <th>Значение</th>
                            </tr>
                            <tr>
                                <td>la.logging.enabled</td>
                                <td>Запись журналов включена</td>
                                <td>11.09.2016 08:12</td>
                                <td>Да</td>
                            </tr>
                            <tr>
                                <td>la.logging.type</td>
                                <td>Тип журнала</td>
                                <td>11.09.2016 08:15</td>
                                <td>База данных</td>
                            </tr>
                            <tr>
                                <td>la.logging.database.connection</td>
                                <td>Настройка соединения с базой</td>
                                <td>11.09.2016 08:15</td>
                                <td>jdbc:mysql://hostname:3306/?Data Source=la_db</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
