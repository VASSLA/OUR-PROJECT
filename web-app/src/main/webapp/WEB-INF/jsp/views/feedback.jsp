<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <button id="btnFeedbackNew" class="btn btn-primary btn-block margin-bottom">Написать в поддержку</button>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Категории</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="#"><i class="fa fa-envelope"></i> Новые <span class="label label-primary pull-right" id="newFeedbackNum">0</span></a></li>
                        <li><a href="#"><i class="fa fa-envelope-open"></i> Принято <span class="label label-warning pull-right" id="acceptedFeedbackNum">0</span></a></li>
                        <li><a href="#"><i class="fa fa-gavel"></i> В обработке <span class="label label-warning pull-right" id="processingFeedbackNum">0</span></a></li>
                        <li><a href="#"><i class="fa fa-child"></i> Закрыто <span class="label label-success pull-right" id="closedFeedbackNum">0</span></a></li>
                        <li><a href="#"><i class="fa fa-ban"></i> Отклонено <span class="label label-danger pull-right" id="rejectedFeedbackNum">0</span></a></li>
                    </ul>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Метки</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="#"><i class="fa fa-circle-o text-red"></i> Критическая ошибка</a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Проблема</a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Запрос на доработку</a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-gray"></i> Другое</a></li>
                    </ul>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Сообщения обратной связи</h3>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <input type="text" class="form-control input-sm" placeholder="Искать в сообщениях">
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="mailbox-controls">
                        <!-- Check all button -->
                        <!--<button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>-->
                        <div class="btn-group">
<!--                            <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                            <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                            <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>-->
                        </div><!-- /.btn-group -->
                        <!--<button class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>-->
                        <div class="pull-right">
                            <span id="feedbackPageCount">
                            1-50/200
                            </span>
                            <div class="btn-group">
                                <button class="btn btn-default btn-sm" id="btnFeedbackLeft"><i class="fa fa-chevron-left"></i></button>
                                <button class="btn btn-default btn-sm" id="btnFeedbackRight"><i class="fa fa-chevron-right"></i></button>
                            </div><!-- /.btn-group -->
                        </div><!-- /.pull-right -->
                    </div>
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped" id="feedbackTable">
                            <tbody>
                                    <tr>
                                            <th>Номер</th>
                                            <th>Инициатор</th>
                                            <th>Текст</th>
                                            <th>Статус</th>
                                            <th><></th>
                                            <th>Дата</th>
                                    </tr>
                            </tbody>
                    </div><!-- /.mail-box-messages -->
                </div>
            </div><!-- /. box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
