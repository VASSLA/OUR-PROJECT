<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<section class="content-header">
    <h1>Заявки на поиск
        <span class="pull-right">
            <div class="btn-group">
                <button id="btnRequestNew" class="btn btn-sm btn-default"><i class="fa fa-newspaper-o"></i> Новая</button>
                <button id="btnRequestViewThumbs" class="btn btn-sm btn-default"><i class="fa fa-th-large"></i></button>
                <button id="btnRequestViewList" class="btn btn-sm btn-default"><i class="fa fa-th-list"></i></button>
            </div>
        </span>
    </h1>
</section>
<section class="content">
    <form action="#" method="get" id="requestSearch" name="requestSearch">
        <div class="row">
            <div class="col-lg-12">
                <div class="input-group">
                    <input type="text" name="query" id="requestSearchQuery" class="form-control" placeholder="Что будем искать ...">
                    <span class="input-group-btn">
                        <button type="button" id="btnSearchRequest" class="btn btn-flat"><i class="fa fa-search"></i></button>
                        <button type="button" id="btnShowExtraParams" class="btn btn-flat"><i class="fa fa-angle-down"></i></button>
                    </span>
                </div>
            </div>
        </div>
        <div class="row" id="requestSearchExtraParams" style="border: 1px solid #ccc; border-top: 0px; margin: 0px; padding: 5px 5px 20px 5px; display: block;">
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="input-group" style="width:100%">
                    <h5>Дальность от вашего местоположения</h5>
                    <input type="text" name="distance" id="requestSearchDistance" value="" style="display: block;">
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="input-group" style="width:100%">
                    <h5>Возраст разыскиваемого</h5>
                    <input type="text" name="age" id="requestSearchAge" value="" style="display: block;">
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <h5>Пол разыскиваемого</h5>
                <div class="form-group">
                    <div class="radio">
                        <label>
                            <input type="radio" name="requestSearchSex" value="-1" checked="">
                            Неважно
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="requestSearchSex" value="0">
                            Мужской
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="requestSearchSex" value="1">
                            Женский
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <ul class="list-group">
                    <li class="list-group-item">
                        Только срочные заявки
                        <div class="material-switch pull-right">
                            <input id="urgentRequests" name="urgentRequests" type="checkbox"/>
                            <label for="urgentRequests" class="label-primary"></label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <ul class="list-group">
                    <li class="list-group-item">
                        Проблемы со здоровьем
                        <div class="material-switch pull-right">
                            <input id="healthTroubles" name="healthTroubles" type="checkbox"/>
                            <label for="healthTroubles" class="label-primary"></label>
                        </div>
                    </li>
<!--                    <li class="list-group-item">
                        Только с открытым набором
                        <div class="material-switch pull-right">
                            <input id="recruitmentRequests" name="recruitmentRequests" type="checkbox"/>
                            <label for="recruitmentRequests" class="label-primary"></label>
                        </div>
                    </li>
                    <li class="list-group-item">
                        Требуется транспорт
                        <div class="material-switch pull-right">
                            <input id="needTransport" name="needTransport" type="checkbox"/>
                            <label for="needTransport" class="label-primary"></label>
                        </div>
                    </li>-->
                </ul>
            </div>
        </div>
    </form>
    <hr>
    <div class="row" id="requestsPlaceholder">
    </div>
    <div id="modalPlaceholder"></div>
</section>