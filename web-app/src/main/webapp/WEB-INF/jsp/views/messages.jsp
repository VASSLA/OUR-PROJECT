<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<section class="content">
    <div class="row">
        <div id="messagesChatsColumn" class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
            <div ID="chats" class="nav-tabs-custom" style="min-height: 200px">
                <!--<div class="background-text">Чтобы начать чат выберите контакт справа</div>-->
                <ul class="nav nav-tabs" id="messageTabContainer" style="min-height: 44px">
                </ul>
                <div class="tab-content" id="messageContainersContainer">
                </div>
            </div>
        </div>
        <div id="messagesContactsColumn" class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div id="contactList" class="box box-primary direct-chat direct-chat-primary direct-chat-contacts-open">
                <div class="box-header with-border">
                    <h3 class="box-title" style="padding-bottom: 5px">Контакты</h3>
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control pull-right" placeholder="Искать" id="messageContactSearchInput">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default" id="messageContactSearchButton"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="direct-chat-contacts-open">
                        <ul class="contacts-list" id="messagesContactListContainer"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
