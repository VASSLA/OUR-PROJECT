<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<section class="content">
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img id="profilePersonImage" src="<spring:url value="resources/lib/adminlte/img/user4-128x128.jpg"/>" style="width: 100px; height: 100px;" class="profile-user-img img-responsive img-circle cursor-pointer"/>
                    <h3 id="profilePersonFullName" class="profile-username text-center">Имя Пользователя</h3>
                    <p id="profilePersonAge" class="text-muted text-center">Возраст</p>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Рейтинг</b>
                            <a id="profilePersonRating" class="pull-right text-blue text-bold">4,6</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3>Обо мне</h3>
                </div>
                <div class="box-body box-profile">
                    <!--<i class="fa fa-book margin-r-5"></i>
                    <strong id="profilePersonEducation">Образование</strong>
                    <p class="text-muted">Московская Государственная Академия Приборостроения и Информатики</p>
                    <hr></hr>-->
                    <i class="fa fa-map-marker margin-r-5"></i>
                    <strong id="profilePersonLivingPlace">Место жительства</strong>
                    <p class="text-muted">Московская обл., г.Кашира</p>
                    <hr></hr>
                    <i class="fa fa-pen margin-r-5"></i>
                    <strong id="profilePersonSkills">Навыки</strong>
                    <p>
                        <span class="label label-primary">Ориентирование</span>
                        <span class="label label-success">Опытный водитель</span>
                        <span class="label label-danger">Координатор</span>
                        <span class="label label-info">Работа в группе</span>
                        <span class="label label-warning">Поиски</span>
                    </p>
                    <hr></hr>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#timeline" data-toggle="tab">Лента активности</a>
                    </li>
                    <li>
                        <a href="#settings" data-toggle="tab">Настройки</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="timeline" class="tab-pane active">
                        <ul class="timeline timeline-inverse">
                            <li class="time-label">
                                <span class="bg-red">06.09.2016</span>
                            </li>
                            <li>
                                <i class="fa fa-envelope bg-blue"></i>
                                <div class="timeline-item">
                                    <span class="time">
                                        <i class="fa fa-clock-o"></i>03:40
                                    </span>
                                    <h3 class="timeline-header">
                                        <a href="#">LizaAlert</a><span class="text"> прислал Вам сообщение</span>
                                    </h3>
                                    <div class="timeline-body">
                                        Список предстоящих мероприятий, направленных на обучение волонтеров
                                    </div>
                                    <div class="timeline-footer">
                                        <a href="#" class="btn btn-primary btn-xs">Подробнее</a>
                                        <a href="#" class="btn btn-danger btn-xs">Удалить</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-map-marker bg-green"></i>
                                <div class="timeline-item">
                                    <span class="time">
                                        <i class="fa fa-clock-o"></i>06:23
                                    </span>
                                    <h3 class="timeline-header">
                                        <a href="#">Ваши данные отправлены</a>
                                    </h3>
                                    <div class="timeline-body">
                                        Ваши данные о трекинге отправлены в координационный центр
                                    </div>
                                    <div class="timeline-footer">
                                        <a href="#" class="btn btn-primary btn-xs">Подробнее</a>
                                        <a href="#" class="btn btn-danger btn-xs">Удалить</a>
                                    </div>
                                </div>
                            </li>
                            <li class="time-label">
                                <span class="bg-red">05.09.2016</span>
                            </li>
                            <li>
                                <i class="fa fa-photo bg-orange"></i>
                                <div class="timeline-item">
                                    <span class="time">
                                        <i class="fa fa-clock-o"></i>18:20
                                    </span>
                                    <h3 class="timeline-header">
                                        <a href="#">Вы приложили фото к заявке</a>
                                    </h3>
                                    <div class="timeline-body">
                                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                                    </div>
                                    <div class="timeline-footer">
                                        <a href="#" class="btn btn-primary btn-xs">Подробнее</a>
                                        <a href="#" class="btn btn-danger btn-xs">Удалить</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-clock-o"></i>
                            </li>
                        </ul>
                    </div>
                    <div id="settings" class="tab-pane">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="profileFirstName">Имя</label>
                                <div class="col-sm-10">
                                    <input id="profileFirstName" class="form-control" placeholder="Имя" type="text"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="profileMiddleName">Отчество</label>
                                <div class="col-sm-10">
                                    <input id="profileMiddleName" class="form-control" placeholder="Отчество" type="text"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="profileLastName">Фамилия</label>
                                <div class="col-sm-10">
                                    <input id="profileLastName" class="form-control" placeholder="Фамилия" type="text"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="profileBirthDate">Возраст</label>
                                <div class="col-sm-10">
                                    <input id="profileBirthDate" class="form-control" placeholder="Дата рождения" type="text"/>
                                </div>
                            </div>
                        </form>
                        <hr/>
                        <h4>Мои навыки</h4>
                        <div class="row" id="profileSkillContainer">
                        </div>
                        <div class="row">
                            <div class="btn-group pull-right" style="margin-right: 30px">
                                <button id="btnProfileUpdate" class="btn btn-sm btn-primary"><i class="fa fa-update"></i> Обновить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>