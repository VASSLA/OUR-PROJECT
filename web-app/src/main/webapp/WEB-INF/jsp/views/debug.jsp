<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<section class="content">
    <div class="btn-group">
        <button class="btn btn-default btn-sm" id="id1"><i class="fa fa-star"></i></button>
        <button class="btn btn-default btn-sm" id="id2"><i class="fa fa-home"></i></button>
        <button class="btn btn-default btn-sm" id="id3"><i class="fa fa-gears"></i></button>
        <button class="btn btn-default btn-sm" id="id4"><i class="fa fa-clone"></i></button>
        <button class="btn btn-default btn-sm" id="id5"><i class="fa fa-filter"></i></button>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="requestAddress" class="text-sm text-black">Укажите адрес для добавления</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="addressListAddress" data-type="address" data-pk="1" placeholder=""/>
                    <span class="input-group-addon" style="background-color: #eee" id="addressListAdd">
                        <span class="glyphicon glyphicon-plus"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>