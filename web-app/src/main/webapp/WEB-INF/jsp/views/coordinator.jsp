<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<section class="content">
    <h4>Заявки в работе</h4>
    <div class="row" id="coordinatorRequestsPlaceholder">
    </div>
</section>
<script>
    $(document).ready(function () {
        $("#distance").ionRangeSlider({min: 10, max: 100, step: 5, postfix: ' км', hasGrid: true});
        $("#age").ionRangeSlider({type: 'double', min: 0, max: 100, from: 20, to: 80, step: 1, postfix: ' лет', hasGrid: true});
        Mate.Selector('#requestRearchExtraParams').toggle();
    });
</script>
