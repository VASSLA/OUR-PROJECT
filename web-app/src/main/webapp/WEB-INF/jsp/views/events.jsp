<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<link rel="stylesheet" href="<spring:url value="/resources/app/css/events.css"/>">
<section class="content">
    <div class="row">
        <div class="col-md-10">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <div id="calendar"></div>
                    <!-- THE CALENDAR -->
                    <!--
                    <div id="calendar" class="fc fc-ltr fc-unthemed">
                    </div>
                    -->
                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->

        <div class="col-md-2">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">Типы событий</h4>
                </div>
                <div class="box-body">
                    <!-- the events -->
                    <div id="external-events">
                    </div>
                </div><!-- /.box-body -->
                <div id="deleteEventType" class="box-footer" hidden>
                    <div class="demo-droppable">
                        <i class="fa fa-remove"/> Удалить
                    </div>
                </div>
            </div><!-- /. box -->
            
            <div class="box box-primary">
                
                <!--<div class="input-group">-->
                <div class="box-header with-border">
                    <h4 class="box-title">Добавить тип события</h4>
                </div>
                <div class="box-body">
                    <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                        <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                        <ul class="fc-color-picker" id="newEventTypeColorChooser">
                            <li><a style="color: crimson" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: red" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: orangered" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: orange" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: goldenrod" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: gold" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: yellow" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: yellowgreen" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: limegreen" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: green" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: lightseagreen" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: lightblue" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: blue" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: darkblue" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: blueviolet" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: darkorchid" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: violet" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: lightpink" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: gray" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: black" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a style="color: brown" href="#"><i class="fa fa-square"></i></a></li>
                        </ul>
                    </div>
                
                    <input id="newEventTypeName" type="text" class="form-control" placeholder="Новый тип события">

                    <div class="input-group-btn">
                        <button id="btnAddEventType" type="button" class="btn btn-primary btn-flat">Добавить</button>
                    </div>
                    <!-- /btn-group -->
                </div>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <script src="<spring:url value="/resources/lib/fullcalendar/fullcalendar.min.js"/>"></script>
</section>
