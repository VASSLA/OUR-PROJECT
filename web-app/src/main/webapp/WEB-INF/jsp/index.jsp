<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Лиза Алерт</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Favicon -->
        <link rel="icon" href="<spring:url value="/resources/app/img/logo.png"/>" type="image/x-png">
        <link rel="shortcut icon" href="<spring:url value="/resources/app/img/logo.png"/>" type="image/x-png" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap/css/bootstrap.min.css"/>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/font-awesome/css/font-awesome.min.css"/>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/ionicons/css/ionicons.min.css"/>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/adminlte/css/AdminLTE.css"/>">
        <link rel="stylesheet" href="<spring:url value="/resources/lib/adminlte/css/skins/_all-skins.min.css"/>">
        <link rel="stylesheet" href="<spring:url value="/resources/app/css/adminlte-extensions.css"/>">
        <!-- iCheck -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/icheck/square/blue.css"/>">
        <!-- ionslider -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/ionslider/css/ion.rangeSlider.css"/>">
        <link rel="stylesheet" href="<spring:url value="/resources/lib/ionslider/css/ion.rangeSlider.skinNice.css"/>">
        <!-- Full Calendar -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/fullcalendar/fullcalendar.min.css"/>">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/datepicker/datepicker3.css"/>">
        <!-- DateTime Picker -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>">
        <!-- X-Editable Controls -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap3-editable/css/bootstrap-editable.css"/>">
        <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap3-editable-ext/inputs-ext/person/person.css"/>">
        <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap3-editable-ext/inputs-ext/address/address.css"/>">
        <!-- Bootstrap 3 Dialog -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap3-dialog/css/bootstrap-dialog.css"/>">
        <!-- Address from Kladr -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/address/jquery.kladr.min.css"/>">
        <!-- Material design selectors -->
        <link rel="stylesheet" href="<spring:url value="/resources/app/css/selectors.css"/>">
        <!-- Material design selectors -->
        <link rel="stylesheet" href="<spring:url value="/resources/app/css/typeahead.css"/>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- LEAFLET -->
        <link rel="stylesheet" href="<spring:url value="/resources/lib/leaflet/leaflet-1.0.1.css"/>">
        <link rel="stylesheet" href="<spring:url value="/resources/lib/leaflet/plugins/draw/leaflet.draw.css"/>">
        <link rel="stylesheet" href="<spring:url value="/resources/lib/leaflet/plugins/easy-button/easy-button.css"/>">
        <link rel="stylesheet" href="<spring:url value="/resources/lib/leaflet/plugins/grid/leaflet-grid.css"/>">
        <link rel="stylesheet" href="<spring:url value="/resources/lib/lewrap/lewrap.css"/>">
    </head>
    <body class="skin-yellow-light sidebar-mini">
        <div class="shadow" id="shadow">
            <div class="shadowText" id="shadowText">Загрузка ...</div>
        </div>
        <div class="wrapper">
            <header id="header" class="main-header">
                <a href="/" class="logo">
                    <span class="logo-mini"><b>L</b>A</span>
                    <span class="logo-lg"><b>Лиза</b>Алерт</span>
                </a>
                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <span class="caption" id="desktopHeader">Текущее место</span>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="label label-success" id="desktopUnreadMessagesCount" style="font-size: 1.1em">4</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 4 messages</li>
                                    <li>
                                        <ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                            <li><!-- start message -->
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Support Team
                                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li><!-- end message -->
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        AdminLTE Design Team
                                                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#" id="desktopGoToMessages">Посмотреть все сообщения</a></li>
                                </ul>
                            </li>
                            <li><a href="/logout">Выход</a></li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img id="sidebarPersonImage" src="<spring:url value="resources/lib/adminlte/img/user2-160x160.jpg"/>" class="img-rounded" style="width: 45px; height: 45px;"alt="User Photo"/>
                        </div>
                        <div class="pull-left info">
                            <p id="sidebarPersonFullName">User name</p>
                            <a href="#" id="desktopConnectWidget">
                                <i class="fa fa-circle text-success"></i>
                                <span>Online</span>
                            </a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="treeview" id="sidebarMenuDashboard" data-view="Dashboard" style="display: none">
                            <a href="#">
                                <i class="fa fa-dashboard"></i>
                                <span>Обзор</span>
                            </a>
                        </li>
                        <li class="active treeview" id="sidebarMenuProfile" data-view="Profile">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span>Профиль</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuRequests" data-view="Requests">
                            <a href="#">
                                <i class="fa fa-envelope"></i>
                                <span>Заявки</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuEvents" data-view="Events">
                            <a href="#">
                                <i class="fa fa-calendar-o"></i>
                                <span>События</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuMessages" data-view="Messages">
                            <a href="#">
                                <i class="fa fa-wechat"></i>
                                <span>Сообщения</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuCoordinator" data-view="Coordinator">
                            <a href="#">
                                <i class="fa fa-bullhorn"></i>
                                <span>Координатор</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuSystemSettings" data-view="SystemSettings">
                            <a href="#">
                                <i class="fa fa-gears"></i>
                                <span>Настройки</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuAdministrator" data-view="Administrator" style="display: none">
                            <a href="#">
                                <i class="fa fa-black-tie"></i>
                                <span>Администрирование</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuLogs" data-view="Logs">
                            <a href="#">
                                <i class="fa fa-clone"></i>
                                <span>Журналы</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuFeedback" data-view="Feedback">
                            <a href="#">
                                <i class="fa fa-support"></i>
                                <span>Обратная связь</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuDebug" data-view="Debug">
                            <a href="#">
                                <i class="fa fa-medkit"></i>
                                <span>Отладка</span>
                            </a>
                        </li>
                        <li class="treeview" id="sidebarMenuMediaLibrary" data-view="MediaLibrary">
                            <a href="#">
                                <i class="fa fa-picture-o"></i>
                                <span>Медиафайлы</span>
                            </a>
                        </li>
                    </ul>
                </section>
            </aside>
            <div id="content-wrapper" class="content-wrapper">
            </div>
        </div>

        <div id="modalPlaceholder"></div>
        <div id="mediaLibraryPlaceholder"></div>
        <div id="waitIndicatorPlaceholder"></div>

        <!-- jQuery 2.2.3 -->
        <script src="<spring:url value="/resources/lib/jquery/jquery-2.2.3.min.js"/>"></script>
        <script src="<spring:url value="/resources/lib/jquery/jquery-ui.min.js"/>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<spring:url value="/resources/lib/bootstrap/js/bootstrap.min.js"/>"></script>
        <!-- iCheck -->
        <script src="<spring:url value="/resources/lib/icheck/icheck.min.js"/>"></script>
        <!-- Admin LTE -->
        <script src="<spring:url value="/resources/lib/adminlte/js/app.js"/>"></script>
        <!-- Moment.js -->
        <script src="<spring:url value="/resources/lib/moment/moment-with-locales.min.js"/>"></script>
        <!-- Date Picker -->
        <script src="<spring:url value="/resources/lib/datepicker/bootstrap-datepicker.js"/>"></script>
        <script src="<spring:url value="/resources/lib/datepicker/locales/bootstrap-datepicker.ru.js"/>"></script>
        <!-- DateTime Picker -->
        <script src="<spring:url value="/resources/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"/>"></script>
        <!-- X-Editable Controls -->
        <script src="<spring:url value="/resources/lib/bootstrap3-editable/js/bootstrap-editable.js"/>"></script>
        <script src="<spring:url value="/resources/lib/bootstrap3-editable-ext/inputs-ext/person/person.js"/>"></script>
        <script src="<spring:url value="/resources/lib/bootstrap3-editable-ext/inputs-ext/address/address.js"/>"></script>
        <!-- Bootstrap 3 Dialog -->
        <script src="<spring:url value="/resources/lib/bootstrap3-dialog/js/bootstrap-dialog.js"/>"></script>
        <!-- Address from kladr -->
        <script src="<spring:url value="/resources/lib/address/jquery.kladr.min.js"/>"></script>
        <!-- Typeahead -->
        <script src="<spring:url value="/resources/lib/typeahead/typeahead.bundle.js"/>"></script>
        <!-- PDF Maker -->
        <script src="<spring:url value="/resources/lib/pdfmake/pdfmake.min.js"/>"></script>
        <script src="<spring:url value="/resources/lib/pdfmake/vfs_resources.js"/>"></script>
        <!-- Mate -->
        <script src="<spring:url value="/resources/lib/mate/Mate.js"/>"></script>
        <!-- Application main script -->
        <script src="<spring:url value="/resources/app/js/app/app.js"/>"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
        <script src="<spring:url value="/resources/lib/ionslider/js/ion.rangeSlider.min.js"/>"></script>

        <!-- MAPS API -->
        <!-- ADD THIS FOR WORK WITH KEY  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY" async defer></script> -->
        <script src="https://maps.googleapis.com/maps/api/js" async defer></script>
        <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
        <!-- LEAFLET WITH WRAPPER -->
        <script src="<spring:url value="/resources/lib/leaflet/leaflet-1.0.1.js"/>"></script>
        <script src="<spring:url value="/resources/lib/leaflet/plugins/draw/leaflet.draw.js"/>"></script>
        <script src="<spring:url value="/resources/lib/leaflet/plugins/googleMutant/Leaflet.GoogleMutant.js"/>"></script>
        <script src="<spring:url value="/resources/lib/leaflet/plugins/yandex/Yandex.js"/>"></script>
        <script src="<spring:url value="/resources/lib/leaflet/plugins/easy-button/easy-button.js"/>"></script>
        <script src="<spring:url value="/resources/lib/leaflet/plugins/grid/leaflet-grids.js"/>"></script>
        <script src="<spring:url value="/resources/lib/leaflet/plugins/grid/leaflet-control.js"/>"></script>
        <script src="<spring:url value="/resources/lib/leaflet/plugins/grid/mgrs.js"/>"></script>
        <script src="<spring:url value="/resources/lib/lewrap/lewrap.js"/>"></script>
        <script src="<spring:url value="/resources/lib/turf/turf.min.js"/>"></script>

    </body>
</html>
