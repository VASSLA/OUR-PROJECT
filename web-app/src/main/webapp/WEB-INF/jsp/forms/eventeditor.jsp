<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<div class="modal" id="eventEditorModalForm">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-primary">Редактирование события
                </h4>
            </div>

            <div id="eventContainer" class="modal-body container-fluid">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <!-- Person -->
                        <div class="form-group">
                            <label for="eventMessage" class="text-sm text-black">Название события</label>
                            <input type="text" class="form-control" id="eventMessage" data-pk="1" placeholder="Название события">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10 col-xs-12">
                        <div class="form-group">
                            <label for="eventType" class="text-sm text-black">Тип события</label>
                            <!--<input type="text" readonly="true" class="form-control" id="eventType" data-pk="1" placeholder="">-->
                            <div class="form-group">
                                <select class="form-control" id="eventType">
                                    <!--<option value="1">Обычная</option>-->
                                    <!--<option value="2">Срочная</option>-->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <div class="form-group">
                            <label for="eventColor" class="text-sm text-black">Цвет</label>
                            <input type="color" class="form-control" id="eventColor" data-pk="1" placeholder="">
                        </div>
                    </div>
                    <!--
                    <div class="col-sm-2 col-xs-12">
                        <div class="checkbox icheck" style="padding-top: 20px">
                            <label>
                                <input type="checkbox" name="eventAlert" id="eventAlert"> Напоминание
                            </label>
                        </div>
                    </div>
                    -->
                </div>
                <div class="row">
                    <div class="col-sm-5 col-xs-12">
                        <div class="form-group">
                            <label for="eventStartDate" class="text-sm text-black">Дата начала</label>
                            <div class="input-group date" id="eventStartDate">
                                <input type="text" class="form-control">
                                <span class="input-group-addon" style="background-color: #eee">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-xs-12">
                        <div class="form-group">
                            <label for="eventEndDate" class="text-sm text-black">Дата окончания</label>
                            <div class="input-group date" id="eventEndDate">
                                <input type="text" class="form-control">
                                <span class="input-group-addon" style="background-color: #eee">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <div class="checkbox icheck" style="padding-top: 20px">
                            <label>
                                <input type="checkbox" name="eventWholeDay" id="eventWholeDay"> Весь день
                            </label>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="eventParticipantsGroup" class="text-sm text-black">Участники</label>
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control pull-right" placeholder="Начните вводить имя участника, чтобы добавить его" id="eventParticipantSearchInput">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default" id="eventParticipantSearchButton"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="input-group date col-sm-12 col-xs-12" id="eventParticipantsGroup">
                                <ul class="contacts-list" id="eventParticipantList" style="column-count: 1;">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                

            </div>
            
            <div class="modal-footer">
                <button id="btnCancelEventEdit" type="button" class="btn btn-default pull-left" data-dismiss="modal">Отменить</button>
                <button id="btnRemoveEvent" type="button" class="btn btn-primary">Удалить</button>
                <button id="btnSaveEvent" type="button" class="btn btn-primary">Сохранить</button>
            </div>
            <div id="subModalPlaceholder"></div>
            
        </div>
    </div>
</div>