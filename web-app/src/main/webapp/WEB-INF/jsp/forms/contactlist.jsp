<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div id="contactsBookView" class="box box-primary" style="width:800px; box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.5); border: 1px solid #aaa">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <i class="ion ion-clipboard"></i>
        <h3 class="box-title">Список контактов</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label for="contactListContactType" class="text-sm text-black">Тип контакта</label>
                    <div class="input-group" style="width: 100%">
                        <select type="text" class="form-control" id="contactListContactType"></select>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="contactListContact" class="text-sm text-black">Контакт</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="contactListContact"/>
                        <span class="input-group-addon" style="background-color: #eee" id="contactListAdd">
                            <span id="contactBookSave" class="glyphicon glyphicon-plus"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <ul class="todo-list ui-sortable" id="contactListContacts">

        </ul>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix no-border">
        <button class="btn btn-default pull-right" id="contactListClose"><i class="fa fa-close"></i> Закрыть</button>
    </div>
</div>