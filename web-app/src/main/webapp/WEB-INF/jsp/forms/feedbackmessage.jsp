<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<div class="modal" id="feedbackmessageNewForm">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-primary" id="feedbackFormTitle">Новое сообщение в техподдержку</h4>
            </div>
            <div class="modal-body">
               <div class="row">                 
                   <div class="col-sm-12 col-xs-12">
                        <label for="feedbackmessageType" class="text-sm text-black">Тип сообщения</label>
                        <div class="form-group">
                            <select class="form-control" id="feedbackmessageType">
                                <option value="1">Проблема</option>
                                <option value="2">Критическая ошибка</option>
                                <option value="3">Запрос на доработку</option>
                                <option value="4">Другое</option>
                            </select>
                        </div>
                   </div>
               </div>
               <div class="row">
                   <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="feedbackmessageText" class="text-sm text-black">Текст сообщения</label>
                            <textarea class="form-control" rows="6" id="feedbackmessageText" placeholder=""></textarea>
                        </div>
                    </div>
               </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <label for="feedbackmessagefile" class="text-sm text-black" id="feedbackmessagefilelabel">Прикрепить файлы</label>
                        <div class="form-group">
                            <input id="btnFeedbackMessageFile" type="file" multiple class="file-loading" data-show-caption="true" data-show-preview="false">
                        </div>
                    </div>
               </div>
            </div>
            <div class="modal-footer">
                <button id="btnCancelFeedback" type="button" class="btn btn-default pull-left" data-dismiss="modal">Отменить</button>
                <button id="btnSendFeedback" type="button" class="btn btn-primary">Отправить</button>
            </div>
            <div id="subModalPlaceholder"></div>
        </div>
    </div>
</div>
