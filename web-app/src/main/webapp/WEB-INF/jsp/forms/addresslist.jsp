<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div id="addressBookView" class="box box-default" style="width:800px; box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.5); border: 1px solid #aaa">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <i class="glyphicon glyphicon-home"></i>
        <h3 class="box-title">Адреса</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group">
            <label for="requestAddress" class="text-sm text-black">Укажите адрес для добавления</label>
            <div class="input-group">
                <input type="text" class="form-control" id="addressListAddress" data-type="address" data-pk="1" placeholder=""/>
                <span class="input-group-addon" style="background-color: #eee" id="addressListAdd">
                    <span id="addressBookSave" class="glyphicon glyphicon-plus"></span>
                </span>
            </div>
        </div>
        <ul class="todo-list ui-sortable" id="addressListAddresses">

        </ul>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix no-border">
        <button class="btn btn-default pull-right" id="addressListClose"><i class="fa fa-close"></i> Закрыть</button>
    </div>
</div>