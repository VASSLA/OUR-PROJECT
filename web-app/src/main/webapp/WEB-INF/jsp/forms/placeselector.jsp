<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="box box-solid"  id="requestPlaceSelector" style="width: 600px; box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.5); border: 1px solid #aaa"">
    <div class="box-header">
        <i class="fa fa-map-marker"></i>
        <h3 class="box-title">
            Уточните место
        </h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label for="clarifiedPlace" class="text-sm text-black">Вы выбрали</label>
            <input type="text" class="form-control" id="clarifiedPlace"/>
        </div>
    </div>
    <div class="box-body">
        <div id="yandexMap" style="width: 100%; height: 500px">
        </div>
    </div><!-- /.box-body-->
    <div class="box-footer no-border bg-gray-light">
        <div class="btn-group">
            <button class="btn btn-default" id="requestPlaceSelectorOk">Отмена</button>
        </div>
        <div class="btn-group pull-right">
            <button class="btn btn-default" id="requestPlaceSelectorCancel">Готово</button>
        </div>
    </div>
</div>