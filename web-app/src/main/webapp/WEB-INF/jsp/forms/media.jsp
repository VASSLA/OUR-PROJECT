<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<div class="modal" id="mediaLibraryModalForm">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-primary">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Media Library

                    <span class="pull-left">
                        <div class="btn-group">
                            <button id="btnCloseMediaModalWindow" class="btn btn-default" data-toggle="tooltip" title="Закрыть"  data-dismiss="modal"><i class="fa fa-remove"></i></button>
                        </div>
                    </span>
                    <span class="pull-right">
                        <label id="lblBrowseFile"/>
                        <div class="btn-group">
                            <label class="btn btn-default btn-file">
                                Выбрать файл <input type="file" id="btnBrowseFile" style="display: none;">
                            </label>
                            <label class="btn btn-default">
                                Загрузить <input type="button" id="btnUploadFile" style="display: none;">
                            </label>

                        </div>
                    </span>

                </h4>
            </div>

            <div id="uploadsContainer" class="modal-body container-fluid">
                <!-- <div id="uploadsContainer" class="modal-body container"> -->

            </div>
        </div>
    </div>
</div>