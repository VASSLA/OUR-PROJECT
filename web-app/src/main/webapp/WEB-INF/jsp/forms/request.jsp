<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="modal" id="requestNewForm">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="requestFormHeader" class="modal-title text-primary">Новая заявка на розыск</h4>
            </div>
            <div class="modal-body">
                <div class="nav-tabs-custom">
                    <!-- TTab headers -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#requestGeneralInfo" id="requestGeneralInfoTab" data-toggle="tab">Основная информация</a></li>
                        <li><a href="#requestMoreInfo" id="requestExtraInfoTab" data-toggle="tab">Дополнительная информация</a></li>
                    </ul>
                    <!-- Tabs contents -->
                    <div class="tab-content">
                        <!-- General Info ab Content -->
                        <div class="active tab-pane" id="requestGeneralInfo">
                            <!-- Date and Time of request -->
                            <div class="row">
                                <div class="col-sm-4 col-xs-12 span4">
                                    <label for="requestPhoto" class="text-sm text-black">Фото</label>
                                    <div style="width: 256px; height: 256px; border:1px solid rgb(210, 214, 222); cursor: pointer;">
                                        <img id="requestPhoto" src="<spring:url value="/resources/app/img/default-user-avatar.png"/>" style="width: 254px; height: 254px; cursor: pointer;"/>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="requestDateTime" class="text-sm text-black">Дата заявки</label>
                                        <div class="input-group date" id="requestDateTime">
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon" style="background-color: #eee; cursor: pointer;">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label for="requestUrgency" class="text-sm text-black">Тип заявки</label>
                                    <div class="form-group">
                                        <select class="form-control" id="requestUrgency">
                                            <option value="1">Обычная</option>
                                            <option value="2">Срочная</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <!-- Person -->
                                    <div class="form-group">
                                        <label for="requestPerson" class="text-sm text-black">Основные данные разыскиваемого</label>
                                        <input type="text" class="form-control" id="requestPerson" data-type="person" data-pk="1" placeholder="">
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <!-- Since -->
                                    <div class="form-group">
                                        <label for="requestMissingFrom" class="text-sm">Дата пропажи</label>
                                        <div class="input-group date" id="requestMissingFrom">
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon" style="background-color: #eee; cursor: pointer;">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-xs-12">
                                    <!-- Last Seen Place -->
                                    <div class="form-group">
                                        <label for="requestLastSeenPlace" class="text-sm">Где последний раз видели</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="requestLastSeenPlace" placeholder=""/>
                                            <span class="input-group-addon" style="background-color: #eee; cursor: pointer;" id="requestLastSeenPlaceSelect">
                                                <span class="glyphicon glyphicon-map-marker"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label for="requestLandscape" class="text-sm text-black">Местность</label>
                                    <div class="form-group">
                                        <select class="form-control" id="requestLandscape">
                                            <option value="1">Город</option>
                                            <option value="2">Лес</option>
                                            <option value="3">Город + Лес</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="checkbox icheck" style="padding-top: 20px">
                                        <label>
                                            <input type="checkbox" name="requestForestOnline" id="requestForestOnline"> Лес на связи
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8 col-xs-12">
                                    <div class="form-group">
                                        <label for="requestAddress" class="text-sm text-black">Адрес пропавшего</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="requestAddress" data-type="address" data-pk="1" placeholder=""/>
                                            <span class="input-group-addon" style="background-color: #eee; cursor: pointer;" id="requestAddressBook">
                                                <span class="glyphicon glyphicon-book"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label for="requestPhone" class="text-sm text-black">Телефон пропавшего</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="requestPhone" placeholder=""/>
                                        <span class="input-group-addon" style="background-color: #eee; cursor: pointer;" id="requestContactBook">
                                            <span class="glyphicon glyphicon-book"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of General Info ab Content -->
                        <!-- Extra Info ab Content -->
                        <div class="tab-pane" id="requestMoreInfo">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <!-- Circumstances of disappearing -->
                                    <div class="form-group">
                                        <label for="requestCircOfDisapp" class="text-sm text-black">Обстоятельства пропажи</label>
                                        <textarea class="form-control" rows="2" id="requestCircOfDisapp" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <!-- Health -->
                                    <div class="form-group">
                                        <label for="requestHealth" class="text-sm text-black">Здоровье</label>
                                        <textarea class="form-control" rows="2" id="requestHealth" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <!-- Appearance -->
                                    <div class="form-group">
                                        <label for="requestAppearance" class="text-sm text-black">Описание внешности</label>
                                        <textarea class="form-control" rows="2" id="requestAppearance" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <!-- Weared in -->
                                    <div class="form-group">
                                        <label for="requestDressedIn" class="text-sm text-black">Одежда</label>
                                        <textarea class="form-control" rows="2" id="requestDressedIn" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <!-- Stuff -->
                                    <div class="form-group">
                                        <label for="requestStuff" class="text-sm text-black">С собой имелось</label>
                                        <textarea class="form-control" rows="2" id="requestStuff" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <!-- Extra Info -->
                                    <div class="form-group">
                                        <label for="requestExtraInfo" class="text-sm text-black">Дополнительная информация</label>
                                        <textarea class="form-control" rows="2" id="requestExtraInfo" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of Extra Info ab Content -->
                    </div>
                    <!-- End of Tabs contents -->
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnCancelRequest" type="button" class="btn btn-default pull-left" data-dismiss="modal">Отменить</button>
                <button id="btnSendRequest" type="button" class="btn btn-primary">Отправить</button>
            </div>
            <div id="subModalPlaceholder"></div>
        </div>
    </div>
</div>