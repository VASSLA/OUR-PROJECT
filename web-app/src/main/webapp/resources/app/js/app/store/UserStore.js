/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: UserStore.js
 * Date: Jan 10, 2017 10:09:01 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.UserStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.UserModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/user'
    })
});
