/* global Mate */
Mate.define('Application.controller.LogsController', {
    extends: 'Mate.app.Controller',
    levelList: null,
    levelMap: null,
    page: 0,
    pageLimit: 30 * 10,
    interactives: {
    },
    afterInit: function () {
        var me = this;
        
        Mate.Selector('#desktopHeader').html('Журналы системы');
        Mate.Selector("#logTable").on('scroll', me.loadNextPageLogs);
        
        this.loadLogLevels();
        this.loadLogs();

//        this.applyDataToForm();
    },
    
    clearForm: function() {
        Mate.Selector("#logTable").find("tr").remove();
    },
    
    loadLogLevels: function() {
        this.levelList = Mate.Application.getStoreByName('LogLevelStore').getData();
        this.levelMap = {};
        for (var i in this.levelList) {
            var pieceOfData = this.levelList[i];
            this.levelMap[pieceOfData.getFieldByName("level").getValue()] = pieceOfData.getFieldByName("localizedName").getValue();
        }
    },
    
    loadLogs: function() {
        var logStore = Mate.Application.getStoreByName('LogStore');
        var me = this;
        
        logStore.setFilters({
            'limit': me.pageLimit,
            'start': me.pageLimit * me.page
        });
        
        logStore.onDataLoaded = function () {
            this.onDataLoaded = null;
            me.applyDataToForm();
        };
        logStore.load();
    },
    
    loadNextPageLogs: function() {
        var me = Mate.Application.getControllerByName('LogsController');
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            me.page++;
            me.loadLogs();
        } 
    },
    
    appendElementToForm: function(log) {
        var level = log.getFieldByName("logMessageType").getValue();
        var style = "label-success";
        switch(level) {
            case 900:
                style = "label-warning";
                break;
            case 1000:
                style = "level-danger";
                break;
            default:
                style = "label-success";
                break;
        }

        var user = log.getFieldByName("user").getValue();
        var date = new Date(log.getFieldByName("logDate").getValue());

        var dateStr = ("0" + date.getDate()).slice(-2) + "." + ("0" + (date.getMonth() + 1)).slice(-2) + "." + date.getFullYear() + " " +
                ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2) + ":" + ("0" + date.getSeconds()).slice(-2);
//        Mate.Logger.trace(dateStr);

        Mate.Selector("#logTable tbody").append(
            "<tr>" +
                "<td>" + log.getFieldByName("id").getValue() + "</td>" +
                "<td>" + user.getFieldByName("login").getValue() + "</td>" +
                "<td>" + log.getFieldByName("callingClass").getValue() + "</td>" +
                "<td>" + dateStr + "</td>" +
                "<td><span class='label " + style + "'>" + this.levelMap[level] + "</span></td>" +
                "<td>" + log.getFieldByName("message").getValue() + "</td>" +
            "</tr>");
    },
    applyDataToForm: function() {
        var logStore = Mate.Application.getStoreByName('LogStore');
        var logList = logStore.getData();
        
        for (var i in logList) {
            var log = logList[i];
            this.appendElementToForm(log);
        }
    }

});

