/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestSourceModel.js
 * Date: Oct 31, 2016 9:17:25 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.RequestSourceModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'name',
            caption: 'Название',
            description: 'Название источника заявки',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'description',
            caption: 'Описание',
            description: 'Описание источника заявки',
            type: 'string',
            size: 255,
            validator: 'string.string'
        }
    ],
    keyField: 'id'
});
