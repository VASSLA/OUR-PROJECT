/* global Mate */
Mate.define('Application.model.EventModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        /* 
        {
            name: 'user',
            caption: 'Пользователь',
            description: 'Пользователь',
            type: 'model',
            mapAs: 'Application.model.UserModel',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'type',
            caption: 'Тип',
            description: 'Тип',
            mapAs: 'Application.model.LogLevelModel',
            size: 0,
            validator: 'object.safe'
        },
        */
        {
            name: 'userId',
            caption: 'Пользователь',
            description: 'Пользователь',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'typeId',
            caption: 'Тип',
            description: 'Тип',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'message',
            caption: 'Сообщение',
            description: 'Сообщение',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'dateStart',
            caption: 'Дата начала',
            description: 'Дата начала',
            size: 0,
            type: 'datetime'
        },
        {
            name: 'dateEnd',
            caption: 'Дата окончания',
            description: 'Дата окончания',
            size: 0,
            type: 'datetime'
        },
        {
            name: 'wholeDay',
            caption: 'Весь день',
            description: 'Весь день',
            type: 'boolean',
            size: 0,
            validator: 'boolean.boolean'
        },
        {
            name: 'color',
            caption: 'Цвет',
            description: 'Цвет',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'participants',
            caption: 'Участники',
            description: 'Список участников',
            type: 'list',
            size: 0,
            validator: 'string.participants'
        }
    ],
    keyField: 'id'
});
