/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestStateModel.js
 * Date: Oct 31, 2016 9:05:53 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.RequestStateModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'state',
            caption: 'Состояние',
            description: 'Состояние заявки на поиск',
            type: 'string',
            size: 255,
            validator: 'string.string'
        }
    ],
    keyField: 'id'
});
