/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: FeedbackStore.js
 * Date: Jan 20, 2017 5:42:30 PM
 * Author: tel-f */

/* global Mate */
Mate.define('Application.store.FeedbackMessageStore', {
    autoLoad: false,
    extends: 'Mate.data.Store',
    model: 'Application.model.FeedbackMessageModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/feedback'
    })
});

