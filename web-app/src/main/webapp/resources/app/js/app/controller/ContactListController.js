/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ContactListController.js
 * Date: Feb 11, 2017 12:30:02 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.ContactListController', {
    extends: 'Mate.app.Controller',
    contacts: null,
    currentlyEditContactId: null,
    interactives: {
        '#contactListClose': {'click': 'onContactListCloseClick'},
        '#contactListAdd': {'click': 'onContactListAddClick'}
    },
    methods: {
        afterInit: function () {
            this.displayContacts();
            this.prepareComponents();
        },
        beforeDeinit: function () {
            Mate.Selector('div[class=tools]>i[class~=fa-home]').off('click', Mate.Selector.proxy(this.onSetAsDefaultClick, this));
            Mate.Selector('div[class=tools]>i[class~=fa-edit]').off('click', Mate.Selector.proxy(this.onEditClick, this));
            Mate.Selector('div[class=tools]>i[class~=fa-trash-o]').off('click', Mate.Selector.proxy(this.onDeleteClick, this));
        },
        displayContacts: function () {
            Mate.Selector('#contactListContacts').empty();
            var contactTemplate = Mate.Application.getResourceContent('blockContact');
            for (var position in this.contacts) {
                var html = contactTemplate.replace(/{id}/g, this.contacts[position].id);
                var html = html.replace(/{contact-type}/g, this.contacts[position].contactType.name);
                var html = html.replace(/{contact}/g, this.contacts[position].contact);
                var html = html.replace(/{hidden}/g, this.contacts[position].defaultContact ? '' : 'hidden');
                Mate.Selector('#contactListContacts').append(html);
            }
            Mate.Selector('div[class=tools]>i[class~=fa-home]').on('click', this, Mate.Selector.proxy(this.onSetAsDefaultClick, this));
            Mate.Selector('div[class=tools]>i[class~=fa-edit]').on('click', this, Mate.Selector.proxy(this.onEditClick, this));
            Mate.Selector('div[class=tools]>i[class~=fa-trash-o]').on('click', this, Mate.Selector.proxy(this.onDeleteClick, this));
        },
        prepareComponents: function () {
            this.fillContactTypes();
        },
        fillContactTypes: function () {
            var contactTypes = Mate.Application.getStoreByName('ContactTypeStore').getData();
            for (var position in contactTypes) {
                var option = '<option value="' + contactTypes[position].get('id') + '">' + contactTypes[position].get('name') + '</option>';
                Mate.Selector('#contactListContactType').append(option);
            }
        },
        clearContactInputField: function () {
            Mate.Selector('#contactListContact').val('');
            Mate.Selector('#contactListContactType').val('1');
            this.currentAddressUniqueId = null;
        },
        onContactListCloseClick: function () {
            Mate.Selector('#contactsBookView').remove();
            Mate.Application.getControllerByName('RequestFormController').currentRequestStore.getData(0).set('missngPerson.contacts', this.contacts);
            this.deinit();
        },
        onContactListAddClick: function () {
            var contactTypesStore = Mate.Application.getStoreByName('ContactTypeStore');
            Mate.Selector('#contactBookSave').removeClass('glyphicon-ok');
            Mate.Selector('#contactBookSave').addClass('glyphicon-plus');
            for (var position in this.contacts) {
                if (this.contacts[position].id === this.currentlyEditContactId) {
                    this.contacts.splice(position, 1);
                    break;
                }
            }
            var contactTemplate = Mate.Application.getResourceContent('blockContact');
            var html = contactTemplate.replace(/{id}/g, '');
            var html = html.replace(/{contact-type}/g, Mate.Selector('#contactListContactType option:selected').text());
            var html = html.replace(/{contact}/g, Mate.Selector('#contactListContact').val());
            var html = html.replace(/{hidden}/g, 'hidden');
            Mate.Selector('#contactListContacts').append(html);
            Mate.ux.centerBlock('#contactBookView', window, 99999);
            this.contacts.push({
                contactType: contactTypesStore.lookup('id', Number.parseInt(Mate.Selector('#contactListContactType').val())).getJson(),
                contact: Mate.Selector('#contactListContact').val(),
                typeId: Number.parseInt(Mate.Selector('#contactListContactType').val()),
                defaultContact: 0
            });
            this.clearContactInputField();
            Mate.Logger.debug(this.contacts);
        },
        showDefaultLabel: function (uniqueId) {
            Mate.Selector('#contactListContacts').find('li').each(function (index, element) {
                var elementUniqueId = Mate.Selector(element).attr('data-contact-unique-id');
                if (elementUniqueId === uniqueId) {
                    Mate.Selector(element).find('small').removeClass('hidden');
                } else {
                    Mate.Selector(element).find('small').addClass('hidden');
                }
            });
        },
        onSetAsDefaultClick: function (eventObject) {
            var defaultContactId = Mate.Selector(eventObject.target).closest('li').attr('data-contact-unique-id');
            for (var position in this.contacts) {
                this.contacts[position].defaultContact = 0;
                if (this.contacts[position].id === parseInt(defaultContactId)) {
                    this.contacts[position].defaultContact = 1;
                    Mate.Selector('#requestPhone').val(this.contacts[position].contact);
                }
            }
            this.showDefaultLabel(defaultContactId);
        },
        onEditClick: function (eventObject) {
            var id = Mate.Selector(eventObject.target).closest('li').attr('data-contact-unique-id');
            for (var position in this.contacts) {
                if (this.contacts[position].id === parseInt(id)) {
                    Mate.Selector('#contactListContact').val(this.contacts[position].contact);
                    Mate.Selector('#contactListContactType').val(this.contacts[position].contactType.id);
                    Mate.Selector('#contactBookSave').removeClass('glyphicon-plus');
                    Mate.Selector('#contactBookSave').addClass('glyphicon-ok');
                    this.currentlyEditContactId = parseInt(id);
                    break;
                }
            }
        },
        onDeleteClick: function (eventObject) {
            var id = Mate.Selector(eventObject.target).closest('li').attr('data-contact-unique-id');
            for (var position in this.contacts) {
                if (this.contacts[position].id === parseInt(id)) {
                    this.contacts.splice(position, 1);
                    Mate.Selector(eventObject.target).closest('li').remove();
                    break;
                }
            }
        }
    }
});
