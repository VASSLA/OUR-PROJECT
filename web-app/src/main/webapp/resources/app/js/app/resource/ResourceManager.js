/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ResourceManager.js
 * Date: Jan 20, 2017 6:54:14 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.resource.ResourceManager', {
    extends: 'Mate.app.ResourceManager',
    resources: {
        'viewProfile': { url: 'profile' },
        'viewRequests': { url: 'requests' },
        'viewEvents': { url: 'events' },
        'viewDashboard': { url: 'dashboard' },
        'viewMessages': { url: 'messages' },
        'viewSystemSettings': { url: 'systemsettings' },
        'viewCoordinator': { url: 'coordinator' },
        'viewAdministrator': { url: 'administrator' },
        'viewLogs': { url: 'logs' },
        'viewFeedback': { url: 'feedback' },
        'viewRequestCard': { url: 'requestcard' },
        'viewDebug': { url: 'debug' },
        'viewMediaLibrary': { url: 'medialibrary' },
        'formAddressList': { url: 'form/addresslist' },
        'formContactList': { url: 'form/contactlist' },
        'formRequest': { url: 'form/request' },
        'formMedia': { url: 'form/media' },
        'formEventEditor': { url: 'form/eventeditor' },
        'formPlaceSelector': { url: 'form/placeselector' },
        'blockCoordinatorRequestCard': { url: 'block/coordinator-requestcard' },
        'blockRequestCard': { url: 'block/requestcard' },
        'blockSkill': { url: 'block/skill' },
        'blockBuddy': { url: 'block/buddy' },
        'blockMessage': { url: 'block/message' },
        'blockMessageTab': { url: 'block/message-tab' },
        'blockMessageContainer': { url: 'block/message-container' },
        'blockImageContainer': { url: 'block/image-container' },
        'blockWait': { url: 'block/wait' },
        'blockAddress': { url: 'block/address' },
        'blockContact': { url: 'block/contact' },
        'blockEventParticipant': { url: 'block/event-participant' },
        'blockEventType': { url: 'block/event-type' },
        'reportRequest': { url: 'report/request'},
        'reportRequestJs': {url: '/resources/app/js/app/report/request.js'}
    }
});
