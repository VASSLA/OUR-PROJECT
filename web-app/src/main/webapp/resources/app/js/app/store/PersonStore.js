/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: PersonStore.js
 * Date: Oct 31, 2016 7:58:56 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.PersonStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.PersonModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/person'
    })
});
