/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: SystemSettingsController.js
 * Date: Sep 20, 2016 10:11:36 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.SystemSettingsController', {
    extends: 'Mate.app.Controller',
    afterInit: function() {
        Mate.Selector('#desktopHeader').html('Настройки системы');
    }
});

