/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: DesktopController.js
 * Date: Sep 7, 2016 10:25:42 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.DesktopController', {
    extends: 'Mate.app.Controller',
    interactives: {
        '#sidebarMenuDashboard': {'click': 'loadViewPort'},
        '#sidebarMenuProfile': {'click': 'loadViewPort'},
        '#sidebarMenuRequests': {'click': 'loadViewPort'},
        '#sidebarMenuEvents': {'click': 'loadViewPort'},
        '#sidebarMenuMessages': {'click': 'loadViewPort'},
        '#sidebarMenuCoordinator': {'click': 'loadViewPort'},
        '#sidebarMenuSystemSettings': {'click': 'loadViewPort'},
        '#sidebarMenuAdministrator': {'click': 'loadViewPort'},
        '#sidebarMenuLogs': {'click': 'loadViewPort'},
        '#sidebarMenuFeedback': {'click': 'loadViewPort'},
        '#sidebarMenuDebug': {'click': 'loadViewPort'},
        '#sidebarMenuMediaLibrary': {'click': 'loadViewPort'},
        '#desktopGoToMessages': {'click': 'onDesktopGoToMessagesClick'}
    },
    viewPrefix: 'view',
    onlineBulb: null,
    onlineText: null,
    cyclesCounter: 0,
    timeoutHandler: null,
    afterInit: function () {
        Mate.Selector('#desktopHeader').html('Рабочий стол');
        this.onlineBulb = Mate.Selector('#desktopConnectWidget>i');
        this.onlineText = Mate.Selector('#desktopConnectWidget>span');
        this.loadCurrentUser();
        this.checkOnline();
        this.checkNewMessages();
    },
    beforeDeinit: function() {
        clearTimeout(this.timeoutHandler);
    },
    setOnlineStatus: function(isOnline) {
        if (isOnline) {
            this.onlineBulb.removeClass('text-red');
            this.onlineBulb.addClass('text-success');
            this.onlineText.html('Сервер онлайн [' + this.cyclesCounter + ']');
        } else {
            this.onlineBulb.removeClass('text-success');
            this.onlineBulb.addClass('text-red');
            this.onlineText.html('Сервер оффлайн');
        }
    },
    checkOnline: function (context) {
        context = context || this;
        Mate.Selector.ajax({
            url: '/api/v1/connectivity',
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(context.cyclesCounter),
            context: context,
            success: function(data, textStatus, request) {
                Mate.Logger.debug(request.status);
                if (request.status === 200) {
                    context.cyclesCounter = data;
                    context.setOnlineStatus(true);
                    context.timeoutHandler = setTimeout(context.checkOnline, 60000, this);
                } else {
                    location.replace('/login');
                }
            },
            error: function(data) {
                if (data.status === 403) {
                    location.replace('/login');
                    Mate.Logger.debug('Session is invalid, reditecting to login page');
                } else {
                    context.setOnlineStatus(true);
                    context.timeoutHandler = setTimeout(context.checkOnline, 60000, this);
                    Mate.Logger.debug('Server is offline');
                }
            }
        });
    },
    checkNewMessages: function (context) {
        context = context || this;
        var filters = {
            receiverId: Mate.Application.currentUser.get('id'),
            read: 0,
            notMarkAsRead: 1
        }
        Mate.Selector.ajax({
            url: '/api/v1/message',
            method: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: filters,
            context: context,
            success: function(data) {
                Mate.Logger.trace(data.length + ' new messages are awaiting on the server');
                if (data.length > 0) {
                    Mate.Selector('#desktopUnreadMessagesCount').show();
                    Mate.Selector('#desktopUnreadMessagesCount').html(data.length);
                } else {
                    Mate.Selector('#desktopUnreadMessagesCount').hide();
                }
                context.timeoutHandler = setTimeout(context.checkNewMessages, 10000, this);
            },
            error: function() {
                Mate.Logger.error('Failed to retrieve new messages');
                context.timeoutHandler = setTimeout(context.checkNewMessages, 10000, this);
            }
        });
    },
    loadCurrentUser: function () {
        var currentUser = Mate.Application.currentUser;
        var fullName = currentUser.get('person.lastName') + ' ' + currentUser.get('person.firstName');
        Mate.Selector('#sidebarPersonFullName').html(fullName);
        Mate.Selector('#sidebarPersonImage').attr('src', '/api/v1/getthumbnail/' + currentUser.get('person.pictureId'));
    },
    highlightCurrentMenuItem: function (menuItem) {
        if (!(menuItem instanceof Object)) {
            menuItem = Mate.Selector('li[data-view='+ menuItem + ']');
        }
        Mate.Selector(menuItem).closest('li').siblings().removeClass('active');
        Mate.Selector(menuItem).closest('li').addClass('active');
    },
    loadViewPort: function (eventObject) {
        var target = eventObject.target;
        var requiredState = Mate.Selector(target).closest('li').attr('data-view');
        var view = this.viewPrefix + requiredState;
        Mate.Selector("#content-wrapper").html(Mate.Application.getResourceContent(view));
        Mate.Application.getStateManager().setState(requiredState);
        this.highlightCurrentMenuItem(target);
    },
    onDesktopGoToMessagesClick: function() {
        Mate.Selector("#content-wrapper").html(Mate.Application.getResourceContent('viewMessages'));
        Mate.Application.setState('Messages');
    }
});