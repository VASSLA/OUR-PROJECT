/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ContactTypeStore.js
 * Date: Nov 17, 2016 9:36:05 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.ContactTypeStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.ContactTypeModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/contacttype'
    })
});
