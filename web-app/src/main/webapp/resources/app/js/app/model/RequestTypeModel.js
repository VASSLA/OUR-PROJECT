/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestTypeModel.js
 * Date: Oct 28, 2016 1:38:47 PM
 * Author: anikeale */

/* global Mate */
Mate.define('Application.model.RequestTypeModel', {
    extends: 'Mate.data.Model',
    fields: [
        {name: 'id', caption: 'Идентификатор'},
        {name: 'name', caption:' Описание'}
    ],
    keyField: 'id'
});
