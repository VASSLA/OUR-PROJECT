/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: AddressListController.js
 * Date: Jan 24, 2017 9:21:32 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.AddressListController', {
    extends: 'Mate.app.Controller',
    addresses: [],
    enteredAddress: null,
    currentAddressUniqueId: null,
    interactives: {
        '#addressListClose': {'click': 'onAddressListCloseClick'},
        '#addressListAdd': {'click': 'onAddressListAddClick'}
    },
    afterInit: function() {
        this.displayAddresses();
        this.prepareComponents();
    },
    beforeDeinit: function() {
        Mate.Selector('div[class=tools]>i[class~=fa-home]').off('click', Mate.Selector.proxy(this.onSetAsDefaultClick, this));
        Mate.Selector('div[class=tools]>i[class~=fa-edit]').off('click', Mate.Selector.proxy(this.onEditClick, this));
        Mate.Selector('div[class=tools]>i[class~=fa-trash-o]').off('click', Mate.Selector.proxy(this.onDeleteClick, this));
    },
    displayAddresses: function() {
        Mate.Selector('#addressListAddresses').empty();
        var addressTemplate = Mate.Application.getResourceContent('blockAddress');
        for (var position in this.addresses) {
            var html = addressTemplate.replace(/{address}/g, this.addresses[position].address);
            var html = html.replace(/{uniqueId}/g, this.addresses[position].uniqueId);
            var html = html.replace(/{hidden}/g, this.addresses[position].defaultAddress ? '' : 'hidden');
            Mate.Selector('#addressListAddresses').append(html);
        }
        Mate.Selector('div[class=tools]>i[class~=fa-home]').on('click', this, Mate.Selector.proxy(this.onSetAsDefaultClick, this));
        Mate.Selector('div[class=tools]>i[class~=fa-edit]').on('click', this, Mate.Selector.proxy(this.onEditClick, this));
        Mate.Selector('div[class=tools]>i[class~=fa-trash-o]').on('click', this, Mate.Selector.proxy(this.onDeleteClick, this));
    },
    prepareComponents: function() {
        Mate.Selector('#addressListAddress').editable({
            title: 'Адрес',
            placement: 'bottom',
            highlight: false,
            unsavedclass: null,
            value: null,
            success: Mate.Selector.proxy(this.onNewAddressEntered, this)
        });
    },
    clearAddressInputField: function() {
            Mate.Selector('#addressListAddress').val('');
            Mate.Selector('#addressListAddress').editable('setValue', null);
            this.currentAddressUniqueId = null;
    },
    onAddressListCloseClick: function() {
        Mate.Selector('#addressBookView').remove();
        Mate.Application.getControllerByName('RequestFormController').currentRequestStore.getData(0).set('missngPerson.addresses', this.addresses);
        this.deinit();
    },
    onNewAddressEntered: function(response, value) {
        this.enteredAddress = value;
    },
    onAddressListAddClick: function() {
        // If it was edit mode for existing address
        Mate.Selector('#addressBookSave').removeClass('glyphicon-ok');
        Mate.Selector('#addressBookSave').addClass('glyphicon-plus');
        // If address wasn't entered then clear all fields and exit
        if (!this.enteredAddress || this.currentAddressUniqueId === this.enteredAddress.uniqueId) {
            this.clearAddressInputField();
            return;
        }
        for (var position in this.addresses) {
            if (this.addresses[position].uniqueId === this.currentAddressUniqueId) {
                this.addresses[position] = this.enteredAddress;
                Mate.Selector('li[data-address-unique-id=' + this.currentAddressUniqueId + ']').remove();
                break;
            }
        }
        var addressTemplate = Mate.Application.getResourceContent('blockAddress');
        var html = addressTemplate.replace(/{address}/g, Mate.Selector('#addressListAddress').val());
        var html = html.replace(/{uniqueId}/g, this.enteredAddress.uniqueId);
        var html = html.replace(/{hidden}/g, 'hidden');
        Mate.Selector('#addressListAddresses').append(html);
        Mate.ux.centerBlock('#addressBookView', window, 99999);
        this.clearAddressInputField();
        this.addresses.push(this.enteredAddress);
    },
    showDefaultLabel: function(uniqueId) {
        Mate.Selector('#addressListAddresses').find('li').each(function(index, element) {
            var elementUniqueId = Mate.Selector(element).attr('data-address-unique-id');
            if (elementUniqueId === uniqueId) {
                Mate.Selector(element).find('small').removeClass('hidden');
            } else {
                Mate.Selector(element).find('small').addClass('hidden');
            }
        });
    },
    onSetAsDefaultClick: function(eventObject) {
        var defaultAddressUniqueId = Mate.Selector(eventObject.target).closest('li').attr('data-address-unique-id');
        for (var position in this.addresses) {
            this.addresses[position].defaultAddress = 0;
            if (this.addresses[position].uniqueId === defaultAddressUniqueId) {
                this.addresses[position].defaultAddress = 1;
                Mate.Selector('#requestAddress').editable('setValue', this.addresses[position]);
            }
        }
        this.showDefaultLabel(defaultAddressUniqueId);
    },
    onEditClick: function(eventObject) {
        var uniqueId = Mate.Selector(eventObject.target).closest('li').attr('data-address-unique-id');
        for (var position in this.addresses) {
            if (this.addresses[position].uniqueId === uniqueId) {
                Mate.Selector('#addressListAddress').editable('setValue', this.addresses[position]);
                this.currentAddressUniqueId = uniqueId;
                Mate.Selector('#addressBookSave').removeClass('glyphicon-plus');
                Mate.Selector('#addressBookSave').addClass('glyphicon-ok');
                break;
            }
        }
    },
    onDeleteClick: function(eventObject) {
        var uniqueId = Mate.Selector(eventObject.target).closest('li').attr('data-address-unique-id');
        for (var position in this.addresses) {
            if (this.addresses[position].uniqueId === uniqueId) {
                if (this.addresses[position].defaultAddress) {
                    alert('Нельзя удалить адрес по умолчанию');
                } else {
                    Mate.Selector(eventObject.target).closest('li').remove();
                    this.addresses.splice(position, 1);
                }
                break;
            }
        }
    }
});
