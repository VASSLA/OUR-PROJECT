/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: DashboardController.js
 * Date: Sep 8, 2016 10:15:27 PM
 * Author: anikeale */

/* global Mate */
Mate.define('Application.controller.DashboardController', {
    extends: 'Mate.app.Controller',
    afterInit: function() {
        Mate.Selector('#desktopHeader').html('Моя личная панель');
    }
});
