/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: DebugController.js
 * Date: Nov 11, 2016 7:21:34 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.DebugController', {
    extends: 'Mate.app.Controller',
    interactives: {
        '#id1': {'click': 'onBtn1Click'},
        '#id2': {'click': 'onBtn2Click'},
        '#id3': {'click': 'onBtn3Click'},
        '#id4': {'click': 'onBtn4Click'}
    },
    afterInit: function () {
        Mate.Selector('#desktopHeader').html('Отладка');
        Mate.Selector('#addressListAddress').editable({
            title: 'Адрес',
            placement: 'bottom',
            highlight: false,
            unsavedclass: null,
            value: null,
            success: Mate.Selector.proxy(this.onNewAddressEntered, this)
        });
    },
    onNewAddressEntered: function (response, value) {
        var context = Mate.Application.getControllerByName('AddressListController');
        context.enteredAddress = value;
        Mate.Logger.debug(value.uniqueId);
        Mate.service.getCoordinates(value.address, Mate.service.GeocodingProvider.YANDEX, this.onGetCoordComplete);
    },
    onGetCoordComplete: function(response) {
        Mate.Logger.debug(response);
    },
    onBtn1Click: function () {
    },
    onBtn2Click: function () {
    },
    onBtn3Click: function () {
    },
    onBtn4Click: function () {
    }
});
