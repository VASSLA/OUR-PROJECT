/* global Mate */
Mate.define('Application.store.EventTypeStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.EventTypeModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/eventType'
    })
});
