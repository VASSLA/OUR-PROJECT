/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: AdministratorController.js
 * Date: Sep 20, 2016 10:12:02 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.AdministratorController', {
    extends: 'Mate.app.Controller',
    afterInit: function() {
        Mate.Selector('#desktopHeader').html('Администратор');        
    }
});

