/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: FeedbackTypeStore.js
 * Date: Feb 18, 2017 9:47:54 PM
 * Author: User */

/* global Mate */
Mate.define('Application.store.FeedbackTypeStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.FeedbackTypeModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/feedbackType'
    })
});
