/* global Mate */
Mate.define('Application.model.MediaInfoModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'userId',
            caption: 'Идентификатор пользователя',
            description: 'Идентификатор пользователя загрузившего файл',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'fileName',
            caption: 'Имя файла',
            description: 'Внутреннее имя файла',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'type',
            caption: 'Тип',
            description: 'Тип файла',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'uploadDate',
            caption: 'Дата загрузки',
            description: 'Дата загрузки файла',
            size: 0,
            type: 'datetime'
        },
        {
            name: 'initialName',
            caption: 'Имя файла',
            description: 'Изначальное имя файла',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'thumbnailName',
            caption: 'Имя файла миниатюры',
            description: 'Имя файла миниатюры',
            type: 'string',
            size: 0,
            validator: 'string.string'
        }
    ],
    keyField: 'id'
});
