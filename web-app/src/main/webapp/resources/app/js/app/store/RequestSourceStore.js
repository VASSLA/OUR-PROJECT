/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestSourceStore.js
 * Date: Oct 31, 2016 9:21:15 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.RequestSourceStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.RequestSourceModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/requestsource'
    })
});
