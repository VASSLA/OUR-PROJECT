/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestStore.js
 * Date: Oct 31, 2016 9:31:28 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.RequestStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.RequestModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/request'
    })
});
