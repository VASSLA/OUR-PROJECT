/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: FeedbackStatusStore.js
 * Date: Feb 18, 2017 9:48:13 PM
 * Author: User */

/* global Mate */
Mate.define('Application.store.FeedbackStatusStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.FeedbackStatusModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/feedbackStatus'
    })
});