/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestStateStore.js
 * Date: Oct 31, 2016 9:07:26 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.RequestStateStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.RequestStateModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/requeststate'
    })
});
