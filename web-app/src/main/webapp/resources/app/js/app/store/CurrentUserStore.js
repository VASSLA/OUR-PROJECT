/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: CurrentUserStore.js
 * Date: 24.10.2016 20:08:23
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.CurrentUserStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.UserModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/user/me'
    })
});
