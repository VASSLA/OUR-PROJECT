/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: SettingStore.js
 * Date: Nov 30, 2016 12:07:43 AM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.SettingStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.SettingModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/setting'
    })
});
