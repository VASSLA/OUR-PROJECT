/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: AddressStore.js
 * Date: Nov 27, 2016 7:13:08 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.AddressStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.AddressModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/address'
    })
});
