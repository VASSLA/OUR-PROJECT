/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestCardMapsController.js
 * Date: Oct 11, 2016 4:40:44 PM
 * Author: anikeale */

/* global Mate, Lewrap */
Mate.define('Application.controller.RequestCardMapsController', {
    extends: 'Mate.app.Controller',
    lewrap: null,
    requestId: null,
    mapWaitIndicator: null,
    tabWasShown: false,
    interactives: {
        '#requestCardMapsTab': {'shown.bs.tab': 'onMapsTabShown'}
    },
    afterInit: function () {
        this.createWaitIndicator();
        this.initializeMap();
        this.initializeTools();
    },
    beforeDeinit: function() {
        this.tabWasShown = false;
    },
    createWaitIndicator: function() {
        this.mapWaitIndicator = Mate.create('Mate.ux.WaitIndicator', {
            parent: '#requestCardMap',
            template: Mate.Application.getResourceContent('blockWait')
        });
    },
    preloadSavedData: function(lewrap) {
        var me = this;
        me.mapWaitIndicator.show();
        var filters = {
            requestId: me.requestId
        };
        Mate.Selector.ajax({
            url: '/api/v1/geodata',
            method: 'GET',
            data: filters,
            contentType: 'application/json',
            success: function(data) {
                lewrap.clearMap();
                me.lewrap.geoJson(data, false, true);
                $('#map-pulse-save-button').fadeOut();
                $('#map-pulse-reload-button').fadeOut();
                me.mapWaitIndicator.hide();
            },
            error: function() {
                me.mapWaitIndicator.hide();
                //FIXME БЛЕАТЬ!
            }
        });
    },
    onSaveButtonClick: function(lewrap) {
        var geoJson = lewrap.toGeoJson();
        this.storeFetureCollection(geoJson);
    },
    onReloadButtonClick: function(lewrap) {
        this.preloadSavedData(lewrap);
    },
    initializeMap: function () {
        var mapsConfig = {
            tiles: [Lewrap.Tiles.OSM, Lewrap.Tiles.GOOGLE, Lewrap.Tiles.YANDEX],
            onSaveButtonClick: Mate.Selector.proxy(this.onSaveButtonClick, this),
            onReloadButtonClick: Mate.Selector.proxy(this.onReloadButtonClick, this)
        };
        this.lewrap = new Lewrap('requestCardMap', mapsConfig);
    },
    initializeTools: function () {
        var buttons = [
            {
                scope: this,
                class: 'fa fa-object-ungroup',
                fn: function() {
                    this._map['cropStart'] = true;
                    $("#map-crop-modal").show();
                    $(".let-cross-marker").show();
                }
            }
        ];
        this.lewrap.addButtonGroup(buttons, { position: 'topleft'});
    },
    storeFetureCollection: function (geojson) {
        var me = this;
        geojson['requestId'] = this.requestId;
        me.mapWaitIndicator.show();
        Mate.Selector.ajax({
            url: '/api/v1/geodata',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(geojson),
            success: function() {
                $('#map-pulse-save-button').fadeOut();
                $('#map-pulse-reload-button').fadeOut();
                me.mapWaitIndicator.hide();
            },
            error: function() {
                me.mapWaitIndicator.hide();
                //FIXME БЛЕАТЬ!
            }
        });  
    },
    onMapsTabShown: function () {
        if (!this.tabWasShown) {
            this.lewrap.redrawMap();
            this.lewrap.initializeUTMGrid();
            this.preloadSavedData(this.lewrap);
            this.tabWasShown = true;
        }
    }
});
