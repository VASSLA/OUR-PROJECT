/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ValidatorModel.js
 * Date: Oct 31, 2016 9:00:18 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.ValidatorModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'regexp',
            caption: 'Валидатор',
            description: 'Регулярное выражение для проверки',
            type: 'string',
            size: 0,
            validator: 'string.regexp'
        },
        {
            name: 'description',
            caption: 'Описание',
            description: 'Описание регулярного выражение для проверки',
            type: 'string',
            size: 0,
            validator: 'string.text'
        }
    ],
    keyField: 'id'
});
