/* global Mate */
Mate.define('Application.store.MediaInfoStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.MediaInfoModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/mediainfoforme'
    })
});
