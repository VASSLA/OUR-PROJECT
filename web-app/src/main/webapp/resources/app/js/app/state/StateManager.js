/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: AppStateManager.js
 * Date: Oct 31, 2016 9:12:06 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.state.StateManager', {
    extends: 'Mate.app.StateManager',
    states: [
        {// Default state of Application - no any controller active
            name: 'Default',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController']
        },
        {
            name: 'Profile',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.ProfileController']
        },
        {
            name: 'Requests',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.RequestsController']
        },
        {
            name: 'Events',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.EventsController']
        },
        {
            name: 'Dashboard',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.DashboardController']
        },
        {
            name: 'Messages',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.MessagesController']
        },
        {
            name: 'Coordinator',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.CoordinatorController']
        },
        {
            name: 'SystemSettings',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.SystemSettingsController']
        },
        {
            name: 'Administrator',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.AdministratorController']
        },
        {
            name: 'Logs',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.LogsController']
        },
        {
            name: 'Feedback',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.FeedbackController']
        },
        {
            name: 'RequestCard',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.RequestCardController']
        },
        {
            name: 'Debug',
            deactivate: ['*'],
            activate: ['Application.controller.DesktopController',
                'Application.controller.DebugController']
        },
        {
            name: 'MediaLibrary',
            deactivate: ['*'],
            activate: ['Application.controller.MediaLibraryController',
                'Application.controller.DesktopController',
                'Application.controller.MediaFormController']
        }
    ]
});
