/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: MessagesController.js
 * Date: Sep 20, 2016 10:09:34 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.MessagesController', {
    extends: 'Mate.app.Controller',
    interactives: {
        '#messageContactSearchButton': {'click': 'onMessageContactAddButtonClick'},
        '#messageContactSearchInput': {'typeahead:selected': 'onSelectContact'},
        'i[data-user-id]': {'click': 'onMessageContainerTabCloseClick'},
        'window': {'resize': 'onWindowResized'}
    },
    suggession: null,
    currentlySelectedContact: null,
    contactTemplate: null,
    messageTemplate: null,
    messageTabTemplate: null,
    messageContainerTemplate: null,
    loadingMessageHandlers: {},
    afterInit: function () {
        Mate.Selector('#desktopHeader').html('Обмен сообщениями');
        this.initializeTypeaheadSearch();
        this.contactTemplate = Mate.Application.getResourceContent('blockBuddy');
        this.messageTemplate = Mate.Application.getResourceContent('blockMessage');
        this.messageTabTemplate = Mate.Application.getResourceContent('blockMessageTab');
        this.messageContainerTemplate = Mate.Application.getResourceContent('blockMessageContainer');
        this.displayContacts();
        Mate.Selector(window).resize();
    },
    beforeDeinit: function () {
        for (var i in this.loadingMessageHandlers) {
            Mate.Logger.trace("Messaging handler " + this.loadingMessageHandlers[i] + " were stopped");
            clearTimeout(this.loadingMessageHandlers[i]);
        }
        Mate.Logger.trace('All messaging handlers were stopped');
    },
    onWindowResized: function() {
        var contentHeaderHeight = Mate.Selector('#content-header').outerHeight(true);
        var contentWrapperHeight = Mate.Selector('#content-wrapper').height();
        var contentSectionPaddingTop = Mate.Selector('section[class=content]').css('paddingTop').replace('px', '');
        var contentSectionPaddingBottom = Mate.Selector('section[class=content]').css('paddingBottom').replace('px', '');
        var messageTabPanHeight = Mate.Selector('#messageTabContainer').height();
        var sectionHeight = contentWrapperHeight - contentHeaderHeight - 5;
        var chatsHeight = sectionHeight - contentSectionPaddingTop - contentSectionPaddingBottom - messageTabPanHeight;
        var contactsHeight = sectionHeight - contentSectionPaddingTop - contentSectionPaddingBottom;

        Mate.Selector('section[class=content]').css('height', sectionHeight);
        Mate.Selector('#chats').css('height', chatsHeight);
        Mate.Selector('#contactList').css('height', contactsHeight);
        Mate.Selector('#messageContainersContainer').css('height', chatsHeight);

        var messageContainersContainerPaddingTop = Mate.Selector('#messageContainersContainer').css('paddingTop').replace('px', '');
        var messageContainersContainerPaddingBottom = Mate.Selector('#messageContainersContainer').css('paddingBottom').replace('px', '');
        var chatsContainersHeight = chatsHeight - messageContainersContainerPaddingTop - messageContainersContainerPaddingBottom;

        Mate.Selector('#messageContainersContainer').find('div[class~=tab-pane]').find('.direct-chat-messages').each(function() {
            Mate.Selector(this).height(chatsContainersHeight - 75);
        });
    },
    displayContacts: function () {
        Mate.Selector('#messagesContactListContainer').empty();
        var buddies = Mate.Application.currentUser.get('buddies');
        for (var i = 0; i < buddies.length; i++) {
            var html = this.contactTemplate;
            var pictureId = buddies[i].person.pictureId;
            var picture = pictureId ? '/api/v1/getthumbnail/' + pictureId : '/resources/app/img/default-user-avatar.png';
            html = html.replace(/{userId}/g, buddies[i].id);
            html = html.replace(/{fullName}/g, buddies[i].person.lastName + ' ' + buddies[i].person.firstName);
            html = html.replace(/{nick}/g, buddies[i].login);
            html = html.replace(/{status}/g, 'На связи');
            html = html.replace(/{picture}/g, picture);
            Mate.Selector('#messagesContactListContainer').append(html);
        }
        Mate.Selector('li[data-user-id]').css('cursor', 'pointer');
        Mate.Selector('#messagesContactListContainer').children('li[data-user-id]').on('click', this, Mate.Selector.proxy(this.onContactClick, this));
        Mate.Selector('#messagesContactListContainer').find('.tool-btn-close').on('click', this, Mate.Selector.proxy(this.onRemoveContactClick, this));
    },
    initializeTypeaheadSearch: function () {
        this.suggession = new Bloodhound({
            datumTokenizer: function (data) {
                return Bloodhound.tokenizers.whitespace(data.login);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function (data) {
                return data.id;
            },
            remote: {
                url: '/api/v1/user?login=%query&precise=0',
                wildcard: '%query'
            }
        });
        Mate.Selector('#messageContactSearchInput').typeahead({highlight: true}, {
            display: 'login',
            source: this.suggession
        });
        Mate.Selector('#messageContactSearchInput').css('height', '30px');
        Mate.Selector('#messageContactSearchInput').css('top', '2px');
    },
    removeContact: function(contact) {
        var contactId = parseInt((contact instanceof Object) ? contact.id : contact);
        var buddies = Mate.Application.currentUser.get('buddies');
        for (var i = 0; i < buddies.length; i++) {
            if (buddies[i].id === contactId) {
                Mate.Logger.trace('Contact ' + buddies[i].login + ' has been removed');
                buddies.splice(i, 1);
                return true;
            }
        }
        return false;
    },
    canBeAdded: function(contact) {
        return !(this.contactAlreadyInList(contact) ||
                this.contactIsMe(contact));
    },
    contactAlreadyInList: function (contact) {
        var contactId = parseInt((contact instanceof Object) ? contact.id : contact);
        var buddies = Mate.Application.currentUser.get('buddies');
        for (var i = 0; i < buddies.length; i++) {
            if (buddies[i].id === contactId) {
                Mate.Logger.trace('Contact ' + buddies[i].login + ' is already in list');
                return true;
            }
        }
        return false;
    },
    contactIsMe: function (contact) {
        if (contact.id === Mate.Application.currentUser.get('id')) {
            Mate.Logger.trace('Can not add myself to contact list');
            return true;
        }
        return false;
    },
    onMessageContactAddButtonClick: function () {
        if (!this.canBeAdded(this.currentlySelectedContact))
            return;
        var currentUser = Mate.Application.currentUser;
        currentUser.get('buddies').push(this.currentlySelectedContact);
        currentUser.getStore().getProxy().url = '/api/v1/user';
        currentUser.getStore().onSynchronized = Mate.Selector.proxy(function () {
            Mate.Logger.trace('Reloading buddies list...');
            this.displayContacts();
            currentUser.getStore().getProxy().url = '/api/v1/user/me';
            currentUser.onSynchronized = null;
            //Mate.Application.waitIndicator.hide();
        }, this);
        currentUser.setChanged(true);
        //Mate.Application.waitIndicator.show();
        currentUser.getStore().sync();
    },
    onRemoveContactClick: function (eventObject) {
        var buddyId = Mate.Selector(eventObject.target).attr('data-user-id');
        if (!buddyId)
            return;
        var currentUser = Mate.Application.currentUser;
        if (!this.removeContact(parseInt(buddyId)))
            return false;
        this.removeContact(buddyId);
        currentUser.getStore().getProxy().url = '/api/v1/user';
        currentUser.getStore().onSynchronized = Mate.Selector.proxy(function () {
            Mate.Logger.trace('Reloading buddies list...');
            this.displayContacts();
            currentUser.getStore().getProxy().url = '/api/v1/user/me';
            currentUser.onSynchronized = null;
            //Mate.Application.waitIndicator.hide();
        }, this);
        currentUser.setChanged(true);
        //Mate.Application.waitIndicator.show();
        currentUser.getStore().sync();
        return false;
    },
    onSelectContact: function (eventObject, data) {
        var me = eventObject.data;
        me.currentlySelectedContact = data;
    },
    makeMessageObject: function(sendButtonObject) {
        return {
            id: null,
            senderId: Mate.Application.currentUser.get('id'),
            receiverId: Mate.Selector(sendButtonObject).attr('data-user-id'),
            message: Mate.Selector(sendButtonObject).closest('span').siblings('input').val(),
            read: 0,
            delivered: 0
        };
    },
    onSendButtonClick: function (eventObject) {
        if (eventObject.type === 'keyup' && eventObject.keyCode !== 13)
            return false;
        var target = eventObject.target;
        if (eventObject.type === 'keyup') {
            target = Mate.Selector(target).siblings('span').find('button');
        }
        var currentUser = Mate.Application.currentUser;
        var message = this.makeMessageObject(target);
        Mate.Selector.ajax({
            url: '/api/v1/message',
            method: 'POST',
            context: {button: Mate.Selector(target), controller: this},
            contentType: 'application/json',
            data: JSON.stringify(message),
            success: function (data) {
                debugger;
                var html = this.controller.messageTemplate;
                var currentUserFullName = currentUser.get('person.lastName') + ' ' + currentUser.get('person.firstName');
                html = html.replace(/{message}/g, data.message);
                html = html.replace(/{fullname}/g, currentUserFullName);
                html = html.replace(/{datetime}/g, data.datetime);
                var messagesList = Mate.Selector('#messageContainersContainer').find('div[data-user-id=' + data.receiverId + ']').find('.direct-chat-messages');
                messagesList.append(html);
                messagesList.scrollTop(messagesList.prop('scrollHeight'));
                this.button.closest('span').siblings('input').val('');
                this.button.closest('span').siblings('input').removeClass('text-danger');
            },
            error: function () {
                this.closest('span').siblings('input').addClass('text-danger');
            }
        });
    },
    onMessageContainerTabCloseClick: function (eventObject) {
        var myId = Mate.Application.currentUser.get('id');
        var buddyId = Mate.Selector(eventObject.target).attr('data-user-id');
        var closestRightTab = Mate.Selector(eventObject.target).closest('li').siblings()[0];
        Mate.Selector(closestRightTab).children('a').click();
        Mate.Selector('#messageContainersContainer').find('div[data-user-id=' + buddyId + ']').remove();
        Mate.Selector('#messageTabContainer').find('li[data-user-id=' + buddyId + ']').remove();
        clearTimeout(this.loadingMessageHandlers[buddyId + '_' + myId]);
    },
    onContactClick: function (eventObject) {
        var userId = Mate.Selector(eventObject.target).closest('li').attr('data-user-id');
        if (Mate.Selector('#messageTabContainer').children('li[data-user-id=' + userId + ']').length > 0) {
            Mate.Selector('#messageTabContainer').find('li[data-user-id=' + userId + ']').find('a').click();
            return true;
        }
        var html = this.messageTabTemplate;
        html = html.replace(/{userId}/g, userId);
        var buddyName = Mate.Selector(eventObject.target).closest('li').find('.contacts-list-name').html();
        html = html.replace(/{fullName}/g, buddyName);
        Mate.Selector('#messageTabContainer').append(html);
        Mate.Selector('#messageTabContainer').find('i[data-user-id=' + userId + ']').on('click', this, Mate.Selector.proxy(this.onMessageContainerTabCloseClick, this));
        var html = this.messageContainerTemplate;
        html = html.replace(/{userId}/g, userId);
        Mate.Selector('#messageContainersContainer').append(html);
        Mate.Selector('#messageTabContainer').find('li[data-user-id=' + userId + ']').find('a').click();
        Mate.Selector('#messageContainersContainer').find('button[data-user-id=' + userId + ']').on('click', this, Mate.Selector.proxy(this.onSendButtonClick, this));
        Mate.Selector('#messageContainersContainer').find('input[data-user-id=' + userId + ']').on('keyup', this, Mate.Selector.proxy(this.onSendButtonClick, this));
        this.loadLastMessages(userId, buddyName);
        Mate.Selector(window).resize();
    },
    loadLastMessages: function (userId, buddyName, context, onlynew) {
        Mate.Logger.trace('Loading last messages ...');
        var currentUser = Mate.Application.currentUser;
        var currentUserId = currentUser.get('id');
        var currentUserFullname = currentUser.get('person.lastName') + ' ' + currentUser.get('person.firstName');
        var ctx = context || this;
        var filters = {};
        var url = '/api/v1/conversation/' + currentUserId + '/' + userId;
        if (onlynew) {
            filters["read"] = 0;
            filters["receiverId"] = currentUserId;
            filters["senderId"] = userId;
            url = '/api/v1/message';
        }
        Mate.Selector.ajax({
            url: url,
            method: 'GET',
            contentType: 'application/json',
            context: ctx,
            data: filters,
            success: function (data) {
                var messagesList = Mate.Selector('#messageContainersContainer').find('div[data-user-id=' + userId + ']').find('.direct-chat-messages');
                for (var i = 0; i < data.length; i++) {
                    var html = this.messageTemplate;
                    html = html.replace(/{message}/g, (!data[i].message || data[i].message.length === 0) ? '&nbsp;' : data[i].message);
                    if (data[i].senderId === currentUserId) {
                        html = html.replace(/{fullname}/g, currentUserFullname);
                    } else {
                        html = html.replace(/{fullname}/g, buddyName);
                    }
                    html = html.replace(/{datetime}/g, data[i].datetime);
                    messagesList.append(html);
                }
                messagesList.scrollTop(messagesList.prop('scrollHeight'));
                var handlerId = userId + "_" + currentUserId;
                this.loadingMessageHandlers[handlerId] = setTimeout(this.loadLastMessages, 10000, userId, buddyName, ctx, true);
            }
        });
    }
});

