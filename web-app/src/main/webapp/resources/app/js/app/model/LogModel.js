/* global Mate */
Mate.define('Application.model.LogModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'user',
            caption: 'Пользователь',
            description: 'Пользователь',
            type: 'model',
            mapAs: 'Application.model.UserModel',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'logMessageType',
            caption: 'Тип сообщения',
            description: 'Тип сообщения',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'message',
            caption: 'Сообщение',
            description: 'Сообщение',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'callingClass',
            caption: 'Класс',
            description: 'Класс, создавший сообщение',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'logDate',
            caption: 'Дата',
            description: 'Дата записи',
            size: 0,
            type: 'datetime'
        }
    ],
    keyField: 'id'
});
