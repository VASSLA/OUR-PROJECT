/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: SkillModel.js
 * Date: Dec 4, 2016 5:07:41 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.SkillModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'skill',
            caption: 'Навык',
            description: 'Навык которым владеет человек или возможность использовать что либо',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'skillIcon',
            caption: 'Иконка навыка',
            description: 'Иконка навыка из Font Awesome',
            type: 'string',
            size: 255,
            validator: 'string.string'
        }
    ],
    keyField: 'id'
});