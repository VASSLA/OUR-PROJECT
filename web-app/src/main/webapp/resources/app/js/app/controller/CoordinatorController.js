/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: CoordinatorController.js
 * Date: Sep 20, 2016 10:11:19 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.CoordinatorController', {
    extends: 'Mate.app.Controller',
    store: null,
    interactives: {
        '[data-request-card]': {'click': 'onRequestCardClick'}
    },
    afterInit: function() {
        Mate.Selector('#desktopHeader').html('Координатор');
        this.template = Mate.Application.getResourceContent('blockRequestCard');
        if (this.store === null) {
            this.createStore();
        }
        this.loadRequests();
    },
    createStore: function() {
        this.store = Mate.create('Application.store.CoordinatorRequestStore');
    },
    loadRequests: function() {
        var me = this;
        Mate.Application.waitIndicator.show();
        var currentUserStore = Mate.Application.getStoreByName('Application.store.CurrentUserStore');
        var currentUser = currentUserStore.getData(0);
        if (currentUser) {
            this.store.setFilters({
                'coordPerson.id': currentUser.get('person.id')
            });
            this.store.onDataLoaded = function() {
                me.requests = me.store.getData();
                //me.loadTemplate();
                me.displayRequests();
                Mate.Application.waitIndicator.hide();
            };
            this.store.load();
        }
    },
    loadTemplate: function() {
        Mate.Selector.ajax({
            url: '/block/coordinator-requestcard',
            method: 'GET',
            contentType: 'text/html; charset=utf-8',
            context: this,
            success: function(data) {
                this.template = data;
                this.displayRequests();
            }
        });
    },
    displayRequests: function() {
        Mate.Selector('#coordinatorRequestsPlaceholder').html('');
        for (var i = 0; i < this.requests.length; i++) {
            this.displayRequest(this.requests[i]);
        }
        Mate.Selector('div[data-request-id]').on('click', this, this.onRequestCardClick);
        var requestController = Mate.Application.getControllerByName('RequestsController');
        Mate.Selector('button[data-request-id][data-prep-type=pdf]').on('click', requestController, Mate.Selector.proxy(requestController.onCreatePdfButtonClick, requestController));
        Mate.Selector('button[data-request-id][data-prep-type=html]').on('click', requestController, Mate.Selector.proxy(requestController.onCreateHtmlButtonClick, requestController));
    },
    displayRequest: function(request) {
        var content = this.template;
        var person = request.getFieldByName('missingPerson').getValue();
        var pictureId = request.get('missingPerson.pictureId');
        var age = ((new Date()).getFullYear() - parseInt(request.get('missingPerson.birthDate').substr(-4))).toString();
        var word = 'лет';
        if (age.substr(-1) === '1') word = 'год';
        if (age.substr(-1) === '2' || age.substr(-1) === '3' || age.substr(-1) === '4') word = 'года';
        var content = content.replace(/{picture}/g, pictureId ? '/api/v1/getthumbnail/' + pictureId : '/resources/app/img/default-user-avatar.png');
        var content = content.replace(/{requestId}/g, request.get('id'));
        var content = content.replace(/{lastName}/g, person.get('lastName'));
        var content = content.replace(/{firstName}/g, person.get('firstName'));
        var content = content.replace(/{middleName}/g, person.get('middleName'));
        var content = content.replace(/{age}/g, age + ' ' + word);
        var content = content.replace(/{missingFrom}/g, request.get('missingFrom'));
        Mate.Selector('#coordinatorRequestsPlaceholder').append(content);
    },
    onRequestCardClick: function (eventObject) {
        var requestId = Mate.Selector(eventObject.target).closest('div[data-request-id]').attr('data-request-id');
        Mate.Application.getControllerByName('Application.controller.RequestCardController').requestId = requestId;
        Mate.Application.getControllerByName('Application.controller.RequestCardMapsController').requestId = requestId;
        Mate.Application.getStateManager().setState('RequestCard');
    }
});

