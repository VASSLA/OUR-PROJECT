/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: AddressModel.js
 * Date: Nov 27, 2016 7:09:26 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.AddressModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'personId',
            caption: 'Идентификатор персоны',
            description: 'Идентификатор персоны, которой принадлежит адрес',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'kladrId',
            caption: 'Идентификатор КЛАДР',
            description: 'Идентификатор по системе КЛАДР',
            type: 'string',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'zip',
            caption: 'Почтовый индекс',
            description: 'Почтовый индекс',
            type: 'string',
            size: 30,
            validator: 'string.numeric'
        },
        {
            name: 'region',
            caption: 'Область/Регион',
            description: 'Область или регион',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'regionTypeShort',
            caption: 'Тип региона',
            description: 'Тип региона (короткий)',
            type: 'string',
            size: 10,
            validator: 'string.string'
        },
        {
            name: 'regionType',
            caption: 'Тип региона',
            description: 'Тип региона',
            type: 'string',
            size: 50,
            validator: 'string.string'
        },
        {
            name: 'district',
            caption: 'Район',
            description: 'Район',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'districtTypeShort',
            caption: 'Тип района',
            description: 'Тип района (короткий)',
            type: 'string',
            size: 10,
            validator: 'string.string'
        },
        {
            name: 'districtType',
            caption: 'Тип района',
            description: 'Тип района',
            type: 'string',
            size: 50,
            validator: 'string.string'
        },
        {
            name: 'city',
            caption: 'Город/Населенный пункт',
            description: 'Город или населенный пункт',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'cityTypeShort',
            caption: 'Тип населенного пункта',
            description: 'Тип населенного пункта (короткий)',
            type: 'string',
            size: 10,
            validator: 'string.string'
        },
        {
            name: 'cityType',
            caption: 'Тип населенного пункта',
            description: 'Тип населенного пункта',
            type: 'string',
            size: 50,
            validator: 'string.string'
        },
        {
            name: 'street',
            caption: 'Улица',
            description: 'Улица',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'streetTypeShort',
            caption: 'Тип улицы',
            description: 'Тип улицы (короткий)',
            type: 'string',
            size: 10,
            validator: 'string.string'
        },
        {
            name: 'streetType',
            caption: 'Тип улицы',
            description: 'Тип улицы',
            type: 'string',
            size: 50,
            validator: 'string.string'
        },
        {
            name: 'building',
            caption: 'Дом/Строение',
            description: 'Дом, строение, владение',
            type: 'string',
            size: 5,
            validator: 'string.string'
        },
        {
            name: 'buildingTypeShort',
            caption: 'Тип здания',
            description: 'Тип здания (короткий)',
            type: 'string',
            size: 10,
            validator: 'string.string'
        },
        {
            name: 'buildingType',
            caption: 'Тип здания',
            description: 'Тип здания',
            type: 'string',
            size: 50,
            validator: 'string.string'
        },
        {
            name: 'apartment',
            caption: 'Квартира',
            description: 'Квартира',
            type: 'string',
            size: 5,
            validator: 'string.string'
        },
        {
            name: 'extras',
            caption: 'Дополнительная информация',
            description: 'Дополнительная информация',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'address',
            caption: 'Адрес',
            description: 'Адрес одной строкой',
            type: 'string',
            size: 255,
            validator: 'number.integer'
        },
        {
            name: 'longitude',
            caption: 'Долгота',
            description: 'Долгота',
            type: 'float',
            size: 255,
            validator: 'number.float'
        },
        {
            name: 'altitude',
            caption: 'Широта',
            description: 'Широта',
            type: 'float',
            size: 0,
            validator: 'number.float'
        },
        {
            name: 'defaultAddress',
            caption: 'Основной адрес',
            description: 'Этот адрес основной',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'uniqueId',
            caption: 'Уникальный идентификатор',
            description: 'Уникальный идентификатор',
            type: 'string',
            size: 255,
            validator: 'string.string'
        }
    ],
    keyField: 'id'
});
