/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: UserModel.js
 * Date: 24.10.2016 19:39:11
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.UserModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            size: 0,
            type: 'integer'
        },
        {
            name: 'login',
            caption: 'Имя для входа',
            description: 'Имя, используемое для входа в систему',
            size: 32,
            type: 'string'
        },
        {
            name: 'creationDate',
            caption: 'Дата создания',
            description: 'Дата создания/регистрации пользователя',
            size: 0,
            type: 'datetime'
        },
        {
            name: 'lastLoginDate',
            caption: 'Дата последнего входа',
            description: 'Дата и время последнего входа в систему',
            size: 0,
            type: 'datetime'
        },
        {
            name: 'lastLoginIp',
            caption: 'Адрес последнего входа',
            description: 'Адрес, с которого последний раз выполнялся вход',
            size: 15,
            type: 'string'
        },
        {
            name: 'disabled',
            caption: 'Отключен',
            description: 'Признак того, что пользователь отключен',
            size: 0,
            type: 'boolean'
        },
        {
            name: 'reasonToDisable',
            caption: 'Причина отключения',
            description: 'Причина, в результате которой пользователь был отключен',
            size: 0,
            type: 'string'
        },
        {
            name: 'disabledToDate',
            caption: 'Отключен до',
            description: 'Пользователь отключен до этой даты',
            size: 0,
            type: 'datetime'
        },
        {
            name: 'personId',
            caption: 'Идентификатор персоны',
            description: 'Иддентификатор персоны',
            size: 0,
            type: 'integer'
        },
        {
            name: 'person',
            caption: 'Персона',
            description: 'Персональные данные, связанные с учетной записью',
            size: 0,
            type: 'model',
            mapAs: 'Application.model.PersonModel'
        },
        {
            name: 'confirmed',
            caption: 'E-Mail проверен',
            description: 'Подтверждение, что E-Mail проверен',
            size: 0,
            type: 'boolean'
        },
        {
            name: 'confirmationToken',
            caption: 'Код подтверждения',
            description: 'Код подтверждения',
            size: 0,
            type: 'string'
        },
        {
            name: 'email',
            caption: 'E-Mail',
            description: 'E-Mail пользователя',
            size: 0,
            type: 'string'
        },
        {
            name: 'passwordHash',
            caption: 'Хэш пароля',
            description: 'Хэш пароля (открыто только в целях отладки)',
            size: 0,
            type: 'string'
        },
        {
            name: 'roles',
            caption: 'Роли пользователя',
            description: 'Список ролей пользователя',
            size: 0,
            type: 'array'
        },
        {
            name: 'buddies',
            caption: 'Друзья пользователя',
            description: 'Список друзей пользователя',
            size: 0,
            type: 'array'
        }
    ],
    keyField: 'id'
});
