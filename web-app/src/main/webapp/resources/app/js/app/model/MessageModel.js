/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: MessageModel.js
 * Date: Dec 8, 2016 11:57:12 AM
 * Author: anikeale */

Mate.define('Application.model.MessageModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'datetime',
            caption: 'Дата и время',
            description: 'Дата и время отправки сообщения',
            type: 'timestamp',
            size: 0,
            validator: 'datetime'
        },
        {
            name: 'senderId',
            caption: 'Идентификатор отправителя',
            description: 'Уникальный идентификатор отправителя',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'receiverId',
            caption: 'Идентификатор получателя',
            description: 'Уникальный идентификатор получателя',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'message',
            caption: 'Сообщение',
            description: 'Текст сообщения',
            type: 'string',
            size: 0,
            validator: 'string.safe'
        },
        {
            name: 'read',
            caption: 'Сообщение прочитано',
            description: 'Признак чтого, что сообщение было прочитано',
            type: 'boolean',
            size: 0,
            validator: 'boolean.boolean'
        },
        {
            name: 'delivered',
            caption: 'Сообщение доставлено',
            description: 'Признак того, что сообщение доставлено',
            type: 'boolean',
            size: 0,
            validator: 'boolean.boolean'
        }
    ]
});
