/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestsController.js
 * Date: Sep 7, 2016 10:36:52 PM
 * Author: Alex */

/* global Mate, BootstrapDialog, pdfMake, ymaps */
Mate.define('Application.controller.RequestsController', {
    extends: 'Mate.app.Controller',
    interactives: {
        '#btnShowExtraParams': {'click': 'onBtnShowExtraParamsClick'},
        '#btnSearchRequest': {'click': 'onBtnSearchRequestClick'},
        '#btnRequestNew': {'click': 'onBtnRequestNewClick'},
        'div[data-request-id]': {'click': 'onRequestCardClick'},
        '#requestSearchQuery': {'keyup': 'onBtnSearchRequestClick'}
    },
    requests: null,
    filters: {},
    template: '',
    afterInit: function () {
        this.template = Mate.Application.getResourceContent('blockRequestCard');
        Mate.Selector('#desktopHeader').html('Заявки');
        Mate.Selector("#requestSearchDistance").ionRangeSlider({min: 0, max: 1000, step: 5, from: 1000, postfix: ' км', grid: true});
        Mate.Selector("#requestSearchAge").ionRangeSlider({type: 'double', min: 0, max: 100, from: 0, to: 100, step: 1, postfix: ' лет', grid: true});
        Mate.Selector('#requestSearchExtraParams').toggle();
        this.loadRequests();
    },
    loadRequests: function () {
        this.requests = Mate.create('Application.store.RequestStore', {
            autoLoad: false,
            autoSync: false,
            model: 'Application.model.RequestModel',
            proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
                url: '/api/v1/request'
            })
        });
        this.requests.onDataLoaded = Mate.Selector.proxy(this.onRequestsLoaded, this);
        this.requests.setFilters(this.filters);
        this.requests.load();
    },
    onRequestsLoaded: function() {
        this.requests.onDataLoaded = null;
        this.displayRequests();
    },
    displayRequests: function () {
        Mate.Selector('#requestsPlaceholder').html('');
        var requests = this.requests.getData();
        for (var i = 0; i < requests.length; i++) {
            this.displayRequest(requests[i]);
        }
        Mate.Selector('div[data-request-id]').on('click', this, this.onRequestCardClick);
        Mate.Selector('button[data-request-id][data-prep-type=pdf]').on('click', this, Mate.Selector.proxy(this.onCreatePdfButtonClick, this));
        Mate.Selector('button[data-request-id][data-prep-type=html]').on('click', this, Mate.Selector.proxy(this.onCreateHtmlButtonClick, this));
    },
    displayRequest: function (request) {
        var content = this.template;
        var pictureId = request.get('missingPerson.pictureId');
        var person = request.getFieldByName('missingPerson').getValue();
        var age = ((new Date()).getFullYear() - parseInt(request.get('missingPerson.birthDate').substr(-4))).toString();
        var word = 'лет';
        if (age.substr(-1) === '1') word = 'год';
        if (age.substr(-1) === '2' || age.substr(-1) === '3' || age.substr(-1) === '4') word = 'года';
        var content = content.replace(/{picture}/g, pictureId ? '/api/v1/getthumbnail/' + pictureId : '/resources/app/img/default-user-avatar.png');
        var content = content.replace(/{requestId}/g, request.get('id'));
        var content = content.replace(/{lastName}/g, person.get('lastName'));
        var content = content.replace(/{firstName}/g, person.get('firstName'));
        var content = content.replace(/{middleName}/g, person.get('middleName'));
        var content = content.replace(/{age}/g, age + ' ' + word);
        var content = content.replace(/{missingFrom}/g, request.get('missingFrom'));
        Mate.Selector('#requestsPlaceholder').append(content);
    },
    onCreateHtmlButtonClick: function (eventObject) {
        var requestId = Mate.Selector(eventObject.target).closest('button').attr('data-request-id');
        BootstrapDialog.show({
            title: 'Запрос',
            closable: false,
            draggable: true,
            requestId: requestId,
            type: BootstrapDialog.TYPE_DANGER,
            message: 'Подготовить HTML для печати?',
            buttons: [
                {label: 'Да', action: this.onCreateHtmlDialogOk},
                {label: 'Нет', action: this.onCreateHtmlDialogCancel}
            ]
        });
        return false;
    },
    onCreateHtmlDialogOk: function (dialog) {
        var report = window.open('', '_blank');
        var reportTemplate = Mate.Application.getResourceContent('reportRequest');
        var request = Mate.Application.getStoreByName('RequestStore').lookup('id', parseInt(dialog.options.requestId));
        var html = reportTemplate;
        html = html.replace(/{missingPerson.birthDate}/g, request.get('missingPerson.birthDate').substr(-4));
        html = Mate.template.applyData(html, request);
        Mate.Selector(report.document.body).ready(function () {
            report.document.write(html);
        });
        dialog.close();
    },
    onCreateHtmlDialogCancel: function (dialog) {
        dialog.close();
    },
    onCreatePdfButtonClick: function (eventObject) {
        var requestId = Mate.Selector(eventObject.target).closest('button').attr('data-request-id');
        BootstrapDialog.show({
            title: 'Запрос',
            closable: false,
            draggable: true,
            requestId: requestId,
            type: BootstrapDialog.TYPE_DANGER,
            message: 'Подготовить PDF файл?<br><input type="checkbox">Сохранить не открывая</input>',
            buttons: [
                {label: 'Да', action: this.onCreatePdfDialogOk},
                {label: 'Нет', action: this.onCreatePdfDialogCancel}
            ]
        });
        return false;
    },
    onCreatePdfDialogOk: function (dialog) {
        var template = Mate.Application.getResourceContent('reportRequestJs');
        var request = Mate.Application.getStoreByName('RequestStore').lookup('id', parseInt(dialog.options.requestId));
        pdfMake.fonts = {
            Roboto: {normal: 'Roboto-Regular.ttf', bold: 'Roboto-Medium.ttf', italics: 'Roboto-Italic.ttf', bolditalics: 'Roboto-MediumItalic.ttf'},
            Impact: {normal: 'Impact.ttf', bold: 'Impact.ttf', italics: 'Impact.ttf', bolditalics: 'Impact.ttf'},
            Georgia: {normal: 'Georgia-R.ttf', bold: 'Georgia-B.ttf', italics: 'Georgia-I.ttf', bolditalics: 'Georgia-BI.ttf'}
        };
        Mate.ux.getBase64FromImageUrl('/api/v1/getthumbnail/' + request.get('missingPerson.pictureId'), function (picture) {
            var pdfDefinition = eval(template)[0];
            pdfMake.createPdf(pdfDefinition).open();
            dialog.close();
        });
    },
    onCreatePdfDialogCancel: function (dialog) {
        dialog.close();
    },
    onBtnShowExtraParamsClick: function () {
        Mate.Selector('#requestSearchExtraParams').toggle();
    },
    onBtnRequestNewClick: function () {
        Mate.Selector('#modalPlaceholder').html(Mate.Application.getResourceContent('formRequest'));
        Mate.Selector('#requestNewForm').modal('show');
        Mate.Application.getControllerByName('Application.controller.RequestFormController').init();
    },
    onRequestCardClick: function (eventObject) {
        var requestId = Mate.Selector(eventObject.target).closest('div[data-request-id]').attr('data-request-id');
        Mate.Selector('#modalPlaceholder').html(Mate.Application.getResourceContent('formRequest'));
        Mate.Selector('#requestNewForm').modal('show');
        Mate.Application.getControllerByName('Application.controller.RequestFormController').requestId = requestId;
        Mate.Application.getControllerByName('Application.controller.RequestFormController').init();
    },
    prepareFilters: function() {
        return {
            nontrivial: true,
            query: Mate.Selector('#requestSearchQuery').val(),
            distance: Mate.Selector('#requestSearchDistance').data("ionRangeSlider").result.from,
            ageFrom: Mate.Selector('#requestSearchAge').data("ionRangeSlider").result.from,
            ageTo: Mate.Selector('#requestSearchAge').data("ionRangeSlider").result.to,
            sex: Mate.Selector('input[name="requestSearchSex"]:checked').val(),
            latitude: ymaps.geolocation.latitude,
            longitude: ymaps.geolocation.longitude,
            urgency: Mate.Selector('input[name="urgentRequests"]').is(':checked') ? 2 : 1,
            healthTroubles: Mate.Selector('input[name="healthTroubles"]').is(':checked') ? 1 : 0
        };
    },
    onBtnSearchRequestClick: function(eventObject) {
        if (eventObject.type === 'keyup' && eventObject.keyCode !== 13)
            return false;
        this.requests.onDataLoaded = Mate.Selector.proxy(this.onRequestsLoaded, this);
        this.requests.setFilters(this.prepareFilters());
        this.requests.load();
    }
});

