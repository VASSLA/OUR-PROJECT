/* global Mate */
Mate.define('Application.store.LogStore', {
    autoLoad: false,
    extends: 'Mate.data.Store',
    model: 'Application.model.LogModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/log'
    })
});
