/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: FeedbackMessage.js
 * Date: Jan 9, 2017 9:39:20 AM
 * Author: tel-f */

/* global Mate */
Mate.define('Application.model.FeedbackMessageModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'createDate',
            caption: 'Дата и время',
            description: 'Дата и время создания сообщения',
            type: 'datetime',
            size: 0,
            validator: 'datetime'
        },
        {
            name: 'closeDate',
            caption: 'Дата и время',
            description: 'Дата и время закрытия сообщения',
            type: 'datetime',
            size: 0,
            validator: 'datetime'
        },
        {
            name: 'feedbackTypeId',
            caption: 'Идентификатор типа сообщения',
            description: 'Уникальный идентификатор типа сообщения',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'text',
            caption: 'Сообщение',
            description: 'Текст сообщения',
            type: 'string',
            size: 0,
            validator: 'string.safe'
        },
//        {
//            name: 'initiatorId',
//            caption: 'Идентификатор нициатора',
//            description: 'Уникальный идентификатор инициатора',
//            type: 'integer',
//            size: 0,
//            validator: 'number.integer'
//        },
//        {
//            name: 'performerId',
//            caption: 'Идентификатор исполнителя',
//            description: 'Уникальный идентификатор исполнителя',
//            type: 'integer',
//            size: 0,
//            validator: 'number.integer'
//        },
        {
            name: 'initiator',
            caption: 'Пользователь',
            description: 'Пользователь',
            type: 'model',
            mapAs: 'Application.model.UserModel',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'performer',
            caption: 'Пользователь',
            description: 'Пользователь',
            type: 'model',
            mapAs: 'Application.model.UserModel',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'rejectReason',
            caption: 'Причина отклонения',
            description: 'Причина отклонения сообщения',
            type: 'string',
            size: 0,
            validator: 'string.safe'
        },
        {
            name: 'feedbackStatusId',
            caption: 'Идентификатор состояния сообщения',
            description: 'Уникальный идентификатор состояния сообщения',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
    ]
});

