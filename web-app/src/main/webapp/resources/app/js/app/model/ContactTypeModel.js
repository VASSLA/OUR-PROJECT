/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ContactTypeModel.js
 * Date: Nov 17, 2016 9:32:08 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.ContactTypeModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'name',
            caption: 'Тип контакта',
            description: 'Тип контакта',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'validatorId',
            caption: 'Идентификатор валидатора',
            description: 'Идентификатор валидатора, которым проверяется поле',
            type: 'number',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'validator',
            caption: 'Валидатор',
            description: 'Объект валидатора',
            type: 'model',
            mapAs: 'Application.model.ValidatorModel',
            size: 0,
            validator: 'string.string'
        }
    ],
    keyField: 'id'
});
