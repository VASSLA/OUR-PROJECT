/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: FeedbackTypeModel.js
 * Date: Feb 18, 2017 9:44:01 PM
 * Author: User */

/* global Mate */
Mate.define('Application.model.FeedbackTypeModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'name',
            caption: 'Описание',
            description: 'Описание',
            type: 'string',
            size: 0,
            validator: 'string.safe'
        }
    ],
    keyField: 'id'
});


