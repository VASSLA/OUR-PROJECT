/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: SettingModel.js
 * Date: Nov 30, 2016 12:05:23 AM
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.SettingModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'name',
            caption: 'Имя настройки',
            description: 'Имя настройки',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'value',
            caption: 'Значение настройки',
            description: 'Значение настройки',
            type: 'string',
            size: 255,
            validator: 'string.string'
        },
        {
            name: 'description',
            caption: 'Описание',
            description: 'Описание настройки',
            type: 'string',
            size: 0,
            validator: 'string.string'
        }
    ],
    keyField: 'id'
});
