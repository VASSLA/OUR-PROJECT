/* global Mate */
Mate.define('Application.store.LogLevelStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.LogLevelModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/log/levels'
    })
});
