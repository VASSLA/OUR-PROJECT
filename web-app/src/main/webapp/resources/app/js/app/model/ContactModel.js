/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ContactModel.js
 * Date: Nov 17, 2016 9:42:15 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.ContactModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'typeId',
            caption: 'Идентификатор типа контакта',
            description: 'Уникальный идентификатор типа контакта',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'personId',
            caption: 'Идентификатор персоны',
            description: 'Уникальный идентификатор персоны, которой принадлежит контакт',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'contact',
            caption: 'Контакт',
            description: 'Текст контакта',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'contactType',
            caption: 'Тип контакта',
            description: 'Объект типа контакта',
            type: 'model',
            mapAs: 'Application.model.ContactTypeModel',
            size: 0,
            validator: 'object.json'
        },
        {
            name: 'defaultContact',
            caption: 'Контакт по умолчанию',
            description: 'Контакт по умолчанию',
            type: 'boolean',
            size: 0,
            validator: 'boolean.boolean'
        }
    ],
    keyField: 'id'
});
