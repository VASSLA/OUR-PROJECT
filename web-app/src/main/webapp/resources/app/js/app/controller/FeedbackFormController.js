/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: FeedbackFormController.js
 * Date: Jan 8, 2017 11:42:47 PM
 * Author: tel-f */

/* global Mate */
Mate.define('Application.controller.FeedbackFormController', {
    feedbackmessage: null,
    edit: false,
    extends: 'Mate.app.Controller', 
    interactives: {
        '#btnCancelFeedback': {'click': 'onBtnCancelFeedbackClick'},
        '#btnSendFeedback': {'click': 'onBtnSendFeedbackClick'}
    },
    afterInit: function () {
        if (this.edit) {
            Mate.Selector('#feedbackFormTitle').html('Сообщение обратной связи');
            Mate.Selector.ajax({
                url: '/api/v1/feedbackById',
                method: 'GET',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: { id : Mate.Application.getControllerByName('Application.controller.FeedbackController').feedbackId },
                success: function(data) {
                    debugger;
                    Mate.Selector('#feedbackmessageText').val(data.text);
                    Mate.Selector('#feedbackmessageText').attr('disabled', true);
                    Mate.Selector('#feedbackmessageType').val(data.feedbackTypeId);
                    Mate.Selector('#feedbackmessageType').attr('disabled', true);
                    Mate.Selector('#btnFeedbackMessageFile').addClass('hidden');
                    
                    var files = "";
                    
                    for(var i = 0; i < data.feedbackAttachments.length; i ++) {
                        var file = data.feedbackAttachments[i];
                        files += ' <a href="/api/v1/getFeedbackFile/' + file.id +  '" target="_blank">' + file.initialName + '</a><br>'
//                        file.initialName
                    } 
  //                 <!--<a href="/api/v1/getFeedbackFile/" target="_blank">Download</a>-->
                   
                    
                    Mate.Selector('#feedbackmessagefilelabel').html(files);
                
                    Mate.Selector('#btnSendFeedback').html('Ок');
                },
                error: function() {
                    Mate.Logger.error('Failed to retrieve feedback messages');

                }
            });
        }
    },
    onBtnCancelFeedbackClick: function () {
        Mate.Application.getControllerByName('FeedbackFormController').deinit();
        Mate.Selector('#feedbackmessageNewForm').modal('hide');
    },
    onBtnSendFeedbackClick: function() {
        if (this.edit) {
            Mate.Selector('#feedbackmessageNewForm').modal('hide');
        } else {
            var feedbackModel = {
                text: Mate.Selector('#feedbackmessageText').val(),
                feedbackTypeId: Mate.Selector('#feedbackmessageType').val(),
                feedbackStatusId: 1,
                initiator: Mate.Application.currentUser.raw
            };
            var request = new FormData();
            request.append('feedback', new Blob([JSON.stringify(feedbackModel)], {type: 'application/json'}));
            var files = Mate.Selector('#btnFeedbackMessageFile')[0].files;
            for (var index in files) {
                request.append('files', files[index]);
            }
            Mate.Selector.ajax({
                url: '/api/v1/feedback',
                method: 'POST',
                dataType: 'json',
                enctype: 'multipart/form-data;charset=UTF-8',
                processData: false,
                contentType: false,
                data: request,
                success: function (data) {
                    Mate.Application.getControllerByName('FeedbackFormController').deinit();
                    Mate.Selector('#feedbackmessageNewForm').modal('hide');
                    Mate.Application.getControllerByName('FeedbackController').loadFeedbackMessages();
                    Mate.Application.getControllerByName('FeedbackController').loadFeedbackMessagesCount();
                },
                error: function(data) {
                    alert('Не удалось отправить сообщение обратной связи!');
                }
            });
        }
    }
});
