/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: EventsController.js
 * Date: Sep 8, 2016 10:11:36 PM
 * Author: anikeale */

/* global Mate */
Mate.define('Application.controller.EventsController', {
    extends: 'Mate.app.Controller',
    interactives: {
        '#newEventTypeColorChooser li a': {'click': 'onNewEventTypeColorChooserClick'},
        '#btnAddEventType': {'click': 'onAddEventTypeClick'}
    },
    calendar: null,
    afterInit: function () {
        Mate.Selector('#desktopHeader').html('События');

        this.initializeCalendar();
        this.appendEventTypes();
//  Более не нужно, загрузятся сами после рендера календаря      
//        this.loadEvents();
    },
    appendEventTypes: function () {
        var eventTypeStore = Mate.Application.getStoreByName('Application.store.EventTypeStore');
        for (var i in eventTypeStore.getData()) {
            var eventType = eventTypeStore.getData(i);
            var active = eventType.get("active");
            if (active) {
                var name = eventType.get("name");
                var color = eventType.get("color");
                var id = eventType.get("id");
                this.appendEventType(name, color, id);
            }
        }
    },
    appendEventType: function (name, color, id) {
        var html = Mate.Application.getResourceContent('blockEventType');

        html = html.replace(/{eventName}/g, name);
        html = html.replace(/{eventTypeId}/g, id);
        html = html.replace(/eventColor/g, color);

        var div = $(html);

        Mate.Selector("#external-events").append(div);

        var eventObject = {
            title: name,
            typeId: id
        };

        $(div).data('eventObject', eventObject);

        $(div).draggable({
            zIndex: 1070,
            revert: true,
            revertDuration: 0,
            start: this.onEventTypeStartDrag,
            stop: this.onEventTypeEndDrag
        });

        Mate.Selector('#deleteEventType').droppable({
            drop: this.onEventTypeDrop
        });
    },
    appendEvents: function () {
        var eventStore = Mate.Application.getStoreByName('EventStore');
        for (var i in eventStore.getData()) {
            var event = eventStore.getData(i);
            this.appendEvent(event);
        }
    },
    appendEvent: function (eventModel) {
        var eventName = eventModel.get("message");
        var eventStartDate = new Date(eventModel.get("dateStart"));
        var eventEndDate = new Date(eventModel.get("dateEnd"));
        var color = eventModel.get("color");
        if ((color === null) || (color === '')) {
            color = "#f39c12";
        }

        var newEvent = {
            title: eventName,
            typeId: eventModel.get("typeId"),
            start: eventStartDate,
            end: eventEndDate,
            allDay: eventModel.get("wholeDay"),
            backgroundColor: color,
            borderColor: color,
            eventId: eventModel.get("id")
        };

        this.calendar.fullCalendar('renderEvent', newEvent, true);
    },
    appendEventById: function (eventId) {
        var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
        for (var i in eventStore.getData()) {
            var event = eventStore.getData(i);
            var id = event.get("id");
            if (id === eventId) {
                this.appendEvent(event);
                break;
            }
        }
    },
    loadEvents: function (view) {
        var me = Mate.Application.getControllerByName('EventsController');
        if (me.calendar) {
            me.calendar.fullCalendar('removeEvents');
        }
        
        var dateStart = view.start;
        var dateEnd = view.end;

        var eventStore = Mate.Application.getStoreByName('EventStore');
        eventStore.setFilters({
            'dateStart': dateStart,
            'dateEnd': dateEnd
        });
        
        eventStore.onDataLoaded = function () {
            this.onDataLoaded = null;
            me.appendEvents();
        };
        eventStore.load();
    },
    removeEvent: function (id) {
        this.calendar.fullCalendar('removeEvents', [id]);
    },
    storeEvent: function (data) {
        var eventController = this;

        var event = {
            id: null,
            userId: Mate.Application.currentUser.get('id'),
            typeId: data.typeId,
            message: data.message,
            dateStart: data.start,
            dateEnd: data.end,
            wholeDay: data.allDay ? 1 : 0,
            color: data.color
        };

        Mate.Selector.ajax({
            url: '/api/v1/event',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(event),
            success: function (data) {
                var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
                eventStore.onDataLoaded = function () {
                    this.onDataLoaded = null;
//                    eventController.removeEvent(me.currentEvent._id);
                    eventController.loadEvents();
//                    eventController.appendEventById(me.currentEvent.eventId);
                };
                eventStore.load();
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    initializeCalendar: function () {
        var me = this;

        /*
         var date = new Date();
         var d = date.getDate(),
         m = date.getMonth(),
         y = date.getFullYear();
         */
        this.calendar = $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'cегодня',
                month: 'месяц',
                week: 'неделя',
                day: 'день'
            },
            views: {
                month: {
//                    titleFormat: 'YYYY, MM, DD'
                },
                week: {
                    columnFormat: 'ddd DD.MM'
                },
                day: {
                    columnFormat: 'dddd'
                }
            },
            defaultTimedEventDuration: '01:00:00',
            timezone: 'local',
            eventClick: me.onEditEventFormClick,
            eventDrop: me.onEventDrop,
            eventResize: me.onEventResize,
            timeFormat: 'HH:mm',
            slotLabelFormat: 'HH:mm',
            allDayText: 'весь день',
            locale: 'ru',
            //Default events
            events: [],
            eventAfterRender: function (event, element, view) {
            },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date) { // this function is called when something is dropped
                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                var startDate = date._d;
                copiedEventObject.start = startDate;
                copiedEventObject.allDay = false;
                copiedEventObject.backgroundColor = $(this).css('background-color');
                copiedEventObject.borderColor = $(this).css('border-color');

                var defaultDuration = me.calendar.fullCalendar('option', 'defaultTimedEventDuration');
                var numbers = defaultDuration.split(':');
                var hours = numbers[0], minutes = numbers[1], seconds = numbers[2];
                var endDate = new Date(startDate.getTime() + (hours * 60 * 60 * 1000) + (minutes * 60 * 1000) + (seconds * 1000));

                copiedEventObject.end = endDate;

                /*
                 // render the event on the calendar
                 // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                 $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                 
                 // is the "remove after drop" checkbox checked?
                 if ($('#drop-remove').is(':checked')) {
                 // if so, remove the element from the "Draggable Events" list
                 $(this).remove();
                 }
                 */

                copiedEventObject.backgroundColor = me.hexColor(copiedEventObject.backgroundColor);
                copiedEventObject.borderColor = me.hexColor(copiedEventObject.borderColor);
                me.onEditEventFormClick(copiedEventObject);
            },
            viewRender: function(view) {
                me.loadEvents(view);
            }
        });
    },
    hexColor: function (colorval) {
        var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        delete(parts[0]);

        var result = '#';
        for (var i = 1; i <= 3; ++i) {
            parts[i] = parseInt(parts[i]);
            parts[i] = parts[i].toString(16);
            if (parts[i].length === 1) {
                parts[i] = '0' + parts[i];
            }
            result += parts[i];
        }
        return result;
    },
    onEditEventFormClick: function (event, jsEvent, view) {
//        Mate.Application.createEvent();
//        return;
        var form = Mate.Application.getResourceContent('formEventEditor');
        Mate.Selector('#modalPlaceholder').html(form);
        Mate.Selector('#eventEditorModalForm').modal('show');

        Mate.Application.getControllerByName('Application.controller.EventFormController').currentEvent = event;
        Mate.Application.getControllerByName('Application.controller.EventFormController').init();
    },
    onEventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
        var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
        var eventModel = eventStore.getDataByKeyFieldValue(event.eventId);
        if (eventModel && (eventModel !== null)) {
            eventModel.set('dateStart', eventModel.get('dateStart') + delta);
            eventModel.set('dateEnd', eventModel.get('dateEnd') + delta);
            eventModel.getStore().sync();
        }
    },
    onEventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
        var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
        var eventModel = eventStore.getDataByKeyFieldValue(event.eventId);
        if (eventModel && (eventModel !== null)) {
            eventModel.set('dateEnd', eventModel.get('dateEnd') + delta);
            eventModel.getStore().sync();
        }
    },
    onNewEventTypeColorChooserClick: function (context) {
        context.preventDefault();
        var color = Mate.Selector(context.target).css('color');
        Mate.Selector('#btnAddEventType').css({"background-color": color, "border-color": color});
    },
    onAddEventTypeClick: (function (context) {
        var newEventTypeName = $('#newEventTypeName').val();
        if (newEventTypeName.length == 0) {
            return;
        }

        var color = Mate.Selector(context.target).css('background-color');

        var eventType = {
            name: newEventTypeName,
            description: newEventTypeName,
            active: 1,
            color: this.hexColor(color)
        };

        var me = this;
        Mate.Selector.ajax({
            url: '/api/v1/eventType',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(eventType),
            success: function (data) {
                var eventTypeStore = Mate.Application.getStoreByName('Application.store.EventTypeStore');
                eventTypeStore.onDataLoaded = function () {
                    this.onDataLoaded = null;
                    me.appendEventType(data.name, data.color, data.id);
                    Mate.Selector('#newEventTypeName').val('');
                };
                eventTypeStore.load();
            },
            error: function (data) {
                console.log(data);
            }
        });
    }),
    onEventTypeStartDrag: function (context, ui) {
        Mate.Selector('#deleteEventType').show();
    },
    onEventTypeEndDrag: function (context, ui) {
        Mate.Selector('#deleteEventType').hide();
    },
    onEventTypeDrop: function (context, ui) {
        var eventObject = Mate.Selector(ui.draggable).data().eventObject;
        var eventTypeId = eventObject.typeId;

        Mate.Selector.ajax({
            url: '/api/v1/eventType/' + eventTypeId,
            method: 'DELETE',
            success: function (data) {
                var eventTypeStore = Mate.Application.getStoreByName('Application.store.EventTypeStore');

                eventTypeStore.onDataLoaded = function () {
                    this.onDataLoaded = null;
                    Mate.Selector(ui.draggable).remove();
                };
                eventTypeStore.load();
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});

