/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: CoordinatorRequestStore.js
 * Date: 16.01.2017 19:17:45
 * Author: Никита */

/* global Mate */
Mate.define('Application.store.CoordinatorRequestStore', {
    autoLoad: false,
    extends: 'Mate.data.Store',
    model: 'Application.model.CoordinatorRequestModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/request'
    })
});
