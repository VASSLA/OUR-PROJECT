/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: SystemSettingsController.js
 * Date: Sep 20, 2016 10:11:36 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.RequestCardController', {
    extends: 'Mate.app.Controller',
    requestId: 0,
    template: '',
    afterInit: function () {
        Mate.Selector('#desktopHeader').html('Работа с заявкой');
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '10%' // optional
        });
        this.loadTemplate();
    },
    beforeDeinit: function() {
        Mate.Application.getControllerByName('Application.controller.RequestCardMapsController').deinit();
    },
    loadTemplate: function() {
        Mate.Selector.ajax({
            url: '/requestcard',
            context: this,
            method: 'GET',
            success: function(template) {
                this.template = template;
                var requestStore = Mate.Application.getControllerByName('Application.controller.CoordinatorController').store;
                var request = requestStore.lookup('id', this.requestId);
                var templateWithData = this.applyRequestData(request);
                Mate.Selector("#content-wrapper").html(templateWithData);
                this.initializeMap();
            }
        });
    },
    applyRequestData: function(request) {
        var card = this.template;
        var missingPersonBirthDate = request.get('missingPerson.birthDate');
        var missingPersonAddresses = request.get('missingPerson.addresses');

        card = card.replace(/{lastName}/g, request.get('missingPerson.lastName'));
        card = card.replace(/{firstName}/g, request.get('missingPerson.firstName'));
        card = card.replace(/{middleName}/g, request.get('missingPerson.middleName'));
        card = card.replace(/{age}/g, this.birthDateToAge(missingPersonBirthDate));
        card = card.replace(/{dressedIn}/g, request.get('dressedIn'));
        card = card.replace(/{appearance}/g, request.get('appearance'));
        card = card.replace(/{missingFrom}/g, request.get('missingFrom'));

        if(missingPersonAddresses.length > 0) {
           card = card.replace(/{missingPersonAddress}/g, missingPersonAddresses[0].address);
        }

        return card;
    },
    initializeMap: function() {
        Mate.Application.getControllerByName('Application.controller.RequestCardMapsController').init();
    },
    birthDateToAge: function(birthDate) {
        birthDate = new Date(birthDate);
        var now = new Date(),
            age = now.getFullYear() - birthDate.getFullYear();
        age = now.setFullYear(1972) < birthDate.setFullYear(1972) ? age - 1 : age;
        return age + ' ' + this.getNumEnding(age, ['год', 'года', 'лет']);
    },
    getNumEnding: function (number, endings) {
        var ending, i;
        number = number % 100;
        if (number>=11 && number<=19) {
            ending=endings[2];
        }
        else {
            i = number % 10;
            switch (i)
            {
                case (1): ending = endings[0]; break;
                case (2):
                case (3):
                case (4): ending = endings[1]; break;
                default: ending = endings[2];
            }
        }
        return ending;
    }
});

