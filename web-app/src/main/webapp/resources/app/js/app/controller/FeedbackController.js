/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: FeedbackController.js
 * Date: Sep 20, 2016 10:12:27 PM
* Author: Alex */

/* global Mate */
Mate.define('Application.controller.FeedbackController', {
    extends: 'Mate.app.Controller',
    FeedbackMessageList: null,
    page: 1,
    limit: 12,
    allCount: 0,
    feedbackId: 0,
    interactives: {
        '#btnFeedbackNew': {'click': 'onBtnFeedbackClick'},
        '#btnFeedbackLeft': {'click': 'onBtnFeedbackLeftClick'},
        '#btnFeedbackRight': {'click': 'onBtnFeedbackRightClick'},
        '[data-feedback-id]': {'click': 'onFeedbackClick'}
    },    
    onBtnFeedbackClick: function () {
       Mate.Selector.ajax({
            url: '/form/feedbackmessage',
            method: 'GET',
            success: function(data) {
                Mate.Selector('#modalPlaceholder').html(data);
                Mate.Selector('#feedbackmessageNewForm').modal('show');
                Mate.Application.getControllerByName('Application.controller.FeedbackFormController').edit = false;
                Mate.Application.getControllerByName('Application.controller.FeedbackFormController').init();
            }
        });
    },
    onBtnFeedbackLeftClick: function () {
        if (this.page > 1) {
            this.page -= 1;
        }
        this.loadFeedbackMessages();
    },
    onBtnFeedbackRightClick: function () {
        if ((this.page) * this.limit <= this.allCount) {
            this.page += 1;
        }
        this.loadFeedbackMessages();
    },
    afterInit: function() {
        Mate.Selector('#desktopHeader').html('Обратная связь');
        this.loadFeedbackMessages();
        this.loadFeedbackMessagesCount();
    },
    loadFeedbackMessages: function() {
        var filters = {
            start: this.page * this.limit - this.limit,
            limit: this.limit,
            distinct: 'id'
        }
        Mate.Selector.ajax({
            url: '/api/v1/feedback',
            method: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: filters,
            success: function(data) {
                var FeedbackMessageStore = Mate.Application.getStoreByName('FeedbackMessageStore');
                FeedbackMessageStore.clear();
                if (data.length > 0) {
                   for (var i = 0; i < data.length; i++) {
                       FeedbackMessageStore.addModelFromRaw(data[i]);
                   }
                }
                Mate.Application.getControllerByName('Application.controller.FeedbackController').pageCountReload();
                Mate.Application.getControllerByName('Application.controller.FeedbackController').FeedbackMessageList = FeedbackMessageStore.getData();  
                Mate.Application.getControllerByName('Application.controller.FeedbackController').applyDataToForm();
            },
            error: function() {
                Mate.Logger.error('Failed to retrieve feedback messages');  
            }
        });
    },
    loadFeedbackMessagesCount: function() {
       Mate.Selector.ajax({
            url: '/api/v1/feedbackCount',
            method: 'GET',
            contentType: 'text/html; charset=utf-8',
            context: this,
            success: function(data) { 
                Mate.Selector('#newFeedbackNum').html(data.new);
                Mate.Selector('#acceptedFeedbackNum').html(data.accepted);
                Mate.Selector('#closedFeedbackNum').html(data.closed);
                Mate.Selector('#rejectedFeedbackNum').html(data.rejected);
                Mate.Selector('#processingFeedbackNum').html(data.processing);
                Mate.Application.getControllerByName('Application.controller.FeedbackController').allCount = data.all;
                Mate.Application.getControllerByName('Application.controller.FeedbackController').pageCountReload();
            }
        });    
    },
    pageCountReload: function() {
        var startCount = (this.page * this.limit - this.limit + 1);
        var secondCount = 0;
        if (this.page * this.limit > this.allCount) {
            secondCount = this.allCount;
        } else {
            secondCount = (this.page * this.limit);
        }
        Mate.Selector('#feedbackPageCount').html(startCount + '-' + secondCount + '/' + this.allCount);
    },
    applyDataToForm: function() {
        //clear
        Mate.Selector("#feedbackTable tbody").html('');
        for (var i in this.FeedbackMessageList) {
            var feedbackMessage = this.FeedbackMessageList[i];
            this.applyElementToForm(feedbackMessage);
        }
        Mate.Selector('tr[data-feedback-id]').on('click', this, this.onFeedbackClick);
    },
    applyElementToForm: function(feedbackMessage) {
        var initiator = feedbackMessage.getFieldByName("initiator").getValue();
        var createDate = new Date(feedbackMessage.getFieldByName("createDate").getValue());
        var feedbackAttachments = feedbackMessage.raw.feedbackAttachments;
        
        switch(feedbackMessage.getFieldByName("feedbackStatusId").getValue()) {
            case 1: 
                feedbackStatusId = '<i class="fa fa-envelope"></i>';
                break;
            case 2: 
                feedbackStatusId = '<i class="fa fa-envelope-open"></i>';
                break;
            case 3: 
                feedbackStatusId = '<i class="fa fa-gavel"></i>';
                break;
            case 4: 
                feedbackStatusId = '<i class="fa fa-child"></i>';
                break;
            case 5: 
                feedbackStatusId = '<i class="fa fa-ban"></i>';
                break;
        }
        
        switch(feedbackMessage.getFieldByName("feedbackTypeId").getValue()) {
            case 1: 
                feedbackTypeId = '<i class="fa fa-circle-o text-red"></i>';
                break;
            case 2: 
                feedbackTypeId = '<i class="fa fa-circle-o text-yellow"></i>';
                break;
            case 3: 
                feedbackTypeId = '<i class="fa fa-circle-o text-light-blue"></i>';
                break;
            case 4: 
                feedbackTypeId = '<i class="fa fa-circle-o text-gray"></i>';
                break;
        }
//        var feedbackTypeId = feedbackMessage.getFieldByName("feedbackTypeId").getValue();
        var feedbackText = feedbackMessage.getFieldByName("text").getValue();
        var feedbackId = feedbackMessage.getFieldByName("id").getValue();
        
        var attached = "";
        if (feedbackAttachments.length > 0) {
            attached = '<i class="fa fa-paperclip">'
        }

        var createDateStr = ("0" + createDate.getDate()).slice(-2) + "." + ("0" + (createDate.getMonth() + 1)).slice(-2) + "." + createDate.getFullYear() + " " ;
//                ("0" + createDate.getHours()).slice(-2) + ":" + ("0" + createDate.getMinutes()).slice(-2) + ":" + ("0" + createDate.getSeconds()).slice(-2);
        Mate.Selector("#feedbackTable tbody").append(
            "<tr data-feedback-id=" + feedbackId + ">" +
                "<td>" + feedbackTypeId + "</td>" +
                "<td>" + feedbackMessage.getFieldByName("id").getValue() + "</td>" +
                "<td>" + initiator.getFieldByName("login").getValue() + "</td>" +
                "<td>" + feedbackText + "</td>" +
                "<td>" + feedbackStatusId + "</td>" +
                "<td>" + attached + "</td>" +
                "<td>" + createDateStr + "</td>" +
            "</tr>");
    },
    onFeedbackClick: function (eventObject) {
        var feedbackId = Mate.Selector(eventObject.target).closest('tr[data-feedback-id]').attr('data-feedback-id');
        Mate.Selector.ajax({
            url: '/form/feedbackmessage',
            method: 'GET',
            success: function(data) {
                Mate.Selector('#modalPlaceholder').html(data);
                Mate.Selector('#feedbackmessageNewForm').modal('show');
                Mate.Application.getControllerByName('Application.controller.FeedbackController').feedbackId = feedbackId;
                Mate.Application.getControllerByName('Application.controller.FeedbackFormController').edit = true;
                Mate.Application.getControllerByName('Application.controller.FeedbackFormController').init();
            }
        });
    }
});

