/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestFormController.js
 * Date: Oct 25, 2016 9:40:15 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.controller.RequestFormController', {
    currenrtUserStore: null,
    request: null,
    address: null,
    person: null,
    declarant: null,
    extends: 'Mate.app.Controller',
    extraPanShown: false,
    currentRequestStore: null,
    interactives: {
        '#btnCancelRequest': {'click': 'onBtnCancelRequestClick'},
        '#btnSendRequest': {'click': 'onBtnSaveRequestClick'},
        '#requestAddressBook': {'click': 'onRequestAddressBookClick'},
        '#requestContactBook': {'click': 'onRequestContactBookClick'},
        '#requestPhoto': {'click': 'onRequestPhotoClick'},
        '#requestLastSeenPlace': {'blur': 'onRequestLastSeenPlaceBlur'},
        '#requestLastSeenPlaceSelect': {'click': 'onRequestLastSeenPlaceSelectClick'}
    },
    mixins: [
        'Mate.mixin.JsonStringifier'
    ],
    beforeDeinit: function () {
        this.requestId = null;
        delete this.currentRequestStore;
    },
    afterInit: function () {
        Mate.Selector('#requestFormHeader').text(this.requestId ?
                'Редактирование заявки на поиск' : 'Новая заявка на поиск');
        var me = this;
        this.prepareFormComponents();
        // Create store for 1 record
        this.currentRequestStore = Mate.create('Application.store.RequestStore', {
            autoLoad: false,
            autoSync: false,
            model: 'Application.model.RequestModel',
            proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
                url: '/api/v1/request'
            })
        });
        // If requestId is set then fill form by data from request
        if (this.requestId) {
            Mate.Application.waitIndicator.show();
            this.currentRequestStore.setFilters({id: this.requestId});
            this.currentRequestStore.onDataLoaded = function () {
                var request = this.getData(0);
                me.fillRequestForm(request);
                Mate.Application.waitIndicator.hide();
            };
            this.currentRequestStore.load();
        } else {
            var request = this.currentRequestStore.newModel();
            Mate.Logger.debug(request);
            var person = Mate.create('Application.model.PersonModel', {});
            var declarant = Mate.Application.currentUser.get('person').getJson();
            request.getFieldByName('missingPerson').setValue(person);
            request.getFieldByName('declarantPerson').setValue(declarant);
        }
    },
    prepareFormComponents: function () {
        // Prepare Date, DateTime fields
        Mate.Selector('#requestDateTime').datetimepicker({defaultDate: 'moment', locale: 'ru'});
        Mate.Selector('#requestMissingFrom').datetimepicker({locale: 'ru'});
        // Prepare checkboxes
        Mate.Selector('#requestForestOnline').iCheck({checkboxClass: 'icheckbox_square-blue', radioClass: 'iradio_square-blue', increaseArea: '20%'});
        // Prepare address fields
        Mate.Selector('#requestAddress').editable({
            title: 'Адрес разыскиваемого',
            placement: 'bottom',
            highlight: false,
            unsavedclass: null,
            value: null,
            success: this.onMissingPersonAddressEntered
        });
        // Prepare missing person data inplace editor
        Mate.Selector('#requestPerson').editable({
            title: 'Введите данные о разыскиваемом',
            placement: 'bottom',
            highlight: false,
            unsavedclass: null,
            value: {lastName: '', firstName: '', middleName: '', birthDate: '', sex: ''},
            success: this.onMissingPersonDataEntered
        });
    },
    onMissingPersonDataEntered: function (response, value) {
        var me = Mate.Application.getControllerByName('RequestFormController');
        var request = me.currentRequestStore.getData(0);
        var person = request.get('missingPerson');
        Mate.Selector('#requestPerson').attr('value', value.lastName + ' ' +
                value.firstName + ' ' + value.middleName + ', ' +
                value.birthDate);
        person.getFieldByName('lastName').setValue(value.lastName);
        person.getFieldByName('firstName').setValue(value.firstName);
        person.getFieldByName('middleName').setValue(value.middleName);
        person.getFieldByName('birthDate').setValue(value.birthDate);
        person.getFieldByName('sex').setValue(value.sex);
    },
    onMissingPersonAddressEntered: function (response, value) {
        var me = Mate.Application.getControllerByName('RequestFormController');
        var request = me.currentRequestStore.getData(0);
        var person = request.get('missingPerson');
        var addresses = person.get('addresses');
        if (!me.checkIsAddressInList(addresses, value)) {
            var address = Mate.create('Application.model.AddressModel', {raw: value});
            address.getFieldByName('defaultAddress').setValue(0);
            addresses.push(address.getJson());
        }
    },
    checkIsAddressInList: function (list, address) {
        if (!(list instanceof Array) || list === null || list === undefined) {
            list = new Array();
        }
        for (var position in list) {
            if (list[position].uniqueId === address.uniqueId) {
                return true;
            }
        }
        return false;
    },
    findFirstOrDefaultAddress: function (list) {
        if (!(list instanceof Array) || list === null || list === undefined)
            return undefined;
        var result = list[0]; // Set first address as a result and try to find default
        for (var position in list) {
            if (list[position].defaultAddress) {
                return list[position];
            }
        }
        return result;
    },
    findFirstOrDefaultContact: function (list) {
        if (!(list instanceof Array) || list === null || list === undefined)
            return undefined;
        var result = list[0]; // Set first contact as a result and try to find default
        for (var position in list) {
            if (list[position].defaultContact) {
                return list[position];
            }
        }
        return result;
    },
    fillRequestForm: function (requestModel) {
        var defaultAddress = this.findFirstOrDefaultAddress(requestModel.get('missingPerson.addresses'));
        if (defaultAddress) {
            Mate.Selector('#requestAddress').editable('setValue', defaultAddress);
        }
        if (requestModel.get('missingPerson.pictureId')) {
            Mate.Selector('#requestPhoto').attr('src', '/api/v1/getthumbnail/' + requestModel.get('missingPerson.pictureId'));
        }
        var defaultContact = this.findFirstOrDefaultContact(requestModel.get('missingPerson.contacts'));
        if (defaultContact) {
            Mate.Selector('#requestPhone').val(defaultContact.contact);
        }
        Mate.Selector('#requestDateTime').data("DateTimePicker").date(requestModel.get('creationDate'));
        Mate.Selector('#requestPerson').editable('setValue', requestModel.get('missingPerson').getJson());
        //FIXME: See how it was maked in Addresses (x-editable), string below will not be needed
        Mate.Selector('#requestPerson').attr('value', requestModel.get('missingPerson.lastName') + ' ' +
                requestModel.get('missingPerson.firstName') + ' ' + requestModel.get('missingPerson.middleName') + ', ' +
                requestModel.get('missingPerson.birthDate'));
        Mate.Selector('#requestLastSeenPlace').val(requestModel.get('lastSeenPlace'));
        Mate.Selector('#requestUrgency').val(requestModel.get('requestTypeId'));
        Mate.Selector('#requestLandscape').val(requestModel.get('localityId'));
        Mate.Selector('#requestMissingFrom').data("DateTimePicker").date(requestModel.get('missingFrom'));
        Mate.Selector('#requestExtraInfo').val(requestModel.get('extraInfo'));
        Mate.Selector('#requestDressedIn').val(requestModel.get('dressedIn'));
        Mate.Selector('#requestCircOfDisapp').val(requestModel.get('circOfDisappear'));
        Mate.Selector('#requestAppearance').val(requestModel.get('appearance'));
        Mate.Selector('#requestHealth').val(requestModel.get('health'));
        Mate.Selector('#requestStuff').val(requestModel.get('stuff'));
        Mate.Selector('#requestForestOnline')[0].checked = requestModel.get('forestOnline');
        Mate.Selector('#requestForestOnline').closest('div').attr('aria-checked', requestModel.get('forestOnline') ? true : false);
        if (requestModel.get('forestOnline')) {
            Mate.Selector('#requestForestOnline').closest('div').addClass('checked');
        }
        // Check that last seen place field is not ambiguous
        if (requestModel.get('latitude') === null && requestModel.get('longitude') === null &&
                requestModel.get('lastSeenPlace') !== null && requestModel.get('lastSeenPlace') !== '') {
            this.onRequestLastSeenPlaceBlur();
        }
    },
    onBtnCancelRequestClick: function () {
        Mate.Application.getControllerByName('Application.controller.RequestFormController').deinit();
    },
    onBtnSaveRequestClick: function (eventObject) {
        Mate.Logger.debug(Mate.Selector('#requestForestOnline')[0].checked);
        var me = eventObject.data;
        var requestController = Mate.Application.getControllerByName('Application.controller.RequestsController');
        var requestStore = Mate.Application.getStoreByName('Application.store.RequestStore');
        var request = me.currentRequestStore.getData(0);
        if (!request.get('id')) {
            isNew = true;
            request.getFieldByName('currentStateId').setValue(1);
            request.getFieldByName('sourceId').setValue(1);
        }
        request.getFieldByName('creationDate').setValue(Mate.Selector('#requestDateTime>input').val());
        request.getFieldByName('requestTypeId').setValue(Mate.Selector('#requestUrgency').val());
        request.getFieldByName('localityId').setValue(Mate.Selector('#requestLandscape').val());
        request.getFieldByName('missingFrom').setValue(Mate.Selector('#requestMissingFrom>input').val());
        request.getFieldByName('lastSeenPlace').setValue(Mate.Selector('#requestLastSeenPlace').val());
        request.getFieldByName('extraInfo').setValue(Mate.Selector('#requestExtraInfo').val());
        request.getFieldByName('dressedIn').setValue(Mate.Selector('#requestDressedIn').val());
        request.getFieldByName('circOfDisappear').setValue(Mate.Selector('#requestCircOfDisapp').val());
        request.getFieldByName('appearance').setValue(Mate.Selector('#requestAppearance').val());
        request.getFieldByName('health').setValue(Mate.Selector('#requestHealth').val());
        request.getFieldByName('stuff').setValue(Mate.Selector('#requestStuff').val());
        request.getFieldByName('forestOnline').setValue(Mate.Selector('#requestForestOnline')[0].checked);
        request.getStore().onSynchronized = function () {
            requestStore.onDataLoaded = function () {
                requestController.loadRequests();
                requestController.displayRequests();
                Mate.Application.getControllerByName('Application.controller.RequestFormController').deinit();
                Mate.Selector('#requestNewForm').modal('hide');
                Mate.Application.waitIndicator.hide();
            };
            requestStore.load();
            this.onSynchronized = null;
        };
        Mate.Application.waitIndicator.show();
        request.getStore().sync();
    },
    onRequestAddressBookClick: function () {
        if (!Mate.Selector('body').is('#addressBookView')) {
            Mate.Selector('body').append(Mate.Application.getResourceContent('formAddressList'));
        }
        Mate.ux.centerBlock('#addressBookView', window, 1055);
        Mate.Selector('#addressBookView').show();
        var addressListController = Mate.Application.getControllerByName('AddressListController');
        addressListController.addresses = this.currentRequestStore.getData(0).get('missingPerson.addresses');
        addressListController.init();
    },
    onRequestContactBookClick: function () {
        Mate.Selector('body').append(Mate.Application.getResourceContent('formContactList'));
        Mate.ux.centerBlock('#contactsBookView', window, 1055);
        Mate.Selector('#contactsBookView').show();
        var contactListController = Mate.Application.getControllerByName('ContactListController');
        contactListController.contacts = this.currentRequestStore.getData(0).get('missingPerson.contacts');
        contactListController.init();
    },
    onRequestPhotoClick: function () {
        var context = this;
        Mate.Application.openMediaLibrary(function (data) {
            var request = context.currentRequestStore.getData(0);
            Mate.Selector('#requestPhoto').attr('src', '/api/v1/getthumbnail/' + data);
            request.get('missingPerson').set('pictureId', data);
            Mate.Application.getControllerByName('MediaFormController').deinit();
            Mate.Selector('#mediaLibraryModalForm').hide();
            Mate.Selector('#mediaLibraryPlaceholder').html('');
        });
    },
    onRequestLastSeenPlaceBlur: function () {
        var request = this.currentRequestStore.getData(0);
        var address = Mate.Selector('#requestLastSeenPlace').val();
        if ((address && (address !== request.get('lastSeenPlace'))) || (!request.get('latitude') && !request.get('longitude'))) {
           Mate.service.getCoordinates(address, Mate.service.GeocodingProvider.YANDEX, Mate.Selector.proxy(this.onGeocoderResponse, this));
        }

    },
    onGeocoderResponse: function (response) {
        var result = response.response.GeoObjectCollection.featureMember;
        Mate.Logger.debug(result);
        if (result.length === 0) {
            Mate.Selector('#requestLastSeenPlace').closest('.form-group').addClass('has-warning');
            return;
        }
        if (result.length === 1) {
            Mate.Selector('#requestLastSeenPlace').closest('.form-group').removeClass('has-warning');
            var coordinates = result[0].GeoObject.Point.pos.split(' ');
            var request = this.currentRequestStore.getData(0);
            request.set('latitude', coordinates[1]);
            request.set('longitude', coordinates[0]);
        } else {
            //var coordinates = result[0].GeoObject.Point.pos.split(' ');
            //var request = this.currentRequestStore.getData(0);
            //request.set('latitude', coordinates[1]);
            //request.set('longitude', coordinates[0]);
            Mate.Selector('#requestLastSeenPlace').closest('.form-group').addClass('has-warning');
        }
        this.lastSeenPlace = result;
    },
    onRequestLastSeenPlaceSelectClick: function() {
        if (!Mate.Selector('body').is('#requestPlaceSelector')) {
            Mate.Selector('body').append(Mate.Application.getResourceContent('formPlaceSelector'));
        }
        Mate.ux.centerBlock('#requestPlaceSelector', window, 1055);
        Mate.Selector('#requestPlaceSelector').show();
        Mate.Application.getControllerByName('PlaceSelectorController').places = this.lastSeenPlace;
        Mate.Application.getControllerByName('PlaceSelectorController').request = this.currentRequestStore.getData(0);
        Mate.Application.getControllerByName('PlaceSelectorController').init();
    }
});
