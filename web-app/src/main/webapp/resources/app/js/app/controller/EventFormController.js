/* global Mate */
Mate.define('Application.controller.EventFormController', {
    extends: 'Mate.app.Controller',
    interactives: {
        '#btnSaveEvent': {'click': 'onSaveEventClick'},
        '#btnRemoveEvent': {'click': 'onRemoveEventClick'},
        '#eventParticipantSearchInput': {'typeahead:selected': 'onSelectContact'},
        '#eventParticipantSearchButton': {'click': 'onAddParticipantButtonClick'},
        '#eventType': {'change': 'onEventTypeSelectionChange'}
    },
    eventTypeList: null,
    currentlySelectedUser: null,
    suggesstion: null,
    participantTemplate: null,
    currentEvent: null,
    afterInit: function() {
        Mate.Selector('#eventStartDate').datetimepicker({defaultDate: 'moment', locale: 'ru'});
        Mate.Selector('#eventEndDate').datetimepicker({defaultDate: 'moment', locale: 'ru'});
        
        this.fillFormWithData();
        this.initializeTypeaheadSearch();
        
        Mate.Selector('#eventWholeDay').iCheck({checkboxClass: 'icheckbox_square-blue', increaseArea: '20%'});
        
        var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
        var eventModel = this.currentEvent ? eventStore.getDataByKeyFieldValue(this.currentEvent.eventId) : null;
        
        if (eventModel && (Mate.Application.currentUser.get('id') != eventModel.get("userId"))) {
            Mate.Selector("#eventEditorModalForm *").prop('disabled', true).off('click');
            Mate.Selector("#btnCancelEventEdit").prop('disabled', false).on('click');
            Mate.Selector("#btnCancelEventEdit").text('Закрыть');
            Mate.Selector("#btnRemoveEvent").hide();
            Mate.Selector("#btnSaveEvent").hide();
        }
    },
    
    initializeTypeaheadSearch: function () {
        this.suggesstion = new Bloodhound({
            datumTokenizer: function (data) {
                return Bloodhound.tokenizers.whitespace(data.login);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function (data) {
                return data.id;
            },
            remote: {
                url: '/api/v1/user?person.lastName=%query&person.firstName=%query&person.middleName=%query&login=%query&method=or&precise=0',
                wildcard: /%query/g
            }
        });
        Mate.Selector('#eventParticipantSearchInput').typeahead({highlight: true}, {
            display: 'login',
            source: this.suggesstion
        });
        Mate.Selector('#eventParticipantSearchInput').css('height', '30px');
//        Mate.Selector('#eventParticipantSearchInput').css('top', '2px');
        Mate.Selector('#eventParticipantSearchInput').css('width', '100%');
    },
    
    fillFormWithData: function() {
        var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
        var eventModel = this.currentEvent ? eventStore.getDataByKeyFieldValue(this.currentEvent.eventId) : null;
        
        var currentUserId = Mate.Application.currentUser.get('id');
        
        Mate.Selector('#btnRemoveEvent').prop('disabled', !eventModel);
        Mate.Selector('#eventMessage').val(this.currentEvent ? this.currentEvent.title: '');
        
        var eventTypeStore = Mate.Application.getStoreByName('Application.store.EventTypeStore');
        for (var i in eventTypeStore.getData()) {
            var eventType = eventTypeStore.getData(i);

            if (eventType.get("active") || (this.currentEvent && (this.currentEvent.typeId == eventType.get("id")))) {
                Mate.Selector('#eventType').append($("<option></option>")
                    .attr("value", eventType.get("id"))
                    .text(eventType.get("name")))
            }
        }
        
        if (this.currentEvent) {
            Mate.Selector('#eventType option[value="' + this.currentEvent.typeId + '"]').css({ 'font-weight': 'bold' });
        }
        
        Mate.Selector('#eventType').val(this.currentEvent ? this.currentEvent.typeId : eventTypeStore.getData(0).get("id"));
        Mate.Selector('#eventColor').val(this.currentEvent ? this.currentEvent.backgroundColor : eventTypeStore.getData(0).get("color"));
        
        var participants = eventModel ? eventModel.get('participants') : [];
        
        Mate.Selector('#eventParticipantList').empty();
        
        var initiatorId = eventModel ? eventModel.get("userId") : currentUserId;
        
        var userStore = Mate.Application.getStoreByName('Application.store.UserStore');
        var personModel = userStore.getDataByKeyFieldValue(initiatorId).get('person');

        this.participantTemplate = Mate.Application.getResourceContent('blockEventParticipant');
        
        var participantImage = personModel.get('lastName') + ' ' + personModel.get('firstName') + ' ' + personModel.get('middleName');
        var html = this.participantTemplate.replace(/{fullName}/g, participantImage );

        var pictureId = personModel.get('pictureId');
        var picture = pictureId ? '/api/v1/getthumbnail/' + pictureId : '/resources/app/img/default-user-avatar.png';
        html = html.replace(/{userId}/g, initiatorId);
        html = html.replace(/{picture}/g, picture);
        html = html.replace(/{nick}/g, userStore.getDataByKeyFieldValue(initiatorId).get('login'));

        html = html.replace(/{btnHidden}/g, 'hidden');
        Mate.Selector('#eventParticipantList').append(html);

        for (var i in participants) {
            var participant = participants[i];
            var canDisable = eventModel ? eventModel.get("userId") == currentUserId : true;
            this.appendParticipant(participant, canDisable);
        }
        
        this.setRemoveButtonEvents();
        
        /*
        Mate.Selector('#eventParticipantList').editable({
            title: 'Адрес',
            placement: 'bottom',
            highlight: false,
            unsavedclass: null,
            value: null,
            success: Mate.Selector.proxy(this.onNewAddressEntered, this)
        });
        */
        
        if (this.currentEvent) {
            Mate.Selector('#eventStartDate').data("DateTimePicker").date(this.currentEvent.start);
            if (this.currentEvent.end === null) {
                this.currentEvent.end = this.currentEvent.start;
            }
            Mate.Selector('#eventEndDate').data("DateTimePicker").date(this.currentEvent.end);
            Mate.Selector('#eventWholeDay').prop("checked", this.currentEvent.allDay);
        }
    },
    
    appendParticipant: function(participant, canRemove) {
        var participantImage = participant.person.lastName + ' ' + participant.person.firstName + ' ' + participant.person.middleName;
        var html = this.participantTemplate.replace(/{fullName}/g, participantImage );

        var pictureId = participant.person.pictureId;
        var picture = pictureId ? '/api/v1/getthumbnail/' + pictureId : '/resources/app/img/default-user-avatar.png';
        html = html.replace(/{userId}/g, participant.id);
        html = html.replace(/{nick}/g, participant.login);
        html = html.replace(/{picture}/g, picture);

        html = html.replace(/{hidden}/g, 'hidden');
        if (!canRemove) {
            html = html.replace(/{btnHidden}/g, 'hidden');
        }
        
        Mate.Selector('#eventParticipantList').append(html);
    },
    
    setRemoveButtonEvents: function() {
        Mate.Selector('#eventParticipantList').find('.tool-btn-close').unbind( "click" );
        Mate.Selector('#eventParticipantList').find('.tool-btn-close').on('click', this, Mate.Selector.proxy(this.removeParticipant, this));
    },
    
    removeParticipant: function(element) {
        $(element.target).closest("li").remove();
    },
    
    onSaveEventClick: function(context) {
        /*
        var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
        var eventId = this.currentEvent.eventId;
        var eventModel = eventId ? eventStore.getDataByKeyFieldValue(eventId) : eventStore.newModel();
        
        console.log(eventModel);
        
        eventModel.set('userId', Mate.Application.currentUser.get('id'));
        eventModel.set('message', Mate.Selector('#eventMessage').val());
        eventModel.set('typeId', this.currentEvent.typeId);
        eventModel.set('dateStart', Mate.Selector('#eventStartDate').data("DateTimePicker").date());
        eventModel.set('dateEnd', Mate.Selector('#eventEndDate').data("DateTimePicker").date());
        eventModel.set('wholeDay', Mate.Selector('#eventWholeDay').prop("checked") === true ? 1 : 0);
        eventModel.set('color', Mate.Selector('#eventColor').val());
        
        var me = this;
        
        eventModel.getStore().onSynchronized = function(result) {
            this.onSynchronized = null;
            var eventController = Mate.Application.getControllerByName('Application.controller.EventsController');

            console.log(result);
            if (me.currentEvent._id) {
                eventController.removeEvent(me.currentEvent._id);
            }
            eventController.appendEventById(eventId);
            
            me.deinit();
            Mate.Selector('#eventEditorModalForm').modal('hide');
        };
        
        eventModel.getStore().sync();
        
        return;
        */
        
        var initiatorId = Mate.Application.currentUser.get('id');
        var userStore = Mate.Application.getStoreByName('Application.store.UserStore');
        var participants = [];
        Mate.Selector('li[participant-id]').each(function(index, elem) {
            var participantId = Number( $(elem).attr('participant-id') );
            if (participantId != initiatorId) {
                var userParticipant = userStore.getDataByKeyFieldValue(participantId);
                participants.push(userParticipant.getJson());
            }
        });
        
        var event = {
            id: this.currentEvent ? this.currentEvent.eventId : null,
            userId: initiatorId,
            message: Mate.Selector('#eventMessage').val(),
            typeId: Mate.Selector('#eventType').val(),
            dateStart: Mate.Selector('#eventStartDate').data("DateTimePicker").date(),
            dateEnd: Mate.Selector('#eventEndDate').data("DateTimePicker").date(),
            wholeDay: Mate.Selector('#eventWholeDay').prop("checked") === true ? 1 : 0,
            color: Mate.Selector('#eventColor').val(),
            participants: participants
        };
        
        var me = this;
        Mate.Selector.ajax({
            url: '/api/v1/event',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(event),
            success: function (data) {
                var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
                var eventController = Mate.Application.getControllerByName('Application.controller.EventsController');
                
                eventStore.onDataLoaded = function() {
                    this.onDataLoaded = null;
                    
                    if ((me.currentEvent) && (me.currentEvent._id)) {
                        eventController.removeEvent(me.currentEvent._id);
                    }
                    
                    eventController.appendEventById(data.id);
                };
                eventStore.load();
                
                me.deinit();
                Mate.Selector('#eventEditorModalForm').modal('hide');
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    
    onRemoveEventClick: function(context) {
        var me = this;
        Mate.Selector.ajax({
            url: '/api/v1/event/' + this.currentEvent.eventId,
            method: 'DELETE',
            success: function (data) {
                var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
                var eventController = Mate.Application.getControllerByName('Application.controller.EventsController');
                
                eventStore.onDataLoaded = function() {
                    this.onDataLoaded = null;
                    if (me.currentEvent._id) {
                        eventController.removeEvent(me.currentEvent._id);
                    }
                };
                eventStore.load();
                
                me.deinit();
                Mate.Selector('#eventEditorModalForm').modal('hide');
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    
    getEventTypeNameById: function(eventTypeId) {
        var eventTypeStore = Mate.Application.getStoreByName('Application.store.EventTypeStore');
        return eventTypeStore.getDataByKeyFieldValue(eventTypeId);
    },
    
    onSelectContact: function (eventObject, data) {
        var me = eventObject.data;
        me.currentlySelectedUser = data;
    },
    
    onAddParticipantButtonClick: function () {
        if (this.currentlySelectedUser == null) {
            return;
        }
        
        var eventStore = Mate.Application.getStoreByName('Application.store.EventStore');
        var eventModel = this.currentEvent ? eventStore.getDataByKeyFieldValue(this.currentEvent.eventId) : null;
        
        if (eventModel) {
            if (this.currentlySelectedUser.id == eventModel.get("userId")) {
                return;
            }
        } else {
            if (this.currentlySelectedUser.id == Mate.Application.currentUser.get('id')) {
                return;
            }
        }
        
        var me = this;
        var alreadyAdded = false;
        Mate.Selector('li[participant-id]').each(function(index, elem) {
            var participantId = $(elem).attr('participant-id');
            if (participantId == me.currentlySelectedUser.id) {
                alreadyAdded = true;
                return;
            }
        });
        
        if (alreadyAdded) {
            return;
        }
        
        this.appendParticipant(this.currentlySelectedUser, true);
        this.currentlySelectedUser = null;
        
        this.setRemoveButtonEvents();
    },
    
    onEventTypeSelectionChange: function(context) {
        var eventType = this.getEventTypeNameById(Number(Mate.Selector(context.target).val()));
        Mate.Selector('#eventColor').val(eventType.get("color"));
    }
});

