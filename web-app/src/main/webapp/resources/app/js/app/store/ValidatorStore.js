/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ValidatorStore.js
 * Date: Nov 17, 2016 9:28:13 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.ValidatorStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.ValidatorModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/validator'
    })
});
