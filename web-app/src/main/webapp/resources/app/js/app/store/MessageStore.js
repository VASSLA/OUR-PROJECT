/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: MessageStore.js
 * Date: Dec 8, 2016 12:10:11 PM
 * Author: anikeale */

Mate.define('Application.store.MessageStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.MessageModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/message'
    })
});

