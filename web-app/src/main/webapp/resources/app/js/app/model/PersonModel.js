/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: PersonModel.js
 * Date: Oct 31, 2016 7:38:32 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.model.PersonModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'firstName',
            caption: 'Имя',
            description: 'Имя',
            type: 'string',
            size: 50,
            validator: 'string.name'
        },
        {
            name: 'middleName',
            caption: 'Отчество',
            description: 'Отчество',
            type: 'string',
            size: 50,
            validator: 'string.name'
        },
        {
            name: 'lastName',
            caption: 'Фамилия',
            description: 'Фамилия',
            type: 'string',
            size: 50,
            validator: 'string.name'
        },
        {
            name: 'birthDate',
            caption: 'Дата рождения',
            description: 'Дата рождения',
            type: 'date',
            size: 0,
            validator: 'datetime.date'
        },
        {
            name: 'sex',
            caption: 'Пол',
            description: 'Пол',
            type: 'integer',
            size: 0,
            validator: 'number.bit'
        },
        {
            name: 'skills',
            caption: 'Навыки',
            description: 'Навыки, которыми обладает человек',
            //type: 'model',
            //mapAs: 'Application.model.SkillModel',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'pictureId',
            caption: 'Идентификатор изображения',
            description: 'Идентификатор изображения пользователя в медиафайлах',
            type: 'integer',
            size: 0,
            validator: 'numeric.integer'
        },
        {
            name: 'addresses',
            caption: 'Адреса',
            description: 'Список адресов',
            type: 'list',
            size: 0,
            validator: 'string.addreses'
        },
        {
            name: 'contacts',
            caption: 'Контакты',
            description: 'Список контактов',
            type: 'list',
            size: 0,
            validator: 'string.contacts'
        }
    ],
    keyField: 'id'
});