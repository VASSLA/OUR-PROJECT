/* global Mate */
Mate.define('Application.model.EventTypeModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Id',
            description: 'Id',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'name',
            caption: 'Имя',
            description: 'Имя',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'description',
            caption: 'Описание',
            description: 'Описание',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'active',
            caption: 'Активный',
            description: 'Активный',
            type: 'boolean',
            size: 0,
            validator: 'boolean.boolean'
        },
        {
            name: 'color',
            caption: 'Цвет',
            description: 'Цвет',
            type: 'string',
            size: 0,
            validator: 'string.string'
        }
    ],
    keyField: 'id'
});
