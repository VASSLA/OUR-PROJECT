/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: CoordinatorRequestModel.js
 * Date: 16.01.2017 19:18:35
 * Author: Никита */

/* global Mate */
Mate.define('Application.model.CoordinatorRequestModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'id',
            caption: 'Идентификатор',
            description: 'Уникальный идентификатор',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'creationDate',
            caption: 'Дата создания',
            description: 'Дата создания ',
            type: 'datetime',
            size: 0,
            validator: 'datetime.date'
        },
        {
            name: 'missingPersonId',
            caption: 'Идентификатор персоны',
            description: 'Уникальный идентификатор разыскиваемой персоны',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'missingFrom',
            caption: 'Разыскивается с',
            description: 'Дата, с которой персона в поиске',
            type: 'datetime',
            size: 0,
            validator: 'datetime.date'
        },
        {
            name: 'lastSeenPlace',
            caption: 'Последний раз видели',
            description: 'Место, где последний раз видели',
            type: 'string',
            size: 255,
            validator: 'string.address'
        },
        {
            name: 'extraInfo',
            caption: 'Дополнительные данные',
            description: 'Особые приметы и другие дополнительные данные',
            type: 'text',
            size: 0,
            validator: 'string.safe'
        },
        {
            name: 'dressedIn',
            caption: 'Одежда',
            description: 'Описание одежды',
            type: 'text',
            size: 0,
            validator: 'string.safe'
        },
        {
            name: 'circOfDisappear',
            caption: 'Обстоятельства пропажи',
            description: 'Обстоятельства пропажи',
            type: 'text',
            size: 0,
            validator: 'string.safe'
        },
        {
            name: 'appearance',
            caption: 'Внешность',
            description: 'Внешность разыскиваемого',
            type: 'text',
            size: 0,
            validator: 'string.safe'
        },
        {
            name: 'health',
            caption: 'Здоровье',
            description: 'Состояние здоровья',
            type: 'text',
            size: 0,
            validator: 'string.safe'
        },
        {
            name: 'stuff',
            caption: 'Вещи',
            description: 'С собой имелось',
            type: 'text',
            size: 0,
            validator: 'string.safe'
        },
        {
            name: 'declarantPersonId',
            caption: 'Идентификатор заявителя',
            description: 'Уникальный идентификатор заявителя',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'requestTypeId',
            caption: 'Идентификатор типа запроса',
            description: 'Уникальный идентификатор типа запроса',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'localityId',
            caption: 'Идентификатор типа местности',
            description: 'Уникальный идентификатор типа местности',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'forestOnline',
            caption: 'Лес на связи',
            description: 'Заблудившийся в лесу находится на связи',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'sourceId',
            caption: 'Идентификатор источника',
            description: 'Уникальный идентификатор источника',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'currentStateId',
            caption: 'Идентификатор состояния',
            description: 'Уникальный идентификатор состояния заявки',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'inforgPersonId',
            caption: 'Идентификатор инфорга',
            description: 'Уникальный идентификатор инфорга',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'coordPersonId',
            caption: 'Идентификатор коорда',
            description: 'Уникальный идентификатор координатора поисков',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'report',
            caption: 'Отчет о поисках',
            description: 'Отчет о процессе поисков',
            type: 'text',
            size: 0,
            validator: 'string.safe'
        },
        {
            name: 'requestType',
            caption: 'Тип заявки',
            description: 'Тип заявки на поиск',
            type: 'object',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'currentState',
            caption: 'Текущее состояние',
            description: 'Текущее состояние заявки',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'source',
            caption: 'Источник заявки',
            description: 'Источник заявки на поиск',
            type: 'model',
            mapAs: 'Application.model.RequestSourceModel',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'missingPerson',
            caption: 'Разыскиваемая персона',
            description: 'Разыскиваемая персона',
            type: 'model',
            mapAs: 'Application.model.PersonModel',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'declarantPerson',
            caption: 'Заявитель',
            description: 'Заявитель о пропаже',
            type: 'object',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'inforgPerson',
            caption: 'Инфорг',
            description: 'Информационный организатор',
            type: 'object',
            size: 0,
            validator: 'object.safe'
        },
        {
            name: 'coordPerson',
            caption: 'Координатор',
            description: 'Координатор поисков',
            type: 'object',
            size: 0,
            validator: 'object.safe'
        }
    ],
    keyField: 'id'
});
