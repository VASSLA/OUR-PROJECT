/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: app.js
 * Date: Aug 27, 2016 7:20:19 PM
 * Author: Alex */

/* global Mate  */
Mate.setConfig({
    useJQuery: true
});

Mate.Logger.setConfig({
    logLevel: Mate.Logger.LogLevel.DEBUG
});

Mate.Microloader.setConfig({
    namespaceMapping: {
        'Application.controller': '/resources/app/js/app/controller',
        'Application.store': '/resources/app/js/app/store',
        'Application.model': '/resources/app/js/app/model',
        'Application.state': '/resources/app/js/app/state',
        'Application.resource': '/resources/app/js/app/resource'
    }
});

Mate.Selector(function () {
    Mate.Selector('#shadowText').html('Загрузка компонентов приложения');
    Mate.Logger.debug('Loading class system ...');
    Mate.require([
        // Mate classes
        'Mate.mixin.IdOwner',
        'Mate.mixin.JsonStringifier',
        'Mate.mixin.ResourceLoader',
        'Mate.core.Class',
        'Mate.app.StateManager',
        'Mate.app.ResourceManager',
        'Mate.app.Controller',
        'Mate.app.Application',
        'Mate.data.proxy.DataProxy',
        'Mate.data.proxy.RemoteDataProxy',
        'Mate.data.Field',
        'Mate.data.Model',
        'Mate.data.Store',
        'Mate.ux.WaitIndicator',
        'Mate.ext.UxUtils',
        'Mate.ext.TemplateProcessor',
        'Mate.ext.Geocoder',
        // Application state manager
        'Application.resource.ResourceManager',
        'Application.state.StateManager',
        // Application controllers
        'Application.controller.ProfileController',
        'Application.controller.RequestsController',
        'Application.controller.DesktopController',
        'Application.controller.EventFormController',
        'Application.controller.EventsController',
        'Application.controller.DashboardController',
        'Application.controller.MessagesController',
        'Application.controller.CoordinatorController',
        'Application.controller.SystemSettingsController',
        'Application.controller.AdministratorController',
        'Application.controller.LogsController',
        'Application.controller.FeedbackController',
        'Application.controller.RequestCardController',
        'Application.controller.RequestCardMapsController',
        'Application.controller.RequestFormController',
        'Application.controller.DebugController',
        'Application.controller.MediaLibraryController',
        'Application.controller.MediaFormController',
        'Application.controller.AddressListController',
        'Application.controller.ContactListController',
        'Application.controller.FeedbackFormController',
        'Application.controller.PlaceSelectorController',
        // Application models
        'Application.model.AddressModel',
        'Application.model.ContactModel',
        'Application.model.ContactTypeModel',
        'Application.model.EventModel',
        'Application.model.EventTypeModel',
        'Application.model.LogLevelModel',
        'Application.model.LogModel',
        'Application.model.MediaInfoModel',
        'Application.model.MessageModel',
        'Application.model.PersonModel',
        'Application.model.RequestModel',
        'Application.model.RequestSourceModel',
        'Application.model.RequestStateModel',
        'Application.model.RequestTypeModel',
        'Application.model.SettingModel',
        'Application.model.SkillModel',
        'Application.model.UserModel',
        'Application.model.ValidatorModel',
        'Application.model.FeedbackMessageModel',
        'Application.model.FeedbackStatusModel',
        'Application.model.FeedbackTypeModel',
        // Application stores
        'Application.store.AddressStore',
        'Application.store.ContactStore',
        'Application.store.ContactTypeStore',
        'Application.store.CurrentUserStore',
        'Application.store.EventStore',
        'Application.store.EventTypeStore',
        'Application.store.LogLevelStore',
        'Application.store.LogStore',
        'Application.store.MediaInfoStore',
        'Application.store.PersonStore',
        'Application.store.RequestStateStore',
        'Application.store.RequestSourceStore',
        'Application.store.RequestStore',
        'Application.store.RequestTypeStore',
        'Application.store.SettingStore',
        'Application.store.SkillStore',
        'Application.store.UserStore',
        'Application.store.ValidatorStore',
        'Application.store.FeedbackMessageStore',
        'Application.store.FeedbackStatusStore',
        'Application.store.FeedbackTypeStore',
    ], false, applicationInitialize);

    function applicationInitialize() {
        Mate.Logger.debug('Class system loaded');
        Mate.Logger.debug('Starting initialize application ...');
        var application = Mate.create('Mate.app.Application', {
            name: 'Application',
            controllers: [
                'Application.controller.ProfileController',
                'Application.controller.RequestsController',
                'Application.controller.DesktopController',
                'Application.controller.EventsController',
                'Application.controller.EventFormController',
                'Application.controller.DashboardController',
                'Application.controller.MessagesController',
                'Application.controller.CoordinatorController',
                'Application.controller.SystemSettingsController',
                'Application.controller.AdministratorController',
                'Application.controller.LogsController',
                'Application.controller.FeedbackController',
                'Application.controller.RequestCardController',
                'Application.controller.RequestCardMapsController',
                'Application.controller.RequestFormController',
                'Application.controller.DebugController',
                'Application.controller.MediaLibraryController',
                'Application.controller.MediaFormController',
                'Application.controller.AddressListController',
                'Application.controller.ContactListController',
                'Application.controller.FeedbackFormController',
                'Application.controller.PlaceSelectorController'
            ],
            stores: [
                'Application.store.AddressStore',
                'Application.store.ContactStore',
                'Application.store.ContactTypeStore',
                'Application.store.CurrentUserStore',
                'Application.store.EventStore',
                'Application.store.EventTypeStore',
                'Application.store.LogLevelStore',
                'Application.store.LogStore',
                'Application.store.MediaInfoStore',
                'Application.store.PersonStore',
                'Application.store.RequestStateStore',
                'Application.store.RequestSourceStore',
                'Application.store.RequestStore',
                'Application.store.RequestTypeStore',
                'Application.store.SettingStore',
                'Application.store.SkillStore',
                'Application.store.UserStore',
                'Application.store.ValidatorStore',
                'Application.store.FeedbackMessageStore',
                'Application.store.FeedbackStatusStore',
                'Application.store.FeedbackTypeStore',        
            ]
        });
        var stateManager = Mate.create('Application.state.StateManager', {});
        var resourceManager = Mate.create('Application.resource.ResourceManager', {maximumWaitTimeoutMs: 30000});
        resourceManager.onResourceListLoaded = function() {
            Mate.Logger.debug('Resources were loaded');
            Mate.Logger.debug('Loading data ...');
            loadStores();
        };
        application.setStateManager(stateManager);
        application.setResourceManager(resourceManager);
        Mate.Application = application;
        Mate.Logger.debug('Application initialized');
        Mate.Logger.debug('Loading resources ...');
        Mate.Application.getResourceManager().load();
        Mate.Application.getResourceContent = function(resource) {
            return Mate.Application.getResourceManager().getResourceContent(resource);
        };
        Mate.Application.setState = function(requiredState) {
            Mate.Application.getStateManager().setState(requiredState);
        };
        Mate.Application.openMediaLibrary = function(callback) {
            var mediaController = Mate.Application.getControllerByName('MediaFormController');
            mediaController.callback = callback;
            mediaController.init();
            mediaController.onBtnMediaWindowClick(mediaController);
        };
        
        Mate.Application.createEvent = function(callback) {
            var form = Mate.Application.getResourceContent('formEventEditor');
            Mate.Selector('#modalPlaceholder').html(form);
            Mate.Selector('#eventEditorModalForm').modal('show');
            
            var eventController = Mate.Application.getControllerByName('EventFormController');
            eventController.callback = callback;
            eventController.init();
        };
        
        Mate.Logger.debug('Waiting for preloading stores ...');
    };

    function loadStores() {
        Mate.Logger.debug('Waiting for data load ...');
        for (var i = 0; i < Mate.Application.stores.length; i++) {
            var currentStore = Mate.Application.getStoreByName(Mate.Application.stores[i]);
            if (!currentStore.isLoaded() && currentStore.autoLoad === true) {
                setTimeout(loadStores, 100);
                return;
            }
        }
        Mate.Logger.debug('Data was loaded');
        Mate.Application.currentUserStore = Mate.Application.getStoreByName('CurrentUserStore');
        Mate.Application.currentUser = Mate.Application.currentUserStore.getData(0);
        Mate.Application.getSetting = function(settingName) {
            var overridedSettingName= Mate.Application.currentUser.get('login') + '.' + settingName;
            var settings = Mate.Application.getStoreByName('SettingStore');
            var setting = settings.lookup('name', overridedSettingName) || settings.lookup('name', settingName);
            if (setting) {
                return setting.get('value');
            }
        };
        prepareDefaultView();
    }

    function prepareDefaultView() {
        Mate.Logger.debug('Preparing default view ...');
        var defaultViewSetting = Mate.Application.getSetting('Interface.DefaultView');
        var defaultSkinSetting = Mate.Application.getSetting('Interface.Skin');
        var defaultView = defaultViewSetting ? defaultViewSetting : 'Profile';
        var defaultSkin = defaultSkinSetting ? defaultSkinSetting : 'skin-yellow-light';
        Mate.Selector('body').attr('class', 'sidebar-mini ' + defaultSkin);
        Mate.Selector('#content-wrapper').html(Mate.Application.getResourceContent('view' + defaultView));
        Mate.Application.getControllerByName('DesktopController').highlightCurrentMenuItem(defaultView);
        Mate.Application.setState(defaultView);
        Mate.Application.waitIndicator = Mate.create('Mate.ux.WaitIndicator', {
            template: Mate.Application.getResourceContent('blockWait')
        });
        Mate.Logger.debug('Application is ready for action!');
        Mate.Selector('#shadow').hide();
    }

});
