/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: PlaceSelectorController.js
 * Date: Feb 19, 2017 8:53:57 PM
 * Author: Alex */

/* global Mate, ymaps */
Mate.define('Application.controller.PlaceSelectorController', {
    extends: 'Mate.app.Controller',
    map: null,
    userCoordinates: null,
    interactives: {
        '#requestPlaceSelectorOk': {'click': 'onRequestPlaceSelectorOkClick'},
        '#requestPlaceSelectorCancel': {'click': 'onRequestPlaceSelectorCancelClick'}
    },
    beforeInit: function() {
        var me = this;
        if (this.places) {
            var firstPlace = this.places[0].GeoObject.Point.pos;
            this.userCoordinates = firstPlace.split(' ');
            this.userCoordinates[0] = parseFloat(this.userCoordinates[0]);
            this.userCoordinates[1] = parseFloat(this.userCoordinates[1]);
            this.userCoordinates.reverse();
            this.initMap();
        } else {
            this.userCoordinates = [ymaps.geolocation.latitude, ymaps.geolocation.longitude];
            Mate.Logger.debug(this.userCoordinates);
            me.initMap();
        }
    },
    initMap: function () {
        this.map = this.map || new ymaps.Map("yandexMap", {
            center: this.userCoordinates,
            zoom: 14
        });
        this.map.geoObjects.each(function(mapObject) {
            this.remove(mapObject);
        }, this.map.geoObjects);
        this.map.controls.add(new ymaps.control.TypeSelector(['yandex#satellite', 'yandex#map', 'yandex#publicMap']));
        this.map.controls.add(new ymaps.control.ZoomControl());
        this.map.behaviors.enable(['scrollZoom']);
        if (this.places && this.places instanceof Array) {
            for (var position in this.places) {
                var coordinates = this.places[position].GeoObject.Point.pos.split(' ');
                coordinates[0] = parseFloat(coordinates[0]);
                coordinates[1] = parseFloat(coordinates[1]);
                coordinates.reverse();
                var marker = new ymaps.Placemark(coordinates, {}, {
                    preset: 'twirl#blueIcon'
                });
                marker.events.add('click', this.onMarkerClick, {controller: this, marker: marker});
                this.map.geoObjects.add(marker);
            }
            var coordinates = this.places[0].GeoObject.Point.pos.split(' ');
            coordinates[0] = parseFloat(coordinates[0]);
            coordinates[1] = parseFloat(coordinates[1]);
            coordinates.reverse();
            this.map.setCenter(coordinates);
        }
    },
    onMarkerClick: function(marker) {
        var coordinates =this.marker.geometry.getCoordinates();
        Mate.Selector('#clarifiedPlace').val('Широта: ' + coordinates[0] + ', ' + 'Долгота: ' + coordinates[1]);
        this.controller.request.set('latitude', coordinates[0]);
        this.controller.request.set('longitude', coordinates[1]);
    },
    onRequestPlaceSelectorCancelClick: function() {
        Mate.Selector('#requestPlaceSelector').hide();
        this.deinit();
    },
    onRequestPlaceSelectorOkClick: function() {
        Mate.Selector('#requestPlaceSelector').hide();
        this.deinit();
    }
});
