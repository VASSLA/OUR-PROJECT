/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: RequestTypeStore.js
 * Date: Oct 28, 2016 2:07:00 PM
 * Author: anikeale */

/* global Mate */
Mate.define('Application.store.RequestTypeStore', {
    extends: 'Mate.data.Store',
    autoLoad: true,
    model: 'Application.model.RequestTypeModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/requesttype'
    })
});
