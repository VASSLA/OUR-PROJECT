/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: SkillStora.js
 * Date: Dec 4, 2016 5:09:51 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.SkillStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.SkillModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/skill'
    })
});
