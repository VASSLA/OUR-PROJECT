/* global request, picture */
[{
    info: {
        title: request.get('missingPerson.lastName') + ' ' +
               request.get('missingPerson.firstName') + ' ' +
               request.get('missingPerson.middleName') + ' - Заявка ' + request.get('id'),
        author: 'Liza Alert (c) 2017',
        subject: 'Заявка на поиск',
        keywords: request.get('missingPerson.lastName') + ' ' +
               request.get('missingPerson.firstName') + ' ' +
               request.get('missingPerson.middleName')
    },
    background: {
        table: {
            widths: [10, '*', 10],
            body: [
                [
                    {border: [false, false, false, false], text: '', fillColor: '#FF6600', margin: [0, 0, 0, 0]},
                    {border: [false, false, false, false], text: 'HTTP://VK.COM/LIZAALERT_REAL', fillColor: '#FF6600', margin: [0, 0, 0, 0], style: ['onbordertext']},
                    {border: [false, false, false, false], text: '', fillColor: '#FF6600', margin: [0, 0, 0, 0]}
                ],
                [
                    {border: [false, false, false, false], text: '', fillColor: '#FF6600', margin: [0, 760, 0, 0]},
                    {border: [false, false, false, false], text: 'НУЖНА ПОМОЩЬ ДОБРОВОЛЬЦЕВ', fillColor: '#FFFFFF', margin: [0, 760, 0, 0], style:['volunteers']},
                    {border: [false, false, false, false], text: '', fillColor: '#FF6600', margin: [0, 760, 0, 0]}
                ],
                [
                    {border: [false, false, false, false], text: '', fillColor: '#FF6600', margin: [0, 0, 0, 0]},
                    {border: [false, false, false, false], text: 'LIZA ALERT | WWW.LIZAALERT.ORG | ORG@LIZAALERT.ORG', fillColor: '#FF6600', margin: [0, 0, 0, 0], style: ['onbordertext']},
                    {border: [false, false, false, false], text: '', fillColor: '#FF6600', margin: [0, 0, 0, 0]}
                ]
            ]
        }
    },
    pageMargins: [25, 25, 25, 25],
    content: [
        {
            columns: [
                {
                    image: picture, width: 256, height: 256
                },
                {
                    stack: [
                        {
                            text: request.get('missingPerson.lastName') + '\n' +
                                    request.get('missingPerson.firstName') + '\n' +
                                    request.get('missingPerson.middleName'), style: ['name']
                        },
                        {
                            text: (function(){
                                var age = ((new Date()).getFullYear() - parseInt(request.get('missingPerson.birthDate').substr(-4))).toString();
                                var word = 'лет';
                                if (age.substr(-1) === '1') word = 'год';
                                if (age.substr(-1) === '2' || age.substr(-1) === '3' || age.substr(-1) === '4') word = 'года';
                                return age + ' ' + word;
                            })(), style: ['age']
                        },
                        {
                            text: (function(){
                                var addresses = request.get('missingPerson.addresses');
                                var address = (addresses && addresses.length > 0) ? addresses[0] : null;
                                for (var position in addresses) {
                                    if (addresses[position].defaultAddress) {
                                        address = addresses[position];
                                        break;
                                    }
                                }
                                if (address) {
                                    return (address.region ? address.region + ' ' + address.regionTypeShort + ', ' : '') + address.city;
                                }
                                return 'Адрес неизвестен';
                            })(), style: ['address']
                        }
                    ]
                }
            ]
        },
        {
            stack: [
                {
                    margin: [50, 10, 50, 0],
                    text: [
                        { text: 'Обстоятельства пропажи: ', style: ['bold'] },
                        { text: request.get('circOfDisappear'), style: ['justified'] }
                    ]
                },
                {
                    margin: [50, 10, 50, 0],
                    text: [
                        { text: 'Приметы: ', style: ['bold'] },
                        { text: request.get('appearance'), style: ['justified'] }
                    ]
                },
                {
                    margin: [50, 10, 50, 0],
                    text: [
                        { text: 'Особые приметы: ', style: ['bold'] },
                        { text: request.get('extraInfo'), style: ['justified'] }
                    ]
                },
                {
                    margin: [50, 10, 50, 0],
                    text: [
                        { text: 'Одежда: ', style: ['bold'] },
                        { text: request.get('dressedIn'), style: ['justified'] }
                    ]
                },
                {
                    text: 'ПОМОГИТЕ', style: ['help1']
                },
                {
                    text: 'НАЙТИ', style: ['help2']
                }
            ]
        }

    ],
    defaultStyle: {
        font: 'Roboto'
    },
    styles: {
        name: {
            color: '#222222',
            fontSize: 48,
            bold: true,
            margin: [20, 0, 0, 10],
            font: 'Impact'
        },
        age: {
            color: '#222222',
            fontSize: 28,
            bold: true,
            margin: [20, 0, 0, 10]
        },
        help1: {
            font: 'Impact',
            color: '#FF6600',
            fontSize: 107,
            bold: false,
            alignment: 'center',
            margin: [0, 40, 0, 0]
        },
        help2: {
            font: 'Impact',
            color: '#FF6600',
            fontSize: 175,
            bold: true,
            alignment: 'center',
            margin: [0, 0, 0, 0]
        },
        address: {
            color: 'grey',
            fontSize: 16,
            bold: true,
            margin: [20, 0, 0, 0]
        },
        onbordertext: {
            color: 'white',
            bold: true,
            alignment: 'center'
        },
        volunteers: {
            color: '#222222',
            fontSize: 32,
            bold: true,
            alignment: 'center'
        },
        justified: {
            margin: [50, 0, 50, 0],
            alignment: 'justify',
            fontSzie: 18

        },
        bold: {
            bold: true
        }
    }
}];