/* global Mate */
Mate.define('Application.store.EventStore', {
    autoLoad: false,
    extends: 'Mate.data.Store',
    model: 'Application.model.EventModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/myevents'
    })
});
