/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ContactStore.js
 * Date: Nov 17, 2016 9:46:08 PM
 * Author: Alex */

/* global Mate */
Mate.define('Application.store.ContactStore', {
    autoLoad: true,
    extends: 'Mate.data.Store',
    model: 'Application.model.ContactModel',
    proxy: Mate.create('Mate.data.proxy.RemoteDataProxy', {
        url: '/api/v1/contact'
    })
});
