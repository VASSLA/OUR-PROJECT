/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ProfileController.js
 * Date: 28.08.2016 17:36:05
 * Author: Blade */

/* global Mate */
Mate.define('Application.controller.ProfileController', {
    extends: 'Mate.app.Controller',
    currentUser: null,
    interactives: {
        '#btnProfileUpdate': {'click': 'onBtnProfileUpdateClick'},
        '#profilePersonImage': {'click': 'onProfilePersonImageClick'}
    },
    afterInit: function () {
        Mate.Selector('#desktopHeader').html('Мой профиль');
        Mate.Selector("#profileBirthDate").datetimepicker({
            defaultDate: 'moment',
            format: 'DD.MM.YYYY',
            locale: 'ru'
        });
        this.currentUser = Mate.Application.currentUser;
        this.skillTemplate = Mate.Application.getResourceContent('blockSkill');
        this.applyDataToForm();
        this.displaySkillList();
    },
    currentUserHasSkill: function(skillId) {
        var person = this.currentUser.getFieldByName('person').getValue();
        var skills = person.getFieldByName('skills').getValue();
        if (!skills || !(skills instanceof Array)) {
            return false;
        }
        for (var i = 0; i < skills.length; i++) {
            if (skills[i].id === skillId) {
                return true;
            }
        }
        return false;
    },
    displaySkillList: function () {
        var skills = Mate.Application.getStoreByName('Application.store.SkillStore');
        for (var i = 0; i < skills.size(); i++) {
            var skill = skills.getData(i);
            var skillHtml = this.skillTemplate;
            skillHtml = skillHtml.replace(/{skillIcon}/g, skill.get('skillIcon'));
            skillHtml = skillHtml.replace(/{skill}/g, skill.get('skill'));
            skillHtml = skillHtml.replace(/{id}/g, skill.get('id'));
            if (this.currentUserHasSkill(skill.get('id'))) {
                skillHtml = skillHtml.replace('{skillColor}', 'text-blue');
            } else {
                skillHtml = skillHtml.replace('{skillColor}', 'text-black');
            }
            Mate.Selector('#profileSkillContainer').append(skillHtml);
        }
        Mate.Selector('a[data-skill]').on('click', this, Mate.Selector.proxy(this.onSkillClick, this));
    },
    loadCurrentUser: function () {
        var cuStore = Mate.Application.getStoreByName('Application.store.CurrentUserStore');
        this.currentUser = cuStore.getData()[0];
    },
    applyDataToForm: function () {
        if (this.currentUser) {
            var linkedPerson = this.currentUser.getFieldByName('person').getValue();
            Mate.Selector('#profilePersonImage').attr('src', '/api/v1/getthumbnail/' + linkedPerson.getFieldByName('pictureId').getValue());
            Mate.Selector('#profilePersonFullName').html(linkedPerson.getFieldByName('lastName').getValue() + ' ' +
                    linkedPerson.getFieldByName('firstName').getValue());
            Mate.Selector('#profileLastName').val(linkedPerson.getFieldByName('lastName').getValue());
            Mate.Selector('#profileMiddleName').val(linkedPerson.getFieldByName('middleName').getValue());
            Mate.Selector('#profileFirstName').val(linkedPerson.getFieldByName('firstName').getValue());
            if (Mate.Selector('#profileBirthDate').data('DateTimePicker')) {
                Mate.Selector('#profileBirthDate').data('DateTimePicker').date(linkedPerson.getFieldByName('birthDate').getValue());
                Mate.Selector('#profileBirthDate').data('DateTimePicker').toggle(); // Trick to make DateTimePicker showing right date after setting date
                Mate.Selector('#profileBirthDate').data('DateTimePicker').toggle();
            }
        }
    },
    onBtnProfileUpdateClick: function (eventObject) {
        var me = eventObject.data;
        var person = me.currentUser.getFieldByName('person').getValue();
        var cuStore = Mate.Application.getStoreByName('Application.store.CurrentUserStore');
        var skills = me.prepareSkills();
        person.getFieldByName('birthDate').setValue(Mate.Selector('#profileBirthDate').val());
        person.getFieldByName('lastName').setValue(Mate.Selector('#profileLastName').val());
        person.getFieldByName('firstName').setValue(Mate.Selector('#profileFirstName').val());
        person.getFieldByName('middleName').setValue(Mate.Selector('#profileMiddleName').val());
        person.getFieldByName('skills').setValue(skills);
        me.currentUser.setChanged(true);
        cuStore.onSynchronized = function () {
            me.applyDataToForm();
            cuStore.getProxy().url = '/api/v1/user/me';
            Mate.Application.waitIndicator.hide();
            cuStore.onDataLoaded = null;
        };
        cuStore.getProxy().url = '/api/v1/user';
        cuStore.sync();
        Mate.Application.waitIndicator.show();
    },
    onSkillClick: function (eventObject) {
        var target = Mate.Selector(eventObject.target).closest('a');
        if (Mate.Selector(target).hasClass('text-black')) {
            Mate.Selector(target).removeClass('text-black');
            Mate.Selector(target).addClass('text-blue');
        } else {
            Mate.Selector(target).removeClass('text-blue');
            Mate.Selector(target).addClass('text-black');
        }
    },
    prepareSkills: function() {
        var skills = [];
        Mate.Selector('a[data-skill][class=text-blue]').each(function(idx, arr) {
            var id = Mate.Selector(arr).attr('data-skill');
            var skill = Mate.Application.getStoreByName('Application.store.SkillStore').lookup('id', id);
            skills.push(skill.getJson());
        });
        return skills;
    },
    onProfilePersonImageClick: function(eventObject) {
        var context = eventObject.data;
        Mate.Application.openMediaLibrary(function(data) {
            Mate.Selector('#profilePersonImage').attr('src', '/api/v1/getthumbnail/' + data);
            var cuStore = Mate.Application.getStoreByName('Application.store.CurrentUserStore');
            context.currentUser.getFieldByName('person').getValue().getFieldByName('pictureId').setValue(data);
            context.currentUser.setChanged(true);
            cuStore.onSynchronized = function() {
                context.applyDataToForm();
                cuStore.onDataLoaded = null;
                cuStore.getProxy().url = '/api/v1/user/me';
                Mate.Application.waitIndicator.hide();
            };
            cuStore.getProxy().url = '/api/v1/user';
            Mate.Application.waitIndicator.show();
            cuStore.sync();
        });
    }
});
