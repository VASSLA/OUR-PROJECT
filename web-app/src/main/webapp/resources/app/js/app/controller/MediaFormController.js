/* global Mate */
Mate.define('Application.controller.MediaFormController', {
    extends: 'Mate.app.Controller',
    mediaInfoList: null,
    template: '',
    modal: false,
    interactives: {
        '#btnMediaWindow': {'click': 'onBtnMediaWindowClick'},
        '#btnUploadFile': {'click': 'onBtnMediaUploadClick'},
        '#btnBrowseFile': {'change': 'onBtnMediaBrowseSelect'},
        '#btnCloseMediaModalWindow': {'click': 'onBtnCloseModalMediaWindowClick'},
//        '#btnMediaSelect': {'click': 'onBtnMediaUploadClick'},
//        '#btnMediaDownload': {'click': 'onBtnMediaUploadClick'},
//        '#btnMediaLink': {'click': 'onBtnMediaUploadClick'},
//        '#btnMediaDelete': {'click': 'onBtnMediaUploadClick'},
        //'.btnMediaSelect': {'click': 'testFunction'}
    },
    afterInit: function (controller) {
        Mate.Selector('#desktopHeader').html('Библиотека загруженных файлов');
        this.loadTemplate();
//        Mate.Logger.trace(this.template);

    },
    loadTemplate: function () {
        Mate.Logger.trace('loadTemplate');
        Mate.Selector.ajax({
            url: '/block/image-container',
            method: 'GET',
            contentType: 'text/html; charset=utf-8',
            context: this,
            success: function (data) {
                Mate.Logger.trace('success loading template');
                this.template = data;
                if (!this.modal) {
                    var foo = Mate.Selector('section.sidebar').find('.active').attr('data-view');
                    if (foo === 'MediaLibrary') {
                        this.loadMediaInfo();
                        this.applyDataToForm();
                    }
                }

            },
            error: function (data) {
                Mate.Logger.trace('error loading template');
            }
        });
    },
    loadMediaInfo: function () {
        Mate.Logger.trace('loadMediaInfo');
        var mediaInfoStore = Mate.Application.getStoreByName('Application.store.MediaInfoStore');
        this.mediaInfoList = mediaInfoStore.getData();
    },
    applyElementToForm: function (mediaInfo) {
        var destination;
        Mate.Logger.trace(this.modal);
        if (this.modal) {
            destination = 'uploadsContainer';
        } else {
            destination = 'uploadsFormContainer';
        }
        var img = this.template;
        var path = '/api/v1/getthumbnail/' + mediaInfo.getFieldByName("id").getValue();
        var img = img.replace('{img-source}', path);
        img = img.replace('{img-name}', mediaInfo.getFieldByName('initialName').getValue());
        img = img.replace('{media-id}', mediaInfo.getFieldByName('id').getValue());
        $('#' + destination).append(img);
    },
    applyDataToForm: function () {
        Mate.Logger.trace('applyDataToForm');
        if (this.mediaInfoList) {
//            var dest = Mate.Selector('#' + container)[0];
//            Mate.Logger.trace(dest);
//            var tr, td;container
            for (var i in this.mediaInfoList) {
                var media = this.mediaInfoList[i];
                this.applyElementToForm(media);
                /*
                 var img = this.template;

                 var path2 = '/api/v1/getthumbnail/' + media.getFieldByName("id").getValue();

                 var img = img.replace('{img-source}', path2);
                 img = img.replace('{img-name}', media.getFieldByName('initialName').getValue());

                 $('#uploadsContainer').append(img);
                 */

                /*
                 var media = this.mediaInfoList[i];
                 tr = document.createElement("tr");
                 td = document.createElement("td");
                 td.innerHTML = media.getFieldByName('initialName').getValue();
                 tr.appendChild(td);

                 td = document.createElement("td");
                 var docId = media.getFieldByName('id').getValue();

                 Mate.Logger.trace(media);

                 var but = document.createElement("input");
                 but.type = "button";
                 but.value = "Download";
                 but.docId = docId;
                 but.onclick = this.onBtnMediaDownloadClick;

                 td.appendChild(but);

                 var but = document.createElement("input");
                 but.type = "button";
                 but.value = "Delete";
                 but.docId = docId;
                 but.onclick = this.onBtnDeleteClick;

                 td.appendChild(but);

                 tr.appendChild(td);
                 table.appendChild(tr);
                 */
            }
            this.setImageButtonsInteractives();
        }
    },
    setImageButtonsInteractives: function () {
        var me = this;
        $('.btnMediaSelect').each(function () {
            this.onclick = me.testFunction;
            this.callback = me.callback;
        });
        $('.btnMediaDownload').each(function () {
            this.onclick = me.onBtnMediaDownloadClick;
        });
        $('.btnMediaLink').each(function () {
            this.onclick = me.onBtnMediaCopyLinkClick;
        });
        $('.btnMediaDelete').each(function () {
            this.onclick = me.onBtnDeleteClick;
        });
    },
    onBtnMediaWindowClick: function (meta) {
        Mate.Logger.trace('open');
        var me = (meta instanceof Event) ? meta.data : meta;
        Mate.Selector.ajax({
            url: '/form/media',
            method: 'GET',
            context: me,
            success: function (data) {
                this.modal = true;
                Mate.Selector('#mediaLibraryPlaceholder').html(data);
                Mate.Selector('#mediaLibraryModalForm').show();
                this.loadMediaInfo();
                this.applyDataToForm();
                this.init();
            }
        });
    },
    testFunction: function (eventObject) {
        var me = Mate.Application.getControllerByName('Application.controller.MediaFormController');
        var p = Mate.Selector(eventObject.target).parents(".bigdiv")[0];
        var docId = p.getAttribute("mediaInfoId");
        Mate.Logger.trace(docId);
        if (me.callback) {
            me.callback(docId);
            Mate.Selector('#mediaLibraryModalForm').modal('hide');
        }
    },

    onBtnMediaCopyLinkClick: function (eventObject) {
        var img = Mate.Selector(eventObject.target).parents('.hovereffect').find('img')[0];
        var text = img.src;

        var id = "clipboardTextarea";
        var textarea = document.getElementById(id);

        if(!textarea){
            textarea = document.createElement("textarea");
            textarea.id = id;

            textarea.style.position = 'fixed';
            textarea.style.top = 0;
            textarea.style.left = 0;

            // Ensure it has a small width and height. Setting to 1px / 1em
            // doesn't work as this gives a negative w/h on some browsers.
            textarea.style.width = '1px';
            textarea.style.height = '1px';
            textarea.style.padding = 0;

            textarea.style.border = 'none';
            textarea.style.outline = 'none';
            textarea.style.boxShadow = 'none';

            textarea.style.background = 'transparent';
            document.querySelector("body").appendChild(textarea);
//                existsTextarea = document.getElementById(id);
        }

        textarea.value = text;
        textarea.select();

        try {
            var status = document.execCommand('copy');
            if(!status){
                console.error("Cannot copy text");
            }else{
                console.log("The text is now on the clipboard");
            }
        } catch (err) {
            console.log('Unable to copy.');
        }

        $(textarea).remove();
    },

    onBtnMediaDownloadClick: function (eventObject) {
        var p = Mate.Selector(eventObject.target).parents(".bigdiv")[0];
        var docId = p.getAttribute("mediaInfoId");
        window.open('/api/v1/getfile/' + docId);
//        Mate.Selector.ajax({

//                url: '/getfile/' + docId,
//                method: 'GET',
//                success: function(response)
//                {
//                    Mate.Logger.trace(response);
//                    alert('got response');
//                    window.open(response);
//                }
//                ,
//                data:  data,
//                dataType: 'number',
//                enctype: 'multipart/form-data',
//                processData: false,
//                contentType: false
//                id: docId,
//            });
    },
    onBtnDeleteClick: function (eventObject) {
        var p = Mate.Selector(eventObject.target).parents(".bigdiv")[0];
        var docId = p.getAttribute("mediaInfoId");
        Mate.Selector.ajax({
            url: '/api/v1/deletefile/' + docId,
            method: 'DELETE',
            success: function (data) {
                $(p).remove();
            }
        });
    },
    onBtnMediaBrowseSelect: function (eventObject) {
        debugger;
        var me = eventObject.data;
        var files = eventObject.target.files;
        if (files.length > 0) {
            var file = files[0];
            $('#lblBrowseFile').text(file.name);
        }
        if (me.callback) {
            callback(file.name);
        }
    },
    onBtnMediaUploadClick: function (eventObject) {
//        var target = eventObject.target;
        var files = $('#btnBrowseFile')[0].files;
        if (files.length > 0) {
            var data = new FormData();
            data.append("file", files[0]);
            Mate.Selector.ajax({
                url: '/api/v1/storefile',
                method: 'POST',
                enctype: 'multipart/form-data',
                data: data,
                processData: false,
                contentType: false,
                success: function (data) {
                    me = eventObject.data;
                    var img = me.template;
                    var path = '/api/v1/getthumbnail/' + data.id;
                    var img = img.replace('{img-source}', path);
                    img = img.replace('{img-name}', data.initialName);
                    img = img.replace('{media-id}', data.id);
//                    var foo = Mate.Selector('section.sidebar').find('.active').attr('data-view');
                    if (!me.modal) {
                        $('#uploadsFormContainer').prepend(img);
                        me.setImageButtonsInteractives();
                    } else {
                        $('#uploadsContainer').prepend(img);
                        me.setImageButtonsInteractives();
                    }

                    /*
                     $(img).closest('.btnMediaSelect').each(function() {
                     Mate.Logger.trace(img);
                     Mate.Logger.trace(this);
                     this.onclick = me.testFunction;
                     });

                     Mate.Selector(img).closest('.btnMediaDownload').each(function() {
                     this.onclick = me.onBtnMediaDownloadClick;
                     });

                     Mate.Selector(img).closest('.btnMediaLink').each(function() {
                     this.onclick = me.testFunction;
                     });

                     Mate.Selector(img).closest('.btnMediaDelete').each(function() {
                     this.onclick = me.onBtnDeleteClick;
                     });
                     */
                }
            });
        }
    },
    onBtnCloseModalMediaWindowClick: function (eventObject) {
        this.deinit();
        Mate.Selector('#mediaLibraryModalForm').hide();
        Mate.Selector('#mediaLibraryPlaceholder').html('');
        Mate.Logger.trace('close');
        var me = eventObject.data;
        me.modal = false;
    }
});

