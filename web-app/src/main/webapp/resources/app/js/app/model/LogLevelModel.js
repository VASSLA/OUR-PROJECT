/* global Mate */
Mate.define('Application.model.LogLevelModel', {
    extends: 'Mate.data.Model',
    fields: [
        {
            name: 'level',
            caption: 'Уровень',
            description: 'Уровень',
            type: 'integer',
            size: 0,
            validator: 'number.integer'
        },
        {
            name: 'name',
            caption: 'Имя',
            description: 'Имя',
            type: 'string',
            size: 0,
            validator: 'string.string'
        },
        {
            name: 'resourceBundleName',
            caption: 'resourceBundleName',
            description: 'resourceBundleName',
            type: 'string',
            size: 0,
            validator: 'string.string'
        }
        ,
        {
            name: 'localizedName',
            caption: 'localizedName',
            description: 'localizedName',
            type: 'string',
            size: 0,
            validator: 'string.string'
        }
    ],
    keyField: 'value'
});
