/**
Person editable input.

@class person
@extends abstractinput
@final

**/
(function ($) {
    "use strict";

    var Person = function (options) {
        this.init('person', options, Person.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(Person, $.fn.editabletypes.abstractinput);

    $.extend(Person.prototype, {
        /**
        Renders input from tpl

        @method render()
        **/
        render: function() {
           this.$input = this.$tpl.find('input');
           this.$input.push(this.$tpl.find('select')[0]);
        },

        /**
        Default method to show value in element. Can be overwritten by display option.

        @method value2html(value, element)
        **/
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return;
            }
            var html = $('<div>').text(value.lastName).html() + ' ' + $('<div>').text(value.firstName).html() + ' ' + $('<div>').text(value.middleName).html() + ', ' + $('<div>').text(value.birthDate).html();
            $(element).html(html);
        },

        /**
        Gets value from element's html

        @method html2value(html)
        **/
        html2value: function(html) {
          /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g.
            editable({
                value: {
                    city: "Moscow",
                    street: "Lenina",
                    building: "15"
                }
            });
          */
          return null;
        },

       /**
        Converts value to string.
        It is used in internal comparing (not for sending to server).

        @method value2str(value)
       **/
       value2str: function(value) {
           var str = '';
           if(value) {
               for(var k in value) {
                   str = str + k + ':' + value[k] + ';';
               }
           }
           return str;
       },

       /*
        Converts string to value. Used for reading value from 'data-value' attribute.

        @method str2value(str)
       */
       str2value: function(str) {
           /*
           this is mainly for parsing value defined in data-value attribute.
           If you will always set value by javascript, no need to overwrite it
           */
           return str;
       },

       /**
        Sets value of input.

        @method value2input(value)
        @param {mixed} value
       **/
       value2input: function(value) {
           if(!value) {
             return;
           }
           this.$input.filter('[name="lastName"]').val(value.lastName);
           this.$input.filter('[name="firstName"]').val(value.firstName);
           this.$input.filter('[name="middleName"]').val(value.middleName);
           this.$input.filter('[name="birthDate"]').val(value.birthDate);
           this.$input.filter('[name="sex"]').val(value.sex);
       },

       /**
        Returns value of input.

        @method input2value()
       **/
       input2value: function() {
           return {
              lastName: this.$input.filter('[name="lastName"]').val(),
              firstName: this.$input.filter('[name="firstName"]').val(),
              middleName: this.$input.filter('[name="middleName"]').val(),
              birthDate: this.$input.filter('[name="birthDate"]').val(),
              sex: this.$input.filter('[name="sex"]').val()
           };
       },

        /**
        Activates input: sets focus on the first field.

        @method activate()
       **/
       activate: function() {
            this.$input.filter('[name="lastName"]').focus();
            $('select[name=sex]').width(172);
            $('select[name=sex]').height(24);
            $('input[name=birthDate]').datepicker({
                language: 'ru',
                format: 'dd.mm.yyyy',
                autoclose: true
            });
       },

       /**
        Attaches handler to submit form in case of 'showbuttons=false' mode

        @method autosubmit()
       **/
       autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
       }
    });

    Person.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-person"><label class="label text-black"><span>Фамилия: </span></label><input type="text" name="lastName" class="input-small"></div>'+
             '<div class="editable-person"><label class="label text-black"><span>Имя: </span></label><input type="text" name="firstName" class="input-small"></div>'+
             '<div class="editable-person"><label class="label text-black"><span>Отчество: </span></label><input type="text" name="middleName" class="input-small"></div>'+
             '<div class="editable-person"><label class="label text-black"><span>Дата рождения: </span></label><input type="text" name="birthDate" class="input-small"></div>'+
             '<div class="editable-person"><label class="label text-black"><span>Пол: </span></label><select class="input-small" name="sex"><option value="0">Мужской</option><option value="1">Женский</option></select></div>',
        inputclass: ''
    });

    $.fn.editabletypes.person = Person;

}(window.jQuery));