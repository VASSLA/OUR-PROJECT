/**
 Address editable input.

 @class address
 @extends abstractinput
 @final

 **/
(function ($) {
    "use strict";

    var Address = function (options) {
        this.init('address', options, Address.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);

    $.extend(Address.prototype, {
        /**
         Renders input from tpl

         @method render()
         **/
        render: function () {
            this.$input = this.$tpl.find('input');
        },

        /**
         Default method to show value in element. Can be overwritten by display option.

         @method value2html(value, element)
         **/
        value2html: function (value, element) {
            if (!value) {
                $(element).empty();
                return;
            }
            var text =  (value.zip ? value.zip + ', ' : '') +
                        (value.region ? value.region + ' ' + value.regionTypeShort + ', ' : '') +
                        (value.district ? value.district + ' ' + value.districtTypeShort + ', ' : '') +
                        (value.city ? value.cityTypeShort + ' ' + value.city + ', ' : '') +
                        (value.street ? value.streetTypeShort + ' ' + value.street + ', ' : '') +
                        (value.building ? value.buildingTypeShort + ' ' + value.building : '') +
                        (value.apartment ? ', кв ' + value.apartment : '') +
                        (value.extras ? ' (' + value.extras + ')': '');
            $(element).val(text);
        },

        /**
         Gets value from element's html

         @method html2value(html)
         **/
        html2value: function (html) {
            /*
             you may write parsing method to get value by element's html
             e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
             but for complex structures it's not recommended.
             Better set value directly via javascript, e.g.
             editable({
             value: {
             city: "Moscow",
             street: "Lenina",
             building: "15"
             }
             });
             */
            return null;
        },

        /**
         Converts value to string.
         It is used in internal comparing (not for sending to server).

         @method value2str(value)
         **/
        value2str: function (value) {
            var str = '';
            if (value) {
                for (var k in value) {
                    str = str + k + ':' + value[k] + ';';
                }
            }
            return str;
        },

        /*
         Converts string to value. Used for reading value from 'data-value' attribute.

         @method str2value(str)
         */
        str2value: function (str) {
            /*
             this is mainly for parsing value defined in data-value attribute.
             If you will always set value by javascript, no need to overwrite it
             */
            return str;
        },

        /**
         Sets value of input.

         @method value2input(value)
         @param {mixed} value
         **/
        value2input: function (value) {
            if (!value) {
                return;
            }
            Mate.Logger.debug(value);
            this.$input.filter('[name="zip"]').val(value.zip);
            this.$input.filter('[name="region"]').val(value.region);
            this.$input.filter('[name="region"]').attr('data-kladr-object-type-short', value.regionTypeShort);
            this.$input.filter('[name="region"]').attr('data-kladr-object-type-long', value.regionType);
            this.$input.filter('[name="district"]').val(value.district);
            this.$input.filter('[name="district"]').attr('data-kladr-object-type-short', value.districtTypeShort);
            this.$input.filter('[name="district"]').attr('data-kladr-object-type-long', value.districtType);
            this.$input.filter('[name="district"]').kladr('parentId', value.kladrId ? value.kladrId.substr(0,2) : null);
            this.$input.filter('[name="city"]').val(value.city);
            this.$input.filter('[name="city"]').attr('data-kladr-object-type-short', value.cityTypeShort);
            this.$input.filter('[name="city"]').attr('data-kladr-object-type-long', value.cityType);
            this.$input.filter('[name="city"]').kladr('parentId', value.kladrId ? value.kladrId.substr(0,5) : null);
            this.$input.filter('[name="street"]').val(value.street);
            this.$input.filter('[name="street"]').attr('data-kladr-object-type-short', value.streetTypeShort);
            this.$input.filter('[name="street"]').attr('data-kladr-object-type-long', value.streetType);
            this.$input.filter('[name="street"]').kladr('parentId', value.kladrId ? value.kladrId.substr(0,13) : null);
            this.$input.filter('[name="building"]').val(value.building);
            this.$input.filter('[name="building"]').attr('data-kladr-object-type-short', value.buildingTypeShort);
            this.$input.filter('[name="building"]').attr('data-kladr-object-type-long', value.buildingType);
            this.$input.filter('[name="building"]').kladr('parentId', value.kladrId ? value.kladrId.substr(0,17) : null);
            this.$input.filter('[name="apartment"]').val(value.apartment);
            this.$input.filter('[name="extras"]').val(value.extras);
            this.$input.filter('[name="building"]').attr("data-kladr-id", value.kladrId);
        },

        /**
         Returns value of input.

         @method input2value()
         **/
        input2value: function () {
            var value = {
                zip: this.$input.filter('[name="zip"]').val(),
                region: this.$input.filter('[name="region"]').val(),
                regionTypeShort: this.$input.filter('[name="region"]').attr('data-kladr-object-type-short'),
                regionType: this.$input.filter('[name="region"]').attr('data-kladr-object-type-long'),
                district: this.$input.filter('[name="district"]').val(),
                districtTypeShort: this.$input.filter('[name="district"]').attr('data-kladr-object-type-short'),
                districtType: this.$input.filter('[name="district"]').attr('data-kladr-object-type-long'),
                city: this.$input.filter('[name="city"]').val(),
                cityTypeShort: this.$input.filter('[name="city"]').attr('data-kladr-object-type-short'),
                cityType: this.$input.filter('[name="city"]').attr('data-kladr-object-type-long'),
                street: this.$input.filter('[name="street"]').val(),
                streetTypeShort: this.$input.filter('[name="street"]').attr('data-kladr-object-type-short'),
                streetType: this.$input.filter('[name="street"]').attr('data-kladr-object-type-long'),
                building: this.$input.filter('[name="building"]').val(),
                buildingTypeShort: this.$input.filter('[name="building"]').attr('data-kladr-object-type-short'),
                buildingType: this.$input.filter('[name="building"]').attr('data-kladr-object-type-long'),
                apartment: this.$input.filter('[name="apartment"]').val(),
                extras: this.$input.filter('[name="extras"]').val(),
                kladrId: this.$input.filter('[name="building"]').attr("data-kladr-id")
            };
            var addressText = (value.zip ? value.zip + ', ' : '') +
                        (value.region ? value.region + ' ' + value.regionTypeShort + ', ' : '') +
                        (value.district ? value.district + ' ' + value.districtTypeShort + ', ' : '') +
                        (value.city ? value.cityTypeShort + ' ' + value.city + ', ' : '') +
                        (value.street ? value.streetTypeShort + ' ' + value.street + ', ' : '') +
                        (value.building ? value.buildingTypeShort + ' ' + value.building : '') +
                        (value.apartment ? ', кв ' + value.apartment : '') +
                        (value.extras ? ' (' + value.extras + ')': '');
            value.address = addressText;
            value.uniqueId = value.kladrId + '-' + (value.building + value.apartment).replace(/\//gim, '');
            return value;
        },

        /**
         Activates input: sets focus on the first field.

         @method activate()
         **/
        activate: function () {
            function setLabel(input, obj) {
                var text = obj.type.charAt(0).toUpperCase() + obj.type.substr(1).toLowerCase() + ': ';
                input.parent().find('label').find('span').text(text);
                input.attr('data-kladr-object-type-short', obj.typeShort);
                input.attr('data-kladr-object-type-long', obj.type);
            }

            function fillParent(obj) {
                if (obj && obj.parents) {
                    for (var i = obj.parents.length - 1; i >= 0; i--) {
                        var parent = obj.parents[i];
                        this.$input.filter('[name="' + parent.contentType + '"]').val(parent.name);
                        this.$input.filter('[name="' + parent.contentType + '"]').attr('data-kladr-object-type-short', parent.typeShort);
                        this.$input.filter('[name="' + parent.contentType + '"]').attr('data-kladr-object-type-long', parent.type);
                    }
                    //if (!this.$input.filter('[name="zip"]').val())
                        this.$input.filter('[name="zip"]').val(obj.zip);
                }
            }

            function showError(input, message) {
                tooltip.find('span').text(message);

                var inputOffset = input.offset(),
                        inputWidth = input.outerWidth(),
                        inputHeight = input.outerHeight();

                var tooltipHeight = tooltip.outerHeight();

                tooltip.css({
                    left: (inputOffset.left + inputWidth + 10) + 'px',
                    top: (inputOffset.top + (inputHeight - tooltipHeight) / 2 - 1) + 'px'
                });

                tooltip.show();
            }


            var zip = $('div[class=editable-address]>input[name=zip]'),
                region = $('div[class=editable-address]>input[name=region]'),
                district = $('div[class=editable-address]>input[name=district]'),
                city = $('div[class=editable-address]>input[name=city]'),
                street = $('div[class=editable-address]>input[name=street]'),
                building = $('div[class=editable-address]>input[name=building]');

            var tooltip = $('div[class=editable-address]>div[class=editable-address-tooltip]');

            $.kladr.setDefault({
                parentInput: '.js-address-form',
                verify: true,
                select: function (obj) {
                    setLabel($(this), obj);
                },
                check: function (obj) {
                    var input = $(this);
                    if (obj) {
                        setLabel(input, obj);
                        tooltip.hide();
                    } else {
                        showError(input, 'Неверный ввод');
                    }
                },
                checkBefore: function () {
                    var input = $(this);
                    if (!$.trim(input.val())) {
                        tooltip.hide();
                        return false;
                    }
                },
                change: $.proxy(fillParent, this)
            });

            region.kladr('type', $.kladr.type.region);
            region.kladr('typeShort', $.kladr.type.regionTypeShort);
            district.kladr({type: $.kladr.type.district, withParents: true, parentInput: region});
            district.kladr('typeShort', $.kladr.type.districtTypeShort);
            city.kladr({type: $.kladr.type.city, withParents: true, parentInput: district});
            city.kladr('typeShort', $.kladr.type.cityTypeShort);
            street.kladr({type: $.kladr.type.street, withParents: true, parentInput: city, parentType: $.kladr.type.city});
            street.kladr('typeShort', $.kladr.type.streetTypeShort);
            building.kladr({type: $.kladr.type.building, parentType: $.kladr.type.city});
            building.kladr('typeShort', $.kladr.type.buildingTypeShort);
            building.kladr('verify', true);
            zip.kladrZip();
            this.$input.filter('[name="zip"]').focus();
        },

        /**
         Attaches handler to submit form in case of 'showbuttons=false' mode

         @method autosubmit()
         **/
        autosubmit: function () {
            this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
            });
        }
    });

    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<form class="js-address-form">' +
                '<div class="editable-address"><label class="label text-black"><span>Индекс: </span></label><input type="text" name="zip" class="input-small"></div>' +
                '<div class="editable-address"><label class="label text-black"><span>Регион: </span></label><input type="text" name="region" class="input-small"></div>' +
                '<div class="editable-address"><label class="label text-black"><span>Район: </span></label><input type="text" name="district" class="input-small"></div>' +
                '<div class="editable-address"><label class="label text-black"><span>Город: </span></label><input type="text" name="city" class="input-small"></div>' +
                '<div class="editable-address"><label class="label text-black"><span>Улица: </span></label><input type="text" name="street" class="input-small"></div>' +
                '<div class="editable-address"><label class="label text-black"><span>Дом: </span></label><input type="text" name="building" class="input-small"></div>' +
                '<div class="editable-address"><label class="label text-black"><span>Квартира: </span></label><input type="text" name="apartment" class="input-small"></div>' +
                '<div class="editable-address"><label class="label text-black"><span>Дополнительно: </span></label><input type="text" name="extras" class="input-small"></div>' +
                '<div class="editable-address-tooltip"></div>' +
             '</form>',
        inputclass: ''
    });

    $.fn.editabletypes.address = Address;

}(window.jQuery));