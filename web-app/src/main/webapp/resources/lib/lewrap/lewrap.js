/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: lewrap.js
 * Date: 25.10.2016 22:14:48
 * Author: Никита */

/* global Mate, L, turf */
var Lewrap = Lewrap || {};

Lewrap.addOsmTile = function() {
    var osm = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    return osm;
};

Lewrap.addGoogleTile = function() {
    //TODO moved to the configuration settings
    //type valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
    var google = L.gridLayer.googleMutant({
        type: 'roadmap'
    });
    return google;
};

Lewrap.addYandexTile = function() {
    //TODO moved to the configuration settings
    // Possible types: yandex#map, yandex#satellite, yandex#hybrid, yandex#publicMap, yandex#publicMapHybrid
    // Or their short names: map, satellite, hybrid, publicMap, publicMapHybrid
    //options: {
    //	minZoom: 0,
    //	maxZoom: 18,
    //	attribution: '',
    //	opacity: 1,
    //	traffic: false
    //}
    var yandex = new L.Yandex("publicMapInHybridView", {traffic: true});
    return yandex;
};

/*
 * Default Lewrap tiles
 */
Lewrap.Tiles = {
    OSM: { tileFunc: Lewrap.addOsmTile, tileName: 'OSM', configProperty: 'osm' },
    YANDEX: { tileFunc: Lewrap.addYandexTile, tileName: 'Yandex', configProperty: 'yandex' },
    GOOGLE: { tileFunc: Lewrap.addGoogleTile, tileName: 'Google', configProperty: 'google' }
};

/*
 * Initialize new instance of Lewrap
 * @param target Target div selector or id
 * @param config Lewrap configuration
 */
function Lewrap(target, config) {
    if (this.checkDependencies()) {
        var defaultConfig = {
            tiles: [ Lewrap.Tiles.OSM ],
            defaultLayer: Lewrap.Tiles.OSM,
            position: [54.842215, 38.237853],
            zoom: 15,
            zoomAnimation: true,
            enableDraw: true,
            onSaveButtonClick: function() {
                alert('Save button clicked! Please override onSaveButtonClick handler.');
            },
            onReloadButtonClick: function() {
                alert('Reload button clicked! Please override onReloadButtonClick handler.');
            }
        };
        
        if (!config) config = {};
        Mate.applyIf(defaultConfig, config);
        
        this.target = target;
        this.config = config;
        this.map, this.tiles, this.drawnItems, this.newDrawnItems = {};
        
        this.initializeMap();
        this.initializeTiles();
        this.initializeMainLayer();
        this.initializeControls();
        
    } else {
        console.error("Fatal error during initialization!");
        console.error("Bye-Bye...");
    }
}

/*
 * Checking wrapper dependencies
 */
Lewrap.prototype.checkDependencies = function() {
    var status = true;
    if (typeof L === 'undefined' || !L.version) {
        console.error("Leaflet library not found!");
        status = false;
    }
    if (typeof Mate === 'undefined' || !Mate.version) {
        console.error("Mate library not found!");
        status = false;
    }
    return status;
};

/*
 * Initialize leaflet map with initial config
 */
Lewrap.prototype.initializeMap = function() {
    var mapConfig = {
        center: new L.LatLng(this.config.position[0], this.config.position[1]),
        zoom: this.config.zoom,
        zoomAnimation: this.config.zoomAnimation
    };
    var map = new L.Map(this.target, mapConfig);
    this.map = map;
    map.lewrapper = this;
};

/*
 * Initialize leaflet tile layers with initial config
 */
Lewrap.prototype.initializeTiles = function() {
    var tileLayers = {};
    var tiles = this.config.tiles;
    var tilesCount = tiles.length;
    if (tilesCount > 0) {
        for (var i = 0; i < tilesCount; i++) {
            var tileType = this.config.tiles[i];
            var layer = tileType.tileFunc(this.config[tileType.configProperty]);
            tileLayers[tileType.tileName] = layer;
        }
    }
    this.tiles = tileLayers;
};

/*
 * Initialize leaflet main tile layer (displayed at the first drawing)
 * defaultLayer property of config
 */
Lewrap.prototype.initializeMainLayer = function() {
    var mainLayer = this.tiles[this.config.defaultLayer.tileName];
    this.map.addLayer(mainLayer);
};

/*
 * Initialize leaflet controls (Control.Layers, Control.Draw)
 */
Lewrap.prototype.initializeControls = function() {
    var controlsConfig = {};
    for (var tileName in this.tiles) {
        var tile = this.tiles[tileName];
        controlsConfig[tileName] = tile;
    }
    this.map.addControl(new L.Control.Layers(controlsConfig));
    this.drawnItems = L.featureGroup().addTo(this.map);
    if (this.config.enableDraw) {
        var drawConfig = {
            edit: {
                featureGroup: this.drawnItems,
                poly : {
                    allowIntersection : true
                }
            },
            draw: {
                polygon : {
                    allowIntersection: true,
                    showArea:true,
                    circle: false
                }
            }
        };
        this.map.addControl(new L.Control.Draw(drawConfig));
    }
    this.map.on('draw:created', function(event) {
        var layer = event.layer;
        this.drawnItems.addLayer(layer);
        this.drawnItems.on('click', function(e) { console.log(e); });
    }, this);
    
    var me = this;
    $('#map-pulse-save-button').on('click', function() { me.config.onSaveButtonClick(me); });
    $('#map-pulse-reload-button').on('click', function() { me.config.onReloadButtonClick(me); });
};

/*
 * Initialize UTM Grid Layer
 */
Lewrap.prototype.initializeUTMGrid = function() {
    var me = this;
    L.Util.requestAnimFrame(function() {
        var options = {
        gridType: 'distance',
        showMetric: true
        };
        var scale = L.control.gridscale(options); 
        scale.addTo(this.map);
        var utmLayer = L.grids.utm({lewrap: me});
        var mgrsLayer = L.grids.mgrs({lewrap: me});
        utmLayer.addTo(this.map);
        mgrsLayer.addTo(this.map);
    }, this);
};

/*
 * Redraw leaflet map (crutch for rendering bug in bootstrap tabs)
 * Bind on shown.bs.tab event
 */
Lewrap.prototype.redrawMap = function() {
    L.Util.requestAnimFrame(this.map.invalidateSize,this.map,!1,this.map._container);
};

/*
 * Clear leaflet map
 */
Lewrap.prototype.clearMap = function() {
    this.drawnItems.clearLayers();
};

/*
 * Add new GeoJSON Feature to current leaflet map GeoJSON layer.
 * @json GeoJSON (http://geojson.org/)
 * @config Feature configuration (http://leafletjs.com/examples/geojson/)
 */
Lewrap.prototype.geoJson = function(json, config, preload) {
    var me = this;
    
    if (Mate.isArray(json)) {
        json = {
            type: 'FeatureCollection',
            features: json
        };
    }
    
    var setConfig = config || {
        pointToLayer: function (feature, latlon) {
            if (feature.properties.point_type) {
                if (feature.properties.point_type === 'circle') {
                    return L.circle(latlon, feature.properties.radius);
                } else if (feature.properties.point_type === 'marker') {
                    return L.marker(latlon);
                }
            }
        },
        style: function (feature) {
//            return {color: feature.properties.color};
        },
        onEachFeature: function (feature, layer) {
            //featureGroup.addLayer(layer);
            //layer.bindPopup(feature.properties.description);
            //layer.openPopup();
            layer.on('mouseover', function(e) { 
                e.target.setStyle({opacity: 0.1, fillOpacity: 0.6});
            });
            layer.on('mouseout', function(e) { 
                e.target.setStyle({opacity: 1, fillOpacity: 0.2});
            });
            if (feature.properties.isGridSquare) {
                me.bindGridPolygon(layer);
            } else {
                me.bindPolygon(layer);
            }
        }
    };
    var geoLayer = L.geoJSON(json, setConfig);
    var layers = geoLayer.getLayers();
    for(var layerIndex in layers) {
        this.drawnItems.addLayer(layers[layerIndex]);
    }
    this.drawnItems.addTo(this.map);
    
    if (!preload) {
        $('#map-pulse-save-button').fadeIn();
        $('#map-pulse-reload-button').fadeIn();
    }
};

Lewrap.prototype.bindPolygon = function(layer) {
    var me = this;
    layer.on('click', function(e) { 
        var popup = new L.Popup();
        var panel = "<h3>Разбиение области на квадраты</h3></br>"
                    +"<form>"
                        + "<div class=\"form-group\">"
                            + "<div class=\"btn-group\" name=\"dogridgroup\" data-toggle=\"buttons\">"
                                + "<label class=\"btn btn-primary active\">"
                                    + "<input type=\"radio\" name=\"gridoptions\" value=\"parts\" autocomplete=\"off\" checked>Части"
                                + "</label>"
                                + "<label class=\"btn btn-primary\">"
                                    + "<input type=\"radio\" name=\"gridoptions\" value=\"sqmeters\" autocomplete=\"off\">Кв. Метры"
                                + "</label>"
                                + "<label class=\"btn btn-primary\">"
                                    + "<input type=\"radio\" name=\"gridoptions\" value=\"width\" autocomplete=\"off\">Длина"
                                + "</label>"
                            + "</div>"
                        + "</div>"
                        + "<div class=\"form-group\">"
                          + "<input type=\"number\" name=\"gridnumber\" class=\"form-control\" placeholder=\"Введите число\">"
                        + "</div>"
                        + "<button type=\"button\" name=\"dogrid\" class=\"btn btn-default\">Разбить</button>"
                    + "</form>"
        popup.setContent(panel);
        var centerPt = turf.center(e.target.feature);
        var gridCenter = new L.LatLng(centerPt.geometry.coordinates[1], centerPt.geometry.coordinates[0]);
        popup.setLatLng(gridCenter);
        me.map.openPopup(popup);
        me.bindPolygonPopupButtons(e.target);
    });
};

Lewrap.prototype.bindGridPolygon = function(layer) {
    var me = this;
    layer.on('click', function(e) { 
        var popup = new L.Popup();
        popup.setContent("<h3>Квадрат поиска #"+e.target.feature.properties.number+"</h3>");
        var centerPt = turf.center(e.target.feature);
        var gridCenter = new L.LatLng(centerPt.geometry.coordinates[1], centerPt.geometry.coordinates[0]);
        popup.setLatLng(gridCenter);
        me.map.openPopup(popup);
    });
};

Lewrap.prototype.bindPolygonPopupButtons = function(layer) {
    var me = this;
    Mate.Selector('button[name=dogrid]').on('click', layer, function(e) {
        var num = Mate.Selector(e.target).parent('form').find('input[name=gridnumber]').val();
        var selectedOption = Mate.Selector(e.target).parent('form').find('input[name=gridoptions]:checked').val()
        
        var bbox = turf.bbox(e.data.feature);
        var bboxPolygon = turf.bboxPolygon(bbox);
        var bboxPolygonCoordinates = bboxPolygon.geometry.coordinates[0];
        var bboxWidth = turf.distance(turf.point(bboxPolygonCoordinates[0]), turf.point(bboxPolygonCoordinates[1]));
        var bboxHeight = turf.distance(turf.point(bboxPolygonCoordinates[0]), turf.point(bboxPolygonCoordinates[3]));
        
        if (selectedOption === 'sqmeters') {
            num = Math.sqrt(num) / 1000;
        }
        
        if (selectedOption === 'parts') {
            var square = bboxWidth * bboxHeight;
            var partSquare = square / num;
            num = Math.sqrt(partSquare);
        }
        
        var squareGrid = turf.squareGrid(bbox, num);
        var featuresInPolygon = [];
        var num = 1;
        for(var i = 0; i < squareGrid.features.length; i++) {
            var curFeature = squareGrid.features[i];
            var intersection = turf.intersect(e.data.feature, curFeature);
            if (intersection) {
                intersection.properties['isGridSquare'] = true;
                intersection.properties['number'] = num;
                featuresInPolygon.push(intersection);
                num++;
            }
        }
        squareGrid.features = featuresInPolygon;
        me.geoJson(squareGrid);
        me.map.closePopup();
    });
};

/*
 * Get GeoJSON from current leaflet map GeoJSON layer.
 */
Lewrap.prototype.toGeoJson = function() {
    //TODO use this.drawnItems.eachLayer() and add manually geometry style properties to geoJson.
    return this.drawnItems.toGeoJSON();
};

/*
 * Add a single button to current map.
 * @param glyphClass Awesome font glyphs class eg. "fa fa-star"
 * @param fn Click handler
 */
Lewrap.prototype.addButton = function(glyphClass, fn, scope) {
    L.easyButton(glyphClass, fn, scope).addTo(this.map);
};

/*
 * Add a button group to current map.
 * @param buttons Array of easyButton configs
 * @param options Options for initialize leaflet control
 */
Lewrap.prototype.addButtonGroup = function(buttons, options) {
    var easyButtons = [];
    for (var i = 0; i < buttons.length; i++) {
        easyButtons.push(L.easyButton(buttons[i].class, buttons[i].fn, buttons[i].scope));
    }
    L.easyBar(easyButtons, options).addTo(this.map);
};

/*
 * Circle configuration for correct translate feature to GeoJson
 */
var circleToGeoJSON = L.Circle.prototype.toGeoJSON;
L.Circle.include({
    toGeoJSON: function() {
        var feature = circleToGeoJSON.call(this);
        feature.properties = {
            point_type: 'circle',
            radius: this.getRadius()
        };
        return feature;
    }
});

/*
 * Marker configuration for correct translate feature to GeoJson
 */
var markerToGeoJSON = L.Marker.prototype.toGeoJSON;
L.Marker.include({
    toGeoJSON: function() {
        var feature = markerToGeoJSON.call(this);
        feature.properties = {
            point_type: 'marker'
        };
        return feature;
    }
});