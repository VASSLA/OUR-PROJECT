/*
 * CREATE A NEW LEAFLET CONTROL FOR THE SCALE (used by distance grids)
 */

L.Control.GridScale = L.Control.Scale.extend({
	options: {
		position: 'bottomright',
		gridType: 'none',
		showMetric: true, // true for metric, false for imperial
		updateWhenIdle: true,
		maxWidth: 100,
	},
	
	onAdd: function (map) {
		this._map = map;

		var mainContainer = L.DomUtil.create('div', '');
		var className = 'leaflet-control-scale',
		    container = L.DomUtil.create('div', className, mainContainer),
		    options = this.options;

		//this._addScales(options, className, container);
		this._addDistanceLabel(options, mainContainer);

		map.on(options.updateWhenIdle ? 'moveend' : 'move', this._update, this);
		map.whenReady(this._update, this);

		return mainContainer;
	},

	_addDistanceLabel: function(options, container){
		this._distLabel = L.DomUtil.create('div', '', container);
	},

	_update: function () {
		var bounds = this._map.getBounds(),
		    centerLat = bounds.getCenter().lat,
		    halfWorldMeters = 6378137 * Math.PI * Math.cos(centerLat * Math.PI / 180),
		    dist = halfWorldMeters * (bounds.getNorthEast().lng - bounds.getSouthWest().lng) / 180,
		    options = this.options,
		    gridSize = this._gridSpacing(options),
		    size = this._map.getSize(),
		    maxMeters = 0;
		if (size.x > 0) {
			maxMeters = dist * (options.maxWidth / size.x);
		}

		this._updateDistance(gridSize);

		//this._updateScales(options, maxMeters);
	},

	_updateDistance: function (gridSize) {	
		if(gridSize){
			this._distLabel.innerHTML = '<b>Сетка : ' + gridSize + '</b>';
		}else{
			this._distLabel.innerHTML = '';
		}
	},

	_gridSpacing: function (options) {
		var zoom = this._map.getZoom(); 
		var metricSpacing = [   
            "Слишком далеко", // 0 
            "Слишком далеко", // 1
            "Слишком далеко", // 2
            "Слишком далеко", // 3
            "Слишком далеко", // 4
            "Слишком далеко", // 5
            "Слишком далеко", // 6
            "Слишком далеко", // 7
            "Слишком далеко", // 8
            "16 km", // 9
            "8 km", // 10
            "4 km", // 11
            "2 km", // 12
            "1000 m", // 13
            "500 m", // 14
            "500 m", // 15
            "500 m", // 16
            "500 m", // 17
            "500 m" // 18
        ]; 
        var imperialSpacing = [
            "10000 mi", // 0
            "5000 mi", // 1
            "2500 mi", // 2
            "1000 mi", // 3
            "500 mi", // 4
            "250 mi", // 5
            "100 mi", // 6
            "50 mi", // 7
            "25 mi", // 8
            "10 mi", // 9
            "5 mi", // 10
            "2.5 mi", // 11
            "1 mi", // 12
            "2500 ft", // 13
            "1000 ft", // 14
            "500 ft", // 15
            "250 ft", // 16
            "100 ft ", // 17
            "50 ft", // 18
        ]; 
        if (options.gridType == 'distance'){
        	if(options.showMetric){
        		return metricSpacing[zoom];
        	}
        	if(!options.showMetric){
        		return imperialSpacing[zoom];
        	}
        }
	},
});

L.control.gridscale = function (options) {
	return new L.Control.GridScale(options);
};
