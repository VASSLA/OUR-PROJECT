/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: TempateProcessor.js
 * Date: Feb 12, 2017 2:06:58 PM
 * Author: Alex */

/* global Mate */
(function(){
    Mate.template = Mate.template || {};
    Mate.template.applyData = function (template, dataModel) {
        if (!Mate.Selector)
            return 'Mate Selector is undefined or null';
        var pattern = /{([^}]*)}/g;
        var result = template;
        result = result.replace(pattern, function(match) {
            var field = match.substring(1, match.length - 1);
            return dataModel.get(field);
        });
        return result;
    };
})();
