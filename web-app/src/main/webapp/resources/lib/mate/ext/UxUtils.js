/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: UxUtils.js
 * Date: Jan 14, 2017 10:40:11 PM
 * Author: Alex */

/* global Mate */
(function () {
    Mate.ux = Mate.ux || {};
    Mate.ux.centerBlock = function (selector, viewPortSelector, zindex) {
        if (!Mate.Selector)
            return 'Mate Selector is undefined or null';
        var outerElementWidth = Mate.Selector(selector).outerWidth();//.replace('px', '');
        var outerElementHeight = Mate.Selector(selector).outerHeight();//.replace('px', '');
        var outerViewPortWidth = Mate.Selector(viewPortSelector).innerWidth();//.replace('px', '');
        var outerViewPortHeight = Mate.Selector(viewPortSelector).innerHeight();//.replace('px', '');
        var left = Math.round((outerViewPortWidth - outerElementWidth) / 2);
        var top = Math.round((outerViewPortHeight - outerElementHeight) / 2);
        Mate.Selector(selector).css('position', 'absolute');
        Mate.Selector(selector).css('left', left + 'px');
        Mate.Selector(selector).css('top', top + 'px');
        if (zindex) {
            Mate.Selector(selector).css('z-index', zindex);
        }
    };
    Mate.ux.getBase64FromImageUrl = function (url, callback) {
        var img = new Image();
        img.setAttribute('crossOrigin', 'anonymous');
        img.onload = function () {
            var canvas = document.createElement("canvas");
            canvas.width = this.width;
            canvas.height = this.height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(this, 0, 0);
            var dataURL = canvas.toDataURL("image/png");
            if (callback) {
                callback(dataURL);
            }
        };
        img.src = url;
    };
})();
