/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: YandexGeocoder.js
 * Date: Feb 19, 2017 1:55:39 PM
 * Author: Alex */

/* global Mate */
(function () {
    Mate.service = Mate.service || {};
    Mate.service.GeocodingProvider = {
        DUMMY: 0,
        YANDEX: 1
    };
    Mate.service.getCoordinates = function (address, provider, callback) {
        var url, parameters, type;
        provider = provider || Mate.service.GeocodingProvider.DUMMY;
        switch (provider) {
            case Mate.service.GeocodingProvider.YANDEX:
                url = 'https://geocode-maps.yandex.ru/1.x/';
                parameters = {
                    geocode: address,
                    format: 'json'
                }
                break;
            default:
                break;
        }
        Mate.Selector.ajax({
            url: url,
            data: parameters,
            dataType: 'jsonp',
            jsonp: 'callback',
            success: function(response) {
                if (callback) callback(response);
            },
            error: function(response) {
                debugger;
                if (callback) callback(response);
            }
        });
    };
})();
