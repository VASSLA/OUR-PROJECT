/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: ResourceManager.js
 * Date: Jan 15, 2017 4:30:56 PM
 * Author: Alex */

/* global Mate */
Mate.define('Mate.app.ResourceManager', {
    extends: 'Mate.core.Class',
    resources: {},
    _resources: null,
    _listLoaded: false,
    _startTime: null,
    _endTime: null,
    _lastLoadTime: null,
    maximumWaitTimeoutMs: 10000,
    mixins: [
        'Mate.mixin.ResourceLoader'
    ],
    ctor: function (config) {
        Mate.apply(config, this, true);
        this._init();
    },
    methods: {
        _init: function () {
            this._resources = new Object();
            for (var name in this.resources) {
                this.addResource(name, this.resources[name].url);
            }
        },
        addResource: function(name, resourceURL) {
            this._resources[name] = {url: resourceURL, content: '', loaded: false, status: 0};
        },
        removeResource: function(name) {
            delete this._resources[name];
        },
        cleanResourceList: function() {
            this._resources.length = 0;
        },
        writeLoadingResult: function(response) {
            for (var name in this._resources) {
                var resource = this._resources[name];
                // Check that url in response ended by request url
                // because /some/thing is converted http://host/some/thing
                // after making request
                var a = response.requestURL.lastIndexOf(resource.url);
                var b = response.requestURL.length - resource.url.length;
                if (a === b) {
                    Mate.Logger.debug(name + ' (' + this._resources[name].url + ') loading has been completed with status ' + response.status);
                    resource.status = response.status;
                    resource.loaded = true;
                    resource.content = response.responseText;
                    this._resources[name] = resource;
                }
            }
        },
        checkIsListLoaded: function(context) {
            this.counter = this.counter || 0;
            this.counter++;
            Mate.Logger.debug('Trial number ' + this.counter);
            for (var name in context._resources) {
                if (!context._resources[name].loaded) {
                    Mate.Logger.debug('Still waiting ...');
                    context._listLoaded = false;
                    if (this.counter < (context.maximumWaitTimeoutMs / 100))
                        setTimeout(context.checkIsListLoaded, 100, context);
                    return;
                }
            }
            context._listLoaded = true;
            Mate.Logger.debug('Resources loading completed');
            context.onResourceListLoaded();
        },
        load: function() {
            Mate.Logger.debug('Resources loading started');
            var me = this;
            for (var name in this._resources) {
                Mate.Logger.debug(name + ' (' + this._resources[name].url + ') is loading ...');
                this.request('GET', this._resources[name].url, null, null, true, function(response) {
                    me.writeLoadingResult(response);
                });
            }
            setTimeout(this.checkIsListLoaded, 100, me);
        },
        getResource: function(name) {
            return this._resources[name];
        },
        getResourceContent: function(name) {
            return this._resources[name].content;
        },
        onResourceListLoaded: Mate.emptyFn
    }
});
