/* Umka - Self Learning Resource
 * Breakpoint Software (c) 2015
 * Project: application
 * File Name: StateManager.js
 * Date: Mar 24, 2016 6:13:56 PM
 * Author: Alex */

/* global Mate */
Mate.define('Mate.app.StateManager', {
    extends: 'Mate.core.Class',
    ctor: function (config) {
        Mate.Logger.trace('"Mate.app.StateManager" constructor has been called for "' + this.classname + '"');
        Mate.apply(config, this, true);
    },
    stateVerbs: ['activate', 'deactivate'],
    application: null,
    currentStateName: '',
    methods: {
        setState: function (stateName) {
            var requestedState = null;
            if (this.currentStateName === stateName) {
                Mate.Logger.info('Application is already in requested state, switching is not needed');
            }
            if (!this.application) {
                Mate.Logger.error('Application was not set for the state manager, nothing to control');
                return;
            }
            if (!this.states || !this.states.length) {
                Mate.Logger.warning('No any states were defined');
            }
            for (var index in this.states) {
                if (this.states[index].name && stateName === this.states[index].name) {
                    requestedState = this.states[index];
                    break;
                }
            }
            if (!requestedState) {
                Mate.Logger.warning('Requested state "' + stateName + '" wasn\'t defined');
                return;
            } else {
                Mate.Logger.info('State "' + stateName + '" was found, switching ...');
            }
            for (var propertyName in requestedState) {
                if (!requestedState.hasOwnProperty(propertyName)) continue;
                if (!~this.stateVerbs.indexOf(propertyName)) continue;
                switch (propertyName) {
                    case 'activate' :
                        var controllersToActivate = requestedState[propertyName];
                        if (~controllersToActivate.indexOf('*')) {
                            controllersToActivate = this.application.controllers;
                        }
                        for (var i in controllersToActivate) {
                            this.application.getControllerByName(controllersToActivate[i]).init();
                        }
                        break;
                    case 'deactivate' :
                        var controllersToDeactivate = requestedState[propertyName];
                        if (~controllersToDeactivate.indexOf('*')) {
                            controllersToDeactivate = this.application.controllers;
                        }
                        for (var i in controllersToDeactivate) {
                            this.application.getControllerByName(controllersToDeactivate[i]).deinit();
                        }
                        break;
                }
            }
            this.currentStateName = stateName;
        },
        setApplication: function(application) {
            this.application = application;
        }
    },
    states: [
    ]
});
