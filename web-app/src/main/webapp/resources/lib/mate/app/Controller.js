/* Umka - Self Learning Resource
 * Breakpoint Software (c) 2015
 * Project: application
 * File Name: Controller.js
 * Date: Jan 17, 2016 1:23:21 PM
 * Author: Alex */

/* global Mate */
Mate.define('Mate.app.Controller', {
    extends: 'Mate.core.Class',
    autoinit: false,
    ctor: function (config) {
        Mate.Logger.trace('"Mate.app.Controller" constructor has been called for "' + this.classname + '"');
        Mate.apply(config, this, true);
        if (this.autoinit)
            this.init();
    },
    init: function () {
        this.beforeInit(this);
        Mate.Logger.trace('"' + this.getClassName() + '" initialization has been called');
        for (var expression in this.interactives) {
            Mate.Logger.trace('Found "' + expression + '" in interactives list');
            var expressionObject = this.interactives[expression];
            if (expression === 'window') {
                expression = window;
            }
            for (var action in expressionObject) {
                Mate.Logger.trace('Function "' + expressionObject[action] + '" has been bound to "'  + expression + '.' + action + '" event');
                Mate.Logger.trace(Mate.Selector(expression));
                Mate.Selector(expression).on(action, this, Mate.Selector.proxy(this[expressionObject[action]], this));
            }
        }
        this.afterInit(this);
    },
    beforeInit: Mate.emptyFn,
    afterInit: Mate.emptyFn,
    beforeDeinit: Mate.emptyFn,
    afterDeinit: Mate.emptyFn,
    deinit: function () {
        this.beforeDeinit(this);
        Mate.Logger.trace('"' + this.getClassName() + '" deinitialization has been called');
        for (var expression in this.interactives) {
            Mate.Logger.trace('Found "' + expression + '" in interactives list');
            var expressionObject = this.interactives[expression];
            if (expression === 'window') {
                expression = window;
            }
            for (var action in expressionObject) {
                Mate.Logger.trace('Function "' + expressionObject[action] + '" has been unbound from "' + expression + '.' + action + '" event');
                Mate.Logger.trace(Mate.Selector(expression));
                Mate.Selector(expression).off(action, Mate.Selector.proxy(this[expressionObject[action]], this));
            }
        }
        this.afterDeinit(this);
    },
    interactives: {}
});
