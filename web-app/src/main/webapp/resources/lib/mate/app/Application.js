/* Umka - Self Learning Resource
 * Breakpoint Software (c) 2015
 * Project: application
 * File Name: App.js
 * Date: Jan 16, 2016 1:50:52 PM
 * Author: Alex Blade <alex.blade.box@gmail.com> */

/* global Mate */
Mate.define('Mate.app.Application', {
    extends: 'Mate.core.Class',
    ctor: function (config) {
        Mate.Logger.trace('"Mate.app.Application" constructor has been called for "' + this.classname + '"');
        Mate.apply(config, this, true);
        Mate.require(this.controllers, true);
        Mate.require(this.stores, true);
        for (var i = 0; i < this.controllers.length; i++) {
            var instance = Mate.create(this.controllers[i]);
            Mate.Logger.trace('An instance of "' + instance.getClassName() +
                    '" has been created');
            this.activeControllers[this.controllers[i]] = instance;
        }
        for (var i = 0; i < this.stores.length; i++) {
            var instance = Mate.create(this.stores[i]);
            Mate.Logger.trace('An instance of "' + instance.getClassName() +
                    '" has been created, data was placed below');
            this.activeStores[this.stores[i]] = instance;
        }
        if (this.onLoad && typeof this.onLoad === 'function') {
            this.onLoad();
        }
    },
    onLoad: null,
    methods: {
        getControllerByName: function(name) {
            if (this.activeControllers[name]) {
                return this.activeControllers[name];
            } else {
                for (var controllerName in this.activeControllers) {
                    if (controllerName.lastIndexOf(name) ===
                            controllerName.length - name.length) {
                        return this.activeControllers[controllerName];
                    }
                }
            }
        },
        getStoreByName: function(name) {
            if (this.activeStores[name]) {
                return this.activeStores[name];
            } else {
                for (var storeName in this.activeStores) {
                    if (storeName.lastIndexOf(name) ===
                            storeName.length - name.length) {
                        return this.activeStores[storeName];
                    }
                }
            }
        },
        setStateManager: function(stateManager) {
            this.stateManager = stateManager;
            if (this.stateManager)
                this.stateManager.setApplication(this);
        },
        getStateManager: function() {
            return this.stateManager;
        },
        setResourceManager: function(resourceManager) {
            this.resourceManager = resourceManager;
        },
        getResourceManager: function() {
            return this.resourceManager;
        }
    },
    stateManager: null,
    controllers: [
    ],
    activeControllers: [
    ],
    stores: [
    ],
    activeStores: [
    ]
});