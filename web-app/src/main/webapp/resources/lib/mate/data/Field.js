/* Mate RIA Framework
 * Breakpoint Software (c) 2015
 * Project: mate
 * File Name: Field.js
 * Date: Sep 2, 2016 7:33:26 PM
 * Author: Alex Blade */

/**
 * @class Mate.data.Field
 * Base class which implements functionality of the data record's field.
 *
 * All of objects which also implements functionality pointed above must be
 * inherited from Mate.data.Field
 */

/* global Mate */
Mate.define('Mate.data.Field', {
    /**
     * Parent class which is class inherited from, defined as a string
     */
    extends: 'Mate.core.Class',
    /**
     * Defines name of data field, value will be selected from raw data object
     * by the name, when the field will be created.
     */
    name: null,
    /**
     * Human understandable name of the field, i.e. a title for a field.
     */
    caption: null,
    /**
     * Description for a field.
     */
    description: null,
    /**
     * Value of the field at the moment. May be different from *_origin* if the
     * field was changed, but commit method wasn't executed.
     */
    value: null,
    /**
     * Type of the field which defines internal logic of the field's data
     * processing. Type can be defined as string, integer, float, boolean
     * string, object, datetime, model.
     */
    type: null,
    /**
     * Maximum size of the field. Actually has the meaning only for string
     * fields. When the field is created and size is defined then a value of
     * the field will be truncated for defined number of symbols.
     */
    size: null,
    /**
     * If type of the field is set as model then value of field will be mapped
     * to a model object which defined in the mapAs parameter.
     */
    mapAs: null,
    /**
     * Returns true if the field is set as unique key field.
     */
    isKey: false,
    /**
     * Private property that holds an original value that was set up to the
     * field when the field was created. Must not be used explicitlty.
     */
    _origin: null,
    /**
     * Private property that indicates if the field was changed. Must not be
     * used explicitly
     */
    _changed: false,
    /**
     * Private property that holds a link to a parent model which contains the
     * field. Must not be used explicitly.
     */
    _model: null,
    /**
     * Private property for storing all of change history if the field is
     * changed. Must not be used explicitly.
     */
    _history: [],
    /**
     * @method
     * If defined then will be called before the field is going to change.
     * Usually, these handlers are defined by parent model to know when fields
     * change their values. If method returns false then the field will not be
     * changed. Syntax: function (field, oldValue, newValue) { ... }.
     */
    beforeChange: null,
    /**
     * @method
     * If defined then will be called atfer the field changed. Usually,
     * these handlers are defined by parent model to know when fields changes
     * their values. Syntax: function (field) { ... }
     */
    afterChange: null,
    /**
     * @method
     * @private
     * Internal consructor which is must **not** be called explicitly. It is
     * using by the Mate subsystem to create new object of any class. Instead of
     * using 'ctor' you must use Mate.create({}) or, if you point class in the
     * required section of config you can use **new** Mate.data.Field({}) form.
     * @param {object} config Configuration object
     */
    ctor: function (config) {
        Mate.apply(config, this, true);
        this._init();
        this.get = this.getValue;
        this.set = this.setValue;
    },
    methods: {
        /**
         * Internal post-construction method to make needed initialization of
         * class fields
         * @returns {void} Returns nothing
         */
        _init: function () {
            this._history = new Array();
            this._origin = this.value;
        },
        /**
         * Returns type of fields, not implemented right now. Reserved for
         * future functionality
         * @returns {string}
         */
        getType: function() {
            return this.type;
        },
        /**
         * Returns name of the model if the field is an object and have to be
         * mapped as one of the application's model.
         * @returns {string}
         */
        getMapAs: function() {
            return this.mapAs;
        },
        /**
         * Returns current value of the field
         * @returns {any}
         */
        getValue: function () {
            return this.value;
        },
        /**
         * Set value of the field. Execute two handlers: before set value and
         * set value. Previous value of the field is written to change hostory.
         * @param {type} value
         * @returns {void}
         */
        setValue: function (value) {
            if (this.beforeChange) {
                allowed = this.beforeChange(this, this.value, value);
            }
            var allowed = allowed || true;
            if (allowed) {
                if (this.value !== value) {
                    this._history.push(this.value);
                }
                this.value = value;
            }
            if (this.value !== this._origin) {
                this._changed = true;
                if (this.afterChange) {
                    this.afterChange(this);
                }
            } else {
                this._changed = false;
            }
        },
        /**
         * Returns original value of the field. If the field was changed by the
         * setValue method then you can get value that was set originally when
         * the field was created.
         * @returns {any}
         */
        getOriginValue: function () {
            return this._origin;
        },
        /**
         * Set up parent model for the field. Usually the method is used
         * internally and there is not necessary to call the method explicitly.
         * @param {type} model
         * @returns {void}
         */
        setModel: function (model) {
            this._model = model;
        },
        /**
         * Returns parent model object for the field.
         * @returns {Mate.data.Model}
         */
        getModel: function () {
            return this._model;
        },
        /**
         * Undo last change that was made with the field value. All of changes
         * of the field is written to history as a stack, so it is possible to
         * use undo to return previous states of the field. After calling commit
         * or rollback methods usage of undo will become impossible.
         * @returns {void}
         */
        undo: function () {
            var previousValue = this._history.pop();
            this.value = previousValue;
            this._changed = this._origin !== this.value;
        },
        /**
         * Return true if the field was changed and commit or rollback methods
         * have not been called
         * @returns {Boolean}
         */
        isChanged: function () {
            return this._changed;
        },
        /**
         * Rollback all changes that were made with the field and restore value
         * of the field to its original state
         * @returns {void}
         */
        rollback: function() {
            this._history = [];
            this.value = this._origin;
            this._changed = false;
        },
        /**
         * Commits all changes that were made with the field and set up the
         * field's original value to current value.
         * @returns {void}
         */
        commit: function() {
            this._history = [];
            this._origin = this.value;
            this._changed = false;
        }
    }
});
