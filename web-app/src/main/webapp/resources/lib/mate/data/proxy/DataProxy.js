/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global Mate */
Mate.define('Mate.data.proxy.DataProxy', {
    /* Private Fields */
    /* Public Fields */
    extends: 'Mate.core.Class',
    /* Constructor */
    ctor: function (config) {
        Mate.apply(config, this, true);
        this._init();
    },
    /* Methods */
    methods: {
        /* Private */
        _init: function() {},
        create: Mate.emptyFn,
        read: Mate.emptyFn(),
        update: Mate.emptyFn(),
        destroy: Mate.emptyFn()
    }
});
