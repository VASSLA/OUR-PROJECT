/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global Mate */
Mate.define('Mate.data.proxy.RemoteDataProxy', {
    /* Public Fields */
    actionsMethods: {
        create: 'POST',
        read: 'GET',
        update: 'PUT',
        destroy: 'DELETE'
    },
    async: true,
    extends: 'Mate.data.proxy.DataProxy',
    headers: [
        {name: 'Accept', value: 'application/json'},
        {name: 'Content-Type', value: 'application/json'}
    ],
    store: null,
    url: '',
    ctor: function (config) {
        Mate.apply(config, this, true);
        this._init();
    },
    /* Methods */
    methods: {
        _init: function () {},
        _ok: function(status) {
            return status >= 200 && status < 300;
        },
        _buildParamString: function(parameters) {
            var params = [];
            for (var key in parameters) {
                params.push(key + '=' + parameters[key]);
            }
            return '?' + params.join('&');
        },
        read: function () {
            var me = this;
            if (!this.async) {
                var response = this.request(this.actionsMethods.read, this.url + this._buildParamString(this.store.getFilters()), null, this.headers, this.async, null);
                this.store.onDataReady(JSON.parse(response.responseText));
            } else {
                this.request(this.actionsMethods.read, this.url + this._buildParamString(this.store.getFilters()), null, this.headers, this.async, function (response) {
                    me.store.onDataReady(JSON.parse(response.responseText));
                });
            }
        },
        synchronize: function () {
            var data = this.store.getData();
            var me = this;
            for (var i = 0; i < data.length; i++) {
                if (!data[i].isChanged()) {
                    continue;
                }
                var action = '';
                var resourceId = '';
                var keyFieldValue = data[i].getKeyFieldValue();
                if (data[i].isDeleted()) {
                    action = this.actionsMethods.destroy;
                    resourceId = keyFieldValue ? ('/' + keyFieldValue) : '';
                } else if (keyFieldValue === null || keyFieldValue === undefined) {
                    action = this.actionsMethods.create;
                } else {
                    action = this.actionsMethods.update;
                }
                if (action !== '') {
                    Mate.Logger.debug(action);
                    if (!this.async) {
                        var response = this.request(action, this.url + resourceId, data[i].getJson(), this.headers, this.async, null);
                        if (this._ok(response.status)) {
                            data[i].commit();
                        }
                        this.store.onSynchronizeComplete(JSON.parse(response.responseText));
                    } else {
                        this.request(action, this.url + resourceId, data[i].getJson(), this.headers, this.async, function (response) {
                            //TODO: Doesn't work :( data[i].commit();
                            me.store.onSynchronizeComplete(JSON.parse(response.responseText));
                        });
                    }
                }
            }
        }
    },
    mixins: [
        'Mate.mixin.ResourceLoader'
    ]
});
