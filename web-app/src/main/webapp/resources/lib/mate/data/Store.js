/* Green Factory Platform
 * Breakpoint Software (c) 2015
 * Project: web-app
 * File Name: Record.js
 * Date: Sep 2, 2016 7:33:26 PM
 * Author: Alex */

/**
 * @class Mate.data.Store
 * Base class which implements functionality of the data storage.
 *
 * All of objects which also implements functionality pointed above must be
 * inherited from Mate.data.Store
 */
/* global Mate */
Mate.define('Mate.data.Store', {
    /* Private Fields */
    _raw: [],
    _data: [],
    _isLoaded: false,
    /* Public Fields */
    autoLoad: false,
    autoSync: false,
    extends: 'Mate.core.Class',
    model: '',
    proxy: 'Mate.data.proxy.RemoteDataProxy',
    filters: null,
    /* Constructor */
    ctor: function (config) {
        Mate.apply(config, this, true);
        this._init();
    },
    methods: {
        _init: function () {
            this._data = new Array();
            this._raw = new Array();
            if (!this.proxy) {
                this.proxy = Mate.create('Mate.data.proxy.DataProxy', {});
            }
            this.proxy.store = this;
            if (this.model === '') {
                this.model = 'Mate.data.Model';
            }
            if (this.autoLoad) {
                this.proxy.read();
            }
        },
        onDataLoaded: null,
        onSynchronized: null,
        onDataReady: function (raw) {
            this._raw = raw;
            if (!(raw instanceof Array)) {
                raw = [raw];
            }
            for (var i = 0; i < raw.length; i++) {
                var model = Mate.create(this.model, {raw: raw[i]});
                model.setStore(this);
                this._data.push(model);
            }
            this._isLoaded = true;
            if (this.onDataLoaded && Mate.isFunction(this.onDataLoaded)) {
                this.onDataLoaded();
            }
        },
        onSynchronizeComplete: function (raw) {
            this._isLoaded = true;
            if (this.onSynchronized && Mate.isFunction(this.onSynchronized)) {
                this.onSynchronized();
            }
        },
        getData: function (index) {
            if (index === undefined) {
                return this._data;
            } else if (this._data.length - 1 >= index) {
                return this._data[index];
            } else {
                Mate.Logger.error('Record index out of bounds (' + index + ')');
                return false;
            }
        },
        getRawData: function() {
            return this._raw;
        },
        /**
         * Method loads data from remote server
         * @returns {boolean} True if there are no any erroes during loading
         */
        load: function () {
            this._isLoaded = false;
            this.clear();
            this.proxy.read();
        },
        /**
         * Method commits changes locally
         * @returns {void}
         */
        commit: function () {
            for (var i = 0; i < this._data.length; i++) {
                this._data.commit();
            }
        },
        /**
         * Method roolbacks changes locally and reset all changes
         * @returns {void}
         */
        rollback: function () {
            for (var i = 0; i < this._data.length; i++) {
                this._data.rollback();
            }
        },
        /**
         * Method synchronizes changes between clients and server stores
         * @returns {boolean} True if there are no any errors during syncing
         */
        sync: function () {
            this._isLoaded = false;
            this.proxy.synchronize();
        },
        /**
         * Method sets proxy object to store
         * @param {Mate.data.proxy.DataProxy} proxy
         * @returns {void}
         */
        setProxy: function (proxy) {
            this.proxy = proxy;
        },
        /**
         * Method returns a proxy object assigned to store
         * @returns {Mate.data.proxy.DataProxy}
         */
        getProxy: function () {
            return this.proxy;
        },
        addModel: function (model) {
            this._data.push(model);
            model.setStore(this);
            model.setChanged(true);
        },
        addModelFromRaw: function (data) {
            this._data.push(Mate.create(this.model, {raw: data}));
            this._data[this._data.length - 1].setStore(this);
            this._data[this._data.length - 1].setChanged(true);
        },
        newModel: function() {
            var model = Mate.create(this.model, {});
            model.setStore(this);
            model.setChanged(true);
            this._data.push(model);
            return model;
        },
        clear: function () {
            this._data = [];
        },
        getDataByKeyFieldValue: function(keyFieldValue) {
            for (var i = 0; i < this._data.length; i++) {
                if (this._data[i].getKeyFieldValue() === keyFieldValue) {
                    return this._data[i];
                }
            }
            return false;
        },
        /**
         *
         * @param {String} field
         * @param {Any} value
         * @param {type} partial
         * @returns {Array|Boolean}
         */
        lookup: function (field, value, partial) {
            partial = partial || false;
            for (var i = 0; i < this._data.length; i++) {
                if (partial) {
                    if (~this._data[i].getFieldByName(field).getValue().indexOf(value)) {
                        return this._data[i];
                    }
                } else {
                    if (this._data[i].getFieldByName(field).getValue() == value) {
                        return this._data[i];
                    }
                }
            }
            return false;
        },
        /**
         * Returns state of store loading, if store id loaded returns **true**
         * else returns **false**
         * @returns {Boolean}
         */
        isLoaded: function () {
            return this._isLoaded;
        },
        /**
         * Returns count of data records which store contains
         * @returns {Number}
         */
        size: function() {
            return this._data.length;
        },
        setFilters: function(filters) {
            this.filters = filters;
        },
        getFilters: function() {
            return this.filters;
        }
    }
});

