/* Green Factory Platform
 * Breakpoint Software (c) 2015
 * Project: web-app
 * File Name: Record.js
 * Date: Sep 2, 2016 7:33:26 PM
 * Author: Alex */

/**
 * @class Mate.data.Model
 * Base class which implements functionality of the data record. A data record
 * consists of any number of data fields,
 *
 * All of objects which also implements functionality pointed above must be
 * inherited from Mate.data.Model
 */
/* global Mate */
Mate.define('Mate.data.Model', {
    //_fieldsMap: {}, //TODO: Check this
    /**
     * Private property which holds parent store object for the model. Do
     * not get value of the property directly. Instead, use getStore() method.
     */
    _store: null,
    /**
     * Private property which holds history of field changing. The property
     * contains names of fields were changed in reverse chronological order.
     * So, each execution of the undo method will revert state of next field in
     * the history.
     */
    _history: [],
    /**
     * Private property which reflect state of the model deletion. Do not use
     * the property directly. Instead, use isDeleted() method.
     */
    _deleted: false,
    /**
     * Private property which reflect state of the model changes. Do not use
     * the property directly. Instead, use isChanged() method.
     */
    _changed: false,
    /**
     * Internal property that holds all of fiedls object. Do not use it
     * directly. Instead, use getFieldByName() method, or get() method.
     */
    _fields: [],
    /**
     * Parent class which is class inherited from, defined as a string.
     */
    extends: 'Mate.core.Class',
    /**
     * The property holds fields definition objects, i.e. configurations for
     * every field of the model. A configuration for fields is set through
     * Mate.define() method.
     */
    fields: [],
    /**
     * The name of the key field for the model.
     */
    keyField: 'id',
    /**
     * The property holds raw data for the model. Raw data is stored as a JSON
     * object.
     */
    raw: {},
    /**
     * @method
     * @private
     * Internal consructor which is must **not** be called explicitly. It is
     * using by the Mate subsystem to create new object of any class. Instead of
     * using 'ctor' you must use Mate.create({}) or, if you point class in the
     * required section of config you can use **new** Mate.data.Field({}) form.
     * @param {object} config Configuration object
     */
    ctor: function (config) {
        Mate.apply(config, this, true);
        this._init();
    },
    // internal methods, do not use it explicitly
    methods: {
        /**
         * @private
         * Internal post-construction method to make needed initialization of
         * class fields
         * @returns {void} Returns nothing
         */
        _init: function () {
            this._checkAndDefineModelFields();
            this._processRawData();
        },
        /**
         * @private
         * Internal method which checks field definitions and create field
         * objects. If definition were passed via config object then defined
         * fields' model will be used. In other case model will be constructed
         * with using syntethical data model which is build from raw object.
         */
        _checkAndDefineModelFields: function () {
            this._fields = [];
            this._fieldsMap = {};
            // if model's fields definition exists
            if (typeof this.fields === 'object' && this.fields instanceof Array && this.fields.length > 0) {
                for (var i = 0; i < this.fields.length; i++) {
                    this._fields[i] = Mate.create('Mate.data.Field', this.fields[i]);
                    this._fields[i].setModel(this);
                    this._fields[i].beforeChange = this._beforeFieldChange;
                    this._fields[i].afterChange = this._afterFieldChange;
                    if (this._fields[i].name === this.keyField) {
                        this._fields[i].isKey = true;
                    }
                    this._fieldsMap[this._fields[i].name] = this._fields[i];
                }
                // if model's definition is absent and we have only raw data
            } else {
                for (var fieldName in this.raw) {
                    if (~Mate.ClassManager.exclusions.indexOf(fieldName)) {
                        continue;
                    }
                    var config = {
                        name: fieldName,
                        caption: fieldName,
                        description: fieldName,
                        size: 0,
                        type: 'generic'
                    };
                    var i = this._fields.length;
                    this._fields[i] = Mate.create('Mate.data.Field', config);
                    this._fields[i].setModel(this);
                    this._fields[i].beforeChange = this._beforeFieldChange;
                    this._fields[i].afterChange = this._afterFieldChange;
                    if (this._fields[i].name === this.keyField) {
                        this._fields[i].isKey = true;
                    }
                    this._fieldsMap[this._fields[i].name] = this._fields[i];
                }
            }
        },
        /**
         * @private
         * Internal method which processes raw data and set up values of the
         * model's fields
         */
        _processRawData: function () {
            if (!this.raw) {
                return false;
            }
            for (var key in this.raw) {
                if (this.getFieldByName(key)) {
                    var value;
                    if (this.getFieldByName(key).getType() === 'model' && this.getFieldByName(key).getMapAs()) {
                        value = Mate.create(this.getFieldByName(key).getMapAs(), {raw: this.raw[key]});
                    } else {
                        value = this.raw[key];
                    }
                    this.getFieldByName(key).setValue(value);
                    this.getFieldByName(key).commit();
                }
            }
            this._history = [];
        },
        /**
         * @private
         * Default handler that is called before field of the model is going to
         * change.
         * @param {Mate.data.Field} field Object of the field which is going to
         * change
         * @param {type} oldValue Old value of the field
         * @param {type} newValue New value of the field
         */
        _beforeFieldChange: function (field, oldValue, newValue) {
            Mate.Logger.trace('Model "' + this.getModel().classname + '" is going to be changed. Field "' +
                    field.name + ', Old value: "' + oldValue + '", New Value: "' + newValue + '"');
        },
        /**
         * @private
         * Default handler which is called when a field changed its value
         * @param {Mate.data.Field} field Object of the field
         */
        _afterFieldChange: function (field) {
            Mate.Logger.trace('Model "' + this.getModel().classname + '" has been changed. Field Value: ' + field.getValue());
            this.getModel()._history.push(field.name);
        },
        /**
         * Returns field object by field name
         * @param {string} fieldName
         * @returns {boolean}
         */
        getFieldByName: function (fieldName) {
            return this._fieldsMap[fieldName];
        },
        /**
         * Returns state of model: true - changed/false - unchanged
         * @returns {boolean}
         */
        isChanged: function () {
            if (this._changed) {
                return true;
            }
            for (var i = 0; i < this._fields.length; i++) {
                if (this._fields[i].isChanged()) {
                    return true;
                }
            }
            return false;
        },
        /**
         * Roolbacks current changes and set the model to unchanged state
         * @returns {undefined}
         */
        rollback: function () {
            for (var i = 0; i < this._fields.length; i++) {
                this._fields[i].rollback();
            }
        },
        /**
         * Locally commits changes, but do not sends it to server
         * when commitChanges is made origin state becomes equal to current
         * @returns {undefined}
         */
        commit: function () {
            for (var i = 0; i < this._fields.length; i++) {
                this._fields[i].commit();
            }
        },
        /**
         * Sends changes of the model to the server by calling store method "sync"
         * @returns {undefined}
         */
        sync: function () {
            if (this.store !== null && this.store !== undefined) {
                this.store.sync(this);
            }
        },
        /**
         * Sets parent store object of the model
         * @param {Mate.data.Store} store
         */
        setStore: function (store) {
            this._store = store;
        },
        /**
         * Returns parent store object of the model
         * @returns {Mate.data.Store}
         */
        getStore: function () {
            return this._store;
        },
        /**
         * Returns field object of the model's key field
         * @returns {Mate.data.Field}
         */
        getKeyFieldValue: function () {
            return this.getFieldByName(this.keyField).getValue();
        },
        /**
         * Returns deletion state of the model.
         * @returns {Boolean}
         */
        isDeleted: function () {
            return this._deleted;
        },
        /**
         * Set
         */
        delete: function () {
            this._deleted = true;
            this.setChanged(true);
        },
        undelete: function () {
            this._deleted = false;
        },
        hasField: function(fieldName) {
            return this._fieldsMap[fieldName] !== undefined;
        },
        getJson: function () {
            var result = {};
            for (var i = 0; i < this._fields.length; i++) {
                if (this._fields[i].type === 'model') {
                    if (this._fields[i].value) {
                        result[this._fields[i].name] = this._fields[i].value.getJson();
                    }
                } else {
                    result[this._fields[i].name] = this._fields[i].value;
                }
            }
            return result;
        },
        setChanged: function (changed) {
            this._changed = changed;
        },
        get: function(item) {
            var currentPointer = this;
            var itemParts = item.split('.');
            for (var i in itemParts) {
                if (currentPointer === undefined) {
                    Mate.Logger.warning('Unable to find \'' + item + '\'');
                    return undefined;
                }
                currentPointer = (currentPointer.getFieldByName && currentPointer.getFieldByName(itemParts[i])) ?
                        currentPointer.getFieldByName(itemParts[i]).getValue() : undefined;
            }
            return currentPointer;
        },
        set: function(item, value) { //request.missingPerson.addresses
            var currentPointer = this;
            var itemParts = item.split('.');
            for (var i in itemParts - 1) {
                if (currentPointer === undefined) {
                    Mate.Logger.warning('Unable to find \'' + item + '\'');
                    return undefined;
                }
                if (!currentPointer.getFieldByName) {
                    Mate.Logger.warning('\'' + item + '\' is not a field object');
                    return undefined;
                }
                currentPointer = currentPointer.getFieldByName(itemParts[i]).getValue();
            }
            if (currentPointer.hasField(itemParts[itemParts.length - 1])) {
                currentPointer.getFieldByName(itemParts[itemParts.length - 1]).setValue(value);
            }
        }
    }
});

