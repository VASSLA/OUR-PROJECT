/**
 * Created with JetBrains PhpStorm.
 * User: Alexey S. Anikeev
 * Date: 14.07.12
 * Time: 19:05
 */

/**
 * Module for getting user agent information
 * @type UserAgent
 */
var UserAgent = (function(){
    var
        /**
         * Регулярное выражение возвращающее для браузеров FireFox, Chrome, Safari и Opera массив строк, где
         * [0] - Совпадение полного паттерна (мало нас интересует)
         * [1] - Чем пытается выглядеть браузер
         * [2] - Версия того, чем пытается выглядеть браузер
         * [3] - Движок браузера
         * [4] - Версия движка браузера
         * [5] - Название браузера (Chrome, FireFox, Opera) или слово Version, если это Safari
         * [6] - Версия браузера
         * Для IE этот паттерн вернет null
         */
        REG_TEST_ALL = /^([a-z]+)(?:\/)([0-9,\.]+)(?:.*)(gecko|presto|applewebkit|trident)(?:\/)([0-9,\.]{1,})(?:.*)(firefox|chrome|version)(?:\/)([0-9,\.]+$)/i,
        /**
         * Регулярное выражение возвращающее для браузеров IE массив строк, где
         * [0] - Совпадение полного паттерна (мало нас интересует)
         * [1] - Чем пытается выглядеть браузер
         * [2] - Версия того, чем пытается выглядеть браузер
         * [3] - Название браузера (Chrome, FireFox, Opera) или слово Version, если это Safari
         * [4] - Версия браузера
         * [5] - Движок браузера
         * [6] - Версия движка браузера
         * Для IE этот паттерн вернет null
         */
        REG_TEST_IE = /^([a-z]+)(?:\/)([0-9,\.]+)(?:.*)(msie)(?:[\s]?)([0-9,\.]+)(?:.*)(trident)(?:\/)([0-9,\.]+$)/i;

    var m_singleUserAgent;

    function getUserAgent() {
        var userAgentSignatureString = this.navigator.userAgent;
        var ua = userAgentSignatureString;
        var browserData = REG_TEST_ALL.exec(ua);
        if (browserData && browserData[5].toString().toLowerCase() === "version") {
            if (browserData[3].toString().toLowerCase() === "presto") {
                browserData[5] = "Opera";
            } else {
                browserData[5] = "Safari";
            }
        } else if (!browserData) {
            browserData = REG_TEST_IE.exec(ua);
            browserData = [browserData[0], browserData[1], browserData[2], browserData[5], browserData[6], browserData[3], browserData[4]];
        }
        browserData.shift();
        var browserCaps = browserData.join(", ");
        var asString = userAgentSignatureString + " [" + browserCaps + "]";
        return {
            browserSignature: userAgentSignatureString,
            lookAs: browserData[0],
            lookAsVersion: browserData[1],
            engine: browserData[2],
            engineVersion: browserData[3],
            browser: browserData[4],
            browserVersion: browserData[5],
            toString: function() {
                return asString;
            }
        };
    }

    if (!m_singleUserAgent) {
        m_singleUserAgent = getUserAgent();
    }

    return {
        info : function() {
            return m_singleUserAgent;
        }
    };
})();