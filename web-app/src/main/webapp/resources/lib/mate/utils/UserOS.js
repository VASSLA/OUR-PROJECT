/**
 * Created with JetBrains PhpStorm.
 * User: Alexey
 * Date: 20.07.12
 * Time: 12:30
 */

var UserOS = (function(){
    var
        REG_TEST_OS = /(Windows NT|Linux|Intel Mac OS X|PPC Mac OS|Android|Blackberry|Rim Tablet OS|WinNT|GNU|Yandex|SymbianOS)[^;][\s]*([a-zA-Z,\s,0-9,\.,_,-,\(,\),/]*)/i;

    var m_singleUserOperationSystem;
    /**
     * Немного строк userAgent для тестирования регулярного выражения
     * @type {Array}
     */
    var agents = [
        "Mozilla/5.0 (Linux; U; Android 1.6; en-us; eeepc Build/Donut) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
        "Mozilla/5.0 (Linux; U; Android 2.1-update1; ru-ru; GT-I9000 Build/ECLAIR) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
        "Mozilla/5.0 (Linux; U; Android 2.2; ru-ru; GT-I9000 Build/FROYO) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
        "Mozilla/5.0 (Linux; U; Android 3.1; en-us; GT-P7510 Build/HMJ37) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13",
        "Mozilla/4.0 (compatible; MSIE 6.0; America Online Browser 1.1; rev1.5; Windows NT 5.1;)",
        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; Avant Browser [avantbrowser.com]; iOpus-I-M; QXW03416; .NET CLR 1.1.4322)",
        "BlackBerry9000/5.0.0.93 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/179",
        "Mozilla/5.0 (BlackBerry; U; BlackBerry 9900; en-US) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.0.0.261 Mobile Safari/534.11+",
        "Mozilla/5.0 (PlayBook; U; RIM Tablet OS 1.0.0; en-US) AppleWebKit/534.8+ (KHTML, like Gecko) Version/0.0.1 Safari/534.8+",
        "Mozilla/5.0 (Macintosh; U; PPC Max OS X Mach-O; en-US; rv:1.8.0.7) Gecko/200609211 Camino/1.0.3",
        "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.7) Gecko/20060928 (Debian|Debian-1.8.0.7-1) Epiphany/2.14",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.205 Safari/534.16",
        "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:16.0) Gecko/20120815 Firefox/16.0",
        "Mozilla/5.0 (Windows; I; Windows NT 5.1; ru; rv:1.9.2.13) Gecko/20100101 Firefox/4.0",
        "Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.8.131 Version/11.10",
        "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.7; U; ru) Presto/2.8.131 Version/11.10",
        "Mozilla/5.0 (Macintosh; I; Intel Mac OS X 10_6_7; ru-ru) AppleWebKit/534.31+ (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1",
        "Mozilla/5.0 (SymbianOS/9.1; U; en-us) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
        "Mozilla/5.0 (SymbianOS/9.2; U; Series60/3.1 NokiaN95_8GB/31.0.015; Profile/MIDP-2.0 Configuration/CLDC-1.1 ) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
        "Links (2.2; GNU/kFreeBSD 6.3-1-486 i686; 80x25)",
        "ELinks (0.4pre5; Linux 2.4.27 i686; 80x25)",
        "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.1.6) Gecko/20070817 IceWeasel/2.0.0.6-g2"
    ];

    function getUserOS() {

        for (var i = 0; i < agents.length; i++)
        {
            var o = REG_TEST_OS.exec(agents[i]);
            console.log(o);
            if (o !== null && o[1].toLowerCase() === "blackberry")
            {
                var t = /[^0,^/,^a-z,\,][0-9,.]+/.exec(o[2]);
                console.log(t);
            }
        }

        var platform = this.navigator.platform;
        var osData = REG_TEST_OS.exec(this.navigator.userAgent);

        var osSignature = "Unknown";
        var osName = "Unknown";
        var osVersion = "Unknown";

        if (osData !== null && osData.length > 0) {
            switch (osData[1].toString().toLowerCase())
            {
                case "android":
                    osSignature = osData[0].toString();
                    osName = osData[1].toString();
                    osVersion = osData[2].toString();
                    break;
                case "windows nt":
                    osSignature = osData[0].toString();
                    osName = osData[1].toString();
                    osVersion = osData[2].toString();
                    break;
                case "intel mac os x":
                    osSignature = osData[0].toString();
                    osName = osData[1].toString();
                    osVersion = osData[2].toString().replace("_", ".");
                    break;
                case "blackberry":
                    osSignature = osData[0].toString();
                    osName = osData[1].toString();
                    osVersion = /[^0,^/,^a-z,\,][0-9,.]+/.exec(osData[2].toString());
                    break;
                case "symbianos":
                    osSignature = osData[0].toString();
                    osName = osData[1].toString();
                    osVersion = osData[2].toString();
                    break;
                case "gnu":
                    osSignature = osData[0].toString();
                    osName = osData[1].toString();
                    osVersion = osData[2].toString();
                    break;
                case "linux":
                    osSignature = osData[0].toString();
                    osName = osData[1].toString();
                    osVersion = /[0-9,.,i]*/.exec(osData[2].toString());
                    break;
                case "rim tables os":
                    osSignature = osData[0].toString();
                    osName = osData[1].toString();
                    osVersion = osData[2].toString();
                    break;
                default:
                    break;
            }
        }

        return {
            platform : platform,
            osSignature: osSignature,
            osName: osName,
            osVersion: osVersion,
            toString : function() {
                return "OS Version: " + osName + " " + osVersion;
            }
        };
    }

    if (!m_singleUserOperationSystem) {
        m_singleUserOperationSystem = getUserOS();
    }
    return {
        info: function() {
            return m_singleUserOperationSystem;
        }
    }
})();