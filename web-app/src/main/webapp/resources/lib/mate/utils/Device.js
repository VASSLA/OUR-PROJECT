/**
 * Created with JetBrains PhpStorm.
 * User: Alexey
 * Date: 27.09.12
 * Time: 16:43
 */

var Device = (function() {
    var m_singleUserDevice;

    function getUserDeviceCaps() {
        return {
            screenWidth: 0,
            screenHeight: 0,
            clientWidth: 0,
            clientHeight: 0
        };
    }

    if (!m_singleUserDevice)
        m_singleUserDevice = getUserDeviceCaps();

    return {
        info: function() {
            return m_singleUserDevice;
        }
    };
})();
