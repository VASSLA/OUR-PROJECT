/* global Mate */
/**
 * Base class for all entity in the Mate Framework
 */
Mate.define('Mate.core.Class', {
    requires: [
        'Mate.mixin.IdOwner'
    ],
    ctor: function(config) {
        Mate.Logger.trace('"Mate.core.Class" constructor has been called for "' + this.classname + '"');
        Mate.apply(config, this, true);
    },
    methods: {
        getClassName : function() {
            return this.classname;
        },
        toString: function() {
            return '[' + typeof this + ': ' + this.classname + ']\n' + this.stringify(this, null, '    ');
        }
    },
    mixins: [
        'Mate.mixin.IdOwner',
        'Mate.mixin.JsonStringifier'
    ]
});

