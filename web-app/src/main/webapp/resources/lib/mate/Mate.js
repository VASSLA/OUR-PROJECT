/* Mate - RIA UI Component Library
 * Breakpoint Software (c) 2015
 * Project: mate
 * File Name: Mate.js
 * Description: Main library file to include
 * Date: Jan 9, 2016 3:29:49 PM
 * Author: Alex Blade <alex.blade.box@gmail.com> */

/**
 * Main namespace for library
 * @type Mate
 */
var Mate = Mate || {};

(function () {
    Mate = {
        version: '1.0',
        namespace: 'Mate',
        useJQuery: true,
        Selector: {}
    };

    Mate.emptyFn = function () {
        Mate.Logger.trace('"Mate.emptyFn" was called');
    };

    Mate.setConfig = function (config) {
        Mate.apply(config, this, true);
        if (!Mate.useJQuery) {
            Mate.require('Mate.dom.Selector', true);
            Mate.Selector = Mate.dom.Selector;
        } else {
            if (typeof $ === 'function' || typeof jQuery === 'function') {
                Mate.Selector = $;//jQuery.noConflict();
            } else {
                Mate.Logger.error('JQuery has not been loaded yet');
            }
        }
        return this;
    };

    Mate.isArray = function (thing) {
        return (typeof thing === 'object' && thing instanceof Array);
    };

    Mate.toArray = function (thing) {
        if (!Mate.isArray(thing))
            return [thing];
        else
            return thing;
    };

    Mate.sleep = function(milliseconds) {
        Mate.Logger.debug('Sleeping for ' + milliseconds + ' ms ...');
        milliseconds =+ new Date().getTime();
        while (milliseconds > new Date().getTime()) {}
    };

    Mate.isFunction = function (thing) {
        return (typeof thing === 'function');
    };

    Mate.require = function (require, sync, afterLoad) {
        var requiresList = null;

        if (typeof require === 'object' && require instanceof Array) {
            requiresList = require;
        } else if (typeof require === 'string') {
            requiresList = [require];
        } else {
            Mate.Logger.error('Unsupported type of "require", it must be an \
                    array, a string or a function');
            return Mate;
        }

        var resourcesToLoad = new Array();
        for (var i in requiresList) {
            if (typeof requiresList[i] !== 'string')
                continue;
            if (!Mate.ClassManager.registry[requiresList[i]]) {
                resourcesToLoad.push(Mate.Microloader.getClassPath(requiresList[i]));
            } else {
                Mate.Logger.trace('"' + requiresList[i] + '" was already loaded');
            }
        }

        if (resourcesToLoad.length > 0) {
            if (sync) {
                Mate.Microloader.loadSync(resourcesToLoad, afterLoad);
                return Mate;
            } else {
                Mate.Microloader.loadAsync(resourcesToLoad, afterLoad);
                return Mate;
            }
        }
    };

    /**
     * Mate.Logger - The class is a wrapper for using console.log
     * @returns {Mate.Logger}
     */
    Mate.Logger = new function () {
        this.LogLevel = {
            TRACE:      5,
            DEBUG:      4,
            INFO:       3,
            WARNING:    2,
            ERROR:      1
        };
        this.logLevel = this.LogLevel.WARNING;
        this.callback = undefined;
    };

    Mate.Logger.setConfig = function (config) {
        Mate.apply(config, this, true);
    };

    Mate.Logger.log = function (logLevel, logMessage) {
        if (logLevel <= Mate.Logger.logLevel) {
            switch (logLevel) {
                case Mate.Logger.LogLevel.TRACE:
                    console.info(logMessage);
                    break;
                case Mate.Logger.LogLevel.DEBUG:
                    console.info(logMessage);
                    break;
                case Mate.Logger.LogLevel.INFO:
                    console.info(logMessage);
                    break;
                case Mate.Logger.LogLevel.WARNING:
                    console.warn(logMessage);
                    break;
                case Mate.Logger.LogLevel.ERROR:
                    console.error(logMessage);
                    break;
                default:
                    console.info(logMessage);
                    break;
            }
        }
        ;
        if (Mate.Logger.callback) {
            Mate.Logger.callback(logLevel, logMessage);
        }
    };

    Mate.Logger.table = function (data, logLevel) {
        logLevel = logLevel || Mate.Logger.LogLevel.TRACE;
        if (logLevel <= Mate.Logger.logLevel) {
            console.table(data);
        }
    };

    Mate.Logger.trace = function (logMessage) {
        Mate.Logger.log(Mate.Logger.LogLevel.TRACE, logMessage);
    };

    Mate.Logger.debug = function (logMessage) {
        Mate.Logger.log(Mate.Logger.LogLevel.DEBUG, logMessage);
    };

    Mate.Logger.info = function (logMessage) {
        Mate.Logger.log(Mate.Logger.LogLevel.INFO, logMessage);
    };

    Mate.Logger.warning = function (logMessage) {
        Mate.Logger.log(Mate.Logger.LogLevel.WARNING, logMessage);
    };

    Mate.Logger.error = function (logMessage) {
        Mate.Logger.log(Mate.Logger.LogLevel.ERROR, logMessage);
    };

    /**
     * Mate.Microloader - The class that is responsible for loading-on-demand
     * @returns {Mate.Microloader}
     */
    Mate.Microloader = new function () {
        this.className = 'Mate.Microloader';
        this.libraryScriptName = 'Mate.js';
        this.scriptExtension = '.js';
        this.baseUrl = '';
        this.host = location.host;
        this.protocol = location.protocol;
        this.url = this.protocol + '//' + this.host;
        this.libraryScriptUrl = '';
        this.namespaceMapping = {};
        this.initialized = false;
    };

    Mate.Microloader.init = function() {
        var scriptsElements = document.getElementsByTagName('script');
        for (var i = 0; i < scriptsElements.length; i++) {
            if (~scriptsElements[i].src.indexOf(this.libraryScriptName)) {
                this.libraryScriptUrl = scriptsElements[i].src;
            }
        }
        this.baseUrl = this.libraryScriptUrl.substr(0,
                this.libraryScriptUrl.lastIndexOf(this.libraryScriptName));
    };

    Mate.Microloader.getClassPath = function (className) {
        if (!this.initialized)
            this.init();
        var originClassName = className;
        var premappedPart = '';
        var classPath = '';
        for (var mappedNamespace in this.namespaceMapping) {
            if (~className.indexOf(mappedNamespace)) {
                className = className.substr(mappedNamespace.length + 1);
                premappedPart = this.namespaceMapping[mappedNamespace];
                if (premappedPart.substr(premappedPart.length - 1) !== '/') {
                    premappedPart = premappedPart + '/';
                }
                Mate.Logger.trace('Found pre-mapped namespace "' + premappedPart + '"');
            }
        }

        var namespaceParts = className.split('.');

        if (namespaceParts.length > 0 && namespaceParts[0] === Mate.namespace) {
            namespaceParts.shift();
        }

        if (premappedPart.substr(0, 1) === '/') {
            classPath = this.url + premappedPart + namespaceParts.join('/') + this.scriptExtension;
        } else {
            classPath = this.baseUrl + premappedPart + namespaceParts.join('/') + this.scriptExtension;
        }
        Mate.Logger.trace('"' + originClassName + '" converted to "' + classPath + '"');
        return classPath;
    };

    Mate.Microloader.loadSync = function (resourceList, afterLoad) {
        resourceList = Mate.toArray(resourceList);
        Mate.Logger.warning('Synchronous loading detected. Application may work slowly');
        Mate.Logger.debug(resourceList);
        var head = document.getElementsByTagName('head')[0];
        for (var position in resourceList) {
            var resource = resourceList[position];
            var script = document.createElement('script');
            var xhr = new XMLHttpRequest();
            xhr.open('GET', resource, false);
            xhr.send();
            if (xhr.status !== 200) {
                Mate.Logger.error('Failed loading ' + resource);
            }
            script.text = xhr.responseText;
            head.appendChild(script);
            script.src = xhr.responseURL;
            xhr = null;
        }
        if (Mate.isFunction(afterLoad)) {
            afterLoad();
        }
    };

    Mate.Microloader.loadAsync = function (resourceList, afterLoad) {
        var insertResources = function() {
            var head = document.getElementsByTagName('head')[0];
            for (var position in responseList) {
                var script = document.createElement('script');
                script.text = responseList[position].content;
                head.appendChild(script);
                script.src = responseList[position].src;
            }
        };
        var loadAsyncComplete = function() {
            var request = this; //Current XMLHttpRequest object
            if (request.readyState === 4) {
                Mate.Logger.trace('Async is completed');
                if (request.status >= 200 && request.status < 300) {
                    Mate.Logger.trace('Asynchronous XMLHTTPRequest was completed (Status: ' + request.status + ') ' + request.responseURL + ' (' + request.position + ')');
                    responseList[request.position] = {
                        content: request.responseText,
                        src: request.responseURL
                    };
                } else {
                    Mate.Logger.error('Asynchronous XMLHTTPRequest was completed with error (Status: ' + request.status + ') ' + request.responseURL + ' (' + request.position + ')');
                    responseList[request.position] = '';
                }
                delete this;
                requestsCounter--;
                if (!requestsCounter) {
                    Mate.Logger.trace('All resources were loaded');
                    insertResources();
                    if (Mate.isFunction(afterLoad)) {
                        afterLoad();
                    }
                }
            }
        };
        resourceList = Mate.toArray(resourceList);
        var responseList = new Array(resourceList.length);
        var requestsCounter = resourceList.length;
        for (var position in resourceList) {
            var resource = resourceList[position];
            var request = new XMLHttpRequest();
            request.onreadystatechange = loadAsyncComplete;
            request.position = position;
            Mate.Logger.trace('Asynchronous XMLHTTPRequest is started ' + request.responseURL);
            request.open('GET', resource, true);
            request.send();
        }
    };

    Mate.Microloader.setConfig = function (config) {
        Mate.apply(config, this, true);
    };

    /**
     * Mate.ClassManager - The class stores all info about classes
     * @returns {undefined}
     */
    Mate.ClassManager = new function () {
        this.classname = 'Mate.ClassManager';
        this.registry = {};
        this.exclusions = ['className', 'classname', 'constructor', 'ctor',
            'methods', 'mixins', 'interfaces', 'extends', 'statics', 'superclass'];
    };

    Mate.ClassManager.renameConstructor = function (name, fn) {
        return new Function('fn',
                "return function " + name + "(){ return fn.apply(this,arguments)}"
                )(fn);
    };

    Mate.ClassManager.define = function (className, config) {
        Mate.Logger.trace('Defining "' + className + '"');

        var classname = className;

        // If defining class requires some resources - load them all
        if (config.requires) {
            Mate.require(config.requires, true);
            config.requires = undefined;
        }

        // If defined class extends other class then check the parent is loaded
        if (config.extends) {
            Mate.Logger.trace('"' + className + '" needs to load "' + config.extends + '"');
            Mate.require(config.extends, true);
        }

        // Getting construction function of the parent
        var superclass = config.extends && Mate.ClassManager.registry[config.extends] ?
                Mate.ClassManager.registry[config.extends] : Object;

        var defaultConstructor = function () {
            var c = config.ctor || function () {
                try {
                    this.superclass.apply(this, arguments);
                } catch (e) {
                    Mate.Logger.error(config);
                    Mate.Logger.error(e);
                }
            };
            c.apply(this, arguments);
        };

        var parts = classname.split('.');
        var renamedConstructor = Mate.ClassManager.renameConstructor(parts[parts.length - 1], defaultConstructor);
        Mate.ClassManager.lookupNamespace(classname, renamedConstructor);

        var methods = config.methods || {};
        var statics = config.statics || {};
        var mixins, interfaces;

        if (!config.mixins) {
            mixins = [];
        } else if (config.mixins instanceof Array) {
            mixins = config.mixins;
        } else {
            mixins = [config.mixins];
        }

        if (!config.interfaces) {
            interfaces = [];
        } else if (config.interfaces instanceof Array) {
            interfaces = config.interfaces;
        } else {
            interfaces = [config.interfaces];
        }

        Mate.require(mixins, true);
        Mate.require(interfaces, true);

        var proto = new superclass();
        for (var propertyName in proto) {
            if (proto.hasOwnProperty(propertyName)) {
                delete proto[propertyName];
            }
        }

        for (var i = 0; i < mixins.length; i++) {
            var mixin = mixins[i];
            if (typeof mixin === 'string')
                mixin = Mate.lookupNamespace(mixin);
            for (var propertyName in mixin.prototype) {
                if (typeof mixin.prototype[propertyName] !== 'function' ||
                        ~Mate.ClassManager.exclusions.indexOf(propertyName))
                    continue;
                proto[propertyName] = mixin.prototype[propertyName];
            }
        }

        for (var propertyName in methods) {
            proto[propertyName] = methods[propertyName];
        }

        for (var propertyName in config) {
            if (~Mate.ClassManager.exclusions.indexOf(propertyName))
                continue;
            proto[propertyName] = config[propertyName];
        }

        //proto.constructor = constructor;
        proto.constructor = Mate.ClassManager.lookupNamespace(classname);
        proto.superclass = superclass;

        if (classname) {
            proto.classname = classname;
        }

        for (var i = 0; i < interfaces; i++) {
            var iface = interfaces[i];
            for (var propertyName in iface.prototype) {
                if (typeof iface.prototype[propertyName] !== 'function')
                    continue;
                if (propertyName === 'constructor' || propertyName === 'superclass')
                    continue;
                if (propertyName in proto &&
                        typeof proto[propertyName] === 'function' &&
                        proto[propertyName].length === iface.prototype[propertyName].length)
                    continue;
                Mate.Logger.error('Class ' + classname + ' does not implement the \'' + propertyName + '\' method');
            }
        }

        //var constructor = Mate.ClassManager.lookupNamespace(classname);
        proto.constructor.prototype = proto;

        for (var propertyName in statics) {
            proto.constructor[propertyName] = config.statics[propertyName];
        }

        if (proto.classname) {
            Mate.ClassManager.registry[proto.classname] = proto.constructor;
        }

        return proto.constructor;
    };

    Mate.ClassManager.create = function (name, config) {
        if (!Mate.ClassManager.registry[name]) {
            var classPath = Mate.Microloader.getClassPath(name);
            Mate.Microloader.loadSync(classPath);
        }
        var ctor = Mate.ClassManager.registry[name];
        try {
            var o = new ctor(config);
        } catch (e) {
            Mate.Logger.debug(name);
            Mate.Logger.debug(config);
            Mate.Logger.debug(e);
        }
        return o;
    };

    /**
     * The method write all properties from one object to another
     * @param {object} from Object whose properties will be copied
     * @param {object} to Object in which properties will be copied
     * @param {object} override Override existing properties
     * @returns {object} Object after properies will be copied
     */
    Mate.ClassManager.apply = function (from, to, override) {
        if (override === undefined)
            override = false;
        if (!from || !to)
            return;
        for (var propertyName in from) {
            if (~Mate.ClassManager.exclusions.indexOf(propertyName))
                continue;
            if (override) { // TODO: Check what if override will be undefined
                to[propertyName] = from[propertyName];
            } else {
                if (to[propertyName] === undefined) {
                    to[propertyName] = from[propertyName];
                }
            }
        }
        return to;
    };

    Mate.ClassManager.applyIf = function (from, to) {
        return Mate.ClassManager.apply(from, to, false);
    };

    /**
     * The method lookups namespaces which is regestered in Mate class structure.
     * If pointed namespace was not found then it will be created. The method
     * returns namespace defined by namespaceString parameter.
     *
     * @param {string} namespaceString
     * @param {boolean} applyFunction
     * @returns {Mate|@exp;Mate|Mate.ClassManager.lookupNamespace.currentNamespace}
     */
    Mate.ClassManager.lookupNamespace = function (namespaceString, applyFunction) {
        var namespaceParts = namespaceString.split('.');
        namespaceParts.shift();
        var currentNamespace = Mate;
        for (var i = 0; i < namespaceParts.length - 1; i++) {
            if (currentNamespace[namespaceParts[i]] === undefined) {
                currentNamespace[namespaceParts[i]] = {};
            }
            currentNamespace = currentNamespace[namespaceParts[i]];
        }
        if (currentNamespace[namespaceParts[namespaceParts.length - 1]] === undefined &&
                applyFunction !== undefined) {
            currentNamespace[namespaceParts[namespaceParts.length - 1]] = applyFunction;
        }
        return currentNamespace[namespaceParts[namespaceParts.length - 1]];
    };

    Mate.define = Mate.ClassManager.define;
    Mate.create = Mate.ClassManager.create;
    Mate.apply = Mate.ClassManager.apply;
    Mate.applyIf = Mate.ClassManager.applyIf;
    Mate.lookupNamespace = Mate.ClassManager.lookupNamespace;
}());
