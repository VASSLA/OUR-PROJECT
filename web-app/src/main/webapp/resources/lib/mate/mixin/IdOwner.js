/* Umka - Self Learning Resource
 * Breakpoint Software (c) 2015
 * Project: application
 * File Name: IdOwner.js
 * Date: Apr 15, 2016 5:42:52 PM
 * Author: Alex */

/*global Mate*/
Mate.define('Mate.mixin.IdOwner', {
    statics: {
        getUniqueId: new function() {
            var sequencer = 1;
            return function() {
                return sequencer++;
            };
        }
    },
    methods: {
        getId: function() {
            if (!this.id) {
                this.id = Mate.mixin.IdOwner.getUniqueId();
            }
            return this.id;
        },
        setId: function(id) {
            this.id = id;
        }
    }
});