/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global Mate, Infinity */
Mate.define('Mate.mixin.JsonStringifier', {
    methods: {
        stringify: function (obj, replacer, spaces, cycleReplacer) {
            return JSON.stringify(obj, this.serializer(replacer, cycleReplacer), spaces);
        },

        serializer: function (replacer, cycleReplacer) {
            var stack = [], keys = [];

            if (cycleReplacer === null || cycleReplacer === undefined)
                cycleReplacer = function (key, value) {
                    if (stack[0] === value)
                        return "[Circular ~]";
                    return "[Circular ~." + keys.slice(0, stack.indexOf(value)).join(".") + "]";
                };

            return function (key, value) {
                if (stack.length > 0) {
                    var thisPos = stack.indexOf(this);
                    ~thisPos ? stack.splice(thisPos + 1) : stack.push(this);
                    ~thisPos ? keys.splice(thisPos, Infinity, key) : keys.push(key);
                    if (~stack.indexOf(value))
                        value = cycleReplacer.call(this, key, value);
                } else
                    stack.push(value);

                return replacer === null ? value : replacer.call(this, key, value);
            };
        }
    }
});
