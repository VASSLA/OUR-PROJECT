/* Umka - Self Learning Resource
 * Breakpoint Software (c) 2015
 * Project: application
 * File Name: ResourceLoader.js
 * Date: Apr 4, 2016 9:31:16 PM
 * Author: Alex */

/* global Mate */
Mate.define('Mate.mixin.ResourceLoader', {
    methods: {
        request: function (method, url, data, headers, async, callback) {
            if (headers && typeof headers === 'object' && !(headers instanceof Array)) {
                headers = [headers];
            }
            function getHeadersArrayFromString(headersString) {
                var headers = headersString.split('\r\n');
                return headers;
            }
            ;
            var request = new XMLHttpRequest();
            switch (async) {
                // =============================================================
                // Synchronous Request Preparation and Execution
                // =============================================================
                case false:
                    request.open(method, url, async);
                    if (headers && headers.length > 0) {
                        for (var i = 0; i < headers.length; i++) {
                            request.setRequestHeader(headers[i].name, headers[i].value);
                        }
                    }
                    try {
                        request.send(JSON.stringify(data) || null);
                    } catch (e) {
                        Mate.Logger.error(e);
                    }
                    if (request.status >= 200 && request.status < 300) {
                        return {
                            responseText: request.responseText,
                            status: request.status,
                            statusText: request.statusText,
                            headers: getHeadersArrayFromString(request.getAllResponseHeaders())
                        };
                    } else {
                        Mate.Logger.error('Synchronous XMLHTTPRequest was completed with error');
                    }
                    break;
                    // =============================================================
                    // Asynchronous Request Preparation and Execution
                    // =============================================================
                case true:
                    request.onreadystatechange = function () {
                        if (request.readyState === 4) {
                            if (request.status >= 200 && request.status < 300) {
                                if (callback) {
                                    var headersString = request.getAllResponseHeaders();
                                    var headers = getHeadersArrayFromString(headersString);
                                    var response = {
                                        requestURL: request.responseURL,
                                        responseText: request.responseText,
                                        status: request.status,
                                        statusText: request.statusText,
                                        headers: getHeadersArrayFromString(request.getAllResponseHeaders())
                                    };
                                    callback(response);
                                }
                            } else {
                                Mate.Logger.error('Asynchronous XMLHTTPRequest was completed with error ' + request.status);
                                return;
                            }
                        }
                    };
                    request.open(method, url, async);
                    if (headers && headers.length > 0) {
                        for (var i = 0; i < headers.length; i++) {
                            request.setRequestHeader(headers[i].name, headers[i].value);
                        }
                    }
                    request.send(JSON.stringify(data) || null);
                    break;
                default:
                    break;
            }
        }
    }
});
