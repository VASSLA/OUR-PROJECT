/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: web-app
 * File Name: WaitIndicator.js
 * Date: Dec 24, 2016 6:54:33 PM
 * Author: Alex */

/* global Mate */
Mate.define('Mate.ux.WaitIndicator', {
    extends: 'Mate.core.Class',
    mixins: [
        'Mate.mixin.IdOwner'
    ],
    template: '',
    callCounter: 0,
    callback: null,
    parent: null,
    _renderTo: null,
    _el: null,
    _elIdPrefix: 'wait-indicator-',
    _parentElement: null,
    ctor: function (config) {
        Mate.apply(config, this, true);
        this._init();
    },
    methods: {
        _init: function () {
            this._initTemplate();
            this._initEl();
        },
        _initTemplate: function() {
            this._parentElement = Mate.Selector('body');
            if (this.parent && Mate.Selector('body').find(this.parent).length > 0) {
                this._parentElement = Mate.Selector(this.parent);
            }
            var template = $(this.template)[0];
            template.id = this._elIdPrefix + this.getId();
            Mate.Selector(this._parentElement).append(template);
        },
        _initEl: function() {
            this._el = Mate.Selector('#' + this._elIdPrefix + this.getId());
            this._el.hide();
        },
        _checkVisibility: function() {
            return this.callCounter > 0;
        },
        _centerIndicator: function(el, parentEl) {
            Mate.ux.centerBlock(el, parentEl);
        },
        _resetRenderPosition: function() {
            if (this._renderTo) {
                this._centerIndicator(this._el, this._renderTo);
                this._renderTo = null;
            } else {
                this._centerIndicator(this._el, this._parentElement);
            }
        },
        renderTo: function(renderTo) {
            var renderParent = Mate.Selector(renderTo);
            this._renderTo = renderParent;
        },
        show: function(message) {
            this._resetRenderPosition();
            this.callCounter++;
            if (message) {
                this._el.children('div[name=waitIndicatorMessage]').html(message);
            }
            this._el.show();
        },
        hide: function() {
            this.callCounter--;
            if (this._checkVisibility())
                return;
            this._el.hide();
        }
    }
});
