/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.interfaces;

public interface IFileStorageService {
    boolean copy(String what, String to);
    boolean move(String what, String to);
}
