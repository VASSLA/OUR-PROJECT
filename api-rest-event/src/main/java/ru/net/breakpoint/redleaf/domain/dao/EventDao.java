package ru.net.breakpoint.redleaf.domain.dao;

import java.util.ArrayList;
import java.util.Date;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Event;

@Repository
public class EventDao  extends AbstractDao<Event> {
    
    public ArrayList<Event> getEventsForUser(Long userId) {
        return this.getEventsForUser(userId, null, null);
    }
    
    public ArrayList<Event> getEventsForUser(Long userId, Date dateStart, Date dateEnd) {
        Criteria criteria = getSession().createCriteria(Event.class);

        criteria.createAlias("participants", "participantsAlias", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.or(Restrictions.eq("participantsAlias.id", userId), Restrictions.eq("userId", userId)));
        
        if (dateStart != null) {
            criteria.add(Restrictions.ge("dateStart", dateStart));
        }
        if (dateEnd != null) {
            criteria.add(Restrictions.le("dateEnd", dateEnd));
        }
        
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        
        ArrayList<Event> result = new ArrayList<>();
        result.addAll(criteria.list());
        return result;
    }
}
