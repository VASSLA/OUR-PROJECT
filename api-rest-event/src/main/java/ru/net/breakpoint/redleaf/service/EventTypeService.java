package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.EventTypeDao;
import ru.net.breakpoint.redleaf.domain.entity.EventType;
import ru.net.breakpoint.redleaf.interfaces.IEventTypeService;

@Service
@Transactional
public class EventTypeService implements IEventTypeService {
    
    @Autowired
    private EventTypeDao eventTypeDao;

    @Override
    public EventType get(Long id) {
        return eventTypeDao.get(id);
    }

    @Override
    public void saveOrUpdate(EventType persistent) {
        eventTypeDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(EventType persistent) {
//        eventTypeDao.delete(persistent);
        eventTypeDao.removeEventType(persistent);
    }

    @Override
    public ArrayList<EventType> getList() {
        return eventTypeDao.getList();
    }

    @Override
    public ArrayList<EventType> getList(Map<String, String> filters) {
        return eventTypeDao.getList(filters);
    }
    
}
