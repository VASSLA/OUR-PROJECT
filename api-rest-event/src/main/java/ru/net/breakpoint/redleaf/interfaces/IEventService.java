package ru.net.breakpoint.redleaf.interfaces;

import java.util.ArrayList;
import java.util.Date;
import ru.net.breakpoint.redleaf.domain.entity.Event;

public interface IEventService extends IService<Event> {
    ArrayList<Event> getEventsForUser(Long userId);
    ArrayList<Event> getEventsForUser(Long userId, Date dateStart, Date dateEnd);
}
