/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-event
 * File Name: Event.java
 * Date: Nov 30, 2016 6:13:17 PM
 * Author: anikeale */
package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

/**
 *
 * @author anikeale
 */
@Entity
@Table(name = "Event")
@NamedQueries({
    @NamedQuery(name = "Event.findAll", query = "SELECT e FROM Event e")
    , @NamedQuery(name = "Event.findById", query = "SELECT e FROM Event e WHERE e.id = :id")
    , @NamedQuery(name = "Event.findByTypeId", query = "SELECT e FROM Event e WHERE e.typeId = :typeId")
    , @NamedQuery(name = "Event.findByDateStart", query = "SELECT e FROM Event e WHERE e.dateStart = :dateStart")
    , @NamedQuery(name = "Event.findByDateEnd", query = "SELECT e FROM Event e WHERE e.dateEnd = :dateEnd")
    , @NamedQuery(name = "Event.findByUserId", query = "SELECT e FROM Event e WHERE e.userId = :userId")})
public class Event implements Serializable, IPersistent {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "typeId")
    private Long typeId;
    @Lob
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @Column(name = "dateStart")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateStart;
    @Column(name = "dateEnd")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEnd;
    @Column(name="wholeDay")
    private Short wholeDay;
    @Column(name="color")
    private String color;
    @Basic(optional = false)
    @Column(name = "userId")
    private Long userId;
    
    @ManyToMany(targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "EventUser",
            joinColumns = {
                @JoinColumn(name = "eventId")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "userId")
            })
    private Set<User> participants;

    public Event() {
    }

    public Event(Long id) {
        this.id = id;
    }

    public Event(Long id, Long typeId, Date dateStart, Long userId) {
        this.id = id;
        this.typeId = typeId;
        this.dateStart = dateStart;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Event)) {
            return false;
        }
        Event other = (Event) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.Event[ id=" + id + " ]";
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the wholeDay
     */
    public Short getWholeDay() {
        return wholeDay;
    }

    /**
     * @param wholeDay the wholeDay to set
     */
    public void setWholeDay(Short wholeDay) {
        this.wholeDay = wholeDay;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the participants
     */
    public Set<User> getParticipants() {
        return participants;
    }

    /**
     * @param participants the persons to set
     */
    public void setParticipants(Set<User> participants) {
        this.participants = participants;
    }
    
}
