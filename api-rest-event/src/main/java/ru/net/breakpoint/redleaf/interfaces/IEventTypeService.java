package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.EventType;

public interface IEventTypeService extends IService<EventType> {
    
}
