package ru.net.breakpoint.redleaf.controller;

import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.EventType;
import ru.net.breakpoint.redleaf.interfaces.IEventTypeService;

@RestController
public class EventTypeController {
    
    private static final Logger logger = Logger.getLogger(EventTypeController.class);
    
    @Autowired
    private IEventTypeService eventTypeService;
    
    @RequestMapping(value = "/api/v{version}/eventType", method = RequestMethod.GET)
    public ResponseEntity<List<EventType>> getEventTypeList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/eventType");
        return new ResponseEntity<List<EventType>>(eventTypeService.getList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/eventTypeActive", method = RequestMethod.GET)
    public ResponseEntity<List<EventType>> getEventTypeListActive(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/eventTypeActive");
        
        HashMap<String, String> filters = new HashMap<>();
        filters.put("active", "1");
        
        return new ResponseEntity<List<EventType>>(eventTypeService.getList(filters), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/eventType/{id}", method = RequestMethod.GET)
    public ResponseEntity<EventType> getEventType(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/eventType/" + id.toString());
        return new ResponseEntity<>(eventTypeService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/eventType", method = RequestMethod.POST)
    public ResponseEntity<EventType> postEventType(@PathVariable Integer version, @RequestBody EventType eventType) {
        logger.trace("Requested /api/v" + version.toString() + "/eventType");
        eventTypeService.saveOrUpdate(eventType);
        return new ResponseEntity<>(eventType, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/eventType", method = RequestMethod.PUT)
    public ResponseEntity<EventType> putEventType(@PathVariable Integer version, @RequestBody EventType eventType) {
        logger.trace("Requested /api/v" + version.toString() + "/eventType");
        eventTypeService.saveOrUpdate(eventType);
        return new ResponseEntity<>(eventType, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/eventType/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<EventType> deleteEventType(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/eventType");
        EventType eventType = eventTypeService.get(id);
        if (eventType != null) {
            eventTypeService.delete(eventType);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
}
