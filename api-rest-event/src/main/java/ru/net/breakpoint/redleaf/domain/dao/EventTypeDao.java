package ru.net.breakpoint.redleaf.domain.dao;

import java.util.ArrayList;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Event;
import ru.net.breakpoint.redleaf.domain.entity.EventType;

@Repository
public class EventTypeDao extends AbstractDao<EventType> {
    
    public void removeEventType(EventType eventType) {
        Criteria criteria = getSession().createCriteria(Event.class)
                .add(Restrictions.eq("typeId", eventType.getId()))
                .setProjection(Projections.rowCount());
        Number eventCount = (Number)criteria.uniqueResult();
        
        if (eventCount.doubleValue() > 0.) {
            eventType.setActive((short)0);
            getSession().saveOrUpdate(eventType);
        } else {
            getSession().delete(eventType);
        }
    }
}
