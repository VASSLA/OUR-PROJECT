package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.EventDao;
import ru.net.breakpoint.redleaf.domain.entity.Event;
import ru.net.breakpoint.redleaf.interfaces.IEventService;

@Service
@Transactional
public class EventService implements IEventService {
    
    @Autowired
    private EventDao eventDao;

    @Override
    public Event get(Long id) {
        return eventDao.get(id);
    }

    @Override
    public void saveOrUpdate(Event persistent) {
        eventDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Event persistent) {
        eventDao.delete(persistent);
    }

    @Override
    public ArrayList<Event> getList() {
        return eventDao.getList();
    }

    @Override
    public ArrayList<Event> getList(Map<String, String> filters) {
        return eventDao.getList(filters);
    }

    @Override
    public ArrayList<Event> getEventsForUser(Long userId) {
        return eventDao.getEventsForUser(userId);
    }
    
    @Override
    public ArrayList<Event> getEventsForUser(Long userId, Date dateStart, Date dateEnd) {
        return eventDao.getEventsForUser(userId, dateStart, dateEnd);
    }
}
