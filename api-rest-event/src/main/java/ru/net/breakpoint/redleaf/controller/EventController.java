package ru.net.breakpoint.redleaf.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Event;
import ru.net.breakpoint.redleaf.domain.entity.User;
import ru.net.breakpoint.redleaf.interfaces.IEventService;
import ru.net.breakpoint.redleaf.interfaces.IUserService;

@RestController
public class EventController {
    
    private static final Logger logger = Logger.getLogger(EventController.class);
    
    @Autowired
    private IEventService eventService;
    
    @Autowired
    private IUserService userService;
    
    @RequestMapping(value = "/api/v{version}/event", method = RequestMethod.GET)
    public ResponseEntity<List<Event>> getEventList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/event");
        return new ResponseEntity<List<Event>>(eventService.getList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/event/{id}", method = RequestMethod.GET)
    public ResponseEntity<Event> getEvebt(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/event/" + id.toString());
        return new ResponseEntity<>(eventService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/event", method = RequestMethod.POST)
    public ResponseEntity<Event> postEvent(@PathVariable Integer version, @RequestBody Event event) {
        logger.trace("Requested /api/v" + version.toString() + "/event");
        eventService.saveOrUpdate(event);
        return new ResponseEntity<>(event, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/api/v{version}/event", method = RequestMethod.PUT)
    public ResponseEntity<Event> putEvent(@PathVariable Integer version, @RequestBody Event event) {
        logger.trace("Requested /api/v" + version.toString() + "/event");
        eventService.saveOrUpdate(event);
        return new ResponseEntity<>(event, HttpStatus.ACCEPTED);
    }
    
    @RequestMapping(value = "/api/v{version}/event/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Event> deleteEvent(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/event");
        Event event = eventService.get(id);
        if (event != null) {
            eventService.delete(event);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(value = "/api/v{version}/myevents", method = RequestMethod.GET)
    public ResponseEntity<List<Event>> getEventListForUser(@PathVariable Integer version, @RequestParam Map<String, String> filters) {
        logger.trace("Requested /api/v" + version.toString() + "/myevents");
        
        Date dateStart = null;
        Date dateEnd = null;
        if (filters.containsKey("dateStart")) {
            Long millis = Long.parseLong(filters.get("dateStart"));
            dateStart = new Date(millis);
//            dateStart = Date.parse(filters.get("dateStart"));
        }
        
        if (filters.containsKey("dateEnd")) {
            Long millis = Long.parseLong(filters.get("dateEnd"));
            dateEnd = new Date(millis);
        }
        
        User user = userService.getCurrentUser();
        
        return new ResponseEntity<List<Event>>(eventService.getEventsForUser(user.getId(), dateStart, dateEnd), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/myevents", method = RequestMethod.POST)
    public ResponseEntity<Event> postMyEvent(@PathVariable Integer version, @RequestBody Event event) {
        logger.trace("Requested /api/v" + version.toString() + "/event");
        eventService.saveOrUpdate(event);
        return new ResponseEntity<>(event, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/api/v{version}/myevents", method = RequestMethod.PUT)
    public ResponseEntity<Event> putMyEvent(@PathVariable Integer version, @RequestBody Event event) {
        logger.trace("Requested /api/v" + version.toString() + "/event");
        eventService.saveOrUpdate(event);
        return new ResponseEntity<>(event, HttpStatus.ACCEPTED);
    }
    
    @RequestMapping(value = "/api/v{version}/myevents/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Event> deleteMyEvent(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/event");
        Event event = eventService.get(id);
        if (event != null) {
            eventService.delete(event);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
}
