package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.MediaInfo;

public interface IMediaInfoService extends IService<MediaInfo> {
    
}
