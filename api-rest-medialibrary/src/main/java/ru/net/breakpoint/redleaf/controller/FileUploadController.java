package ru.net.breakpoint.redleaf.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.net.breakpoint.redleaf.domain.entity.MediaInfo;
import ru.net.breakpoint.redleaf.domain.entity.Setting;
import ru.net.breakpoint.redleaf.domain.entity.User;
import ru.net.breakpoint.redleaf.interfaces.IMediaInfoService;
import ru.net.breakpoint.redleaf.interfaces.ISettingService;
import ru.net.breakpoint.redleaf.interfaces.IUserService;

@RestController
public class FileUploadController {

    private static final Logger logger = Logger.getLogger(FileUploadController.class);

    @Autowired
    private IMediaInfoService mediainfoService;

    @Autowired
    private ISettingService settingService;

    @Autowired
    private IUserService userService;

    private String getUploadDirectoryForUser(User user) {
        HashMap<String, String> settingFilters = new HashMap<>();
        settingFilters.put("name", "Common.FileSystem.MediaLibraryTemporaryFolder");

        ArrayList<Setting> settings = settingService.getList(settingFilters);
        if (settings.size() > 0) {
            Setting setting = settings.get(0);
            File dir = new File(setting.getValue() + "/" + user.getLogin());
            dir.mkdirs();
            return dir.getAbsolutePath();
        }

        return null;
    }

    private String getThumbnailsDirectoryForUser(User user) {
        HashMap<String, String> settingFilters = new HashMap<>();
        settingFilters.put("name", "Common.FileSystem.MediaLibraryTemporaryFolder");

        ArrayList<Setting> settings = settingService.getList(settingFilters);
        if (settings.size() > 0) {
            Setting setting = settings.get(0);
            File dir = new File(setting.getValue() + "/" + user.getLogin() + "/" + "_thumbnails");
            dir.mkdirs();
            return dir.getAbsolutePath();
        }

        return null;
    }

    @RequestMapping(value = "/api/v{version}/storefile", method = RequestMethod.POST)
//    @PostMapping("/api/v{version}/storefile")
    public ResponseEntity<MediaInfo> storeFile(@RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
        User user = userService.getCurrentUser();

        MediaInfo media = new MediaInfo();
        mediainfoService.saveOrUpdate(media);
        String fileIdStr = Long.toString(media.getId(), 16);
        String fileName = "";
        for (int i = fileIdStr.length(); i < 11; i++) {
            fileName += "0";
        }
        fileName += (fileIdStr.toUpperCase());

        System.out.println(file.getContentType());

        try {
            String[] strings = file.getOriginalFilename().split("\\.");
            String fileExtension = strings[strings.length - 1];
            String filePath = getUploadDirectoryForUser(user) + "/" + fileName + "." + fileExtension;

            File resultFile = new File(filePath);
            resultFile.getParentFile().mkdirs();

            file.transferTo(resultFile);

            File thumbnailFile = new File(getThumbnailsDirectoryForUser(user) + "/" + fileName + ".jpg");

            Thumbnails.of(new File(filePath))
                    .size(320, 240)
                    .outputFormat("jpg")
                    .toFile(thumbnailFile);

            media.setInitialName(file.getOriginalFilename());
            media.setFileName(resultFile.getName());
            media.setUserId(user.getId());
            media.setType(file.getContentType());
            media.setThumbnailName(thumbnailFile.getName());

            mediainfoService.saveOrUpdate(media);
        } catch (Exception e) {
            mediainfoService.delete(media);
            logger.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.I_AM_A_TEAPOT);
        }
        return new ResponseEntity<>(media, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/getfile/{id}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long id, HttpServletRequest request, HttpServletResponse response) {
        MediaInfo media = mediainfoService.get(id);

        if (media == null) {
            // error
            return;
        }

        String fileName = media.getFileName();

        try {
            FileInputStream stream = new FileInputStream(getUploadDirectoryForUser(userService.get(media.getUserId())) + "/" + fileName);
            response.setHeader("Content-disposition", "attachment; filename="+ media.getInitialName());
            response.setContentType("application/octet-stream");
            IOUtils.copy(stream, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/api/v{version}/getthumbnail/{id}", method = RequestMethod.GET)
    public void getThumbnail(@PathVariable Long id, HttpServletRequest request, HttpServletResponse response) {
        MediaInfo media = mediainfoService.get(id);

        if (media == null) {
            // error
            return;
        }

        try {
            FileInputStream stream = new FileInputStream(getThumbnailsDirectoryForUser(userService.get(media.getUserId())) + "/" + media.getThumbnailName());
            response.setContentType(media.getType());
            IOUtils.copy(stream, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/api/v{version}/deletefile/{id}", method = RequestMethod.DELETE)
    public void deleteFile(@PathVariable Long id, HttpServletRequest request, HttpServletResponse response) {
        MediaInfo media = mediainfoService.get(id);

        File file = new File(getUploadDirectoryForUser(userService.get(media.getUserId())) + "/" + media.getFileName());
        if (file.delete()) {
            if (media.getThumbnailName() != null) {
                file = new File(getThumbnailsDirectoryForUser(userService.get(media.getUserId())) + "/" + media.getThumbnailName());
                file.delete();
            }
            mediainfoService.delete(media);
        }

    }

}
