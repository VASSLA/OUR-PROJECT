package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.MediaInfoDao;
import ru.net.breakpoint.redleaf.domain.entity.MediaInfo;
import ru.net.breakpoint.redleaf.interfaces.IMediaInfoService;

@Service
@Transactional
public class MediaInfoService implements IMediaInfoService {
    @Autowired
    private MediaInfoDao logDao;

    @Override
    public MediaInfo get(Long id) {
        return logDao.get(id);
    }

    @Override
    public void saveOrUpdate(MediaInfo persistent) {
        logDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(MediaInfo persistent) {
        logDao.delete(persistent);
    }

    @Override
    public ArrayList<MediaInfo> getList() {
        return logDao.getList();
    }

    @Override
    public ArrayList<MediaInfo> getList(Map<String, String> filters) {
        return logDao.getList(filters);
    }
}
