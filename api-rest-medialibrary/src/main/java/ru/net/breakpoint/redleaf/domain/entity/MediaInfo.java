package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "MediaInfo")
@NamedQueries({
    @NamedQuery(name = "MediaInfo.findAll", query = "SELECT m FROM MediaInfo m order by m.uploadDate desc"),
    @NamedQuery(name = "MediaInfo.findById", query = "SELECT m FROM MediaInfo m WHERE m.id = :id"),
    @NamedQuery(name = "MediaInfo.findByFileName", query = "SELECT m FROM MediaInfo m WHERE m.fileName = :fileName"),
    @NamedQuery(name = "MediaInfo.findByType", query = "SELECT m FROM MediaInfo m WHERE m.type = :type"),
    @NamedQuery(name = "MediaInfo.findByUserId", query = "SELECT m FROM MediaInfo m WHERE m.userId = :userId order by m.uploadDate desc"),
    @NamedQuery(name = "MediaInfo.findByUploadDate", query = "SELECT m FROM MediaInfo m WHERE m.uploadDate = :uploadDate")})
public class MediaInfo implements IPersistent, Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "fileName")
    private String fileName;
    @Column(name = "type")
    private String type;
    @Column(name = "userId")
    private Long userId;
    @Column(name = "uploadDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date uploadDate;
    @Column(name="initialName")
    private String initialName;
    @Column(name="thumbnailName")
    private String thumbnailName;

    public MediaInfo() {
    }

    public MediaInfo(Long id) {
        this.id = id;
    }

    public MediaInfo(Long id, Date uploadDate) {
        this.id = id;
        this.uploadDate = uploadDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String path) {
        this.fileName = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    public String getInitialName() {
        return initialName;
    }

    public void setInitialName(String initialName) {
        this.initialName = initialName;
    }
    
    public String getThumbnailName() {
        return thumbnailName;
    }

    public void setThumbnailName(String thumbnailName) {
        this.thumbnailName = thumbnailName;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MediaInfo)) {
            return false;
        }
        MediaInfo other = (MediaInfo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.MediaInfo[ id=" + id + " ]";
    }
    
}
