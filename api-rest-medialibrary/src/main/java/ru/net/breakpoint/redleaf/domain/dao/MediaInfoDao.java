package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.MediaInfo;

@Repository
public class MediaInfoDao extends AbstractDao<MediaInfo> {
    
}
