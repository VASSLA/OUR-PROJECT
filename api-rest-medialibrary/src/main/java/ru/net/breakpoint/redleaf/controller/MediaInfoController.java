package ru.net.breakpoint.redleaf.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.MediaInfo;
import ru.net.breakpoint.redleaf.domain.entity.User;
import ru.net.breakpoint.redleaf.interfaces.IMediaInfoService;
import ru.net.breakpoint.redleaf.interfaces.IUserService;

@RestController
public class MediaInfoController {

    private static final Logger logger = Logger.getLogger(MediaInfoController.class);

    @Autowired
    private IMediaInfoService mediainfoService;
    
    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/api/v{version}/mediainfo", method = RequestMethod.GET)
    public ResponseEntity<List<MediaInfo>> getMediainfoList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/mediainfo");
        return new ResponseEntity<List<MediaInfo>>(mediainfoService.getList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/mediainfoforme", method = RequestMethod.GET)
    public ResponseEntity<List<MediaInfo>> getMediainfoListCurrentUser(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/mediainfo");
        
        User user = userService.getCurrentUser();
        HashMap<String, String> filters = new HashMap<>();
        filters.put("userId", user.getId().toString());
        
        return new ResponseEntity<List<MediaInfo>>(mediainfoService.getList(filters), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/mediainfo/{id}", method = RequestMethod.GET)
    public ResponseEntity<MediaInfo> getMediainfo(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/mediainfo/" + id.toString());
        return new ResponseEntity<>(mediainfoService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/mediainfo", method = RequestMethod.POST)
    public ResponseEntity<MediaInfo> postMediainfo(@PathVariable Integer version, @RequestBody MediaInfo mediainfo) {
        logger.trace("Requested /api/v" + version.toString() + "/mediainfo");
        mediainfoService.saveOrUpdate(mediainfo);
        return new ResponseEntity<>(mediainfo, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/mediainfo", method = RequestMethod.PUT)
    public ResponseEntity<MediaInfo> putMediainfo(@PathVariable Integer version, @RequestBody MediaInfo mediainfo) {
        logger.trace("Requested /api/v" + version.toString() + "/mediainfo");
        mediainfoService.saveOrUpdate(mediainfo);
        return new ResponseEntity<>(mediainfo, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/mediainfo/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<MediaInfo> deleteMediainfo(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/mediainfo");
        MediaInfo mediainfo = mediainfoService.get(id);
        if (mediainfo != null) {
            mediainfoService.delete(mediainfo);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
