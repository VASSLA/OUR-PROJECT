/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-settings
 * File Name: ISettingService.java
 * Date: Nov 3, 2016 4:35:39 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.Setting;

public interface ISettingService extends IService<Setting>{
    Setting getByName(String name);
}
