/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-settings
 * File Name: SettingController.java
 * Date: Nov 3, 2016 4:46:30 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Setting;
import ru.net.breakpoint.redleaf.interfaces.ISettingService;

@RestController
public class SettingController {
    private static final Logger logger = Logger.getLogger(SettingController.class);

    @Autowired
    private ISettingService settingService;

    @RequestMapping(value = "/api/v{version}/setting", method = RequestMethod.GET)
    public ResponseEntity<List<Setting>> getSettingList(@PathVariable Integer version, @RequestParam Map<String, String> filters) {
        logger.trace("Requested /api/v" + version.toString() + "/setting");
        return new ResponseEntity<List<Setting>> (settingService.getList(filters), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/setting/{id}", method = RequestMethod.GET)
    public ResponseEntity<Setting> getSetting(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/setting/" + id.toString());
        return new ResponseEntity<> (settingService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/setting", method = RequestMethod.POST)
    public ResponseEntity<Setting> postSetting(@PathVariable Integer version, @RequestBody Setting setting) {
        logger.trace("Requested /api/v" + version.toString() + "/setting");
        settingService.saveOrUpdate(setting);
        return new ResponseEntity<> (setting, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/setting", method = RequestMethod.PUT)
    public ResponseEntity<Setting> putSetting(@PathVariable Integer version, @RequestBody Setting setting) {
        logger.trace("Requested /api/v" + version.toString() + "/setting");
        settingService.saveOrUpdate(setting);
        return new ResponseEntity<> (setting, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/setting/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Setting> deleteSetting(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/setting");
        Setting setting = settingService.get(id);
        if (setting != null) {
            settingService.delete(setting);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }
}
