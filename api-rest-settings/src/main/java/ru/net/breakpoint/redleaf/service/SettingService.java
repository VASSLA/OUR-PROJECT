/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-settings
 * File Name: SettingService.java
 * Date: Nov 3, 2016 4:39:44 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.SettingDao;
import ru.net.breakpoint.redleaf.domain.entity.Setting;
import ru.net.breakpoint.redleaf.interfaces.ISettingService;

@Service
@Transactional
public class SettingService implements ISettingService{
    @Autowired
    private SettingDao settingDao;

    @Override
    public Setting get(Long id) {
        return settingDao.get(id);
    }

    @Override
    public void saveOrUpdate(Setting persistent) {
        settingDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Setting persistent) {
        settingDao.delete(persistent);
    }

    @Override
    public ArrayList<Setting> getList() {
        return settingDao.getList();
    }

    @Override
    public ArrayList<Setting> getList(Map<String, String> filters) {
        return settingDao.getList(filters);
    }

    @Override
    public Setting getByName(String name) {
        Map<String, String> filters = new HashMap<>();
        filters.put("name", name);
        List<Setting> settings = settingDao.getList(filters);
        return settings.size() > 0 ? settings.get(0) : null;
    }
}
