/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-settings
 * File Name: SettingDao.java
 * Date: Nov 3, 2016 3:03:49 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Setting;

@Repository
public class SettingDao extends AbstractDao<Setting>{

}
