/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestService.java
 * Date: Oct 3, 2016 9:40:50 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.RequestDao;
import ru.net.breakpoint.redleaf.domain.entity.Request;
import ru.net.breakpoint.redleaf.interfaces.IRequestService;

@Service
@Transactional
public class RequestService implements IRequestService{
    @Autowired
    private RequestDao requestDao;

    @Override
    public Request get(Long id) {
        return requestDao.get(id);
    }

    @Override
    public void saveOrUpdate(Request persistent) {
        requestDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Request persistent) {
        requestDao.delete(persistent);
    }

    @Override
    public ArrayList<Request> getList() {
        return requestDao.getList();
    }

    @Override
    public ArrayList<Request> getList(Map<String, String> filters) {
        return requestDao.getList(filters);
    }

    @Override
    public ArrayList<Request> getListNonTrivial(Map<String, String> filters) {
        return requestDao.getListNonTrivial(filters);
    }
}
