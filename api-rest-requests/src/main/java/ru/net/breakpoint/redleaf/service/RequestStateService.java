/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestStateService.java
 * Date: Oct 1, 2016 6:05:19 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.RequestStateDao;
import ru.net.breakpoint.redleaf.domain.entity.RequestState;
import ru.net.breakpoint.redleaf.interfaces.IRequestStateService;

@Service
@Transactional
public class RequestStateService implements IRequestStateService {
    @Autowired
    private RequestStateDao requestStateDao;

    @Override
    public RequestState get(Long id) {
        return requestStateDao.get(id);
    }

    @Override
    public void saveOrUpdate(RequestState persistent) {
        requestStateDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(RequestState persistent) {
        requestStateDao.delete(persistent);
    }

    @Override
    public ArrayList<RequestState> getList() {
        return requestStateDao.getList();
    }

    @Override
    public ArrayList<RequestState> getList(Map<String, String> filters) {
        return requestStateDao.getList(filters);
    }
}
