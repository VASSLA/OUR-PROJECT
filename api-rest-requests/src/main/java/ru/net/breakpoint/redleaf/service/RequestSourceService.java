/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestSourceService.java
 * Date: Oct 1, 2016 6:01:50 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.RequestSourceDao;
import ru.net.breakpoint.redleaf.domain.entity.RequestSource;
import ru.net.breakpoint.redleaf.interfaces.IRequestSourceService;

@Service
@Transactional
public class RequestSourceService implements IRequestSourceService {
    @Autowired
    private RequestSourceDao requestSourceDao;

    @Override
    public RequestSource get(Long id) {
        return requestSourceDao.get(id);
    }

    @Override
    public void saveOrUpdate(RequestSource persistent) {
        requestSourceDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(RequestSource persistent) {
        requestSourceDao.delete(persistent);
    }

    @Override
    public ArrayList<RequestSource> getList() {
        return requestSourceDao.getList();
    }

    @Override
    public ArrayList<RequestSource> getList(Map<String, String> filters) {
        return requestSourceDao.getList(filters);
    }
}
