/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "RequestSource")
@NamedQueries({
    @NamedQuery(name = "RequestSource.findAll", query = "SELECT r FROM RequestSource r"),
    @NamedQuery(name = "RequestSource.findById", query = "SELECT r FROM RequestSource r WHERE r.id = :id"),
    @NamedQuery(name = "RequestSource.findByName", query = "SELECT r FROM RequestSource r WHERE r.name = :name"),
    @NamedQuery(name = "RequestSource.findByDescription", query = "SELECT r FROM RequestSource r WHERE r.description = :description")})
public class RequestSource implements Serializable, IPersistent {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;

    public RequestSource() {
    }

    public RequestSource(Long id) {
        this.id = id;
    }

    public RequestSource(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequestSource)) {
            return false;
        }
        RequestSource other = (RequestSource) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.RequestSource[ id=" + id + " ]";
    }

}
