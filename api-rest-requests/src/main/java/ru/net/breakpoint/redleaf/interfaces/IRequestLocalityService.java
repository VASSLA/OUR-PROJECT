/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: IRequestLocalityService.java
 * Date: Nov 17, 2016 7:02:07 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.RequestLocality;

/**
 *
 * @author Alex
 */
public interface IRequestLocalityService extends IService<RequestLocality>{

}
