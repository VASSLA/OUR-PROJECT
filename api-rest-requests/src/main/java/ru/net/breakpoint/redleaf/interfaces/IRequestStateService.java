/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestState.java
 * Date: Oct 1, 2016 5:55:28 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.RequestState;

/**
 *
 * @author Alex
 */
public interface IRequestStateService extends IService<RequestState>{

}
