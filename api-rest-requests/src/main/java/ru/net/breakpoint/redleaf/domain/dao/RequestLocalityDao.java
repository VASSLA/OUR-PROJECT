/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestLocalityDao.java
 * Date: Nov 17, 2016 7:01:27 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.RequestLocality;

@Repository
public class RequestLocalityDao extends AbstractDao<RequestLocality>{

}
