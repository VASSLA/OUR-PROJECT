/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestSourceDao.java
 * Date: Sep 28, 2016 8:58:51 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.RequestSource;

@Repository
public class RequestSourceDao extends AbstractDao<RequestSource>{

}
