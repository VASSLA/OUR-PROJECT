/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestLocality.java
 * Date: Nov 17, 2016 6:58:28 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "RequestLocality")
@NamedQueries({
    @NamedQuery(name = "RequestLocality.findAll", query = "SELECT r FROM RequestLocality r")
    , @NamedQuery(name = "RequestLocality.findById", query = "SELECT r FROM RequestLocality r WHERE r.id = :id")
    , @NamedQuery(name = "RequestLocality.findByName", query = "SELECT r FROM RequestLocality r WHERE r.name = :name")})
public class RequestLocality implements Serializable, IPersistent {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    public RequestLocality() {
    }

    public RequestLocality(Long id) {
        this.id = id;
    }

    public RequestLocality(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequestLocality)) {
            return false;
        }
        RequestLocality other = (RequestLocality) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.RequestLocality[ id=" + id + " ]";
    }

}
