/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestLocalityService.java
 * Date: Nov 17, 2016 7:03:24 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.RequestLocalityDao;
import ru.net.breakpoint.redleaf.domain.entity.RequestLocality;
import ru.net.breakpoint.redleaf.interfaces.IRequestLocalityService;

@Service
@Transactional
public class RequestLocalityService implements IRequestLocalityService{
    @Autowired
    private RequestLocalityDao requestLocalityDao;

    @Override
    public RequestLocality get(Long id) {
        return requestLocalityDao.get(id);
    }

    @Override
    public void saveOrUpdate(RequestLocality persistent) {
        requestLocalityDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(RequestLocality persistent) {
        requestLocalityDao.delete(persistent);
    }

    @Override
    public ArrayList<RequestLocality> getList() {
        return requestLocalityDao.getList();
    }

    @Override
    public ArrayList<RequestLocality> getList(Map<String, String> filters) {
        return requestLocalityDao.getList(filters);
    }
}
