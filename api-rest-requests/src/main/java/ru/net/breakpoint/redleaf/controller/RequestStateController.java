/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestStateController.java
 * Date: Oct 3, 2016 9:00:01 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.RequestState;
import ru.net.breakpoint.redleaf.interfaces.IRequestStateService;

@RestController
public class RequestStateController {
    private static final Logger logger = Logger.getLogger(RequestStateController.class);

    @Autowired
    private IRequestStateService requestStateService;

    @RequestMapping(value = "/api/v{version}/requeststate", method = RequestMethod.GET)
    public ResponseEntity<List<RequestState>> getRequestStateList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/requeststate");
        return new ResponseEntity<List<RequestState>> (requestStateService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/requeststate/{id}", method = RequestMethod.GET)
    public ResponseEntity<RequestState> getRequestState(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/requeststate/" + id.toString());
        return new ResponseEntity<> (requestStateService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/requeststate", method = RequestMethod.POST)
    public ResponseEntity<RequestState> postRequestState(@PathVariable Integer version, @RequestBody RequestState requestState) {
        logger.trace("Requested /api/v" + version.toString() + "/requeststate");
        requestStateService.saveOrUpdate(requestState);
        return new ResponseEntity<> (requestState, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/requeststate", method = RequestMethod.PUT)
    public ResponseEntity<RequestState> putRequestState(@PathVariable Integer version, @RequestBody RequestState requestState) {
        logger.trace("Requested /api/v" + version.toString() + "/requeststate");
        requestStateService.saveOrUpdate(requestState);
        return new ResponseEntity<> (requestState, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/requeststate/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<RequestState> deleteRequestState(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/requeststate");
        RequestState requestState = requestStateService.get(id);
        if (requestState != null) {
            requestStateService.delete(requestState);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }

}
