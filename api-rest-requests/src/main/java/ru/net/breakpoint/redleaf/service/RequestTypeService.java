/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestTypeService.java
 * Date: Oct 1, 2016 6:10:31 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.RequestTypeDao;
import ru.net.breakpoint.redleaf.domain.entity.RequestType;
import ru.net.breakpoint.redleaf.interfaces.IRequestTypeService;

@Service
@Transactional
public class RequestTypeService implements IRequestTypeService {
    @Autowired
    private RequestTypeDao requestTypeDao;

    @Override
    public RequestType get(Long id) {
        return requestTypeDao.get(id);
    }

    @Override
    public void saveOrUpdate(RequestType persistent) {
        requestTypeDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(RequestType persistent) {
        requestTypeDao.delete(persistent);
    }

    @Override
    public ArrayList<RequestType> getList() {
        return requestTypeDao.getList();
    }

    @Override
    public ArrayList<RequestType> getList(Map<String, String> filters) {
        return requestTypeDao.getList(filters);
    }
}
