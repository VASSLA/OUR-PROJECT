/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestTypeDao.java
 * Date: Sep 28, 2016 9:00:50 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.RequestType;

@Repository
public class RequestTypeDao extends AbstractDao<RequestType>{

}
