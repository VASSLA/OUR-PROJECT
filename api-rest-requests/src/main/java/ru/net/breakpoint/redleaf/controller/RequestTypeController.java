/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestTypeController.java
 * Date: Oct 3, 2016 9:06:08 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.RequestType;
import ru.net.breakpoint.redleaf.interfaces.IRequestTypeService;

@RestController
public class RequestTypeController {
    private static final Logger logger = Logger.getLogger(RequestTypeController.class);

    @Autowired
    private IRequestTypeService requestTypeService;

    @RequestMapping(value = "/api/v{version}/requesttype", method = RequestMethod.GET)
    public ResponseEntity<List<RequestType>> getRequestTypeList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/requesttype");
        return new ResponseEntity<List<RequestType>> (requestTypeService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/requesttype/{id}", method = RequestMethod.GET)
    public ResponseEntity<RequestType> getRequestType(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/requesttype/" + id.toString());
        return new ResponseEntity<> (requestTypeService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/requesttype", method = RequestMethod.POST)
    public ResponseEntity<RequestType> postRequestType(@PathVariable Integer version, @RequestBody RequestType requestType) {
        logger.trace("Requested /api/v" + version.toString() + "/requesttype");
        requestTypeService.saveOrUpdate(requestType);
        return new ResponseEntity<> (requestType, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/requesttype", method = RequestMethod.PUT)
    public ResponseEntity<RequestType> putRequestType(@PathVariable Integer version, @RequestBody RequestType requestType) {
        logger.trace("Requested /api/v" + version.toString() + "/requesttype");
        requestTypeService.saveOrUpdate(requestType);
        return new ResponseEntity<> (requestType, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/requesttype/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<RequestType> deleteRequestType(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/requesttype");
        RequestType requestType = requestTypeService.get(id);
        if (requestType != null) {
            requestTypeService.delete(requestType);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }

}
