/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestLocalityController.java
 * Date: Nov 17, 2016 7:06:20 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.RequestLocality;
import ru.net.breakpoint.redleaf.interfaces.IRequestLocalityService;

@RestController
public class RequestLocalityController {
    private static final Logger logger = Logger.getLogger(RequestLocalityController.class);

    @Autowired
    private IRequestLocalityService requestLocalityService;

    @RequestMapping(value = "/api/v{version}/requestlocality", method = RequestMethod.GET)
    public ResponseEntity<List<RequestLocality>> getRequestLocalityList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/requestlocality");
        return new ResponseEntity<List<RequestLocality>> (requestLocalityService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/requestlocality/{id}", method = RequestMethod.GET)
    public ResponseEntity<RequestLocality> getRequestLocality(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/requestlocality/" + id.toString());
        return new ResponseEntity<> (requestLocalityService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/requestlocality", method = RequestMethod.POST)
    public ResponseEntity<RequestLocality> postRequestLocality(@PathVariable Integer version, @RequestBody RequestLocality requestLocality) {
        logger.trace("Requested /api/v" + version.toString() + "/requestlocality");
        requestLocalityService.saveOrUpdate(requestLocality);
        return new ResponseEntity<> (requestLocality, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/requestlocality", method = RequestMethod.PUT)
    public ResponseEntity<RequestLocality> putRequestLocality(@PathVariable Integer version, @RequestBody RequestLocality requestLocality) {
        logger.trace("Requested /api/v" + version.toString() + "/requestlocality");
        requestLocalityService.saveOrUpdate(requestLocality);
        return new ResponseEntity<> (requestLocality, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/requestlocality/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<RequestLocality> deleteRequestLocality(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/requestlocality");
        RequestLocality requestLocality = requestLocalityService.get(id);
        if (requestLocality != null) {
            requestLocalityService.delete(requestLocality);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }

}
