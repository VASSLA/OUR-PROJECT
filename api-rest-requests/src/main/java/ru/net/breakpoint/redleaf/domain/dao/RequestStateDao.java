/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestStateDao.java
 * Date: Sep 28, 2016 9:00:13 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.RequestState;

@Repository
public class RequestStateDao extends AbstractDao<RequestState>{

}
