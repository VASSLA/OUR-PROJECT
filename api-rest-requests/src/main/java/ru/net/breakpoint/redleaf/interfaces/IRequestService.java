/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: IRequestService.java
 * Date: Oct 3, 2016 9:39:43 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import java.util.ArrayList;
import java.util.Map;
import ru.net.breakpoint.redleaf.domain.entity.Request;

public interface IRequestService extends IService<Request>{
    ArrayList<Request> getListNonTrivial(Map<String, String> filters);
}
