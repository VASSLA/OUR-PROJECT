/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: IRequestTypeService.java
 * Date: Oct 1, 2016 5:56:26 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.RequestType;

public interface IRequestTypeService extends IService<RequestType>{

}
