/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "RequestState")
@NamedQueries({
    @NamedQuery(name = "RequestState.findAll", query = "SELECT r FROM RequestState r"),
    @NamedQuery(name = "RequestState.findById", query = "SELECT r FROM RequestState r WHERE r.id = :id"),
    @NamedQuery(name = "RequestState.findByState", query = "SELECT r FROM RequestState r WHERE r.state = :state")})
public class RequestState implements Serializable, IPersistent {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "state")
    private String state;

    public RequestState() {
    }

    public RequestState(Long id) {
        this.id = id;
    }

    public RequestState(Long id, String state) {
        this.id = id;
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequestState)) {
            return false;
        }
        RequestState other = (RequestState) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.RequestState[ id=" + id + " ]";
    }

}
