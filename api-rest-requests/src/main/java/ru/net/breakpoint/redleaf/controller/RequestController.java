/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestController.java
 * Date: Oct 3, 2016 9:43:15 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Request;
import ru.net.breakpoint.redleaf.interfaces.IRequestService;

@RestController
public class RequestController {
    private static final Logger logger = Logger.getLogger(RequestController.class);

    @Autowired
    private IRequestService requestService;

    @RequestMapping(value = "/api/v{version}/request", method = RequestMethod.GET)
    public ResponseEntity<List<Request>> getRequestList(@PathVariable Integer version, @RequestParam Map<String, String> filters) {
        logger.trace("Requested /api/v" + version.toString() + "/request");
        if (filters.keySet().contains("nontrivial")) {
            return new ResponseEntity<List<Request>> (requestService.getListNonTrivial(filters), HttpStatus.OK);
        } else {
            return new ResponseEntity<List<Request>> (requestService.getList(filters), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/api/v{version}/request/{id}", method = RequestMethod.GET)
    public ResponseEntity<Request> getRequest(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/request/" + id.toString());
        return new ResponseEntity<> (requestService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/request", method = RequestMethod.POST)
    public ResponseEntity<Request> postRequest(@PathVariable Integer version, @RequestBody Request request) {
        logger.trace("Requested /api/v" + version.toString() + "/request");
        requestService.saveOrUpdate(request);
        return new ResponseEntity<> (request, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/request", method = RequestMethod.PUT)
    public ResponseEntity<Request> putRequest(@PathVariable Integer version, @RequestBody Request request) {
        logger.trace("Requested /api/v" + version.toString() + "/request");
        requestService.saveOrUpdate(request);
        return new ResponseEntity<> (request, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/request/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Request> deleteRequest(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/request");
        Request request = requestService.get(id);
        if (request != null) {
            requestService.delete(request);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }
}
