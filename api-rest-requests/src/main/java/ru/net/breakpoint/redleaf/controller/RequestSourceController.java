/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestSourceController.java
 * Date: Oct 3, 2016 6:13:56 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.RequestSource;
import ru.net.breakpoint.redleaf.interfaces.IRequestSourceService;

@RestController
public class RequestSourceController {
    private static final Logger logger = Logger.getLogger(RequestSourceController.class);

    @Autowired
    private IRequestSourceService requestSourceService;

    @RequestMapping(value = "/api/v{version}/requestsource", method = RequestMethod.GET)
    public ResponseEntity<List<RequestSource>> getRequestSourceList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/requestsource");
        return new ResponseEntity<List<RequestSource>> (requestSourceService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/requestsource/{id}", method = RequestMethod.GET)
    public ResponseEntity<RequestSource> getRequestSource(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/requestsource/" + id.toString());
        return new ResponseEntity<> (requestSourceService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/requestsource", method = RequestMethod.POST)
    public ResponseEntity<RequestSource> postRequestSource(@PathVariable Integer version, @RequestBody RequestSource requestSource) {
        logger.trace("Requested /api/v" + version.toString() + "/requestsource");
        requestSourceService.saveOrUpdate(requestSource);
        return new ResponseEntity<> (requestSource, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/requestsource", method = RequestMethod.PUT)
    public ResponseEntity<RequestSource> putRequestSource(@PathVariable Integer version, @RequestBody RequestSource requestSource) {
        logger.trace("Requested /api/v" + version.toString() + "/requestsource");
        requestSourceService.saveOrUpdate(requestSource);
        return new ResponseEntity<> (requestSource, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/requestsource/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<RequestSource> deleteRequestSource(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/requestsource");
        RequestSource requestSource = requestSourceService.get(id);
        if (requestSource != null) {
            requestSourceService.delete(requestSource);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }

}
