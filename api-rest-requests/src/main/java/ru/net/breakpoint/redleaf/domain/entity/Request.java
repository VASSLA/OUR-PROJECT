/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: Request.java
 * Date: Oct 3, 2016 9:34:36 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "Request")
@NamedQueries({
    @NamedQuery(name = "Request.findAll", query = "SELECT r FROM Request r"),
    @NamedQuery(name = "Request.findById", query = "SELECT r FROM Request r WHERE r.id = :id"),
    @NamedQuery(name = "Request.findByMissingFrom", query = "SELECT r FROM Request r WHERE r.missingFrom = :missingFrom"),
    @NamedQuery(name = "Request.findByLastSeenPlace", query = "SELECT r FROM Request r WHERE r.lastSeenPlace = :lastSeenPlace"),
    @NamedQuery(name = "Request.findByRequestType", query = "SELECT r FROM Request r WHERE r.requestType = :requestType"),
    @NamedQuery(name = "Request.findBySourceId", query = "SELECT r FROM Request r WHERE r.sourceId = :sourceId"),
    @NamedQuery(name = "Request.findByCurrentStateId", query = "SELECT r FROM Request r WHERE r.currentStateId = :currentStateId")})
public class Request implements IPersistent, Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "creationDate")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm", timezone = "GMT+3")
    private Date creationDate;

//    @Column(name = "missingPersonId")
//    private Long missingPersonId;

    @Column(name = "missingFrom")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy", timezone = "GMT+3")
    private Date missingFrom;

    @Column(name = "lastSeenPlace")
    private String lastSeenPlace;

    @Lob
    @Column(name = "extraInfo")
    private String extraInfo;

    @Lob
    @Column(name = "dressedIn")
    private String dressedIn;

    @Lob
    @Column(name = "circOfDisappear")
    private String circOfDisappear;

    @Lob
    @Column(name = "appearance")
    private String appearance;

    @Lob
    @Column(name = "health")
    private String health;

    @Lob
    @Column(name = "stuff")
    private String stuff;

//    @Column(name = "declarantPersonId")
//    private Long declarantPersonId;

    @Basic(optional = false)
    @Column(name = "requestTypeId")
    private short requestTypeId;

    @Basic(optional = false)
    @Column(name = "localityId")
    private short localityId;

    @Column(name = "forestOnline")
    private Boolean forestOnline;

    @Column(name = "sourceId")
    private Integer sourceId;

    @Basic(optional = false)
    @Column(name = "currentStateId")
    private Integer currentStateId;

    @Lob
    @Column(name = "report")
    private String report;

    @Column(name = "latitude")
    private Float latitude;

    @Column(name = "longitude")
    private Float longitude;

    // Mapped fields
    @OneToOne(targetEntity = RequestType.class, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "requestTypeId", insertable = false, updatable = false)
    private RequestType requestType;

    @OneToOne(targetEntity = RequestLocality.class, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "localityId", insertable = false, updatable = false)
    private RequestLocality locality;

    @OneToOne(targetEntity = RequestState.class, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "currentStateId", insertable = false, updatable = false)
    private RequestState currentState;

    @OneToOne(targetEntity = RequestSource.class, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "sourceId", insertable = false, updatable = false)
    private RequestSource source;

    @OneToOne(targetEntity = Person.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "missingPersonId", insertable = true, updatable = true)
    private Person missingPerson;

    @OneToOne(targetEntity = Person.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "declarantPersonId", insertable = true, updatable = true)
    private Person declarantPerson;

    @OneToOne(targetEntity = Person.class, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "inforgPersonId", insertable = false, updatable = false)
    private Person inforgPerson;

    @OneToOne(targetEntity = Person.class, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "coordPersonId", insertable = false, updatable = false)
    private Person coordPerson;

    public Request() {
    }

    public Request(Long id) {
        this.id = id;
    }

    public Request(Long id, short requestTypeId, Integer currentStateId) {
        this.id = id;
        this.requestTypeId = requestTypeId;
        this.currentStateId = currentStateId;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getMissingFrom() {
        return missingFrom;
    }

    public void setMissingFrom(Date missingFrom) {
        this.missingFrom = missingFrom;
    }

    public String getLastSeenPlace() {
        return lastSeenPlace;
    }

    public void setLastSeenPlace(String lastSeenPlace) {
        this.lastSeenPlace = lastSeenPlace;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public String getDressedIn() {
        return dressedIn;
    }

    public String getCircOfDisappear() {
        return circOfDisappear;
    }

    public void setCircOfDisappear(String circOfDisappear) {
        this.circOfDisappear = circOfDisappear;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getStuff() {
        return stuff;
    }

    public void setStuff(String stuff) {
        this.stuff = stuff;
    }

    public void setDressedIn(String dressedIn) {
        this.dressedIn = dressedIn;
    }

    public short getRequestTypeId() {
        return requestTypeId;
    }

    public void setRequestTypeId(short requestTypeId) {
        this.requestTypeId = requestTypeId;
    }

    public short getLocalityId() {
        return localityId;
    }

    public void setLocalityId(short localityId) {
        this.localityId = localityId;
    }

    public Boolean getForestOnline() {
        return forestOnline;
    }

    public void setForestOnline(Boolean forestOnline) {
        this.forestOnline = forestOnline;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getCurrentStateId() {
        return currentStateId;
    }

    public void setCurrentStateId(Integer currentStateId) {
        this.currentStateId = currentStateId;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public RequestLocality getLocality() {
        return locality;
    }

    public void setLocality(RequestLocality locality) {
        this.locality = locality;
    }

    public RequestState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(RequestState currentState) {
        this.currentState = currentState;
    }

    public RequestSource getSource() {
        return source;
    }

    public void setSource(RequestSource source) {
        this.source = source;
    }

    public Person getMissingPerson() {
        return missingPerson;
    }

    public void setMissingPerson(Person missingPerson) {
        this.missingPerson = missingPerson;
    }

    public Person getDeclarantPerson() {
        return declarantPerson;
    }

    public void setDeclarantPerson(Person declarantPerson) {
        this.declarantPerson = declarantPerson;
    }

    public Person getInforgPerson() {
        return inforgPerson;
    }

    public void setInforgPerson(Person inforgPerson) {
        this.inforgPerson = inforgPerson;
    }

    public Person getCoordPerson() {
        return coordPerson;
    }

    public void setCoordPerson(Person coordPerson) {
        this.coordPerson = coordPerson;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setAltitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Request)) {
            return false;
        }
        Request other = (Request) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.Request[ id=" + id + " ]";
    }

}
