/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: IRequestSourceService.java
 * Date: Oct 1, 2016 5:13:05 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.RequestSource;

public interface IRequestSourceService extends IService<RequestSource> {

}