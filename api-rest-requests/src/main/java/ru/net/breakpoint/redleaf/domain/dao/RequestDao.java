/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-requests
 * File Name: RequestDao.java
 * Date: Oct 3, 2016 9:38:00 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import java.util.ArrayList;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Request;
import ru.net.breakpoint.redleaf.utils.BirthDateCalculator;

@Repository
public class RequestDao extends AbstractDao<Request> {

    public ArrayList<Request> getListNonTrivial(Map<String, String> filters) {
        String query = filters.get("query");
        String latitude = filters.get("latitude");
        String longitude = filters.get("longitude");
        String maxDistance = filters.get("distance");
        String ageFrom = filters.get("ageFrom");
        String ageTo = filters.get("ageTo");
        Integer sex = Integer.parseInt(filters.get("sex"));
        String distanceQuery = "la_distance({alias}.latitude, {alias}.longitude, %s, %s) <= %s";
        Long urgency = Long.parseLong(filters.get("urgency"));
        Integer healthTroubles = Integer.parseInt(filters.get("healthTroubles"));
        distanceQuery = String.format(distanceQuery, latitude, longitude, maxDistance);
        Criteria requestCriteria = getSession().createCriteria(Request.class)
                .createAlias("missingPerson", "mp")
                .createAlias("requestType", "rt")
                .add(Restrictions.sqlRestriction(distanceQuery))
                .add(Restrictions.le("mp.birthDate", BirthDateCalculator.getBirthDateFromAge(Integer.parseInt(ageFrom))))
                .add(Restrictions.ge("mp.birthDate", BirthDateCalculator.getBirthDateFromAge(Integer.parseInt(ageTo))))
                .add(Restrictions.or(
                        Restrictions.like("lastSeenPlace", "%" + query + "%"),
                        Restrictions.like("extraInfo", "%" + query + "%"),
                        Restrictions.like("dressedIn", "%" + query + "%"),
                        Restrictions.like("circOfDisappear", "%" + query + "%"),
                        Restrictions.like("appearance", "%" + query + "%"),
                        Restrictions.like("health", "%" + query + "%"),
                        Restrictions.like("stuff", "%" + query + "%")    ,
                        Restrictions.like("mp.lastName", "%" + query + "%"),
                        Restrictions.like("mp.firstName", "%" + query + "%"),
                        Restrictions.like("mp.middleName", "%" + query + "%")
                ))
                ;
        if (!sex.equals(-1)) {
            requestCriteria.add(Restrictions.eq("mp.sex", sex));
        }
        if (urgency.equals(new Long(2))) {
            requestCriteria.add(Restrictions.eq("rt.id", urgency));
        }
        if (healthTroubles.equals(1)) {
            requestCriteria.add(Restrictions.and(
                    Restrictions.isNotNull("health"),
                    Restrictions.ne("health", "")
            ));
        }
        requestCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        ArrayList<Request> list = new ArrayList<>();
        list.addAll(requestCriteria.list());
        return list;
    }
}
