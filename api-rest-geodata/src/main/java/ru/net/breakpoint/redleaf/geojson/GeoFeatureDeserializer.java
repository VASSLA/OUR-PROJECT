/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-geodata
 * File Name: Deserealizer.java
 * Date: 17.01.2017 12:59:10
 * Author: Никита */
package ru.net.breakpoint.redleaf.geojson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import java.io.IOException;
import ru.net.breakpoint.redleaf.domain.entity.GeoFeature;
/**
 *
 * @author Никита
 */
public class GeoFeatureDeserializer extends JsonDeserializer {
    private final GeometryFactory jtsFactory;
    
    public GeoFeatureDeserializer() {
        this.jtsFactory = new GeometryFactory();
    }
    
    @Override
    public GeoFeature deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        
        Long featureId = null;
        if (node.has("id")) {
            if (node.get("id").textValue().length() > 0) {
                featureId = node.get("id").asLong();
            }
        }
        
        JsonNode properties = node.get("properties");
        String jsonProperties = properties.toString();
        
        JsonNode geometry = node.get("geometry");
        Geometry jtsGeometry = decodeGeometry(geometry);
        
        return new GeoFeature(featureId, jsonProperties, jtsGeometry);
    }
    
    private Geometry decodeGeometry(JsonNode geometry) {
        String type = geometry.get("type").textValue(); 
        final Geometry jtsGeometry;
        
        if (type.equals("GeometryCollection")) { 
            JsonNode geoCoords = geometry.get("geometries");
            Geometry[] geometries = new Geometry[geoCoords.size()];
            Integer i = 0;
            for (final JsonNode coordNode : geoCoords) {
                geometries[i] = decodeGeometry(coordNode);
                i++;
            } 
            jtsGeometry = jtsFactory.createGeometryCollection(geometries); 
            
        } else {
            
            JsonNode coordinates = geometry.get("coordinates"); 
            switch (type) {
                case "Point":
                    jtsGeometry = jtsFactory.createPoint(decodeCoordinate(coordinates));
                    break;
                case "LineString":
                    jtsGeometry = jtsFactory.createLineString(decodeCoordinates(coordinates));
                    break;
                case "Polygon":
                    jtsGeometry = decodePolygon(coordinates);
                    break;
                case "MultiPoint":
                    Point[] points = new Point[coordinates.size()];
                    for (int i = 0; i < points.length; ++i) {
                        points[i] = jtsFactory.createPoint(decodeCoordinate(coordinates.get(i)));
                    }   
                    jtsGeometry = jtsFactory.createMultiPoint(points);
                    break;
                case "MultiLineString":
                    LineString[] lineStrings = new LineString[coordinates.size()];
                    for (int i = 0; i < lineStrings.length; ++i) {
                        lineStrings[i] = jtsFactory.createLineString(decodeCoordinates(coordinates.get(i)));
                    }   
                    jtsGeometry = jtsFactory.createMultiLineString(lineStrings);
                    break;
                case "MultiPolygon":
                    Polygon[] polygons = new Polygon[coordinates.size()];
                    for (int i = 0; i < polygons.length; ++i) {
                        polygons[i] = decodePolygon(coordinates.get(i));
                    }   
                    jtsGeometry = jtsFactory.createMultiPolygon(polygons);
                    break;
                default:
                    return null;
            } 
        } 
 
        return jtsGeometry; 
    }
    
    private Polygon decodePolygon(JsonNode coordinates) { 
        LinearRing outer = jtsFactory.createLinearRing(decodeCoordinates(coordinates.get(0))); 
        LinearRing[] holes = new LinearRing[coordinates.size() - 1]; 
        for (int i = 1; i < coordinates.size(); ++i) { 
            holes[i - 1] = jtsFactory.createLinearRing(decodeCoordinates(coordinates.get(i))); 
        } 
        return jtsFactory.createPolygon(outer, holes); 
    } 
 
    private Coordinate[] decodeCoordinates(JsonNode coordinates) { 
        Coordinate[] result = new Coordinate[coordinates.size()]; 
        for (int i = 0; i < result.length; ++i) { 
            result[i] = decodeCoordinate(coordinates.get(i)); 
        } 
        return result; 
    } 
 
    private Coordinate decodeCoordinate(JsonNode coord) { 
        if (coord.size() > 2) { 
            return new Coordinate(coord.get(0).asDouble(), coord.get(1).asDouble(), coord.get(2).asDouble()); 
        } else { 
            return new Coordinate(coord.get(0).asDouble(), coord.get(1).asDouble()); 
        } 
    } 
}
