/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-geodata
 * File Name: FeatureCollection.java
 * Date: 17.01.2017 12:40:51
 * Author: Никита */
package ru.net.breakpoint.redleaf.domain.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import com.vividsolutions.jts.geom.Geometry;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import ru.net.breakpoint.redleaf.geojson.GeoFeatureDeserializer;
import ru.net.breakpoint.redleaf.geojson.GeoFeatureSerializer;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

/**
 *
 * @author Nikita
 */
@Entity
@Table(name = "GeoFeature")
@JsonDeserialize(using = GeoFeatureDeserializer.class)
@JsonSerialize(using = GeoFeatureSerializer.class)
public class GeoFeature implements Serializable, IPersistent  {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "geometry",columnDefinition="Geometry")
    @Type(type="org.hibernate.spatial.GeometryType")
    private Geometry geometry;
    
    @Basic(optional = true)
    private String properties;

    @Basic(optional = true)
    private Integer requestId;
    
    public GeoFeature() {
    }
    
    public GeoFeature(Long id, String properties, Geometry geometry) {
        this.id = id;
        this.properties = properties;
        this.geometry = geometry;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
    
    public String getProperties() {
        return properties;
    }
    
    public void setProperties(String properties) {
        this.properties = properties;
    }
    
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }
}
