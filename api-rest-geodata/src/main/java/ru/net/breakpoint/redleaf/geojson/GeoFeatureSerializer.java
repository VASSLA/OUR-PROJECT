/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-geodata
 * File Name: Serializer.java
 * Date: 17.01.2017 13:03:55
 * Author: Никита */
package ru.net.breakpoint.redleaf.geojson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import java.io.IOException;
import ru.net.breakpoint.redleaf.domain.entity.GeoFeature;

/**
 *
 * @author Никита
 */
public class GeoFeatureSerializer extends JsonSerializer {
    private JsonGenerator jsonGenerator;
    
    @Override
    public void serialize(Object geoFeature, JsonGenerator jsonGenerator,SerializerProvider serializerProvider) 
            throws IOException, JsonProcessingException {
        this.jsonGenerator = jsonGenerator;
        GeoFeature feature = (GeoFeature)geoFeature;
        encodeFeature(feature);
    }
 
    private void encodeFeature(GeoFeature feature) throws IOException { 
        jsonGenerator.writeStartObject();
        
        jsonGenerator.writeStringField("type", "Feature");

        if (feature.getId() != null) { 
            jsonGenerator.writeStringField("id", feature.getId().toString()); 
        }
        
        jsonGenerator.writeFieldName("geometry");
        Geometry geometry;
        if ((geometry = feature.getGeometry()) != null) { 
            encodeGeometry(geometry); 
        } else { 
            jsonGenerator.writeStartObject();
            jsonGenerator.writeEndObject();
        }
 
        jsonGenerator.writeFieldName("properties");
        if (feature.getProperties() != null) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode properties = mapper.readTree(feature.getProperties());
            jsonGenerator.writeObject(properties);
        } else {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeEndObject();
        }
        
        jsonGenerator.writeEndObject(); 
    }
     
    private void encodeGeometry(Geometry geometry) throws IOException { 
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("type", getGeometryName(geometry));
 
        GeometryType geometryType = getGeometryType(geometry);
        
        if (geometryType != GeometryType.MULTIGEOMETRY) { 
            encodeGeometryCoordinates(geometry); 
        } else { 
            encodeGeomCollection((GeometryCollection)geometry); 
        } 
        jsonGenerator.writeEndObject();
    }
 
    private void encodeGeometryCoordinates(Geometry geometry) throws IOException { 
        GeometryType geometryType = getGeometryType(geometry); 
        
        if (geometryType != GeometryType.MULTIGEOMETRY) { 
            jsonGenerator.writeFieldName("coordinates"); 
 
            switch (geometryType) { 
            case POINT: 
                encodeCoordinate(geometry.getCoordinate()); 
                break; 
 
            case LINESTRING: 
            case MULTIPOINT: 
                encodeCoordinates(geometry.getCoordinates()); 
                break; 
 
            case POLYGON: 
                encodePolygon((Polygon) geometry); 
                break; 
 
            case MULTILINESTRING: 
                jsonGenerator.writeStartArray(); 
                for (int i = 0, n = geometry.getNumGeometries(); i < n; i++) { 
                    encodeCoordinates(geometry.getGeometryN(i).getCoordinates()); 
                } 
                jsonGenerator.writeEndArray(); 
                break; 
 
            case MULTIPOLYGON: 
                jsonGenerator.writeStartArray(); 
                for (int i = 0, n = geometry.getNumGeometries(); i < n; i++) { 
                    encodePolygon((Polygon) geometry.getGeometryN(i)); 
                } 
                jsonGenerator.writeEndArray();
                break; 
 
            default: 
                //should never happen. 
                throw new RuntimeException("No implementation for "+geometryType); 
            } 
        }  
    } 
    
    private void encodeGeomCollection(GeometryCollection collection) throws IOException { 
        jsonGenerator.writeFieldName("coordinates");         
        jsonGenerator.writeStartArray(); 
 
        for (int i = 0, n = collection.getNumGeometries(); i < n; i++) { 
            encodeGeometry(collection.getGeometryN(i)); 
        } 
 
        jsonGenerator.writeEndArray(); 
    } 
    
     /**
     * Write the coordinates of a geometry 
     * @param coords The coordinates to encode 
     * @throws JSONException 
     */ 
    private void encodeCoordinates(Coordinate[] coords) throws IOException { 
        jsonGenerator.writeStartArray(); 
 
        for (Coordinate coord : coords) {
            encodeCoordinate(coord);
        } 
 
        jsonGenerator.writeEndArray(); 
    } 
 
    private void encodeCoordinate(Coordinate coord) throws IOException { 
        jsonGenerator.writeStartArray();
        jsonGenerator.writeNumber(coord.x);
        jsonGenerator.writeNumber(coord.y);
        jsonGenerator.writeEndArray(); 
    } 
    
    /**
     * Writes a polygon 
     * @param geometry The polygon to encode 
     * @throws JSONException 
     */ 
    private void encodePolygon(Polygon geometry) throws IOException { 
        jsonGenerator.writeStartArray();
        encodeCoordinates(geometry.getExteriorRing().getCoordinates()); 
 
        for (int i = 0, ii = geometry.getNumInteriorRing(); i < ii; i++) { 
            encodeCoordinates(geometry.getInteriorRingN(i).getCoordinates()); 
        } 
 
        jsonGenerator.writeEndArray(); 
    } 
    
    private static enum GeometryType { 
        POINT("Point"), 
        LINESTRING("LineString"), 
        POLYGON("Polygon"), 
        MULTIPOINT("MultiPoint"), 
        MULTILINESTRING("MultiLineString"), 
        MULTIPOLYGON("MultiPolygon"), 
        MULTIGEOMETRY("GeometryCollection"); 
 
        private final String name; 
 
        private GeometryType(String name) { 
            this.name = name; 
        } 
 
        public String getName() { 
            return name; 
        } 
    } 
    
    private static String getGeometryName(Geometry geometry) { 
        final GeometryType type = getGeometryType(geometry); 
        return type != null ? type.getName() : null; 
    } 
    
    /**
     * Gets the internal representation for the given Geometry 
     * @param geometry a Geometry 
     * @return int representation of Geometry 
     */ 
    private static GeometryType getGeometryType(Geometry geometry) { 
        final Class<?> geomClass = geometry.getClass(); 
        final GeometryType returnValue; 
 
        if (geomClass.equals(Point.class)) { 
            returnValue = GeometryType.POINT; 
        } else if (geomClass.equals(LineString.class)) { 
            returnValue = GeometryType.LINESTRING; 
        } else if (geomClass.equals(Polygon.class)) { 
            returnValue = GeometryType.POLYGON; 
        } else if (geomClass.equals(MultiPoint.class)) { 
            returnValue = GeometryType.MULTIPOINT; 
        } else if (geomClass.equals(MultiLineString.class)) { 
            returnValue = GeometryType.MULTILINESTRING; 
        } else if (geomClass.equals(MultiPolygon.class)) { 
            returnValue = GeometryType.MULTIPOLYGON; 
        } else if (geomClass.equals(GeometryCollection.class)) { 
            returnValue = GeometryType.MULTIGEOMETRY; 
        } else { 
            returnValue = null; 
            //HACK ATTEMPT!!! throw exception??. 
        } 
 
        return returnValue; 
    } 
}
