/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-geodata
 * File Name: FeatureCollection.java
 * Date: 17.01.2017 12:40:51
 * Author: Никита */
package ru.net.breakpoint.redleaf.dto;

import java.util.List;
import ru.net.breakpoint.redleaf.domain.entity.GeoFeature;

/**
 *
 * @author Никита
 */
public class FeatureCollection {
    private String type;
    private List<GeoFeature> features;
    private Integer requestId;
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<GeoFeature> getFeatures() {
        return features;
    }

    public void setFeatures(List<GeoFeature> features) {
        this.features = features;
    }

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }
}
