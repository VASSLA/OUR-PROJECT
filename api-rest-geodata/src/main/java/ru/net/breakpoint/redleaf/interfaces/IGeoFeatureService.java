/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-geodata
 * File Name: FeatureCollection.java
 * Date: 17.01.2017 12:40:51
 * Author: Никита */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.GeoFeature;
import ru.net.breakpoint.redleaf.dto.FeatureCollection;

public interface IGeoFeatureService extends IService<GeoFeature> {
    void saveOrUpdate(FeatureCollection featureCollection);
}
