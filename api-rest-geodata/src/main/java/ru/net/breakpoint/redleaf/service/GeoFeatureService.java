/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.GeoDataDao;
import ru.net.breakpoint.redleaf.domain.entity.GeoFeature;
import ru.net.breakpoint.redleaf.dto.FeatureCollection;
import ru.net.breakpoint.redleaf.interfaces.IGeoFeatureService;

/**
 *
 * @author Никита
 */
@Service
@Transactional
public class GeoFeatureService implements IGeoFeatureService {
    @Autowired
    private GeoDataDao geoDataDao;
    
    @Override
    public GeoFeature get(Long id) {
        return geoDataDao.get(id);
    }

    @Override
    public void saveOrUpdate(GeoFeature persistent) {
        geoDataDao.saveOrUpdate(persistent);
    }
    
    @Override
    public void saveOrUpdate(FeatureCollection featureCollection) {
        for(GeoFeature geoFeature : featureCollection.getFeatures()) {
            if (featureCollection.getRequestId() != null && featureCollection.getRequestId() > 0) {
                geoFeature.setRequestId(featureCollection.getRequestId());
            }
            geoDataDao.saveOrUpdate(geoFeature);
        }
    }

    @Override
    public void delete(GeoFeature persistent) {
        geoDataDao.delete(persistent);
    }

    @Override
    public ArrayList<GeoFeature> getList() {
        return geoDataDao.getList();
    }
     
    @Override
    public ArrayList<GeoFeature> getList(Map<String, String> filters) {
        return geoDataDao.getList(filters);
    }
}
