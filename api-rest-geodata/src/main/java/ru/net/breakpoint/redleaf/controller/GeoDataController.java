/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.geotools.feature.FeatureCollections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.GeoFeature;
import ru.net.breakpoint.redleaf.dto.FeatureCollection;
import ru.net.breakpoint.redleaf.interfaces.IGeoFeatureService;

@RestController
public class GeoDataController {
    private static final Logger logger = Logger.getLogger(GeoDataController.class);

    @Autowired
    private IGeoFeatureService geoFeatureService;

    @RequestMapping(value = "/api/v{version}/geodata", method = RequestMethod.GET)
    public ResponseEntity<List<GeoFeature>> getGeoFeatureList(@PathVariable Integer version, @RequestParam Map<String, String> filters) {
        logger.trace("Get GeoData /api/v" + version.toString() + "/geodata");
        return new ResponseEntity<List<GeoFeature>> (geoFeatureService.getList(filters), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/geodata", method = RequestMethod.POST)
    public ResponseEntity<List<GeoFeature>> postGeoData(@PathVariable Integer version, @RequestBody FeatureCollection featureCollection) {
        logger.trace("Post GeoData /api/v" + version.toString() + "/geodata");
        geoFeatureService.saveOrUpdate(featureCollection);
        return new ResponseEntity(HttpStatus.OK);
    }
}
