/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Message;
import ru.net.breakpoint.redleaf.interfaces.IMessageService;

@RestController
public class MessageController {
    private static final Logger logger = Logger.getLogger(MessageController.class);

    @Autowired
    private IMessageService messageService;

    @RequestMapping(value = "/api/v{version}/message", method = RequestMethod.GET)
    public ResponseEntity<List<Message>> getMessageList(@PathVariable Integer version, @RequestParam Map<String, String> filters) {
        logger.trace("Messageed /api/v" + version.toString() + "/message");
        ArrayList<Message> messages = messageService.getList(filters);
        if (!filters.containsKey("notMarkAsRead")) {
            for (Message message : messages) {
                if (message.getRead() != 1) {
                    message.setRead((short)1);
                    messageService.saveOrUpdate(message);
                }
            }
        }
        return new ResponseEntity<List<Message>> (messages, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/message/{id}", method = RequestMethod.GET)
    public ResponseEntity<Message> getMessage(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Messageed /api/v" + version.toString() + "/message/" + id.toString());
        return new ResponseEntity<> (messageService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/message", method = RequestMethod.POST)
    public ResponseEntity<Message> postMessage(@PathVariable Integer version, @RequestBody Message message) {
        logger.trace("Messageed /api/v" + version.toString() + "/message");
        messageService.saveOrUpdate(message);
        message = messageService.get(message.getId());
        return new ResponseEntity<> (message, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/message", method = RequestMethod.PUT)
    public ResponseEntity<Message> putMessage(@PathVariable Integer version, @RequestBody Message message) {
        logger.trace("Messageed /api/v" + version.toString() + "/message");
        messageService.saveOrUpdate(message);
        return new ResponseEntity<> (message, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/message/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Message> deleteMessage(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Messageed /api/v" + version.toString() + "/message");
        Message message = messageService.get(id);
        if (message != null) {
            messageService.delete(message);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/api/v{version}/conversation/{id1}/{id2}", method = RequestMethod.GET)
    public ResponseEntity<List<Message>> getConversation(@PathVariable Integer version, @PathVariable Long id1, @PathVariable Long id2, @RequestParam Map<String, String> filters) {
        logger.trace("Messageed /api/v" + version.toString() + "/conversation/" + id1.toString() + "/" + id2.toString());
        ArrayList<Message> conversation = messageService.getConversation(id1, id2, filters);
        for (Message message : conversation) {
            message.setRead((short)1);
            messageService.saveOrUpdate(message);
        }
        return new ResponseEntity<List<Message>> (conversation, HttpStatus.OK);
    }
}
