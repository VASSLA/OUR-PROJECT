/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

/**
 *
 * @author anikeale
 */
@Entity
@Table(name = "Message")
@NamedQueries({
    @NamedQuery(name = "Message.findAll", query = "SELECT m FROM Message m")
    , @NamedQuery(name = "Message.findById", query = "SELECT m FROM Message m WHERE m.id = :id")
    , @NamedQuery(name = "Message.findBySenderId", query = "SELECT m FROM Message m WHERE m.senderId = :senderId")
    , @NamedQuery(name = "Message.findByReceiverId", query = "SELECT m FROM Message m WHERE m.receiverId = :receiverId")
    , @NamedQuery(name = "Message.findByRead", query = "SELECT m FROM Message m WHERE m.read = :read")
    , @NamedQuery(name = "Message.findByDelivered", query = "SELECT m FROM Message m WHERE m.delivered = :delivered")})
public class Message implements Serializable, IPersistent {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss", timezone = "GMT+3")
    @Column(name = "datetime")
    private Date datetime;
    @Basic(optional = false)
    @Column(name = "senderId")
    private Long senderId;
    @Basic(optional = false)
    @Column(name = "receiverId")
    private Long receiverId;
    @Lob
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @Column(name = "isRead")
    private short read;
    @Basic(optional = false)
    @Column(name = "isDelivered")
    private short delivered;

    public Message() {
    }

    public Message(Long id) {
        this.id = id;
    }

    public Message(Long id, Long senderId, Long receiverId, short read, short delivered) {
        this.id = id;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.read = read;
        this.delivered = delivered;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public short getRead() {
        return read;
    }

    public void setRead(short read) {
        this.read = read;
    }

    public short getDelivered() {
        return delivered;
    }

    public void setDelivered(short delivered) {
        this.delivered = delivered;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Message)) {
            return false;
        }
        Message other = (Message) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.Message[ id=" + id + " ]";
    }

}
