/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.interfaces;

import java.util.ArrayList;
import java.util.Map;
import ru.net.breakpoint.redleaf.domain.entity.Message;

public interface IMessageService extends IService<Message> {
    public ArrayList<Message> getConversation(Long user1Id, Long user2Id, Map<String, String> filters);
}
