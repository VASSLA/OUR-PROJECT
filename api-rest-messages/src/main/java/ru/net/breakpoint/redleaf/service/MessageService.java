/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.MessageDao;
import ru.net.breakpoint.redleaf.domain.entity.Message;
import ru.net.breakpoint.redleaf.interfaces.IMessageService;

@Service
@Transactional
public class MessageService implements IMessageService{
    @Autowired
    private MessageDao messageDao;

    @Override
    public Message get(Long id) {
        return messageDao.get(id);
    }

    @Override
    public void saveOrUpdate(Message persistent) {
        messageDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Message persistent) {
        messageDao.delete(persistent);
    }

    @Override
    public ArrayList<Message> getList() {
        return messageDao.getList();
    }

    @Override
    public ArrayList<Message> getList(Map<String, String> filters) {
        return messageDao.getList(filters);
    }
    
    @Override
    public ArrayList<Message> getConversation(Long user1Id, Long user2Id, Map<String, String> filters) {
        return messageDao.getConversation(user1Id, user2Id, filters);
    }

}
