/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.net.breakpoint.redleaf.domain.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Message;

@Repository
public class MessageDao extends AbstractDao<Message> {

    public ArrayList<Message> getConversation(Long user1Id, Long user2Id, Map<String, String> filters) {
        Criteria criteria = getSession().createCriteria(Message.class);
        Criterion user1asSender = Restrictions.eq("senderId", user1Id);
        Criterion user2asReceiver = Restrictions.eq("receiverId", user2Id);
        Criterion user2asSender = Restrictions.eq("senderId", user2Id);
        Criterion user1asReceiver = Restrictions.eq("receiverId", user1Id);
        LogicalExpression user1SentToUser2 = Restrictions.and(user1asSender, user2asReceiver);
        LogicalExpression user2SentToUser1 = Restrictions.and(user2asSender, user1asReceiver);
        LogicalExpression conversation = Restrictions.or(user1SentToUser2, user2SentToUser1);
        criteria.add(conversation);
        int startResult = filters.get("start") != null
                ? Integer.parseInt(filters.get("start")) : -1;

        int limitResult = filters.get("limit") != null
                ? Integer.parseInt(filters.get("limit")) : -1;
        ArrayList <Order> orders = getOrderList(filters.get("orderBy"));
        List<String> aliasCache = new ArrayList<>();
        for (String key : filters.keySet()) {
            addFilter(key, filters.get(key), criteria, Restrictions.conjunction(), true, aliasCache);
        }
        if (startResult > -1) {
            criteria.setFirstResult(startResult);
        }
        if (limitResult > -1) {
            criteria.setMaxResults(limitResult);
        }
        if (!orders.isEmpty()) {
            for (Order order : orders) {
                criteria.addOrder(order);
            }
        }
        ArrayList<Message> result = new ArrayList<>();
        result.addAll(criteria.list());
        return result;
    }
}
