/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: IValidatorService.java
 * Date: Nov 17, 2016 8:38:42 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.Validator;

public interface IValidatorService extends IService<Validator>{

}
