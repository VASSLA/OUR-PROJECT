/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: TokenGenerator.java
 * Date: Nov 8, 2016 10:15:17 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.utils;

public class TokenGenerator {
    public static final String generate() {
        String result = "";
        for (int i = 0; i < 8; i++) {
            result = result + String.valueOf(Math.round(Math.ceil(Math.random() * 10)));
        }
        return result;
    }
}
