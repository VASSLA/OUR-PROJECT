/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: AbstractDao.java
 * Date: Jul 15, 2016 11:36:45 AM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.domain.dao;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import ru.net.breakpoint.redleaf.interfaces.IDao;

@Repository
public abstract class AbstractDao<T extends IPersistent> implements IDao<T> {

    @Autowired
    private SessionFactory sessionFactory;

    protected Class<T> getClassT() {
        ParameterizedType superclass = (ParameterizedType) getClass().getGenericSuperclass();
        return (Class<T>) superclass.getActualTypeArguments()[0];
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    protected void addFilter(String property, String value, Criteria criteria, Junction criterion, boolean precise, List<String> aliasCache) {
        addFilter(property, value, criteria, criterion, precise, aliasCache, null, null);
    }

    /**
     * The method add filter for Hibernate criteria for the property
     * @param property Property name
     * @param value Filter value, may contain % wildcard for String property
     * @param criteria
     * @param criterion
     * @param precise
     * @param aliasCache
     * @param fieldClass
     * @param fieldAlias
     */
    private void addFilter(String property, String value, Criteria criteria, Junction criterion, boolean precise, List<String> aliasCache, Class fieldClass, String fieldAlias) {
        Class classOfType = fieldClass != null ? fieldClass : getClassT();
        Field[] fields = classOfType.getDeclaredFields();

        String[] parts = null;
        if (fieldClass == null) {
            if (property.contains(".")) {
                parts = property.split(Pattern.quote("."));
                property = parts[0];
            }
        }

        for (int i = 0; i <= fields.length - 1; i++) {
            Field field = fields[i];
            if (!property.equals(fields[i].getName())) {
                continue;
            }
            if (fieldAlias != null) {
                property = fieldAlias + "." + property;
            }
            String fieldTypeName = field.getType().getSimpleName();
            switch (fieldTypeName) {
                case "Long":
                case "long":
                    criterion.add(Restrictions.eq(property, Long.parseLong(value)));
                    break;
                case "Integer":
                case "int":
                    criterion.add(Restrictions.eq(property, Integer.parseInt(value)));
                    break;
                case "Short":
                case "short":
                    criterion.add(Restrictions.eq(property, Short.parseShort(value)));
                    break;
                case "Byte":
                case "byte":
                    criterion.add(Restrictions.eq(property, Byte.parseByte(value)));
                    break;
                case "String":
                    criterion.add(precise ? Restrictions.eq(property, value) : Restrictions.like(property, "%" + value + "%"));
                    break;
                case "Date":
                    criterion.add(precise ? Restrictions.eq(property, Date.valueOf(value)) : Restrictions.like(property, Date.valueOf(value)));
                    break;
                case "Time":
                    criterion.add(precise ? Restrictions.eq(property, Time.valueOf(value)) : Restrictions.like(property, Time.valueOf(value)));
                    break;
                case "Boolean":
                case "bool":
                    criterion.add(Restrictions.eq(property, Boolean.valueOf(value)));
                    break;
                default:
                    if (IPersistent.class.isAssignableFrom(field.getType())) {
                        Class propFieldClass = field.getType();
                        String alias = property + "Alias";
                        if (!aliasCache.contains(alias)) {
                            criteria.createAlias(property, alias);
                            aliasCache.add(alias);
                        }
                        addFilter(parts[1], value, criteria, criterion, precise, aliasCache, propFieldClass, alias);
                    }
                    break;
            }
            break;
        }
    }

    protected ArrayList<Order> getOrderList(String orderString) {
        ArrayList<Order> result = new ArrayList<>();
        if (orderString == null) {
            return result;
        }
        String[] splits = orderString.split(",");
        for (String order : splits) {
            String[] fieldOrder = order.split(":");
            if (fieldOrder.length < 2 || fieldOrder.length > 2
                    || (!fieldOrder[1].toLowerCase().equals("asc")
                    && !fieldOrder[1].toLowerCase().equals("desc"))) {
                continue;
            }
            if (fieldOrder[1].equals("asc")) {
                result.add(Order.asc(fieldOrder[0]));
            } else {
                result.add(Order.desc(fieldOrder[0]));
            }
        }
        return result;
    }

    @Override
    public T get(Long id) {
        Class classOfType = getClassT();
        return (T) getSession().get(classOfType, id);
    }

    @Override
    public void saveOrUpdate(T persistent) {
        getSession().saveOrUpdate(persistent);
    }

    @Override
    public void delete(T persistent) {
        getSession().delete(persistent);
    }

    @Override
    public ArrayList<T> getList() {
        Class classOfType = getClassT();
        Criteria criteria = getSession().createCriteria(classOfType);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        ArrayList<T> list = new ArrayList<>();
        list.addAll(criteria.list());
        return list;
    }

    @Override
    public ArrayList<T> getList(Map<String, String> filters) {
        if (filters == null || filters.isEmpty()) {
            return getList();
        }

        boolean precise = filters.get("precise") == null
                || filters.get("precise").equals("1")
                || filters.get("precise").toLowerCase().equals("true");

        int startResult = filters.get("start") != null
                ? Integer.parseInt(filters.get("start")) : -1;

        int limitResult = filters.get("limit") != null
                ? Integer.parseInt(filters.get("limit")) : -1;

        String distinct = filters.get("distinct") != null ?
                filters.get("distinct") : "id";
        
        boolean useOrMethod = filters.get("method") != null
                ? filters.get("method").equalsIgnoreCase("or") : false;

        ArrayList<Order> orders = getOrderList(filters.get("orderBy"));

        Class classOfType = getClassT();
        Criteria criteria = getSession().createCriteria(classOfType);

        Junction criterion = useOrMethod ? Restrictions.disjunction() : Restrictions.conjunction();

        List<String> aliasCache = new ArrayList<>();
        for (String key : filters.keySet()) {
            addFilter(key, filters.get(key), criteria, criterion, precise, aliasCache);
        }

        if (filters.get("distinct") == null) {
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            criteria.add(criterion);
        } else {
            criteria.setProjection(Projections.distinct(Projections.property(distinct)));
        }  

        if (startResult > -1) {
            criteria.setFirstResult(startResult);
        }
        if (limitResult > -1) {
            criteria.setMaxResults(limitResult);
        }
        if (!orders.isEmpty()) {
            for (Order order : orders) {
                criteria.addOrder(order);
            }
        }
        ArrayList<T> list = new ArrayList<>();
        list.addAll(criteria.list());
          
        if (filters.get("distinct") != null) {
            Criteria secondCriteria = getSession().createCriteria(classOfType);
            secondCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            secondCriteria.add(Restrictions.in("id", list));
            ArrayList<T> secondList = new ArrayList<>();
            secondList.addAll(secondCriteria.list());
            return secondList;
        }
        
        return list;
    }
}
