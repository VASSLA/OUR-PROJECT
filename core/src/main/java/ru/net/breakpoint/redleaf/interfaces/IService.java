/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: IService.java
 * Date: Jul 15, 2016 11:36:45 AM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.interfaces;

import java.util.ArrayList;
import java.util.Map;

public interface IService<T extends IPersistent> {
    T get(Long id);
    void saveOrUpdate(T persistent);
    void delete(T persistent);
    ArrayList<T> getList();
    ArrayList<T> getList(Map<String, String> filters);
}
