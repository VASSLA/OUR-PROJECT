/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: ValidatorService.java
 * Date: Nov 17, 2016 8:39:28 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.ValidatorDao;
import ru.net.breakpoint.redleaf.domain.entity.Validator;
import ru.net.breakpoint.redleaf.interfaces.IValidatorService;

@Service
@Transactional
public class ValidatorService implements IValidatorService{
    @Autowired
    private ValidatorDao validatorDao;

    @Override
    public Validator get(Long id) {
        return validatorDao.get(id);
    }

    @Override
    public void saveOrUpdate(Validator persistent) {
        validatorDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Validator persistent) {
        validatorDao.delete(persistent);
    }

    @Override
    public ArrayList<Validator> getList() {
        return validatorDao.getList();
    }

    @Override
    public ArrayList<Validator> getList(Map<String, String> filters) {
        return validatorDao.getList(filters);
    }
}
