/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: ValidatorDao.java
 * Date: Nov 17, 2016 8:37:58 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Validator;

@Repository
public class ValidatorDao extends AbstractDao<Validator>{

}
