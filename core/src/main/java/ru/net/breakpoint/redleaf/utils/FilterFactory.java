/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: FilterFactory.java
 * Date: Jul 15, 2016 11:36:45 AM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.utils;

import java.util.HashMap;
import java.util.Map;

public class FilterFactory {
    public static final Map<String, String> prepareFilters(String filterString) {
        Map<String, String> filters = new HashMap<>();
        String[] split = filterString.split("&");
        for (int i = 0; i < split.length; i++) {
            String[] nameValuePair = split[i].split("=");
            filters.put(nameValuePair[0], nameValuePair[1]);
        }
        return filters;
    }
}
