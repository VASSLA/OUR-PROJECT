/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: BirthDateCalculator.java
 * Date: Mar 4, 2017 11:40:41 AM
 * Author: Alex */
package ru.net.breakpoint.redleaf.utils;

import java.util.Date;
import org.joda.time.LocalDate;

public class BirthDateCalculator {
    public static Date getBirthDateFromAge(Integer age) {
        LocalDate today = LocalDate.now();
        LocalDate birthDate = today.minusYears(age);
        return birthDate.toDate();
    }
}
