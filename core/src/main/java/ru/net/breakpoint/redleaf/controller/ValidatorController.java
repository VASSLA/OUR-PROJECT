/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: ValidatorController.java
 * Date: Nov 17, 2016 8:41:54 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Validator;
import ru.net.breakpoint.redleaf.interfaces.IValidatorService;

@RestController
public class ValidatorController {
    private static final Logger logger = Logger.getLogger(ValidatorController.class);

    @Autowired
    private IValidatorService validatorService;

    @RequestMapping(value = "/api/v{version}/validator", method = RequestMethod.GET)
    public ResponseEntity<List<Validator>> getValidatorList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/validator");
        return new ResponseEntity<List<Validator>> (validatorService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/validator/{id}", method = RequestMethod.GET)
    public ResponseEntity<Validator> getValidator(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/validator/" + id.toString());
        return new ResponseEntity<> (validatorService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/validator", method = RequestMethod.POST)
    public ResponseEntity<Validator> postValidator(@PathVariable Integer version, @RequestBody Validator validator) {
        logger.trace("Requested /api/v" + version.toString() + "/validator");
        validatorService.saveOrUpdate(validator);
        return new ResponseEntity<> (validator, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/validator", method = RequestMethod.PUT)
    public ResponseEntity<Validator> putValidator(@PathVariable Integer version, @RequestBody Validator validator) {
        logger.trace("Requested /api/v" + version.toString() + "/validator");
        validatorService.saveOrUpdate(validator);
        return new ResponseEntity<> (validator, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/validator/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Validator> deleteValidator(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/validator");
        Validator validator = validatorService.get(id);
        if (validator != null) {
            validatorService.delete(validator);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }
}
