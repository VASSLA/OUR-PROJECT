/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: core
 * File Name: Validator.java
 * Date: Nov 17, 2016 8:36:31 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "Validator")
@NamedQueries({
    @NamedQuery(name = "Validator.findAll", query = "SELECT v FROM Validator v")
    , @NamedQuery(name = "Validator.findById", query = "SELECT v FROM Validator v WHERE v.id = :id")
    , @NamedQuery(name = "Validator.findByDescription", query = "SELECT v FROM Validator v WHERE v.description = :description")})
public class Validator implements Serializable, IPersistent {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @Lob
    @Column(name = "rexegp")
    private String rexegp;

    @Column(name = "description")
    private String description;

    public Validator() {
    }

    public Validator(Long id) {
        this.id = id;
    }

    public Validator(Long id, String rexegp) {
        this.id = id;
        this.rexegp = rexegp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRexegp() {
        return rexegp;
    }

    public void setRexegp(String rexegp) {
        this.rexegp = rexegp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Validator)) {
            return false;
        }
        Validator other = (Validator) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.Validator[ id=" + id + " ]";
    }

}
