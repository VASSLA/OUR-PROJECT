﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.2.53.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 09.01.2017 21:27:38
-- Версия сервера: 5.7.16-log
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Описание для таблицы Address
--
DROP TABLE IF EXISTS Address;
CREATE TABLE Address (
  id INT(11) NOT NULL AUTO_INCREMENT,
  personId INT(11) NOT NULL,
  kladrId VARCHAR(25) DEFAULT NULL,
  address VARCHAR(255) DEFAULT NULL,
  defaultAddress TINYINT(4) DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 8
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы BuddiesList
--
DROP TABLE IF EXISTS BuddiesList;
CREATE TABLE BuddiesList (
  userId INT(11) NOT NULL,
  buddyId INT(11) NOT NULL,
  PRIMARY KEY (userId, buddyId)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 1638
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Capability
--
DROP TABLE IF EXISTS Capability;
CREATE TABLE Capability (
  id INT(11) NOT NULL AUTO_INCREMENT,
  capability VARCHAR(255) DEFAULT NULL,
  capabilityIcon VARCHAR(255) DEFAULT 'fa-square-o',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Contact
--
DROP TABLE IF EXISTS Contact;
CREATE TABLE Contact (
  id INT(11) NOT NULL AUTO_INCREMENT,
  typeId INT(11) NOT NULL DEFAULT 1,
  personId INT(11) NOT NULL,
  contact VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы ContactType
--
DROP TABLE IF EXISTS ContactType;
CREATE TABLE ContactType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  validatorId INT(11) DEFAULT NULL,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Типы контактной информации'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Event
--
DROP TABLE IF EXISTS Event;
CREATE TABLE Event (
  id INT(11) NOT NULL AUTO_INCREMENT,
  typeId INT(11) NOT NULL,
  message TEXT DEFAULT NULL,
  datetime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  userId INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы EventType
--
DROP TABLE IF EXISTS EventType;
CREATE TABLE EventType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы GeoFeature
--
DROP TABLE IF EXISTS GeoFeature;
CREATE TABLE GeoFeature (
  id INT(11) NOT NULL AUTO_INCREMENT,
  geometry GEOMETRY NOT NULL COMMENT 'Contains Geometry Feature Data',
  requestId VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX id_UNIQUE (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Table for GeoData FeatureCollection items.'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Log
--
DROP TABLE IF EXISTS Log;
CREATE TABLE Log (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  logMessageType INT(11) NOT NULL COMMENT 'Тип сообщения',
  callingClass VARCHAR(128) NOT NULL COMMENT 'Класс создавший сообщение',
  message VARCHAR(256) NOT NULL COMMENT 'Сообщение',
  logDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата',
  userId INT(11) DEFAULT NULL COMMENT 'Пользователь, вызвавший сообщение',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 54
AVG_ROW_LENGTH = 963
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Лог'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы LogMessageType
--
DROP TABLE IF EXISTS LogMessageType;
CREATE TABLE LogMessageType (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `key` VARCHAR(16) NOT NULL COMMENT 'Ключ',
  description VARCHAR(32) NOT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица для типов сообщений лога'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы MediaInfo
--
DROP TABLE IF EXISTS MediaInfo;
CREATE TABLE MediaInfo (
  id INT(11) NOT NULL AUTO_INCREMENT,
  fileName VARCHAR(256) DEFAULT '0',
  type VARCHAR(64) DEFAULT '0',
  userId INT(11) DEFAULT 0,
  uploadDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  initialName VARCHAR(64) DEFAULT NULL,
  thumbnailName VARCHAR(256) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 81
AVG_ROW_LENGTH = 1024
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Информация о медиафайлах'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Message
--
DROP TABLE IF EXISTS Message;
CREATE TABLE Message (
  id INT(11) NOT NULL AUTO_INCREMENT,
  datetime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  senderId INT(11) NOT NULL,
  receiverId INT(11) NOT NULL,
  message TEXT DEFAULT NULL,
  isRead TINYINT(4) NOT NULL DEFAULT 0,
  isDelivered TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 102
AVG_ROW_LENGTH = 399
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Person
--
DROP TABLE IF EXISTS Person;
CREATE TABLE Person (
  id INT(11) NOT NULL AUTO_INCREMENT,
  firstName VARCHAR(50) NOT NULL,
  middleName VARCHAR(50) DEFAULT NULL,
  lastName VARCHAR(50) NOT NULL,
  birthDate DATE DEFAULT NULL,
  sex TINYINT(4) NOT NULL DEFAULT 0,
  pictureId INT(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 51
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы PersonSkill
--
DROP TABLE IF EXISTS PersonSkill;
CREATE TABLE PersonSkill (
  personId INT(11) NOT NULL,
  skillId INT(11) NOT NULL,
  PRIMARY KEY (personId, skillId)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 1024
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Request
--
DROP TABLE IF EXISTS Request;
CREATE TABLE Request (
  id INT(11) NOT NULL AUTO_INCREMENT,
  creationDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  missingPersonId INT(11) NOT NULL,
  missingFrom DATETIME DEFAULT NULL,
  lastSeenPlace VARCHAR(255) DEFAULT NULL,
  extraInfo TEXT DEFAULT NULL,
  dressedIn TEXT DEFAULT NULL,
  circOfDisappear TEXT DEFAULT NULL,
  health TEXT DEFAULT NULL,
  appearance TEXT DEFAULT NULL,
  stuff TEXT DEFAULT NULL,
  declarantPersonId INT(11) NOT NULL,
  requestTypeId TINYINT(4) NOT NULL DEFAULT 1 COMMENT '0 - Обычная, 1 - Срочная',
  localityId INT(11) NOT NULL DEFAULT 1,
  sourceId INT(11) DEFAULT NULL,
  currentStateId INT(11) NOT NULL DEFAULT 0,
  inforgPersonId INT(11) DEFAULT NULL,
  coordPersonId INT(11) DEFAULT NULL,
  report TEXT DEFAULT NULL,
  forestOnline TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 26
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Запросы на поиск'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestLocality
--
DROP TABLE IF EXISTS RequestLocality;
CREATE TABLE RequestLocality (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestSource
--
DROP TABLE IF EXISTS RequestSource;
CREATE TABLE RequestSource (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestState
--
DROP TABLE IF EXISTS RequestState;
CREATE TABLE RequestState (
  id INT(11) NOT NULL AUTO_INCREMENT,
  state VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 7
AVG_ROW_LENGTH = 2730
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestType
--
DROP TABLE IF EXISTS RequestType;
CREATE TABLE RequestType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Role
--
DROP TABLE IF EXISTS Role;
CREATE TABLE Role (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор роли',
  roleName VARCHAR(255) NOT NULL COMMENT 'Название роли',
  roleDescription VARCHAR(255) DEFAULT NULL COMMENT 'Описание роли',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица ролей'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Setting
--
DROP TABLE IF EXISTS Setting;
CREATE TABLE Setting (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) DEFAULT NULL,
  value VARCHAR(255) DEFAULT NULL,
  description VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 7
AVG_ROW_LENGTH = 2730
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Skill
--
DROP TABLE IF EXISTS Skill;
CREATE TABLE Skill (
  id INT(11) NOT NULL AUTO_INCREMENT,
  skill VARCHAR(255) NOT NULL,
  skillIcon VARCHAR(255) DEFAULT 'fa-square-o',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 6
AVG_ROW_LENGTH = 3276
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы User
--
DROP TABLE IF EXISTS User;
CREATE TABLE User (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  login VARCHAR(50) NOT NULL COMMENT 'Логин пользователя',
  email VARCHAR(255) NOT NULL,
  passwordHash VARCHAR(255) NOT NULL COMMENT 'Хэш пароля',
  creationDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата создания пользователя',
  lastLoginDate DATETIME DEFAULT NULL COMMENT 'Дата последнего входа',
  lastLoginIp VARCHAR(30) DEFAULT NULL COMMENT 'IP-адрес последнего входа',
  disabled TINYINT(4) NOT NULL COMMENT 'Признак заблокированного пользователя',
  reasonToDisable VARCHAR(255) DEFAULT NULL COMMENT 'Причина блокировки',
  disabledToDate DATETIME DEFAULT NULL COMMENT 'Заблокирован до даты',
  personId INT(11) DEFAULT NULL,
  confirmed TINYINT(4) NOT NULL DEFAULT 0,
  confirmationToken VARCHAR(10) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 40
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица пользователей'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы UserRole
--
DROP TABLE IF EXISTS UserRole;
CREATE TABLE UserRole (
  userId INT(11) NOT NULL COMMENT 'Идентификатор пользователя (User)',
  roleId INT(11) NOT NULL COMMENT 'Идентификатор роли',
  PRIMARY KEY (userId, roleId)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Списки ролей для идентификатора пользователя'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Validator
--
DROP TABLE IF EXISTS Validator;
CREATE TABLE Validator (
  id INT(11) NOT NULL AUTO_INCREMENT,
  rexegp TEXT NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

-- 
-- Вывод данных для таблицы Address
--
INSERT INTO Address VALUES
(1, 1, '50505000000000000', '142900, обл. Московская, р-н. Каширский, г. Кашира, ул. Гвардейская, д.8', 1),
(5, 35, '5003400100000030006', '142800, Московская, Ступинский, Ступино, Андропова, 12 (5003400100000030006)', NULL),
(6, 48, '5001400100000040001', '142700, Московская, Ленинский, Видное, Гаевского, 1 (5001400100000040001)', NULL),
(7, 49, '5000002600000450001', '142600, Московская, , Орехово-Зуево, Вокзальная, 1 (5000002600000450001)', NULL);

-- 
-- Вывод данных для таблицы BuddiesList
--
INSERT INTO BuddiesList VALUES
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(36, 1),
(36, 39),
(37, 1),
(37, 38),
(38, 1),
(38, 37);

-- 
-- Вывод данных для таблицы Capability
--

-- Таблица redleaf_db.Capability не содержит данных

-- 
-- Вывод данных для таблицы Contact
--
INSERT INTO Contact VALUES
(1, 1, 1, '+7 926 5752524'),
(2, 1, 1, '+7 916 0907888');

-- 
-- Вывод данных для таблицы ContactType
--
INSERT INTO ContactType VALUES
(1, 1, 'Телефон'),
(2, NULL, 'Электронная почта');

-- 
-- Вывод данных для таблицы Event
--

-- Таблица redleaf_db.Event не содержит данных

-- 
-- Вывод данных для таблицы EventType
--

-- Таблица redleaf_db.EventType не содержит данных

-- 
-- Вывод данных для таблицы GeoFeature
--

-- Таблица redleaf_db.GeoFeature не содержит данных

-- 
-- Вывод данных для таблицы Log
--
INSERT INTO Log VALUES
(1, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-21 19:38:09', 1),
(2, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-21 19:40:00', 1),
(3, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-22 11:52:06', 1),
(4, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-22 11:52:32', 1),
(5, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-22 11:54:13', 1),
(6, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log/level', '2016-12-22 11:55:03', 1),
(7, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-22 11:55:03', 1),
(8, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log/level', '2016-12-22 11:58:40', 1),
(9, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-22 11:58:40', 1),
(10, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log/level', '2016-12-22 12:02:42', 1),
(11, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-22 12:02:42', 1),
(12, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log/level', '2016-12-22 12:04:20', 1),
(13, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-22 12:04:20', 1),
(14, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log/level', '2016-12-22 12:05:51', 1),
(15, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-22 12:05:51', 1),
(16, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log', '2016-12-22 12:07:01', 1),
(17, 800, 'ru.net.breakpoint.redleaf.controller.LogController', 'Requested /api/v1/log/level', '2016-12-22 12:07:01', 1);

-- 
-- Вывод данных для таблицы LogMessageType
--
INSERT INTO LogMessageType VALUES
(1, 'error', 'Ошибка'),
(2, 'warning', 'Предупреждение'),
(3, 'info', 'Уведомление');

-- 
-- Вывод данных для таблицы MediaInfo
--
INSERT INTO MediaInfo VALUES
(1, '0000000001', 'image/jpg', 1, '2016-11-23 22:01:35', 'picture1.jpg', '0000000001.jpg'),
(6, '0000000005', 'image/jpg', 1, '2016-11-23 22:34:49', 'picture5.jpg', '0000000005.jpg'),
(7, '0000000006', 'image/jpg', 1, '2016-11-23 22:35:01', 'picture6.jpg', '0000000006.jpg'),
(8, '0000000007', 'image/jpg', 1, '2016-11-23 22:42:26', 'picture7.jpg', '0000000007.jpg'),
(9, '0000000008', 'image/jpg', 1, '2016-11-23 22:43:40', 'picture8.jpg', '0000000008.jpg'),
(10, '0000000009', 'image/jpg', 1, '2016-11-23 22:45:10', 'picture9.jpg', '0000000009.jpg'),
(11, '000000000A', 'image/jpg', 1, '2016-11-23 22:46:15', 'picture10.jpg', '000000000A.jpg'),
(60, '0000000003C.jpg', 'image/jpeg', 1, '2016-12-16 13:38:51', 'WP_20160217_10_51_07_Pro.jpg', '0000000003C.jpg'),
(61, '0000000003D.jpg', 'image/jpeg', 1, '2016-12-16 13:40:25', 'WP_20160217_20_28_05_Pro.jpg', '0000000003D.jpg'),
(62, '0000000003E.jpg', 'image/jpeg', 1, '2016-12-16 13:41:24', 'WP_20160216_20_33_10_Pro.jpg', '0000000003E.jpg'),
(63, '0000000003F.jpg', 'image/jpeg', 1, '2016-12-16 13:42:24', 'WP_20160216_15_16_00_Pro.jpg', '0000000003F.jpg'),
(70, '00000000046.blade', 'image/jpeg', 1, '2016-12-16 15:09:02', 'alex.blade.box@gmail.com avatar.jpg', '00000000046.jpg'),
(75, '0000000004B.jpg', 'image/jpeg', 1, '2016-12-16 19:53:46', '0_d2630_7eb52abf_orig.jpg', '0000000004B.jpg'),
(76, '0000000004C.png', 'image/png', 1, '2016-12-23 21:39:07', 'IMG_16122016_150957.png', '0000000004C.jpg'),
(77, '0000000004D.png', 'image/png', 1, '2016-12-23 21:39:07', 'IMG_16122016_150957.png', '0000000004D.jpg'),
(80, '00000000050.bmp', 'image/bmp', 39, '2017-01-07 19:02:33', '1.bmp', '00000000050.jpg');

-- 
-- Вывод данных для таблицы Message
--
INSERT INTO Message VALUES
(47, '2017-01-09 00:00:00', 36, 1, 'Один', 1, 0),
(48, '2017-01-09 00:00:00', 37, 1, 'Здрассте', 1, 0),
(49, '2017-01-09 00:00:00', 37, 1, 'До свидания', 1, 0),
(50, '2017-01-09 00:00:00', 1, 37, 'Привет', 1, 0),
(51, '2017-01-09 00:00:00', 1, 37, 'Работает', 1, 0),
(52, '2017-01-09 00:00:00', 37, 1, 'Вроде как ', 1, 0),
(53, '2017-01-09 00:00:00', 38, 37, 'Привет', 1, 0),
(54, '2017-01-09 00:00:00', 37, 38, 'Привет', 1, 0),
(55, '2017-01-09 00:00:00', 37, 38, 'Как дела?', 1, 0),
(56, '2017-01-09 00:00:00', 37, 1, 'Даже много пользователей работают', 1, 0),
(57, '2017-01-09 00:00:00', 38, 1, 'Оп', 1, 0),
(58, '2017-01-09 00:00:00', 1, 38, 'Сам такой', 1, 0),
(59, '2017-01-09 00:00:00', 1, 38, 'Упс', 1, 0),
(60, '2017-01-09 00:00:00', 1, 38, 'ПЫшш', 1, 0),
(61, '2017-01-09 00:00:00', 1, 36, 'Два', 1, 0),
(62, '2017-01-09 00:00:00', 1, 36, 'Три', 1, 0),
(77, '2017-01-09 00:00:00', 1, 36, NULL, 1, 0),
(78, '2017-01-09 00:00:00', 1, 36, 'tttt', 1, 0),
(79, '2017-01-09 00:00:00', 1, 36, 'oiuoiuo', 1, 0),
(80, '2017-01-09 00:00:00', 1, 36, NULL, 1, 0),
(81, '2017-01-09 00:00:00', 1, 36, 'ffghfghf', 1, 0),
(82, '2017-01-09 00:00:00', 1, 36, 'gjbnghfng', 1, 0),
(83, '2017-01-09 00:00:00', 1, 36, '', 1, 0),
(84, '2017-01-09 00:00:00', 1, 36, '8979879', 1, 0),
(85, '2017-01-09 00:00:00', 1, 36, '080980980', 1, 0),
(86, '2017-01-09 00:00:00', 1, 36, '-99-9-', 1, 0),
(87, '2017-01-09 00:00:00', 1, 36, '9879879', 1, 0),
(88, '2017-01-09 00:00:00', 1, 36, 'r5gy54', 1, 0),
(89, '2017-01-09 00:00:00', 1, 36, '09809890', 1, 0),
(90, '2017-01-09 14:10:42', 1, 36, '7987979', 1, 0),
(91, '2017-01-09 14:13:14', 1, 36, '654646', 1, 0),
(92, '2017-01-09 14:20:15', 1, 36, '9845739487593', 1, 0),
(93, '2017-01-09 14:58:19', 1, 36, 'Опачки ', 1, 0),
(94, '2017-01-09 15:16:50', 1, 36, '9709707079', 1, 0),
(95, '2017-01-09 15:16:56', 1, 36, 'dfthvdrdbhrdhn', 1, 0),
(96, '2017-01-09 15:16:59', 1, 36, 'drtfgrdybdr', 1, 0),
(97, '2017-01-09 15:17:11', 1, 38, '987080809', 1, 0),
(98, '2017-01-09 15:17:21', 1, 39, '9879879879', 1, 0),
(99, '2017-01-09 15:18:17', 1, 36, 'rg67e65he57', 1, 0),
(100, '2017-01-09 21:09:58', 1, 36, 'ldijgldkjgldk', 0, 0),
(101, '2017-01-09 21:10:00', 1, 36, 'dkgjdgjldjgdldkg', 0, 0);

-- 
-- Вывод данных для таблицы Person
--
INSERT INTO Person VALUES
(1, 'Алексей', 'Сергеевич', 'Аникеев', '1978-12-16', 0, 70),
(35, 'Мария', 'Петровна', 'Иванова', '1960-06-08', 1, NULL),
(43, 'Alex', '', 'Blade', '1978-12-16', 0, NULL),
(44, 'vdfgb', 'vdgdxf', 'xdfcgv', '2016-11-30', 0, NULL),
(45, 'Роман', 'Гаврилович', 'Нефедов', '1976-06-17', 0, 11),
(46, 'Иван', 'Иванович', 'Иванов', '1987-10-29', 0, 11),
(47, 'rty', 'rtyrty', 'rtyrt', '1989-06-07', 0, NULL),
(48, '7567', '56756', '656756', '2016-12-25', 0, NULL),
(49, 'vdgvbdf', 'gvdbgd', 'df gdfv', '2016-12-15', 0, NULL),
(50, 'Антон', 'Петрович', 'Бицура', '1986-10-08', 0, 80);

-- 
-- Вывод данных для таблицы PersonSkill
--
INSERT INTO PersonSkill VALUES
(1, 1),
(43, 1),
(43, 2),
(43, 3),
(43, 4),
(43, 5),
(45, 1),
(45, 3),
(45, 5),
(46, 1),
(46, 2),
(46, 3),
(46, 4),
(50, 1),
(50, 3),
(50, 4);

-- 
-- Вывод данных для таблицы Request
--
INSERT INTO Request VALUES
(23, '2016-12-01 15:25:28', 35, '2016-10-31 04:00:00', 'Рынок Ступино', '16.12.2016', 'Обычная', 'Неизвестны', 'Нормальное', 'Обычная', 'Обычная', 1, 1, 2, 1, 1, NULL, NULL, NULL, 1),
(24, '2016-12-25 18:55:55', 48, '2016-12-12 03:00:00', '567567567', '56756', '7567', '567567', '5756756', '56756', '7567', 1, 1, 1, 1, 1, NULL, NULL, NULL, 1),
(25, '2016-12-25 19:01:36', 49, NULL, '', 'bdfgbhf', 'bfnhfd', 'drtfdby', 'bnfdbhfn', 'dfbbt', 'dbrhnydrt', 1, 1, 1, 1, 1, NULL, NULL, NULL, 1);

-- 
-- Вывод данных для таблицы RequestLocality
--
INSERT INTO RequestLocality VALUES
(1, 'Город'),
(2, 'Лес'),
(3, 'Город + Лес');

-- 
-- Вывод данных для таблицы RequestSource
--
INSERT INTO RequestSource VALUES
(1, 'Сайт lizaalert.org', 'Заявка с сайта Лиза Алерт'),
(2, 'Система LizaAlert', 'Заявка, заполненная через приложение или сайт системы LizaAlert'),
(3, 'Телефон', 'Заявка принятая по телефону');

-- 
-- Вывод данных для таблицы RequestState
--
INSERT INTO RequestState VALUES
(1, 'Новая'),
(2, 'Модерация'),
(3, 'Готова к поиску'),
(4, 'Активна'),
(5, 'Приостановлена'),
(6, 'Закрыта');

-- 
-- Вывод данных для таблицы RequestType
--
INSERT INTO RequestType VALUES
(1, 'Обычная'),
(2, 'Срочная');

-- 
-- Вывод данных для таблицы Role
--
INSERT INTO Role VALUES
(1, 'ROOT', 'Администратор'),
(2, 'DEVELOPER', 'Разработчик'),
(3, 'USER', 'Пользователь');

-- 
-- Вывод данных для таблицы Setting
--
INSERT INTO Setting VALUES
(1, 'Common.FileSystem.MediaLibraryTemporaryFolder', 'C:\\Temp', 'Временная папка для загрузки файлов на сервер'),
(2, 'root.Interface.DefaultView', 'Messages', 'Представление по умолчанию'),
(3, 'roma.Interface.Skin', 'skin-blue', 'Скин пользователя'),
(4, 'alex.Interface.Skin', 'skin-blue', 'Скин пользователя'),
(5, 'alex.Interface.DefaultView', 'Messages', 'Представление по умолчанию'),
(6, 'root.Interface.Skin', 'skin-blue', 'Скин пользователя');

-- 
-- Вывод данных для таблицы Skill
--
INSERT INTO Skill VALUES
(1, 'Ориентирование', 'fa-safari'),
(2, 'Плавсредство', 'fa-anchor'),
(3, 'Велосипед', 'fa-bicycle'),
(4, 'Машина', 'fa-car'),
(5, 'Видеокамера', 'fa-video-camera');

-- 
-- Вывод данных для таблицы User
--
INSERT INTO User VALUES
(1, 'root', '', '63a9f0ea7bb98050796b649e85481845', '2016-12-01 21:54:21', '2013-11-04 17:20:19', '', 0, '', NULL, 1, 1, NULL),
(36, 'alex', 'alex.blade.box@mail.ru', 'f7ddd46a08418d9850974259c8dd1faa', '2016-12-14 22:28:58', NULL, NULL, 1, NULL, NULL, 43, 1, '106839336'),
(37, 'roma', 'alex.blade.box@mail.ru', '202cb962ac59075b964b07152d234b70', '2016-12-14 22:28:59', NULL, NULL, 1, NULL, NULL, 45, 1, '58818342'),
(38, 'ivan', 'alex.blade.box@gmail.com', '202cb962ac59075b964b07152d234b70', '2016-12-14 22:29:30', NULL, NULL, 1, NULL, NULL, 46, 1, '710599793'),
(39, 'AntonBitsura', 'tel-fm@ya.ru', 'fc5364bf9dbfa34954526becad136d4b', '2017-01-07 17:05:24', NULL, NULL, 1, NULL, NULL, 50, 0, '23689629');

-- 
-- Вывод данных для таблицы UserRole
--
INSERT INTO UserRole VALUES
(1, 1),
(1, 2),
(36, 3),
(37, 3),
(38, 3),
(39, 3);

-- 
-- Вывод данных для таблицы Validator
--
INSERT INTO Validator VALUES
(1, '//s', 'Пустой валидатор');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;