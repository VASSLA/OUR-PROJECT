﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.1.26.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 09.11.2016 23:12:57
-- Версия сервера: 5.7.16-Log
-- Версия клиента: 4.1
--


--
-- Отключение внешних ключей
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Установить режим SQL (SQL mode)
--
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Описание для таблицы ContactType
--
DROP TABLE IF EXISTS ContactType;
CREATE TABLE ContactType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Типы контактной информации'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Log
--
DROP TABLE IF EXISTS Log;
CREATE TABLE Log (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  logMessageTypeId INT(11) NOT NULL COMMENT 'Тип сообщения',
  callingClass VARCHAR(128) NOT NULL COMMENT 'Класс создавший сообщение',
  message VARCHAR(256) NOT NULL COMMENT 'Сообщение',
  date DATETIME NOT NULL COMMENT 'Дата',
  userId INT(11) DEFAULT NULL COMMENT 'Пользователь, вызвавший сообщение',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Лог'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы LogMessageType
--
DROP TABLE IF EXISTS LogMessageType;
CREATE TABLE LogMessageType (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `key` VARCHAR(16) NOT NULL COMMENT 'Ключ',
  description VARCHAR(32) NOT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица для типов сообщений лога'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Person
--
DROP TABLE IF EXISTS Person;
CREATE TABLE Person (
  id INT(11) NOT NULL AUTO_INCREMENT,
  firstName VARCHAR(50) NOT NULL,
  middleName VARCHAR(50) DEFAULT NULL,
  lastName VARCHAR(50) NOT NULL,
  birthDate DATE DEFAULT NULL,
  sex TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 20
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Request
--
DROP TABLE IF EXISTS Request;
CREATE TABLE Request (
  id INT(11) NOT NULL AUTO_INCREMENT,
  creationDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  missingPersonId INT(11) NOT NULL,
  missingFrom DATETIME DEFAULT NULL,
  lastSeenPlace VARCHAR(255) DEFAULT NULL,
  extraInfo TEXT DEFAULT NULL,
  dressedIn TEXT DEFAULT NULL,
  declarantPersonId INT(11) NOT NULL,
  requestTypeId TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 - Обычная, 1 - Срочная',
  sourceId INT(11) DEFAULT NULL,
  currentStateId VARCHAR(255) NOT NULL DEFAULT '0',
  inforgPersonId INT(11) DEFAULT NULL,
  coordPersonId INT(11) DEFAULT NULL,
  report TEXT DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 16
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Запросы на поиск'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestSource
--
DROP TABLE IF EXISTS RequestSource;
CREATE TABLE RequestSource (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestState
--
DROP TABLE IF EXISTS RequestState;
CREATE TABLE RequestState (
  id INT(11) NOT NULL AUTO_INCREMENT,
  state VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 7
AVG_ROW_LENGTH = 2730
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestType
--
DROP TABLE IF EXISTS RequestType;
CREATE TABLE RequestType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Role
--
DROP TABLE IF EXISTS Role;
CREATE TABLE Role (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор роли',
  roleName VARCHAR(255) NOT NULL COMMENT 'Название роли',
  roleDescription VARCHAR(255) DEFAULT NULL COMMENT 'Описание роли',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица ролей'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Setting
--
DROP TABLE IF EXISTS Setting;
CREATE TABLE Setting (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) DEFAULT NULL,
  description VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы User
--
DROP TABLE IF EXISTS User;
CREATE TABLE User (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  login VARCHAR(50) NOT NULL COMMENT 'Логин пользователя',
  email VARCHAR(255) NOT NULL,
  passwordHash VARCHAR(255) NOT NULL COMMENT 'Хэш пароля',
  creationDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата создания пользователя',
  lastLoginDate DATETIME DEFAULT NULL COMMENT 'Дата последнего входа',
  lastLoginIp VARCHAR(30) DEFAULT NULL COMMENT 'IP-адрес последнего входа',
  disabled TINYINT(4) NOT NULL COMMENT 'Признак заблокированного пользователя',
  reasonToDisable VARCHAR(255) DEFAULT NULL COMMENT 'Причина блокировки',
  disabledToDate DATETIME DEFAULT NULL COMMENT 'Заблокирован до даты',
  personId INT(11) DEFAULT NULL,
  confirmed TINYINT(4) NOT NULL DEFAULT 0,
  confirmationToken VARCHAR(10) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 33
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица пользователей'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы UserRole
--
DROP TABLE IF EXISTS UserRole;
CREATE TABLE UserRole (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор записи',
  userId INT(11) NOT NULL COMMENT 'Идентификатор пользователя (User)',
  roleId INT(11) NOT NULL COMMENT 'Идентификатор роли',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 30
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Списки ролей для идентификатора пользователя'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Validator
--
DROP TABLE IF EXISTS Validator;
CREATE TABLE Validator (
  id INT(11) NOT NULL AUTO_INCREMENT,
  rexegp TEXT NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Вывод данных для таблицы ContactType
--

-- Таблица redleaf_db.ContactType не содержит данных

--
-- Вывод данных для таблицы Log
--

-- Таблица redleaf_db.Log не содержит данных

--
-- Вывод данных для таблицы LogMessageType
--
INSERT INTO LogMessageType VALUES
(1, 'error', 'Ошибка'),
(2, 'warning', 'Предупреждение'),
(3, 'info', 'Уведомление');

--
-- Вывод данных для таблицы Person
--
INSERT INTO Person VALUES
(1, 'Aleksey', 'Sergeevitch', 'Anikeev', '1978-12-16', 0),
(2, 'Petr', 'Petrovitch', 'Petrov', '1993-02-01', 0),
(18, 'Петр', 'Петрович', 'Петров', '2016-11-02', 0),
(19, 'Мария', 'Евгеньевна', 'Иванова', '1960-03-16', 0);

--
-- Вывод данных для таблицы Request
--
INSERT INTO Request VALUES
(14, '2016-11-07 22:22:13', 18, '2016-11-01 03:00:00', 'Город Ступино', 'Родинка на щеке', 'Джинсы, толстовка, кроссовки', 1, 1, 1, '1', NULL, NULL, NULL),
(15, '2016-11-07 22:21:31', 19, '2016-11-01 03:00:00', 'Кашира', 'На руке кольцо', 'Платье', 1, 1, 1, '1', NULL, NULL, NULL);

--
-- Вывод данных для таблицы RequestSource
--
INSERT INTO RequestSource VALUES
(1, 'Сайт lizaalert.org', 'Заявка с сайта Лиза Алерт'),
(2, 'Система LizaAlert', 'Заявка, заполненная через приложение или сайт системы LizaAlert'),
(3, 'Телефон', 'Заявка принятая по телефону');

--
-- Вывод данных для таблицы RequestState
--
INSERT INTO RequestState VALUES
(1, 'Новая'),
(2, 'Модерация'),
(3, 'Готова к поиску'),
(4, 'Активна'),
(5, 'Приостановлена'),
(6, 'Закрыта');

--
-- Вывод данных для таблицы RequestType
--
INSERT INTO RequestType VALUES
(1, 'Обыная'),
(2, 'Срочная');

--
-- Вывод данных для таблицы Role
--
INSERT INTO Role VALUES
(1, 'ROOT', 'Администратор'),
(2, 'DEVELOPER', 'Разработчик'),
(3, 'USER', 'Пользователь');

--
-- Вывод данных для таблицы Setting
--

-- Таблица redleaf_db.Setting не содержит данных

--
-- Вывод данных для таблицы User
--
INSERT INTO User VALUES
(1, 'root', '', '63a9f0ea7bb98050796b649e85481845', '2016-11-08 22:23:11', '2013-11-04 17:20:19', '', 0, '', NULL, 1, 1, NULL),
(2, 'admin', '', '63a9f0ea7bb98050796b649e85481845', '2016-11-08 22:23:14', NULL, NULL, 0, NULL, NULL, 2, 1, NULL),
(32, 'mouse', 'alex.blade.box@gmail.com', '202cb962ac59075b964b07152d234b70', '2016-11-09 23:07:05', NULL, NULL, 1, NULL, NULL, NULL, 1, '48531525');

--
-- Вывод данных для таблицы UserRole
--
INSERT INTO UserRole VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 2, 3),
(11, 14, 3),
(12, 15, 3),
(13, 16, 3),
(14, 17, 3),
(15, 18, 3),
(16, 19, 3),
(17, 20, 3),
(18, 21, 3),
(19, 22, 3),
(20, 23, 3),
(21, 24, 3),
(22, 25, 3),
(23, 26, 3),
(24, 27, 3),
(25, 28, 3),
(26, 29, 3),
(27, 30, 3),
(28, 31, 3),
(29, 32, 3);

--
-- Вывод данных для таблицы Validator
--

-- Таблица redleaf_db.Validator не содержит данных

--
-- Восстановить предыдущий режим SQL (SQL mode)
--
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

--
-- Включение внешних ключей
--
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;