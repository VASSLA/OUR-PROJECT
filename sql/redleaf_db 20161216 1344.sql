﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.1.26.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 16.12.2016 13:44:38
-- Версия сервера: 5.7.16-log
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Описание для таблицы Address
--
DROP TABLE IF EXISTS Address;
CREATE TABLE Address (
  id INT(11) NOT NULL AUTO_INCREMENT,
  personId INT(11) NOT NULL,
  kladrId VARCHAR(25) DEFAULT NULL,
  address VARCHAR(255) DEFAULT NULL,
  defaultAddress TINYINT(4) DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 6
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы BuddiesList
--
DROP TABLE IF EXISTS BuddiesList;
CREATE TABLE BuddiesList (
  userId INT(11) NOT NULL,
  buddyId INT(11) NOT NULL,
  PRIMARY KEY (userId, buddyId)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Capability
--
DROP TABLE IF EXISTS Capability;
CREATE TABLE Capability (
  id INT(11) NOT NULL AUTO_INCREMENT,
  capability VARCHAR(255) DEFAULT NULL,
  capabilityIcon VARCHAR(255) DEFAULT 'fa-square-o',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Contact
--
DROP TABLE IF EXISTS Contact;
CREATE TABLE Contact (
  id INT(11) NOT NULL AUTO_INCREMENT,
  typeId INT(11) NOT NULL DEFAULT 1,
  personId INT(11) NOT NULL,
  contact VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы ContactType
--
DROP TABLE IF EXISTS ContactType;
CREATE TABLE ContactType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  validatorId INT(11) DEFAULT NULL,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Типы контактной информации'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Event
--
DROP TABLE IF EXISTS Event;
CREATE TABLE Event (
  id INT(11) NOT NULL AUTO_INCREMENT,
  typeId INT(11) NOT NULL,
  message TEXT DEFAULT NULL,
  datetime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  userId INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы EventType
--
DROP TABLE IF EXISTS EventType;
CREATE TABLE EventType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Log
--
DROP TABLE IF EXISTS Log;
CREATE TABLE Log (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  logMessageTypeId INT(11) NOT NULL COMMENT 'Тип сообщения',
  callingClass VARCHAR(128) NOT NULL COMMENT 'Класс создавший сообщение',
  message VARCHAR(256) NOT NULL COMMENT 'Сообщение',
  date DATETIME NOT NULL COMMENT 'Дата',
  userId INT(11) DEFAULT NULL COMMENT 'Пользователь, вызвавший сообщение',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Лог'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы LogMessageType
--
DROP TABLE IF EXISTS LogMessageType;
CREATE TABLE LogMessageType (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `key` VARCHAR(16) NOT NULL COMMENT 'Ключ',
  description VARCHAR(32) NOT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица для типов сообщений лога'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы MediaInfo
--
DROP TABLE IF EXISTS MediaInfo;
CREATE TABLE MediaInfo (
  id INT(11) NOT NULL AUTO_INCREMENT,
  fileName VARCHAR(256) DEFAULT '0',
  type VARCHAR(64) DEFAULT '0',
  userId INT(11) DEFAULT 0,
  uploadDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  initialName VARCHAR(64) DEFAULT NULL,
  thumbnailName VARCHAR(256) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 64
AVG_ROW_LENGTH = 1489
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Информация о медиафайлах'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Message
--
DROP TABLE IF EXISTS Message;
CREATE TABLE Message (
  id INT(11) NOT NULL AUTO_INCREMENT,
  datetime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  senderId INT(11) NOT NULL,
  receiverId INT(11) NOT NULL,
  message TEXT DEFAULT NULL,
  isRead TINYINT(4) NOT NULL DEFAULT 0,
  isDelivered TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 25
AVG_ROW_LENGTH = 682
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Person
--
DROP TABLE IF EXISTS Person;
CREATE TABLE Person (
  id INT(11) NOT NULL AUTO_INCREMENT,
  firstName VARCHAR(50) NOT NULL,
  middleName VARCHAR(50) DEFAULT NULL,
  lastName VARCHAR(50) NOT NULL,
  birthDate DATE DEFAULT NULL,
  sex TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 47
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы PersonSkill
--
DROP TABLE IF EXISTS PersonSkill;
CREATE TABLE PersonSkill (
  personId INT(11) NOT NULL,
  skillId INT(11) NOT NULL,
  PRIMARY KEY (personId, skillId)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 1170
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Request
--
DROP TABLE IF EXISTS Request;
CREATE TABLE Request (
  id INT(11) NOT NULL AUTO_INCREMENT,
  creationDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  missingPersonId INT(11) NOT NULL,
  missingFrom DATETIME DEFAULT NULL,
  lastSeenPlace VARCHAR(255) DEFAULT NULL,
  extraInfo TEXT DEFAULT NULL,
  dressedIn TEXT DEFAULT NULL,
  circOfDisappear TEXT DEFAULT NULL,
  health TEXT DEFAULT NULL,
  appearance TEXT DEFAULT NULL,
  stuff TEXT DEFAULT NULL,
  declarantPersonId INT(11) NOT NULL,
  requestTypeId TINYINT(4) NOT NULL DEFAULT 1 COMMENT '0 - Обычная, 1 - Срочная',
  localityId INT(11) NOT NULL DEFAULT 1,
  sourceId INT(11) DEFAULT NULL,
  currentStateId INT(11) NOT NULL DEFAULT 0,
  inforgPersonId INT(11) DEFAULT NULL,
  coordPersonId INT(11) DEFAULT NULL,
  report TEXT DEFAULT NULL,
  forestOnline TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 24
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Запросы на поиск'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestLocality
--
DROP TABLE IF EXISTS RequestLocality;
CREATE TABLE RequestLocality (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestSource
--
DROP TABLE IF EXISTS RequestSource;
CREATE TABLE RequestSource (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestState
--
DROP TABLE IF EXISTS RequestState;
CREATE TABLE RequestState (
  id INT(11) NOT NULL AUTO_INCREMENT,
  state VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 7
AVG_ROW_LENGTH = 2730
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы RequestType
--
DROP TABLE IF EXISTS RequestType;
CREATE TABLE RequestType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Role
--
DROP TABLE IF EXISTS Role;
CREATE TABLE Role (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор роли',
  roleName VARCHAR(255) NOT NULL COMMENT 'Название роли',
  roleDescription VARCHAR(255) DEFAULT NULL COMMENT 'Описание роли',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица ролей'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Setting
--
DROP TABLE IF EXISTS Setting;
CREATE TABLE Setting (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) DEFAULT NULL,
  value VARCHAR(255) DEFAULT NULL,
  description VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 7
AVG_ROW_LENGTH = 2730
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Skill
--
DROP TABLE IF EXISTS Skill;
CREATE TABLE Skill (
  id INT(11) NOT NULL AUTO_INCREMENT,
  skill VARCHAR(255) NOT NULL,
  skillIcon VARCHAR(255) DEFAULT 'fa-square-o',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 6
AVG_ROW_LENGTH = 3276
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы User
--
DROP TABLE IF EXISTS User;
CREATE TABLE User (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  login VARCHAR(50) NOT NULL COMMENT 'Логин пользователя',
  email VARCHAR(255) NOT NULL,
  passwordHash VARCHAR(255) NOT NULL COMMENT 'Хэш пароля',
  creationDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата создания пользователя',
  lastLoginDate DATETIME DEFAULT NULL COMMENT 'Дата последнего входа',
  lastLoginIp VARCHAR(30) DEFAULT NULL COMMENT 'IP-адрес последнего входа',
  disabled TINYINT(4) NOT NULL COMMENT 'Признак заблокированного пользователя',
  reasonToDisable VARCHAR(255) DEFAULT NULL COMMENT 'Причина блокировки',
  disabledToDate DATETIME DEFAULT NULL COMMENT 'Заблокирован до даты',
  personId INT(11) DEFAULT NULL,
  confirmed TINYINT(4) NOT NULL DEFAULT 0,
  confirmationToken VARCHAR(10) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 39
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица пользователей'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы UserRole
--
DROP TABLE IF EXISTS UserRole;
CREATE TABLE UserRole (
  userId INT(11) NOT NULL COMMENT 'Идентификатор пользователя (User)',
  roleId INT(11) NOT NULL COMMENT 'Идентификатор роли',
  PRIMARY KEY (userId, roleId)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Списки ролей для идентификатора пользователя'
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы Validator
--
DROP TABLE IF EXISTS Validator;
CREATE TABLE Validator (
  id INT(11) NOT NULL AUTO_INCREMENT,
  rexegp TEXT NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

-- 
-- Вывод данных для таблицы Address
--
INSERT INTO Address VALUES
(1, 1, '50505000000000000', '142900, обл. Московская, р-н. Каширский, г. Кашира, ул. Гвардейская, д.8', 1),
(5, 35, '5003400100000030006', '142800, Московская, Ступинский, Ступино, Андропова, 12 (5003400100000030006)', NULL);

-- 
-- Вывод данных для таблицы BuddiesList
--
INSERT INTO BuddiesList VALUES
(1, 36),
(1, 37);

-- 
-- Вывод данных для таблицы Capability
--

-- Таблица redleaf_db.Capability не содержит данных

-- 
-- Вывод данных для таблицы Contact
--
INSERT INTO Contact VALUES
(1, 1, 1, '+7 926 5752524'),
(2, 1, 1, '+7 916 0907888');

-- 
-- Вывод данных для таблицы ContactType
--
INSERT INTO ContactType VALUES
(1, 1, 'Телефон'),
(2, NULL, 'Электронная почта');

-- 
-- Вывод данных для таблицы Event
--

-- Таблица redleaf_db.Event не содержит данных

-- 
-- Вывод данных для таблицы EventType
--

-- Таблица redleaf_db.EventType не содержит данных

-- 
-- Вывод данных для таблицы Log
--

-- Таблица redleaf_db.Log не содержит данных

-- 
-- Вывод данных для таблицы LogMessageType
--
INSERT INTO LogMessageType VALUES
(1, 'error', 'Ошибка'),
(2, 'warning', 'Предупреждение'),
(3, 'info', 'Уведомление');

-- 
-- Вывод данных для таблицы MediaInfo
--
INSERT INTO MediaInfo VALUES
(1, '0000000001', 'image/jpg', 1, '2016-11-23 22:01:35', 'picture1.jpg', '0000000001.jpg'),
(6, '0000000005', 'image/jpg', 1, '2016-11-23 22:34:49', 'picture5.jpg', '0000000005.jpg'),
(7, '0000000006', 'image/jpg', 1, '2016-11-23 22:35:01', 'picture6.jpg', '0000000006.jpg'),
(8, '0000000007', 'image/jpg', 1, '2016-11-23 22:42:26', 'picture7.jpg', '0000000007.jpg'),
(9, '0000000008', 'image/jpg', 1, '2016-11-23 22:43:40', 'picture8.jpg', '0000000008.jpg'),
(10, '0000000009', 'image/jpg', 1, '2016-11-23 22:45:10', 'picture9.jpg', '0000000009.jpg'),
(11, '000000000A', 'image/jpg', 1, '2016-11-23 22:46:15', 'picture10.jpg', '000000000A.jpg'),
(60, '0000000003C.jpg', 'image/jpeg', 1, '2016-12-16 13:38:51', 'WP_20160217_10_51_07_Pro.jpg', '0000000003C.jpg'),
(61, '0000000003D.jpg', 'image/jpeg', 1, '2016-12-16 13:40:25', 'WP_20160217_20_28_05_Pro.jpg', '0000000003D.jpg'),
(62, '0000000003E.jpg', 'image/jpeg', 1, '2016-12-16 13:41:24', 'WP_20160216_20_33_10_Pro.jpg', '0000000003E.jpg'),
(63, '0000000003F.jpg', 'image/jpeg', 1, '2016-12-16 13:42:24', 'WP_20160216_15_16_00_Pro.jpg', '0000000003F.jpg');

-- 
-- Вывод данных для таблицы Message
--
INSERT INTO Message VALUES
(1, '2016-12-15 21:40:05', 1, 37, '87987987987', 0, 0),
(2, '2016-12-15 21:40:05', 1, 36, '987987979797', 0, 0),
(3, '2016-12-15 21:40:05', 1, 36, '987979879', 0, 0),
(4, '2016-12-15 21:40:05', 1, 36, '8979797979', 0, 0),
(5, '2016-12-15 21:40:05', 1, 36, '97979797', 0, 0),
(6, '2016-12-15 21:40:05', 1, 36, '654646464', 0, 0),
(7, '2016-12-15 21:40:05', 1, 37, '21313546653', 0, 0),
(8, '2016-12-15 21:40:05', 1, 36, 'opipoipoipoipp', 0, 0),
(9, '2016-12-15 21:40:05', 1, 36, 'tybuytutyy', 0, 0),
(10, '2016-12-15 21:40:05', 1, 36, 'tyuntyuty', 0, 0),
(11, '2016-12-15 21:40:05', 1, 36, 'tnutyutyut', 0, 0),
(12, '2016-12-15 21:40:05', 1, 36, 'tbuyytutyunt', 0, 0),
(13, '2016-12-15 21:40:05', 1, 36, '2131321313', 0, 0),
(14, '2016-12-15 21:40:05', 1, 36, 'апапииапи', 0, 0),
(15, '2016-12-15 21:40:05', 1, 36, 'апиапиаипи', 0, 0),
(16, '2016-12-15 21:40:05', 1, 36, 'иапиапиапи', 0, 0),
(17, '2016-12-15 21:40:05', 1, 36, 'апиапиапи', 0, 0),
(18, '2016-12-15 21:40:05', 1, 36, 'ывывыв м', 0, 0),
(19, '2016-12-15 21:40:05', 1, 36, '768768787878', 0, 0),
(20, '2016-12-15 21:40:05', 1, 36, '86949464646', 0, 0),
(21, '2016-12-15 21:40:05', 1, 37, '8798797979', 0, 0),
(22, '2016-12-15 21:40:13', 1, 36, '56757567575', 0, 0),
(23, '2016-12-16 11:10:20', 1, 36, '79979798799', 0, 0),
(24, '2016-12-16 11:10:29', 1, 37, '778945', 0, 0);

-- 
-- Вывод данных для таблицы Person
--
INSERT INTO Person VALUES
(1, 'Алексей', 'Сергеевич', 'Аникеев', '1978-12-16', 0),
(35, 'Мария', 'Петровна', 'Иванова', '1960-06-08', 1),
(43, 'Алексей', 'Сергеевич', 'Аникеев', '1978-12-16', 0),
(44, 'vdfgb', 'vdgdxf', 'xdfcgv', '2016-11-30', 0),
(45, 'Роман', 'Гаврилович', 'Нефедов', '1976-06-17', 0),
(46, 'Иван', 'Иванович', 'Иванов', '1987-10-29', 0);

-- 
-- Вывод данных для таблицы PersonSkill
--
INSERT INTO PersonSkill VALUES
(1, 1),
(1, 3),
(1, 4),
(1, 5),
(43, 1),
(43, 2),
(43, 3),
(45, 1),
(45, 3),
(45, 5),
(46, 1),
(46, 2),
(46, 3),
(46, 4);

-- 
-- Вывод данных для таблицы Request
--
INSERT INTO Request VALUES
(23, '2016-12-01 15:25:28', 35, '2016-10-31 04:00:00', 'Рынок Ступино', '16.12.2016', 'Обычная', 'Неизвестны', 'Нормальное', 'Обычная', 'Обычная', 1, 1, 2, 1, 1, NULL, NULL, NULL, 1);

-- 
-- Вывод данных для таблицы RequestLocality
--
INSERT INTO RequestLocality VALUES
(1, 'Город'),
(2, 'Лес'),
(3, 'Город + Лес');

-- 
-- Вывод данных для таблицы RequestSource
--
INSERT INTO RequestSource VALUES
(1, 'Сайт lizaalert.org', 'Заявка с сайта Лиза Алерт'),
(2, 'Система LizaAlert', 'Заявка, заполненная через приложение или сайт системы LizaAlert'),
(3, 'Телефон', 'Заявка принятая по телефону');

-- 
-- Вывод данных для таблицы RequestState
--
INSERT INTO RequestState VALUES
(1, 'Новая'),
(2, 'Модерация'),
(3, 'Готова к поиску'),
(4, 'Активна'),
(5, 'Приостановлена'),
(6, 'Закрыта');

-- 
-- Вывод данных для таблицы RequestType
--
INSERT INTO RequestType VALUES
(1, 'Обыная'),
(2, 'Срочная');

-- 
-- Вывод данных для таблицы Role
--
INSERT INTO Role VALUES
(1, 'ROOT', 'Администратор'),
(2, 'DEVELOPER', 'Разработчик'),
(3, 'USER', 'Пользователь');

-- 
-- Вывод данных для таблицы Setting
--
INSERT INTO Setting VALUES
(1, 'Common.FileSystem.MediaLibraryTemporaryFolder', 'D:\\Temp', 'Временная папка для загрузки файлов на сервер'),
(2, 'root.Interface.DefaultView', 'Requests', 'Представление по умолчанию'),
(3, 'roma.Interface.Skin', 'skin-blue', 'Скин пользователя'),
(4, 'alex.Interface.Skin', 'skin-blue', 'Скин пользователя'),
(5, 'alex.Interface.DefaultView', 'Messages', 'Представление по умолчанию'),
(6, 'root.Interface.Skin', 'skin-blue', 'Скин пользователя');

-- 
-- Вывод данных для таблицы Skill
--
INSERT INTO Skill VALUES
(1, 'Ориентирование', 'fa-safari'),
(2, 'Плавсредство', 'fa-anchor'),
(3, 'Велосипед', 'fa-bicycle'),
(4, 'Машина', 'fa-car'),
(5, 'Видеокамера', 'fa-video-camera');

-- 
-- Вывод данных для таблицы User
--
INSERT INTO User VALUES
(1, 'root', '', '63a9f0ea7bb98050796b649e85481845', '2016-12-01 21:54:21', '2013-11-04 17:20:19', '', 0, '', NULL, 1, 1, NULL),
(36, 'alex', 'alex.blade.box@mail.ru', 'f7ddd46a08418d9850974259c8dd1faa', '2016-12-14 22:28:58', NULL, NULL, 1, NULL, NULL, 43, 1, '106839336'),
(37, 'roma', 'alex.blade.box@mail.ru', '202cb962ac59075b964b07152d234b70', '2016-12-14 22:28:59', NULL, NULL, 1, NULL, NULL, 45, 1, '58818342'),
(38, 'ivan', 'alex.blade.box@gmail.com', '202cb962ac59075b964b07152d234b70', '2016-12-14 22:29:30', NULL, NULL, 1, NULL, NULL, 46, 1, '710599793');

-- 
-- Вывод данных для таблицы UserRole
--
INSERT INTO UserRole VALUES
(1, 1),
(1, 2),
(36, 3),
(37, 3),
(38, 3);

-- 
-- Вывод данных для таблицы Validator
--
INSERT INTO Validator VALUES
(1, '//s', 'Пустой валидатор');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;