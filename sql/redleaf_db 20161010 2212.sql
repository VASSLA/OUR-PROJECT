﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.1.26.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 10.10.2016 22:12:45
-- Версия сервера: 5.5.30-log
-- Версия клиента: 4.1
--


--
-- Отключение внешних ключей
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Установить режим SQL (SQL mode)
--
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Описание для таблицы ContactType
--
DROP TABLE IF EXISTS ContactType;
CREATE TABLE ContactType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Типы контактной информации';

--
-- Описание для таблицы Log
--
DROP TABLE IF EXISTS Log;
CREATE TABLE Log (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  logMessageTypeId INT(11) NOT NULL COMMENT 'Тип сообщения',
  callingClass VARCHAR(128) NOT NULL COMMENT 'Класс создавший сообщение',
  message VARCHAR(256) NOT NULL COMMENT 'Сообщение',
  date DATETIME NOT NULL COMMENT 'Дата',
  userId INT(11) DEFAULT NULL COMMENT 'Пользователь, вызвавший сообщение',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Лог';

--
-- Описание для таблицы LogMessageType
--
DROP TABLE IF EXISTS LogMessageType;
CREATE TABLE LogMessageType (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `key` VARCHAR(16) NOT NULL COMMENT 'Ключ',
  description VARCHAR(32) NOT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица для типов сообщений лога';

--
-- Описание для таблицы Person
--
DROP TABLE IF EXISTS Person;
CREATE TABLE Person (
  id INT(11) NOT NULL AUTO_INCREMENT,
  firstName VARCHAR(50) DEFAULT NULL,
  middleName VARCHAR(50) DEFAULT NULL,
  lastName VARCHAR(50) DEFAULT NULL,
  age INT(10) UNSIGNED DEFAULT NULL,
  nickName VARCHAR(50) DEFAULT NULL,
  birthDate DATE DEFAULT NULL,
  photoLink VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы Request
--
DROP TABLE IF EXISTS Request;
CREATE TABLE Request (
  id INT(11) NOT NULL AUTO_INCREMENT,
  missingPersonId INT(11) NOT NULL,
  missingFrom DATETIME DEFAULT NULL,
  lastSeenPlace VARCHAR(255) DEFAULT NULL,
  extraInfo TEXT DEFAULT NULL,
  dressedIn TEXT DEFAULT NULL,
  declarantPersonId INT(11) NOT NULL,
  requestTypeId TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 - Обычная, 1 - Срочная',
  sourceId INT(11) DEFAULT NULL,
  currentStateId VARCHAR(255) NOT NULL DEFAULT '0',
  inforgPersonId INT(11) DEFAULT NULL,
  coordPersonId INT(11) DEFAULT NULL,
  report TEXT DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Запросы на поиск';

--
-- Описание для таблицы RequestSource
--
DROP TABLE IF EXISTS RequestSource;
CREATE TABLE RequestSource (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы RequestState
--
DROP TABLE IF EXISTS RequestState;
CREATE TABLE RequestState (
  id INT(11) NOT NULL AUTO_INCREMENT,
  state VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы RequestType
--
DROP TABLE IF EXISTS RequestType;
CREATE TABLE RequestType (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы Role
--
DROP TABLE IF EXISTS Role;
CREATE TABLE Role (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор роли',
  roleName VARCHAR(255) NOT NULL COMMENT 'Название роли',
  roleDescription VARCHAR(255) DEFAULT NULL COMMENT 'Описание роли',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица ролей';

--
-- Описание для таблицы User
--
DROP TABLE IF EXISTS User;
CREATE TABLE User (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  login VARCHAR(50) NOT NULL COMMENT 'Логин пользователя',
  passwordHash VARCHAR(255) NOT NULL COMMENT 'Хэш пароля',
  creationDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата создания пользователя',
  lastLoginDate DATETIME DEFAULT NULL COMMENT 'Дата последнего входа',
  lastLoginIp VARCHAR(30) DEFAULT NULL COMMENT 'IP-адрес последнего входа',
  disabled TINYINT(4) NOT NULL COMMENT 'Признак заблокированного пользователя',
  reasonToDisable VARCHAR(255) DEFAULT NULL COMMENT 'Причина блокировки',
  disabledToDate DATETIME DEFAULT NULL COMMENT 'Заблокирован до даты',
  personId INT(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 16
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица пользователей';

--
-- Описание для таблицы UserRole
--
DROP TABLE IF EXISTS UserRole;
CREATE TABLE UserRole (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор записи',
  userId INT(11) NOT NULL COMMENT 'Идентификатор пользователя (User)',
  roleId INT(11) NOT NULL COMMENT 'Идентификатор роли',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 13
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Списки ролей для идентификатора пользователя';

--
-- Описание для таблицы Validator
--
DROP TABLE IF EXISTS Validator;
CREATE TABLE Validator (
  id INT(11) NOT NULL AUTO_INCREMENT,
  rexegp TEXT NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Вывод данных для таблицы ContactType
--

-- Таблица redleaf_db.ContactType не содержит данных

--
-- Вывод данных для таблицы Log
--

-- Таблица redleaf_db.Log не содержит данных

--
-- Вывод данных для таблицы LogMessageType
--
INSERT INTO LogMessageType VALUES
(1, 'error', 'Ошибка'),
(2, 'warning', 'Предупреждение'),
(3, 'info', 'Уведомление');

--
-- Вывод данных для таблицы Person
--
INSERT INTO Person VALUES
(1, 'Aleksey', 'Sergeevitch', 'Anikeev', 37, 'Kent', '1978-12-16', NULL),
(2, 'Ivan', 'Ivanovitch', 'Ivanoff', 37, 'Ivanische', '1978-12-01', NULL);

--
-- Вывод данных для таблицы Request
--

-- Таблица redleaf_db.Request не содержит данных

--
-- Вывод данных для таблицы RequestSource
--

-- Таблица redleaf_db.RequestSource не содержит данных

--
-- Вывод данных для таблицы RequestState
--

-- Таблица redleaf_db.RequestState не содержит данных

--
-- Вывод данных для таблицы RequestType
--

-- Таблица redleaf_db.RequestType не содержит данных

--
-- Вывод данных для таблицы Role
--
INSERT INTO Role VALUES
(1, 'ROOT', 'Администратор'),
(2, 'DEVELOPER', 'Разработчик'),
(3, 'USER', 'Пользователь');

--
-- Вывод данных для таблицы User
--
INSERT INTO User VALUES
(1, 'root', '63a9f0ea7bb98050796b649e85481845', '2013-11-04 17:20:19', '2013-11-04 17:20:19', '', 0, '', NULL, 1),
(2, 'admin', '63a9f0ea7bb98050796b649e85481845', '2016-01-30 00:00:00', NULL, NULL, 0, NULL, NULL, 2),
(14, 'alex', '827ccb0eea8a706c4c34a16891f84e7b', '2016-09-22 20:56:15', NULL, NULL, 1, NULL, NULL, NULL),
(15, 'mouse', '827ccb0eea8a706c4c34a16891f84e7b', '2016-09-25 17:00:55', NULL, NULL, 1, NULL, NULL, NULL);

--
-- Вывод данных для таблицы UserRole
--
INSERT INTO UserRole VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 2, 3),
(11, 14, 3),
(12, 15, 3);

--
-- Вывод данных для таблицы Validator
--

-- Таблица redleaf_db.Validator не содержит данных

--
-- Восстановить предыдущий режим SQL (SQL mode)
--
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

--
-- Включение внешних ключей
--
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;