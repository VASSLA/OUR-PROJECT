package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.FeedbackAttachment;


public interface IFeedbackAttachmentService extends IService<FeedbackAttachment> {
    
}
