package ru.net.breakpoint.redleaf.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.FeedbackAttachmentDao;
import ru.net.breakpoint.redleaf.domain.dao.FeedbackDao;
import ru.net.breakpoint.redleaf.domain.dao.FeedbackStatusDao;
import ru.net.breakpoint.redleaf.domain.dao.FeedbackTypeDao;
import ru.net.breakpoint.redleaf.domain.entity.Feedback;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackAttachment;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackStatus;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackType;
import ru.net.breakpoint.redleaf.interfaces.IFeedbackService;

@Service
@Transactional
public class FeedbackService implements IFeedbackService {
    @Autowired
    private FeedbackDao feedbackDao;
    
    @Autowired
    private FeedbackAttachmentDao feedbackAttachmentDao;
    
    @Autowired
    private FeedbackStatusDao feedbackStatusDao;
    
    @Autowired
    private FeedbackTypeDao feedbackTypeDao;
    
    @Override
    public ArrayList<Feedback> getFeedbackByStatus(int feedbackStatus){
        ArrayList<Feedback> result = new ArrayList<>();
        result = feedbackDao.getFeedbackByStatus(feedbackStatus);
        return result;
    }
    
    @Override
    public Feedback get(Long id) {
        return feedbackDao.get(id);
    }
    
    @Override
    public void saveOrUpdate(Feedback persistent) {
        feedbackDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Feedback persistent) {
        feedbackDao.delete(persistent);
    }

    @Override
    public ArrayList<Feedback> getList() {
        return feedbackDao.getList();
    }

    @Override
    public ArrayList<Feedback> getList(Map<String, String> filters) {
        return feedbackDao.getList(filters);
    }
    
    public ArrayList<FeedbackStatus> getStatusList() {
        return feedbackStatusDao.getList();
    }
    
    public ArrayList<FeedbackType> getTypeList() {
        return feedbackTypeDao.getList();
    }
    
}
