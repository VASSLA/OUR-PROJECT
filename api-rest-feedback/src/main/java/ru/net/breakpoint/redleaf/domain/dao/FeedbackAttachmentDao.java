package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackAttachment;

@Repository
public class FeedbackAttachmentDao extends AbstractDao<FeedbackAttachment> {
    
}
