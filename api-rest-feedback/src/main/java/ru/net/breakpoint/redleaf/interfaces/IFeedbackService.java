package ru.net.breakpoint.redleaf.interfaces;

import java.io.File;
import java.util.ArrayList;
import ru.net.breakpoint.redleaf.domain.entity.Feedback;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackStatus;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackType;

public interface IFeedbackService extends IService<Feedback> {
    public ArrayList<FeedbackStatus> getStatusList();
    public ArrayList<FeedbackType> getTypeList();
    public ArrayList<Feedback> getFeedbackByStatus(int feedbackStatus);
}
