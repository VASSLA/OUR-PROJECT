package ru.net.breakpoint.redleaf.domain.dao;

import java.io.File;
import java.util.ArrayList;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Feedback;

@Repository
public class FeedbackDao extends AbstractDao<Feedback> {
    public ArrayList<Feedback> getFeedbackByStatus(int feedbackStatus){
        ArrayList<Feedback> result = new ArrayList<>();
        Criteria criteria = getSession().createCriteria(Feedback.class)
                .setProjection(Projections.distinct(Projections.projectionList()
                .add(Projections.property("id"), "id")))
                .add(Restrictions.eq("feedbackStatusId", (short)feedbackStatus));
        result.addAll(criteria.list());
        return result;
    }
}
