package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "FeedbackAttachment")
public class FeedbackAttachment implements IPersistent, Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "fileName")
    private String fileName;

    @Column(name = "type")
    private String type;

    @Column(name = "feedbackId", insertable = false, updatable = false)
    private Long feedbackId;

    @Column(name = "uploadDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date uploadDate;

    @Column(name="initialName")
    private String initialName;

    @Column(name="onDiskName")
    private String onDiskName;

    @ManyToOne(targetEntity = Feedback.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "feedbackId", nullable = false)
    private Feedback feedback;

    public FeedbackAttachment() {
    }

    public FeedbackAttachment(Long id) {
        this.id = id;
    }

    public FeedbackAttachment(Long id, Date uploadDate) {
        this.id = id;
        this.uploadDate = uploadDate;
    }
    
    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String path) {
        this.fileName = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Long feedbackId) {
        this.feedbackId = feedbackId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public String getInitialName() {
        return initialName;
    }

    public void setInitialName(String initialName) {
        this.initialName = initialName;
    }

    public String getOnDiskName() {
        return onDiskName;
    }

    public void setOnDiskName(String onDiskName) {
        this.onDiskName = onDiskName;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FeedbackAttachment)) {
            return false;
        }
        FeedbackAttachment other = (FeedbackAttachment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.FeedbackAttachment[ id=" + id + " ]";
    }

}
