package ru.net.breakpoint.redleaf.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.net.breakpoint.redleaf.domain.entity.Feedback;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackAttachment;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackStatus;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackType;
import ru.net.breakpoint.redleaf.domain.entity.Setting;
import ru.net.breakpoint.redleaf.interfaces.IFeedbackAttachmentService;
import ru.net.breakpoint.redleaf.interfaces.IFeedbackService;
import ru.net.breakpoint.redleaf.interfaces.ISettingService;


@RestController
public class FeedbackController {
    private static final Logger logger = Logger.getLogger(FeedbackController.class);
    private static final String FILE_STORAGE_DIR = "Common.FileSystem.FeedbackAttachmentFolder";

    @Autowired
    private IFeedbackService feedbackService;

    @Autowired
    private IFeedbackAttachmentService feedbackAttachmentService;

    @Autowired
    private ISettingService settingService;

    @RequestMapping(value = "/api/v{version}/feedback", method = RequestMethod.GET)
//    public ResponseEntity<List<Feedback>> getFeedbackList(@PathVariable Integer version) {
    public ResponseEntity<List<Feedback>> getFeedbackList(@PathVariable Integer version, @RequestParam Map<String, String> filters) {        
        logger.trace("Requested /api/v" + version.toString() + "/feedback");
        return new ResponseEntity<List<Feedback>>(feedbackService.getList(filters), HttpStatus.OK);
    } 
    
    @RequestMapping(value = "/api/v{version}/feedbackCount", method = RequestMethod.GET)
    public ResponseEntity<Map<String, String>> getFeedbackCount(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/feedbackCount");
        Map<String, String> result = new HashMap<String, String>();
        result.put("accepted", Integer.toString(feedbackService.getFeedbackByStatus(Feedback.Status.ACCEPTED.getCode()).size()));
        result.put("closed", Integer.toString(feedbackService.getFeedbackByStatus(Feedback.Status.CLOSED.getCode()).size()));
        result.put("new", Integer.toString(feedbackService.getFeedbackByStatus(Feedback.Status.NEW.getCode()).size()));
        result.put("processing", Integer.toString(feedbackService.getFeedbackByStatus(Feedback.Status.PROCESSING.getCode()).size()));
        result.put("rejected", Integer.toString(feedbackService.getFeedbackByStatus(Feedback.Status.REJECTED.getCode()).size()));
        result.put("all", Integer.toString(feedbackService.getList().size()));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/feedbackStatus", method = RequestMethod.GET)
    public ResponseEntity<List<FeedbackStatus>> getFeedbackStatusList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/feedbackStatus");
        return new ResponseEntity<List<FeedbackStatus>>(feedbackService.getStatusList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/feedbackType", method = RequestMethod.GET)
    public ResponseEntity<List<FeedbackType>> getFeedbackTypeList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/feedbackType");
        return new ResponseEntity<List<FeedbackType>>(feedbackService.getTypeList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/feedbackById", method = RequestMethod.GET)
    public ResponseEntity<Feedback> getFeedbackById(@PathVariable Integer version, @RequestParam Map<String, String> data) {
        logger.trace("Requested /api/v" + version.toString() + "/feedbackById");
        long id = data.get("id") != null ?
		Long.parseLong(data.get("id")) : -1;
        return new ResponseEntity<Feedback>(feedbackService.get(id), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/v{version}/getFeedbackFile/{id}", method = RequestMethod.GET)
    public void getFeedbackFile(@PathVariable Integer version, @PathVariable Long id, HttpServletRequest request, HttpServletResponse response) {
        logger.trace("Get /api/v" + version.toString() + "/getFeedbackFile/" + id.toString());
        FeedbackAttachment feedbackAttachment = feedbackAttachmentService.get(id);
        try {
            FileInputStream stream = new FileInputStream(feedbackAttachment.getOnDiskName());
            response.setHeader("Content-disposition", "attachment; filename="+ feedbackAttachment.getInitialName());
            response.setContentType("application/octet-stream");
            IOUtils.copy(stream, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        
    }

    @RequestMapping(value = "/api/v{version}/feedback", method = RequestMethod.POST)
    public ResponseEntity<Feedback> postFeedback(@RequestPart("feedback") Feedback feedback, @RequestPart("files") List<MultipartFile> files) {
        feedbackService.saveOrUpdate(feedback);
        Setting fileStoragePath = settingService.getByName(FILE_STORAGE_DIR);
        for (MultipartFile mp : files) {
            Calendar calendar = Calendar.getInstance();
            File destinationFile = new File(fileStoragePath.getValue() + "/" +
                    calendar.get(Calendar.YEAR) + "/" +
                    calendar.get(Calendar.MONTH) + "/" +
                    calendar.get(Calendar.DAY_OF_MONTH) + "/issue-" +
                    feedback.getId() + "/" + mp.getOriginalFilename());
            destinationFile.mkdirs();
            FeedbackAttachment feedbackAttachment = new FeedbackAttachment();
            feedbackAttachment.setFeedbackId(feedback.getId());
            feedbackAttachment.setFileName(destinationFile.getAbsolutePath());
            feedbackAttachment.setInitialName(mp.getOriginalFilename());
            feedbackAttachment.setOnDiskName(destinationFile.getAbsolutePath());
            feedbackAttachment.setUploadDate(new Date());
            feedbackAttachment.setFeedback(feedback);
            feedbackAttachmentService.saveOrUpdate(feedbackAttachment);
            try {
                logger.debug("Copying: " + mp.getOriginalFilename() + " (" +
                        mp.getContentType() + " / " +
                        mp.getSize() + " bytes) to " +
                        destinationFile.getAbsolutePath());
                mp.transferTo(destinationFile);
            } catch (IOException ex) {
                feedbackAttachmentService.delete(feedbackAttachment);
                feedbackService.delete(feedback);
                logger.error(ex.getMessage());
            }
        }
        return new ResponseEntity<>(feedback, HttpStatus.CREATED);
    }
}
