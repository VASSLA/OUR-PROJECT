/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-feedback
 * File Name: FeedbackTypeDao.java
 * Date: Feb 18, 2017 9:59:40 PM
 * Author: AntonBitsura */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackType;

@Repository
public class FeedbackTypeDao extends AbstractDao<FeedbackType> {
    
}


