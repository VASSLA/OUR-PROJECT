package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.FeedbackAttachmentDao;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackAttachment;
import ru.net.breakpoint.redleaf.interfaces.IFeedbackAttachmentService;

@Service
@Transactional
public class FeedbackAttachmentService implements IFeedbackAttachmentService {
    @Autowired
    private FeedbackAttachmentDao feedbackAttachmentDao;

    @Override
    public FeedbackAttachment get(Long id) {
        return feedbackAttachmentDao.get(id);
    }

    @Override
    public void saveOrUpdate(FeedbackAttachment persistent) {
        feedbackAttachmentDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(FeedbackAttachment persistent) {
        feedbackAttachmentDao.delete(persistent);
    }

    @Override
    public ArrayList<FeedbackAttachment> getList() {
        return feedbackAttachmentDao.getList();
    }

    @Override
    public ArrayList<FeedbackAttachment> getList(Map<String, String> filters) {
        return feedbackAttachmentDao.getList(filters);
    }
}
