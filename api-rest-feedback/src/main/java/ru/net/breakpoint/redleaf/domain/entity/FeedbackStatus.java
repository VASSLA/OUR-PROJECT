/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-feedback
 * File Name: FeedbackStatus.java
 * Date: Feb 18, 2017 9:51:48 PM
 * Author: AntonBitsura */
package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "FeedbackStatus")
public class FeedbackStatus implements IPersistent, Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "name")
    private String name;
    
    public FeedbackStatus() {
    }

    public FeedbackStatus(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
    
        public String getName() {
        return name;
    }

    public void setName(String text) {
        this.name = text;
    }
}

