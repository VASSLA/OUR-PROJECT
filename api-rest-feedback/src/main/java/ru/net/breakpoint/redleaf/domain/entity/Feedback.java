package ru.net.breakpoint.redleaf.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

@Entity
@Table(name = "Feedback")
@NamedQueries({
@NamedQuery(name = "Feedback.findByStatus", query = "SELECT f FROM Feedback f WHERE f.feedbackStatusId = :feedbackStatusId")})
public class Feedback implements IPersistent, Serializable {

    // Статус сообщения обратной связи
    public static enum Status {
    NEW(1), ACCEPTED(2), PROCESSING(3), CLOSED(4), REJECTED(5);

        private final int code;

        private Status(int code) {
            this.code = code;
        }

        public short getCode() {
            return (short)code;
        }
    }

    // Тип сообщения обратной связи
    public static enum Type {
    ERROR(1), CRITICAL(2), WORK(3), OTHER(4);

        private final int code;

        private Type(int code) {
            this.code = code;
        }

        public short getCode() {
            return (short)code;
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "createDate")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm", timezone = "GMT+3")
    private Date createDate;

    @Column(name = "closeDate")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm", timezone = "GMT+3")
    private Date closeDate;

    @Column(name = "feedbackTypeId")
    private short feedbackTypeId;

    @Column(name = "text")
    private String text;

//    @Column(name = "initiatorId")
//    private Long initiatorId;
//
//    @Column(name = "performerId")
//    private Long performerId;

    @Column(name = "rejectReason")
    private String rejectReason;

    @Column(name = "feedbackStatusId")
    private short feedbackStatusId;

    @OneToMany(targetEntity = FeedbackAttachment.class, fetch = FetchType.EAGER, mappedBy = "feedback")
    private Set<FeedbackAttachment> feedbackAttachments;
    
    @JoinColumn(name = "initiatorId", referencedColumnName = "id")
    @ManyToOne
    private User initiator;

    @JoinColumn(name = "performerId", referencedColumnName = "id")
    @ManyToOne
    private User performer;
    
    public Feedback() {
    }

    public Feedback(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreationDate(Date createDate) {
        this.createDate = createDate;
    }

   public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public short getFeedbackTypeId() {
        return feedbackTypeId;
    }

    public void setFeedBackTypeId(short feedbackTypeId) {
        this.feedbackTypeId = feedbackTypeId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

//    public Long getInitiatorId() {
//        return initiatorId;
//    }
//
//    public void setInitiatorId(Long initiatorId) {
//        this.initiatorId = initiatorId;
//    }
//
//    public Long getPerformerId() {
//        return performerId;
//    }
//
//    public void setPerformerId(Long performerId) {
//        this.performerId = performerId;
//    }
    
     public User getInitiator() {
        return initiator;
    }

    public void setInitiator(User initiator) {
        this.initiator = initiator;
    }

    public User getPerformerId() {
        return performer;
    }

    public void setPerformer(User performer) {
        this.performer = performer;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public short getFeedbackStatusId() {
        return feedbackStatusId;
    }

    public void setFeedBackStatusId(short feedbackStatusId) {
        this.feedbackStatusId = feedbackStatusId;
    }

    public Set<FeedbackAttachment> getFeedbackAttachments() {
        return feedbackAttachments;
    }

    public void setFeedbackAttachments(Set<FeedbackAttachment> feedbackAttachments) {
        this.feedbackAttachments = feedbackAttachments;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Feedback)) {
            return false;
        }
        Feedback other = (Feedback) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.Feedback[ id=" + id + " ]";
    }

}
