/* Red Leaf Platform 
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-feedback
 * File Name: FeedbackStatusDao.java
 * Date: Feb 18, 2017 9:50:58 PM
 * Author: User */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.FeedbackStatus;

@Repository
public class FeedbackStatusDao extends AbstractDao<FeedbackStatus> {
    
}
