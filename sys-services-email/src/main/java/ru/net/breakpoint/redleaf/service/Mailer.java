/* Red Leaf Platform
 * Breakpoint Developers Group (c) 2016
 * Project: sys-services-email
 * File Name: Mailer.java
 * Date: Nov 7, 2016 10:45:27 PM
 * Author: Alex */
package ru.net.breakpoint.redleaf.service;

import java.util.Properties;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

@EnableAsync
@Service
public class Mailer {
    @Async
    public void sendMail(String to, String subject, String body) {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.starttls.enable", true);
        properties.put("mail.smtp.starttls.required", true);
        properties.put("mail.smtp.debug", true);
        properties.put("mail.smtp.socketFactory.port", 465);
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.fallback", false);

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setJavaMailProperties(properties);
        mailSender.setHost("smtp.yandex.ru");
        mailSender.setPort(465);
        mailSender.setProtocol("smtp");
        mailSender.setUsername("aleksey.anikeev@emsu.info");
        mailSender.setPassword("SuakL3Su");

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setFrom("aleksey.anikeev@emsu.info");
        message.setSubject(subject);
        message.setText(body);

        mailSender.send(message);
    }
}
