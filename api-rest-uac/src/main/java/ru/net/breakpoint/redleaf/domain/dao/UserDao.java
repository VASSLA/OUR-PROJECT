/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: UserDao.java
 * Date: Jun 27, 2016 2:08:23 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.domain.dao;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.User;

import java.util.List;
import org.hibernate.criterion.Restrictions;

@Repository
public class UserDao extends AbstractDao<User> {

    public User getByLogin(String login) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("login", login));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<User> list = criteria.list();
        if (list.size() > 0) {
            return (User) criteria.list().get(0);
        } else {
            return null;
        }
    }

    public void delete(Long id) {
        User user = this.get(id);
        getSession().delete(user);
    }
}
