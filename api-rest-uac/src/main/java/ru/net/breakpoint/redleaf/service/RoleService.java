/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: RoleService.java
 * Date: Jun 27, 2016 2:11:04 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.RoleDao;
import ru.net.breakpoint.redleaf.domain.entity.Role;
import ru.net.breakpoint.redleaf.interfaces.IRoleService;

@Service
@Transactional
public class RoleService implements IRoleService{
    @Autowired
    private RoleDao roleDao;

    @Override
    public Role get(Long id) {
        return roleDao.get(id);
    }

    @Override
    public void saveOrUpdate(Role persistent) {
        roleDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(Role persistent) {
        roleDao.delete(persistent);
    }

    @Override
    public ArrayList<Role> getList() {
        return roleDao.getList();
    }

    @Override
    public ArrayList<Role> getList(Map<String, String> filters) {
        return roleDao.getList(filters);
    }
}
