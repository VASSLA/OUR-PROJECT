/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: IUserService.java
 * Date: Jun 27, 2016 2:09:40 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.interfaces;

import ru.net.breakpoint.redleaf.domain.entity.User;

public interface IUserService extends IService<User> {
    User getCurrentUser();
}
