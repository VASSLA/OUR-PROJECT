/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: UserController.java
 * Date: Jun 27, 2016 1:16:10 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.User;
import ru.net.breakpoint.redleaf.interfaces.IUserService;

@RestController
public class UserController {
    private static final Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/api/v{version}/user", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUserList(@PathVariable Integer version, @RequestParam Map<String, String>filters) {
        logger.trace("Requested /api/v" + version.toString() + "/user");
        return new ResponseEntity<List<User>> (userService.getList(filters), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/user/me", method = RequestMethod.GET)
    public ResponseEntity<User> getCurrentUser(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/user/me");
        return new ResponseEntity<User> (userService.getCurrentUser(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getUser(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/user/" + id.toString());
        return new ResponseEntity<User> (userService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/user", method = RequestMethod.POST)
    public ResponseEntity<User> postUser(@PathVariable Integer version, @RequestBody User user) {
        logger.trace("Requested /api/v" + version.toString() + "/user");
        userService.saveOrUpdate(user);
        return new ResponseEntity<> (user, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/user", method = RequestMethod.PUT)
    public ResponseEntity<User> putUser(@PathVariable Integer version, @RequestBody User user) {
        logger.trace("Requested /api/v" + version.toString() + "/user");
        userService.saveOrUpdate(user);
        return new ResponseEntity<> (user, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/user");
        User user = userService.get(id);
        if (user != null) {
            userService.delete(user);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }
}
