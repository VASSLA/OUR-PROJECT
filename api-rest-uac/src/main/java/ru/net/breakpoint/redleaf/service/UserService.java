/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: UserService.java
 * Date: Jun 27, 2016 2:11:04 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.net.breakpoint.redleaf.domain.dao.UserDao;
import ru.net.breakpoint.redleaf.domain.entity.User;
import ru.net.breakpoint.redleaf.interfaces.IUserService;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Service
@Transactional
public class UserService implements IUserService{
    @Autowired
    private UserDao userDao;

    @Override
    public User get(Long id) {
        return userDao.get(id);
    }

    @Override
    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null)
            return null;
        return userDao.getByLogin(auth.getName());
    }

    @Override
    public void saveOrUpdate(User persistent) {
        userDao.saveOrUpdate(persistent);
    }

    @Override
    public void delete(User persistent) {
        userDao.delete(persistent);
    }

    @Override
    public ArrayList<User> getList() {
        return userDao.getList();
    }

    @Override
    public ArrayList<User> getList(Map<String, String> filters) {
        return userDao.getList(filters);
    }
}
