/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: User.java
 * Date: Jun 27, 2016 2:08:23 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.domain.entity;

import java.io.Serializable;
import ru.net.breakpoint.redleaf.interfaces.IPersistent;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class User implements Serializable, IPersistent {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String login;
    private String email;
    private String passwordHash;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date lastLoginDate;
    private String lastLoginIp;
    private Boolean disabled;
    private String reasonToDisable;
    @Temporal(value = TemporalType.DATE)
    private Date disabledToDate;
    private Boolean confirmed;
    private String confirmationToken;
    @OneToOne(targetEntity = Person.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "personId", insertable = true, updatable = true, referencedColumnName = "id")
    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @ManyToMany(targetEntity = Role.class, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(name = "UserRole",
            joinColumns = {
                @JoinColumn(name = "userId", nullable = false, insertable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "roleId", nullable = false, insertable = false, updatable = false)
            }
    )
    private Set<Role> roles;

    @ManyToMany(targetEntity = Buddy.class, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(name = "BuddiesList",
            joinColumns = {
                @JoinColumn(name = "userId", nullable = false, insertable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "buddyId", nullable = false, insertable = false, updatable = false)
            }
    )
    private Set<Buddy> buddies;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public String getReasonToDisable() {
        return reasonToDisable;
    }

    public void setReasonToDisable(String reasonToDisable) {
        this.reasonToDisable = reasonToDisable;
    }

    public Date getDisabledToDate() {
        return disabledToDate;
    }

    public void setDisabledToDate(Date disabledToDate) {
        this.disabledToDate = disabledToDate;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Buddy> getBuddies() {
        return buddies;
    }

    public void setBuddies(Set<Buddy> buddies) {
        this.buddies = buddies;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public boolean hasRole(String role) {
        for (Role r : this.getRoles()) {
            if (r.getRoleName().equals(role))
                return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "ru.net.breakpoint.redleaf.domain.entity.User[ id=" + id + " ]";
    }
}
