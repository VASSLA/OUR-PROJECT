/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: RoleDao.java
 * Date: Jun 27, 2016 2:08:23 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.domain.dao;

import org.springframework.stereotype.Repository;
import ru.net.breakpoint.redleaf.domain.entity.Role;

@Repository
public class RoleDao extends AbstractDao<Role>{

}
