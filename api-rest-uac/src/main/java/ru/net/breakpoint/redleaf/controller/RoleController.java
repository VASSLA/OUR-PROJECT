/* Red Leaf Project
 * Breakpoint Developers Group (c) 2016
 * Project: api-rest-uac
 * File Name: RoleController.java
 * Date: Jun 27, 2016 1:35:51 PM
 * Author: Aleksey S. Anikeev */
package ru.net.breakpoint.redleaf.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.net.breakpoint.redleaf.domain.entity.Role;
import ru.net.breakpoint.redleaf.interfaces.IRoleService;

@RestController
public class RoleController {
    private static final Logger logger = Logger.getLogger(RoleController.class);

    @Autowired
    private IRoleService roleService;

    @RequestMapping(value = "/api/v{version}/role", method = RequestMethod.GET)
    public ResponseEntity<List<Role>> getRoleList(@PathVariable Integer version) {
        logger.trace("Requested /api/v" + version.toString() + "/role");
        return new ResponseEntity<List<Role>> (roleService.getList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/role/{id}", method = RequestMethod.GET)
    public ResponseEntity<Role> getRole(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/role/" + id.toString());
        return new ResponseEntity<> (roleService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/v{version}/role", method = RequestMethod.POST)
    public ResponseEntity<Role> postRole(@PathVariable Integer version, @RequestBody Role role) {
        logger.trace("Requested /api/v" + version.toString() + "/role");
        roleService.saveOrUpdate(role);
        return new ResponseEntity<> (role, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/v{version}/role", method = RequestMethod.PUT)
    public ResponseEntity<Role> putRole(@PathVariable Integer version, @RequestBody Role role) {
        logger.trace("Requested /api/v" + version.toString() + "/role");
        roleService.saveOrUpdate(role);
        return new ResponseEntity<> (role, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api/v{version}/role/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Role> deleteRole(@PathVariable Integer version, @PathVariable Long id) {
        logger.trace("Requested /api/v" + version.toString() + "/role");
        Role role = roleService.get(id);
        if (role != null) {
            roleService.delete(role);
            return new ResponseEntity<> (HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }
}
